export default {
  api: {
    url: process.env.REACT_APP_API_URL || "https://savage-api.xellcoders.com"
  }
};
