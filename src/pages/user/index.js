import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import cn from "classnames";
import { withRouter } from "react-router-dom";
import IconSearch from "components/Icons/IconSearch";
import IconFilter from "components/Icons/IconFilter";
import { getUserItems } from "services/api.item.service";
import TextInput from "components/TextInput";
import Section from "pages/home/section";
import Filters from "components/Filters";
import styles2 from "pages/home/home.module.sass";
import styles from "./user.module.sass";
import BackButton2 from "../../components/BackButton2";
import { getUser } from "../../services/api.user.service";
import Loading from "../../components/Loading";

function Home({ match: { params: { id } }}) {
  const [loading, setLoading] = useState(true);
  const [user, setUser] = useState('all');
  const [category] = useState('all');
  const [isVisibleFilters, setIsVisibleFilters] = useState(false);

  const userItems = () => {
    return getUserItems( id );
  }

  useEffect(() => {
    getUser( id ).then( result => {
      setUser( result );
      setLoading( false );
    } ).catch( () => {
      setLoading( false );
    } );
  }, [id])

  if (loading) return <Loading status="Cargando..." />;

  return (
    <>
      <div className={styles2.wrapper}>
        <div className={styles.backButton}>
          <BackButton2 redirectTo="/" />
        </div>
        <div className={styles.header}>
          <div className={styles.avatar}>
            <figure>
              <img src={user.avatar || "/images/icons/avatar.png"} alt="avatar" style={{ width: '100%' }} />
            </figure>
          </div>
          <div className={styles.name}>{user.username}</div>
          <div className={styles.wallet}>
            <span className={styles.address}>{user.wallet}</span>
            <span className={styles.copy}>
              {" "}
            </span>
          </div>
        </div>
        <div className={styles.filters}>
          <div className={styles2.container_search_input_general}>
            <TextInput
              Icon={<IconSearch className={styles2.icon_search} />}
              name="hash"
              type="text"
              placeholder="Buscar en marketplace"
              required
              className={styles2.container_search_input}
              classNameInput={styles2.input_search}
              onChange={() => { }}
            />
            <button type="button" className={styles2.cancel_btn}>
              <IconSearch />
            </button>
          </div>
          <div className={cn( styles2.container_filters, styles2.container_filter_hide )}>
            {/* eslint-disable-next-line */}
            <div onClick={() => setIsVisibleFilters(true)}>
              <IconFilter />
            </div>
            <Filters
              filters={[]}
              catalogs={{}}
              close={() => {
                setIsVisibleFilters(false);
              }}
              isVisible={isVisibleFilters}
              handleOpenModal={()=>{}}
              modalOpen
            />
          </div>
        </div>
        <div className={styles2.main}>
          {(category === 'all' || category === 'onsale') && <Section title="En venta" load={userItems} />}
          {(category === 'all' || category === 'onauction') && <Section title="En subasta" load={userItems} />}
          {(category === 'all' || category === 'ongame') && <Section title="En juego" load={userItems} />}
          {(category === 'all' || category === 'oncollection') && <Section title="En colección" load={userItems} />}
        </div>
      </div>
    </>
  );
}

export default connect(({ user }) => ({ user }))( withRouter( Home ) );
