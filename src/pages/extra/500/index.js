import React from "react";
import { Helmet } from "react-helmet";

const System500 = () => {
  return (
    <div>
      <Helmet title="Page 500" />
      Error 500
    </div>
  );
};

export default System500;
