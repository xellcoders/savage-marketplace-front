import React from "react";
import { Helmet } from "react-helmet";

const System404 = () => {
  return (
    <div>
      <Helmet title="Page 404" />
      Error 404
    </div>
  );
};

export default System404;
