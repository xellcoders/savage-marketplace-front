import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Form, Row, Col, Input, Button } from "antd";
import { withRouter } from "react-router-dom";
import BackButton2 from "components/BackButton2";
import Loading, { Success } from "components/Loading";
import { getItem, putOnGame } from "services/api.item.service";
import cn from "classnames";
import ArrowIcon from "components/Icons/arrow";
import { TokenType } from "util/constants";
import { getItemImage } from "util/files";
import styles from "./styles.module.sass";
import { history } from '../../../index';
import Confirm from "./confirm";

const Game = ({ match: { params: { id } }}) => {
  const [sending, setSending] = useState(false);
  const [error, setError] = useState();
  const [success, setSuccess] = useState( false );
  const [loading, setLoading] = useState(true);
  const [item, setItem] = useState({ nft: {} });
  const [values, setValues] = useState();

  useEffect(() => {
    setLoading( true );
    getItem( id ).then( data => {
      setItem( data );
      setLoading( false );
    } ).catch( () => {
      history.push( '/404' );
      setLoading( false );
    } );
  }, [id]);

  const game = data => {
    setSending( true );
    setSuccess( false );
    setError( null );
    putOnGame( id, data ).then( () => {
      setSending( false );
      setSuccess( true );
    } ).catch( e => {
      setSending( false );
      setSuccess( false );
      setError( `${e.message} (code: ${e.code})` );
    } )
  }

  if( values ) {
    return (
      <>
        {success && <Success text="El NFT ya está en disponible para obtenerlo en su juego" link={`/nft/${id}`} />}
        {sending && <Loading status="Enviando..." />}
        <Confirm
          item={item}
          mode="game"
          values={values}
          error={error}
          onCancel={() => setValues( null )}
          onContinue={data => game( data )}
        />
      </>
    )
  }
  return (
    <section className={styles.wrapper}>
      {loading && <Loading status="Cargando..." /> }
      <div className={styles.backButton}>
        <BackButton2 redirectTo="/" />
      </div>
      <div className={styles.container}>
        <div>
          <div className={styles.image}>
            <figure>
              <img src={getItemImage(item.nft)} alt={item.nft.name} />
            </figure>
          </div>
          <div className={cn(styles.block, styles.summary)}>
            <h4>Poner en juego</h4>
            <div className={styles.title}>{item.nft.name}</div>
            <div className={styles.subtitle}>{item.nft.videogame || 'N/A'}</div>
            <div className={styles.extra}>Tipo de token <b>{TokenType[item.nft.rarity]}</b></div>
          </div>
          <Form
            layout="vertical"
            onFinish={setValues}
            hideRequiredMark
          >
            <div className={cn(styles.block, styles.game)}>
              <Row gutter={[16, 0]} className={styles.form}>
                <Col xs={24}>
                  <Form.Item
                    hasFeedback
                    label="¿Cómo obtener este NFT?"
                    name="instructions"
                    rules={[{ required: true, message: "El campo es obligatorio" }]}
                  >
                    <Input.TextArea rows={3} size="large" placeholder="Ej. Completando los 6 mundos de Mario Bros..." />
                  </Form.Item>
                </Col>
              </Row>
            </div>
            <Button htmlType="submit" className={styles.button}>
              <span>Continuar</span> <div><ArrowIcon /></div>
            </Button>
          </Form>
        </div>
      </div>
    </section>
  );
};

export default connect(({ user }) => ({ user }))( withRouter( Game ) );
