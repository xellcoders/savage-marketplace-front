import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Form, Row, Col, Input, Button, Radio } from "antd";
import { withRouter } from "react-router-dom";
import BackButton2 from "components/BackButton2";
import Loading, { Success } from "components/Loading";
import { getItem, transferItem } from "services/api.item.service";
import cn from "classnames";
import ArrowIcon from "components/Icons/arrow";
import { getItemImage } from "util/files";
import { TokenType } from "util/constants";
import styles from "./styles.module.sass";
import { history } from '../../../index';
import Confirm from "./confirm";

const Transfer = ({ match: { params: { id } }}) => {
  const [sending, setSending] = useState(false);
  const [error, setError] = useState();
  const [success, setSuccess] = useState( false );
  const [loading, setLoading] = useState(true);
  const [item, setItem] = useState({ nft: {} });
  const [values, setValues] = useState();

  useEffect(() => {
    setLoading( true );
    getItem( id ).then( data => {
      setItem( data );
      setLoading( false );
    } ).catch( () => {
      history.push( '/404' );
      setLoading( false );
    } );
  }, [id]);

  const transfer = data => {
    setSending( true );
    setSuccess( false );
    setError( null );
    transferItem( id, data ).then( () => {
      setSending( false );
      setSuccess( true );
    } ).catch( e => {
      setSending( false );
      setSuccess( false );
      setError( `${e.message} (code: ${e.code})` );
    } )
  }

  if( values ) {
    return (
      <>
        {success && <Success text={`El NFT ya fue transferido a: ${values.address}`} link={`/nft/${id}`} />}
        {sending && <Loading status="Enviando..." />}
        <Confirm
          item={item}
          mode="transfer"
          values={values}
          error={error}
          onCancel={() => setValues( null )}
          onContinue={data => transfer( data )}
        />
      </>
    )
  }

  return (
    <section className={styles.wrapper}>
      {loading && <Loading status="Cargando..." /> }
      <div className={styles.backButton}>
        <BackButton2 redirectTo="/" />
      </div>
      <div className={styles.container}>
        <div>
          <div className={styles.image}>
            <figure>
              <img src={getItemImage(item.nft)} alt={item.nft.name} />
            </figure>
          </div>
          <div className={cn(styles.block, styles.summary)}>
            <h4>Transferir</h4>
            <div className={styles.title}>{item.nft.name}</div>
            <div className={styles.subtitle}>{item.nft.videogame || 'N/A'}</div>
            <div className={styles.extra}>Tipo de token <b>{TokenType[item.nft.rarity]}</b></div>
          </div>
          <Form
            layout="vertical"
            onFinish={setValues}
            hideRequiredMark
          >
            <div className={cn(styles.block, styles.transfer)}>
              <Row gutter={[16, 0]} className={styles.form}>
                <Col xs={24}>
                  <Form.Item
                    hasFeedback
                    name="address"
                    rules={[{ required: true, message: "La dirección es obligatoria" }]}
                  >
                    <Input rows={3} size="large" placeholder="Ingresar dirección del usuario" />
                  </Form.Item>
                </Col>
              </Row>
            </div>
            <div className={cn(styles.block, styles.transfer)}>
              <Row gutter={[16, 0]} className={styles.form}>
                <Col xs={24}>
                  <Form.Item
                    hasFeedback
                    label="¿Quieres transferir o conservar las regalías?"
                    name="royalities"
                    rules={[{ required: true, message: "Campo obligatorio" }]}
                  >
                    <Radio.Group>
                      <Radio value={1}>Transferir</Radio>
                      <Radio value={0}>Conservar</Radio>
                    </Radio.Group>
                  </Form.Item>
                </Col>
              </Row>
            </div>
            <div className={cn(styles.block, styles.transfer)}>
              <Row gutter={[16, 0]} className={styles.form}>
                <Col xs={24}>
                  <Form.Item
                    hasFeedback
                    label="Concepto (opcional)"
                    name="concept"
                  >
                    <Input rows={3} size="large" placeholder="Ej. Pago de item" />
                  </Form.Item>
                </Col>
              </Row>
            </div>
            <Button htmlType="submit" className={styles.button}>
              <span>Continuar</span> <div><ArrowIcon /></div>
            </Button>
          </Form>
        </div>
      </div>
    </section>
  );
};

export default connect(({ user }) => ({ user }))( withRouter( Transfer ) );
