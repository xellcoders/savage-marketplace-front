import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Col, Form, Input, Row, Select, Button } from "antd";
import { withRouter } from "react-router-dom";
import BackButton2 from "components/BackButton2";
import Loading, { Success } from "components/Loading";
import { getItem, putOnSale } from "services/api.item.service";
import numeral from "numeral";
import cn from "classnames";
import Chevron from "components/Dropdown/chevron";
import ArrowIcon from "components/Icons/arrow";
import Payment from "components/Payment";
import Price from "components/Price";
import validators from "util/validators";
import { getItemImage } from "util/files";
import styles from "./styles.module.sass";
import { history } from '../../../index';
import Confirm from "./confirm";
import { getETHPrice } from "../../../services/api.util.service";

const Sell = ({ user, match: { params: { id } }}) => {
  const [sending, setSending] = useState(false);
  const [error, setError] = useState();
  const [success, setSuccess] = useState( false );
  const [loading, setLoading] = useState(true);
  const [item, setItem] = useState({ nft: {} });
  const [wallet, setWallet] = useState('nsc' );
  const [values, setValues] = useState();
  const [amount, setAmount] = useState(0);
  const [eth, setETH] = useState(0);
  const [currency, setCurrency] = useState('mxn');

  const [price, setPrice] = useState( 0 );

  useEffect(() => {
    setLoading( true );
    getItem( id ).then( data => {
      setItem( data );
      setLoading( false );
    } ).catch( () => {
      history.push( '/404' );
      setLoading( false );
    } );
    getETHPrice().then( response => {
      setPrice( response.price );
    } );
  }, [id]);

  const sell = data => {
    setSending( true );
    setSuccess( false );
    setError( null );
    putOnSale( id, data ).then( () => {
      setSending( false );
      setSuccess( true );
    } ).catch( e => {
      setSending( false );
      setSuccess( false );
      setError( `${e.message} (code: ${e.code})` );
    } )
  }

  if( values ){
    return (
      <>
        {success && <Success text="Tu NFT ya está a la venta en el Marketplace de Neon Savage" link={`/nft/${id}`} /> }
        {sending && <Loading status="Enviando..." /> }
        <Confirm
          item={item}
          mode="sell"
          values={values}
          wallet={wallet}
          error={error}
          eth={price}
          dollar={user.dollar}
          onCancel={() => setValues(null)}
          onContinue={data => sell( data )}
        />
      </>
    )
    // return <Confirm item={item} mode="auction" values={{ start: 52, days: 2, hours: 13, minutes: 20 }} wallet="nsc" />
    // return <Confirm item={item} mode="game" values={{ instructions: 'asdasdasdasdasdasdasd' }} />
    // return <Confirm item={item} mode="transfer" values={{ instructions: 'asdasdasdasdasdasdasd' }} />
  }
  return (
    <section className={styles.wrapper}>
      {loading && <Loading status="Cargando..." /> }
      <div className={styles.backButton}>
        <BackButton2 to="/dashboard/onsale" />
      </div>
      <div className={styles.container}>
        <div>
          <div className={styles.image}>
            <figure>
              <img src={getItemImage(item.nft)} alt={item.nft.name} />
            </figure>
          </div>
          <div className={cn(styles.block, styles.summary)}>
            <h4>Venta</h4>
            <div className={styles.title}>{item.nft.name}</div>
            <div className={styles.subtitle}>{item.nft.videogame || 'N/A'}</div>
            <div className={styles.extra}>
              {item.price &&
                <>Compraste este NFT por <b><Price item={item} fixed={item.price} /> MXN</b></>
              }
            </div>
          </div>
          <Form
            layout="vertical"
            onFinish={setValues}
            initialValues={{ currency: 'mxn' }}
            hideRequiredMark
          >
            <div className={cn(styles.block, styles.sell)}>
              <Row gutter={[16, 0]} className={styles.form}>
                <Col sm={12}>
                  <Form.Item
                    hasFeedback
                    label="¿En cuanto lo quieres vender?"
                    name="price"
                    rules={[{ required: true, message: "El valor es obligatorio" }, validators.number]}
                  >
                    <Input
                      type="number"
                      size="large"
                      placeholder="Ej. 35.00"
                      onChange={e => {
                        setAmount( e.target.value );
                        if( currency === 'mxn' ){
                          setETH( e.target.value / (price * user.dollar) );
                          return;
                        }
                        setETH( e.target.value );
                      }}
                    />
                  </Form.Item>
                </Col>
                <Col sm={12}>
                  <Form.Item
                    hasFeedback
                    label="Valor"
                    name="currency"
                    rules={[{ required: true, message: "El valor es obligatorio" }]}
                  >
                    <Select
                      size="large"
                      className="w-100"
                      suffixIcon={<Chevron />}
                      onChange={value => {
                        setCurrency( value );
                        if( value === 'mxn' ) {
                          setETH( amount / (price * user.dollar) );
                          return;
                        }
                        setETH( amount );
                      }}
                    >
                      <Select.Option value="mxn">MXN</Select.Option>
                      <Select.Option value="eth">ETH</Select.Option>
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
              <div className={styles.conversion}>
                <b>{numeral(eth).format('##,###.00')} - ETH</b> Tarifa: 1 ETH = <b>{numeral(price * user.dollar).format('##,###.00')} MXN</b>
              </div>
            </div>
            <div className={cn(styles.block, styles.payment)}>
              <h4>Recibiras el pago en:</h4>
              <Payment selectedWallet={wallet} setSelectedWallet={value => setWallet( value )} eth={eth} />
            </div>
            <Button htmlType="submit" className={styles.button}>
              <span>Continuar</span> <div><ArrowIcon /></div>
            </Button>
          </Form>
        </div>
      </div>
    </section>
  );
};

export default connect(({ user }) => ({ user }))( withRouter( Sell ) );
