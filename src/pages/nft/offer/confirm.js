import React from "react";
import moment from "moment";
import numeral from "numeral";
import { connect } from "react-redux";
import { Alert, Button } from "antd";
import { getItemImage } from "util/files";
import { withRouter } from "react-router-dom";
import cn from "classnames";
import { TokenType } from "util/constants";
import styles from "./styles.module.sass";

const wallets = {
  'nsc': <>Savage <b>NSC</b></>,
  'eth': <>Ethereum <b>ETH</b></>,
};

const Confirm = ({ item, mode, values, wallet, eth, dollar, error, onContinue = ()=>{}, onCancel = ()=>{} }) => {
  const date = moment().format('DD/MM/YYYY');
  const time = moment().format('HH:mm');

  const price = values.currency === 'mxn' ? values.price : values.price / (eth * dollar);
  const start = values.currency === 'mxn' ? values.start : values.price / (eth * dollar);
  return (
    <section className={styles.wrapper}>
      <div className={styles.container}>
        <div className={styles.confirm}>
          {error &&
            <Alert
              className={styles.error}
              message="Error"
              description={error}
              type="error"
              showIcon
              closable
            />
          }
          <h2>Revisa y confirma</h2>
          <div className={cn(styles.block, styles.summary)}>
            <div className={styles.icon}>
              <figure>
                <img src={getItemImage(item.nft)} alt={item.nft.name} />
              </figure>
            </div>
            <div className={styles.title}>{item.nft.name}</div>
            <div className={styles.subtitle}>{item.nft.videogame || 'N/A'}</div>
            {mode === 'sell' &&
              <div className={styles.section}>
                <div>
                  <span>Precio de venta</span>
                  <b>${numeral(price).format('##,###.00')} MXN</b>
                </div>
                <div>
                  <span>A tu cuenta</span>
                  <span>{wallets[wallet]}</span>
                </div>
              </div>
            }
            {mode === 'auction' &&
              <>
                <div className={styles.section}>
                  <div>
                    <span>Precio inicial</span>
                    <b>${numeral(start).format('##,###.00')} MXN</b>
                  </div>
                  <div>
                    <span>Tiempo en subasta</span>
                    <b>{values.days} días/{values.hours}hrs/{values.minutes}min</b>
                  </div>
                </div>
                <Alert
                  type="info"
                  message={
                    <>
                      <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0 8C0 3.58172 3.58172 0 8 0C12.4183 0 16 3.58172 16 8C16 12.4183 12.4183 16 8 16C3.58172 16 0 12.4183 0 8Z" fill="#4C0099" />
                        <path d="M7.88495 5.44154C8.36014 5.44154 8.74536 5.05631 8.74536 4.58112C8.74536 4.10592 8.36014 3.7207 7.88495 3.7207C7.40975 3.7207 7.02453 4.10592 7.02453 4.58112C7.02453 5.05631 7.40975 5.44154 7.88495 5.44154Z" fill="white" />
                        <path d="M10.2604 11.1446V12.1446H5.80225V11.1446H7.27079V7.42657H6.16045V6.42657H8.72912V11.1446H10.2604Z" fill="white" />
                      </svg>
                      Una vez tu NFT este en subasta, no hay forma de retirarlo, hasta que termine la misma.
                    </>
                  }
                />
              </>
            }
            {(mode === 'game' || mode === 'transfer') &&
              <div className={styles.section}>
                <div>
                  <span>Tipo de token</span>
                  <b>{TokenType[item.nft.rarity]}</b>
                </div>
                <div>
                  <span>Habilidad principal</span>
                  <b>Ejemplo</b>
                </div>
              </div>
            }
            <div className={styles.section}>
              <div className={styles.date}>
                <span>Siendo las {time}hrs. del día de <strong>{date}</strong></span>
              </div>
            </div>
            {mode === 'game' &&
              <>
                <div className={styles.section}>
                  <div>
                    <b>Misión en el juego para obtener est NFT</b>
                    <span style={{ lineHeight: '20px' }}>
                      {values.instructions}
                    </span>
                  </div>
                </div>
                <div className={styles.section}>
                  <Alert
                    type="info"
                    message={
                      <>
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M0 8C0 3.58172 3.58172 0 8 0C12.4183 0 16 3.58172 16 8C16 12.4183 12.4183 16 8 16C3.58172 16 0 12.4183 0 8Z" fill="#4C0099" />
                          <path d="M7.88495 5.44154C8.36014 5.44154 8.74536 5.05631 8.74536 4.58112C8.74536 4.10592 8.36014 3.7207 7.88495 3.7207C7.40975 3.7207 7.02453 4.10592 7.02453 4.58112C7.02453 5.05631 7.40975 5.44154 7.88495 5.44154Z" fill="white" />
                          <path d="M10.2604 11.1446V12.1446H5.80225V11.1446H7.27079V7.42657H6.16045V6.42657H8.72912V11.1446H10.2604Z" fill="white" />
                        </svg>
                        Este NFT será entregado al primer gamer que logre completar esta misión
                      </>
                    }
                  />
                </div>
              </>
            }
            <div className={styles.buttons}>
              <Button htmlType="submit" onClick={() => onContinue( values )}>Continuar</Button>
              <Button htmlType="cancel" onClick={onCancel} className={styles.cancel}>Cancelar</Button>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default connect(({ user }) => ({ user }))( withRouter( Confirm ) );
