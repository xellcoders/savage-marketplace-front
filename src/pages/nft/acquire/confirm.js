import React from "react";
import moment from "moment";
import numeral from "numeral";
import { connect } from "react-redux";
import { Alert, Button } from "antd";
import Price from "components/Price";
import { withRouter } from "react-router-dom";
import { getItemImage } from "util/files";
import cn from "classnames";
import styles from "./styles.module.sass";

const wallets = {
  'nsc': <>Savage <b>NSC</b></>,
  'eth': <>Ethereum <b>ETH</b></>,
};

const Confirm = ({ item, mode, values, wallet, eth, dollar, error, onContinue = ()=>{}, onCancel = ()=>{} }) => {
  const date = moment().format('DD/MM/YYYY');
  const time = moment().format('HH:mm');

  const bid = values?.currency === 'mxn' ? values?.bid : values?.bid * (eth * dollar);
  return (
    <section className={styles.wrapper}>
      <div className={styles.container}>
        <div className={styles.confirm}>
          {error &&
            <Alert
              message="Error al comprar el NFT"
              description={error}
              type="error"
              showIcon
              closable
            />
          }
          <h2>Revisa y confirma</h2>
          <div className={cn(styles.block, styles.summary)}>
            <div className={styles.icon}>
              <figure>
                <img src={getItemImage(item.nft)} alt={item.nft.name} />
              </figure>
            </div>
            <div className={styles.title}>{item.nft.name}</div>
            <div className={styles.subtitle}>{item.nft.videogame || 'N/A'}</div>
            {mode === 'buy' &&
              <div className={styles.section}>
                <div>
                  <span>Pagas</span>
                  <b><Price item={item} fixed={item.price || 0} /> MXN</b>
                </div>
                <div>
                  <span>Desde tu cuenta</span>
                  <span>{wallets[wallet]}</span>
                </div>
              </div>
            }
            {mode === 'bid' &&
              <div className={styles.section}>
                <div>
                  <span>Precio de subasta</span>
                  <b><Price item={item} fixed={item.bid || item.start} /> MXN</b>
                </div>
                <div>
                  <span>Ofertas</span>
                  <b>${numeral(bid).format('##,###.00')} MXN</b>
                </div>
                <div>
                  <span>Desde tu cuenta</span>
                  <span>{wallets[wallet]}</span>
                </div>
              </div>
            }
            <div className={styles.section}>
              <div className={styles.date}>
                <span>{time}hrs, <strong>{date}</strong></span>
              </div>
            </div>
            <div className={styles.buttons}>
              <Button htmlType="submit" onClick={() => onContinue( values )}>Continuar</Button>
              <Button htmlType="cancel" onClick={onCancel} className={styles.cancel}>Cancelar</Button>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default connect(({ user }) => ({ user }))( withRouter( Confirm ) );
