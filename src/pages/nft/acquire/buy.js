import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Form, Button } from "antd";
import { withRouter } from "react-router-dom";
import BackButton2 from "components/BackButton2";
import Loading, { Success } from "components/Loading";
import { buyItem, getItem } from "services/api.item.service";
import cn from "classnames";
import ArrowIcon from "components/Icons/arrow";
import { getItemImage } from "util/files";
import Payment from "components/Payment";
import Price from "components/Price";
import styles from "./styles.module.sass";
import { history } from '../../../index';
import Confirm from "./confirm";
import { useWalletProvider } from "../../../providers/wallet";

const Buy = ( { match: { params: { id } }}) => {
  const { account } = useWalletProvider();
  const [error, setError] = useState();
  const [success, setSuccess] = useState( false );
  const [loading, setLoading] = useState(true);
  const [item, setItem] = useState({ nft: {} });
  const [wallet, setWallet] = useState('nsc' );
  const [confirm, setConfirm] = useState(false);
  const [sending, setSending] = useState(false);

  useEffect(() => {
    setLoading( true );
    getItem( id ).then( data => {
      setItem( data );
      setLoading( false );
    } ).catch( () => {
      history.push( '/404' );
      setLoading( false );
    } );
  }, [id]);

  const buy = () => {
    setSending( true );
    setSuccess( false );
    setError( null );
    buyItem( id, wallet, account ).then( () => {
      setSending( false );
      setSuccess( true );
    } ).catch( e => {
      setSending( false );
      setSuccess( false );
      setError( `${e.message} (code: ${e.code})` );
    } )
  }

  if( confirm ){
    return (
      <>
        {success && <Success text="¡Tu compra se hizo con éxito!" link={`/nft/${id}`}  /> }
        {sending && <Loading status="Comprando..." /> }
        <Confirm
          item={item}
          mode="buy"
          wallet={wallet}
          error={error}
          onCancel={() => setConfirm(false)}
          onContinue={data => buy( data )}
        />
      </>
    )
  }
  return (
    <section className={styles.wrapper}>
      {loading && <Loading status="Cargando..." /> }
      <div className={styles.backButton}>
        <BackButton2 redirectTo="/" />
      </div>
      <div className={styles.container}>
        <div>
          <div className={styles.image}>
            <figure>
              <img src={getItemImage(item.nft)} alt={item.nft.name} />
            </figure>
          </div>
          <div className={cn(styles.block, styles.summary)}>
            <div className={styles.title}>{item.nft.name}</div>
            <div className={styles.subtitle}>{item.nft.videogame || 'N/A'}</div>
            <div className={styles.buy}>Compras este NFT por <b><Price item={item} /> MXN</b></div>
          </div>
          <Form
            layout="vertical"
            onFinish={() => setConfirm(true)}
            initialValues={{ currency: 'mxn' }}
            hideRequiredMark
          >
            <div className={cn(styles.block, styles.payment)}>
              <h4>Desde:</h4>
              <Payment selectedWallet={wallet} setSelectedWallet={value => setWallet( value )} />
            </div>
            <Button htmlType="submit" className={styles.button}>
              <span>Continuar</span> <div><ArrowIcon /></div>
            </Button>
          </Form>
        </div>
      </div>
    </section>
  );
};

export default connect(({ user }) => ({ user }))( withRouter( Buy ) );
