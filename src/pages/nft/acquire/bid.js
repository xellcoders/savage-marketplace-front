import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import numeral from "numeral";
import { Form, Button, Row, Col, Input, Select } from "antd";
import { withRouter } from "react-router-dom";
import BackButton2 from "components/BackButton2";
import Loading, { Success } from "components/Loading";
import { bidItem, getItem } from "services/api.item.service";
import cn from "classnames";
import ArrowIcon from "components/Icons/arrow";
import Payment from "components/Payment";
import Chevron from "components/Dropdown/chevron";
import { getItemImage } from "util/files";
import validators from "util/validators";
import styles from "./styles.module.sass";
import { history } from '../../../index';
import Confirm from "./confirm";
import { getETHPrice } from "../../../services/api.util.service";
import Price from "../../../components/Price";
import { useWalletProvider } from "../../../providers/wallet";

const Bid = ( { user, match: { params: { id } }}) => {
  const { account } = useWalletProvider();
  const [error, setError] = useState();
  const [success, setSuccess] = useState( false );
  const [loading, setLoading] = useState(true);
  const [item, setItem] = useState({ nft: {} });
  const [wallet, setWallet] = useState('nsc' );
  const [values, setValues] = useState();
  const [sending, setSending] = useState(false);
  const [amount, setAmount] = useState(0);
  const [eth, setETH] = useState(0);
  const [currency, setCurrency] = useState('mxn');
  const [validator, setValidator] = useState({});

  const [price, setPrice] = useState( 0 );

  useEffect(() => {
    setLoading( true );
    Promise.all([getItem( id ), getETHPrice()]).then( ([data, response]) => {
      setItem( data );
      setPrice( response.price );
      if( data.currency.toLowerCase() === 'eth' )
        setValidator(validators.minimum((data.bid || data.start || 0) * response.price * user.dollar, 'MXN'));
      else
        setValidator(validators.minimum((data.bid || data.start || 0), 'MXN'));
      setLoading( false );
    } ).catch( () => {
      history.push( '/404' );
      setLoading( false );
    } );
  }, [id]);

  const bid = () => {
    setSending( true );
    setSuccess( false );
    setError( null );
    bidItem( id, values, wallet, account ).then( () => {
      setSending( false );
      setSuccess( true );
    } ).catch( e => {
      setSending( false );
      setSuccess( false );
      setError( `${e.message} (code: ${e.code})` );
    } )
  }

  const onChangeCurrency = value => {
    if( value === 'mxn' ) {
      if( item.currency.toLowerCase() === 'eth' )
        setValidator(validators.minimum((item.bid || item.start || 0) * price * user.dollar, 'MXN'));
      else
        setValidator(validators.minimum((item.bid || item.start || 0), 'MXN'));
    } else {
      setValidator(validators.minimum(item.bid || item.start || 0, 'ETH'));
    }
  }

  if( loading ) return <Loading status="Cargando..." />;

  if( values ){
    return (
      <>
        {success && <Success text="¡Tu oferta se hizo con éxito!" link={`/nft/${id}`}  /> }
        {sending && <Loading status="Ofertando..." /> }
        <Confirm
          item={item}
          mode="bid"
          values={values}
          wallet={wallet}
          error={error}
          eth={price}
          dollar={user.dollar}
          onCancel={() => setValues(null)}
          onContinue={data => bid( data )}
        />
      </>
    )
  }
  return (
    <section className={styles.wrapper}>
      <div className={styles.backButton}>
        <BackButton2 redirectTo="/" />
      </div>
      <div className={styles.container}>
        <div>
          <div className={styles.image}>
            <figure>
              <img src={getItemImage(item.nft)} alt={item.nft.name} />
            </figure>
          </div>
          <div className={cn(styles.block, styles.summary)}>
            <div className={styles.title}>{item.nft.name}</div>
            <div className={styles.subtitle}>{item.nft.videogame || 'N/A'}</div>
            <div className={styles.bid}>
              <div><span>Precio de subasta</span> <b><Price item={item} fixed={item.start || 0} /> MXN</b></div>
              <div><span>Mejor ofeta</span> <b><Price item={item} fixed={item.bid || 0} /> MXN</b></div>
            </div>
          </div>
          <Form
            layout="vertical"
            onFinish={setValues}
            initialValues={{ currency: 'mxn' }}
            hideRequiredMark
          >
            <div className={cn(styles.block, styles.payment)}>
              <h4>Desde:</h4>
              <Payment selectedWallet={wallet} setSelectedWallet={value => setWallet( value )} />
            </div>
            <div className={cn(styles.block, styles.bid)}>
              <h5>Ingresa los datos de la transacción</h5>
              <Row gutter={[16, 0]} className={styles.form}>
                <Col sm={12}>
                  <Form.Item
                    hasFeedback
                    label="¿Cuanto quieres ofertar?"
                    name="bid"
                    rules={[{ required: true, message: "El valor ofertado es obligatorio" }, validator]}
                  >
                    <Input
                      type="number"
                      size="large"
                      placeholder="Ej. 35.00"
                      onChange={e => {
                        setAmount( e.target.value );
                        if( currency === 'mxn' ){
                          setETH( e.target.value / (price * user.dollar) );
                          return;
                        }
                        setETH( e.target.value );
                      }}
                    />
                  </Form.Item>
                </Col>
                <Col sm={12}>
                  <Form.Item
                    hasFeedback
                    label="Valor"
                    name="currency"
                    rules={[{ required: true, message: "El valor es obligatorio" }]}
                  >
                    <Select
                      size="large"
                      className="w-100"
                      suffixIcon={<Chevron />}
                      onChange={value => {
                        setCurrency( value );
                        onChangeCurrency( value );
                        if( value === 'mxn' ) {
                          setETH( amount / (price * user.dollar) );
                          return;
                        }
                        setETH( amount );
                      }}
                    >
                      <Select.Option value="mxn">MXN</Select.Option>
                      <Select.Option value="eth">ETH</Select.Option>
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
              <div className={styles.conversion}>
                <b>{numeral(eth).format('##,###.00')} - ETH</b> Tarifa: 1 ETH = <b>{numeral(price * user.dollar).format('##,###.00')} MXN</b>
              </div>
            </div>
            <Button htmlType="submit" className={styles.button}>
              <span>Continuar</span> <div><ArrowIcon /></div>
            </Button>
          </Form>
        </div>
      </div>
    </section>
  );
};

export default connect(({ user }) => ({ user }))( withRouter( Bid ) );
