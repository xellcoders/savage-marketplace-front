import React, { useState, useEffect } from "react";
import moment from "moment";
import { connect } from "react-redux";
import { Form, Row, Col, Input, Button, Alert, Rate } from "antd";
import { withRouter } from "react-router-dom";
import BackButton2 from "components/BackButton2";
import Loading from "components/Loading";
import { getItem, addComment, getComments, getRating, rateItem } from "services/api.item.service";
import { getItemImage } from "util/files";
import cn from "classnames";
import styles from "./styles.module.sass";
import { history } from '../../../index';

const CommentItem = ({ match: { params: { id } }}) => {
  const [showForm, setShowForm] = useState(false);
  const [sending, setSending] = useState(false);
  const [error, setError] = useState();
  const [success, setSuccess] = useState( false );
  const [loading, setLoading] = useState(true);
  const [comments, setComments] = useState([]);
  const [rating, setRating] = useState(0);
  const [item, setItem] = useState({ nft: {} });
  const [form] = Form.useForm();

  const loadComments = () => {
    setLoading( true );
    getComments( id ).then( results => {
      setComments( results );
      setLoading( false );
    } ).catch( () => {
      setLoading( false );
    } );
  }

  const loadRating = () => {
    getRating( id ).then( score => {
      setRating( score );
    } );
  }

  useEffect(() => {
    setLoading( true );
    getItem( id ).then( data => {
      setItem( data );
      setLoading( false );
    } ).catch( () => {
      history.push( '/404' );
      setLoading( false );
    } );
    loadComments();
    loadRating();
    // eslint-disable-next-line
  }, [id]);

  const rate = async value => {
    setRating( value );
    rateItem( id, value ).catch( () => {
      setSuccess( false );
    } )
  }

  const comment = ({ comment: text }) => {
    setSending( true );
    setSuccess( false );
    setError( null );
    addComment( id, text ).then( () => {
      setSending( false );
      setSuccess( true );
      form.setFieldsValue({ comment: '' });
      loadComments();
    } ).catch( e => {
      setSending( false );
      setSuccess( false );
      setError( `${e.message} (code: ${e.code})` );
    } )
  }

  return (
    <section className={styles.wrapper}>
      {loading && <Loading status="Cargando..." /> }
      <div className={styles.backButton}>
        <BackButton2 redirectTo="/" />
      </div>
      <div className={styles.container}>
        <div>
          <div className={styles.image}>
            <figure>
              {item.nft.image && <img src={getItemImage(item.nft)} alt={item.nft.name} />}
            </figure>
          </div>
          <div className={cn(styles.block)}>
            <div className={styles.info}>
              <div className={styles.summary}>
                <div className={styles.title}>{item.nft.name}</div>
                <div className={styles.subtitle}>{item.nft.videogame || 'N/A'}</div>
              </div>
              <div className={styles.rating}>
                <div>Califica:</div>
                <div>
                  <Rate allowHalf value={rating} className={styles.rate} onChange={rate} />
                </div>
              </div>
            </div>
            {showForm &&
              <Form
                layout="vertical"
                onFinish={comment}
                initialValues={{ comment: '' }}
                className={styles.form}
                form={form}
                hideRequiredMark
              >
                {error &&
                  <Alert message="Error al enviar el comentario" description={error} className={styles.error} type="error" showIcon closable />
                }
                {success &&
                  <Alert message="Se ha enviado tu comentario" type="success" className={styles.success} showIcon closable />
                }
                <Row gutter={[16, 0]}>
                  <Col xs={24}>
                    <Form.Item
                      hasFeedback
                      name="comment"
                      rules={[{ required: true, message: "El campo es obligatorio" }]}
                    >
                      <Input.TextArea rows={6} size="large" placeholder="Escribe tu comentario sobre este NFT." />
                    </Form.Item>
                  </Col>
                </Row>
                <Button loading={sending} htmlType="submit" className={styles.button}>Comentar</Button>
              </Form>
            }
            <div
              className={cn(styles.toggle)}
              role="button"
              tabIndex={0}
              onKeyDown={()=>{}}
              onClick={() => setShowForm(!showForm)}
            >
              {showForm ?
                <>
                  Ocultar {" "}
                  <svg width="16" height="9" viewBox="0 0 16 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd" clipRule="evenodd" d="M7.99678 2.1941L1.99303 8.19785L0.932373 7.13719L7.99678 0.0727816L15.0674 7.14344L14.0068 8.2041L7.99678 2.1941Z" fill="#01B4C3" />
                    <path fillRule="evenodd" clipRule="evenodd" d="M7.99678 2.1941L1.99303 8.19785L0.932373 7.13719L7.99678 0.0727816L15.0674 7.14344L14.0068 8.2041L7.99678 2.1941Z" fill="#01B4C3" />
                  </svg>
                </>:
                <>
                  Comenta {" "}
                  <svg width="16" height="9" viewBox="0 0 16 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd" clipRule="evenodd" d="M8.0031 6.8059L14.0068 0.802152L15.0675 1.86281L8.0031 8.92722L0.932439 1.85656L1.9931 0.795898L8.0031 6.8059Z" fill="#01B4C3" />
                    <path fillRule="evenodd" clipRule="evenodd" d="M8.0031 6.8059L14.0068 0.802152L15.0675 1.86281L8.0031 8.92722L0.932439 1.85656L1.9931 0.795898L8.0031 6.8059Z" fill="#01B4C3" />
                  </svg>

                </>
              }

            </div>
          </div>
          <div className={cn(styles.block, styles.comments)}>
            <div className={styles.title}>Comentarios acerca de este NFT</div>
            {comments.map( comm => (
              <div className={styles.comment} key={comm.created_at}>
                <figure>
                  <img src={comm.user.avatar} alt="" />
                </figure>
                <div>
                  <div className={styles.user}>
                    <span className={styles.name}>{comm.user.name}</span>
                    <span className={styles.date}>{moment(comm.created_at).format('DD/MM/YYYY')}</span>
                  </div>
                  <div className={styles.text}>{comm.text}</div>
                </div>
              </div>
            ) )}
          </div>
        </div>
      </div>
    </section>
  );
};

export default connect(({ user }) => ({ user }))( withRouter( CommentItem ) );
