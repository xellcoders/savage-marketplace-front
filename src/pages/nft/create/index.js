import React, { useState, useEffect } from "react";
import { Col, Form, Input, Row, Select, Slider, Button, Switch, Upload, Alert, Divider, Space, Empty } from "antd";
import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import cn from "classnames";
import ReactTooltip from "react-tooltip";
import "rc-slider/assets/index.css";
import BackButton2 from "components/BackButton2";
import Chevron from "components/Dropdown/chevron";
import LabelTip from "components/LabelTip";
import { getBase64 } from "util/files";
import { createNFT, getCollections } from "services/api.nft.service";
import Loading from "components/Loading";
import styles from "./styles.module.sass";
import { history } from "../../../index";

const CreateNFT = () => {
  const [loading, setLoading] = useState(true);
  const [creating, setCreating] = useState(false);
  const [status, setStatus] = useState("Subiendo asset");
  const [error, setError] = useState(null);
  const [percent, setPercent] = useState(0);
  const [imageUrl, setImageUrl] = useState();
  const [file, setFile] = useState();

  const [collections, setCollections] = useState([]);
  const [collection, setCollection] = useState('');

  useEffect(() => {
    init();
    // eslint-disable-next-line
  }, []);

  function init() {
    getCollections().then( result => {
      setCollections( result.map( c => c.name ) );
      setLoading(false);
    } );
    console.log(loading);
  }

  const onNameChange = event => {
    setCollection(event.target.value);
  };

  const addItem = e => {
    e.preventDefault();
    setCollections([...collections, collection]);
    setCollection('');
  };

  const onCreateProgress = progressEvent => {
    if( progressEvent.loaded >= file.size ) {
      setPercent( 99 );
      setStatus( "Desplegando NFT" );
    }
    else setPercent(  (progressEvent.loaded * 100 / file.size).toFixed( 1 ) );
  }

  const submit = values => {
    setPercent(0);
    setError( null );
    setCreating( true );
    setStatus( "Subiendo asset" );
    createNFT( file, values, onCreateProgress ).then( () => {
      setPercent(100);
      setTimeout(() => {
        setCreating( false );
        history.push( '/dashboard/collection' );
      }, 2000);
    } ).catch( e => {
      setPercent(100);
      setStatus( "Falló la creación del NFT" );
      setError( `${e.message} (code: ${e.code})` );
      setTimeout(() => setCreating( false ), 2000);
    } );
  }

  const uploadButton = (
    <div className={styles.preview}>
      {creating ?
        <LoadingOutlined /> :
        <img src="/images/icons/img.svg" width={50} style={{ marginTop: 20 }} alt="Upload" />
      }
    </div>
  );

  const handleChange = info => {
    const { type } = info.file;
    if( type.startsWith('image') ){
      getBase64(info.file, url => {
        setImageUrl( url );
      });
    } else if( type.startsWith('video')  ) {
      setImageUrl( '/images/content/video.png' );
    } else if( type.startsWith('audio')  ) {
      setImageUrl( '/images/content/sound.png' );
    } else {
      setImageUrl( '/images/content/3d-model.png' );
    }
    setFile( info.file );
  };

  return (
    <section className={styles.wrapper}>
      {creating && <Loading status={status} percent={percent} error={error} /> }
      <div className={styles.back_btn_container}>
        <BackButton2 redirectTo="/dashboard" />
      </div>
      <div className={styles.main_title}>
        Crear NFT
      </div>
      <div className={cn(styles.container)}>
        <Form layout="vertical" onFinish={submit} hideRequiredMark>
          <ReactTooltip
            className={styles.tooltip}
            backgroundColor="#00B7C4"
            place="top"
          />
          <div>
            {error &&
              <Alert
                message="Error al crear el NFT"
                description={error}
                type="error"
                showIcon
                closable
              />
            }
            <div className={styles.title}>
              Identidad y personalización
            </div>
            <div className={styles.section}>
              <Row gutter={[16, 0]}>
                <Col className={styles.upload}>
                  <Form.Item
                    hasFeedback
                    name="file"
                    rules={[{ required: true, message: "El archivo es obligatorio" }]}
                  >
                    <Upload
                      accept=".jpg,.jpeg,.png,.mov,.mp4,.avi,.mp3,.wav,.flac,.mpeg"
                      name="avatar"
                      listType="picture-card"
                      className="avatar-uploader"
                      showUploadList={false}
                      beforeUpload={() => false}
                      onChange={handleChange}
                    >
                      {imageUrl ?
                        <div className={styles.preview}>
                          <img src={imageUrl} alt="avatar" style={{ width: '100%' }} />
                        </div> : uploadButton
                      }
                      <a>{file ? file.name : 'Importar archivo'}</a>
                      <div className={styles.details}>(imagen, video, audio o modelo 3D)</div>
                    </Upload>
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={[16, 0]}>
                <Col sm={12}>
                  <Form.Item
                    hasFeedback
                    label="Nombre"
                    name="name"
                    rules={[{ required: true, message: "El nombre es obligatorio" }]}
                  >
                    <Input size="large" placeholder="Nombre del NFT" />
                  </Form.Item>
                </Col>
                <Col sm={12}>
                  <Form.Item
                    hasFeedback
                    label="Símbolo"
                    name="symbol"
                    rules={[{ required: true, message: "El símbolo es obligatorio" }]}
                  >
                    <Input size="large" placeholder="Ej. SVG" onInput={e => { e.target.value = (`${e.target.value}`).toUpperCase() }} />
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={[16, 0]}>
                <Col sm={12}>
                  <div>
                    <Form.Item
                      hasFeedback
                      name="collection"
                      label={<LabelTip label="Colección" tip="Este es el lugar en donde aparecerá tu artículo" />}
                      rules={[
                        {
                          required: true,
                          message: "La colección es obligatoria"
                        }
                      ]}
                    >
                      <Select
                        size="large"
                        placeholder="Colección"
                        notFoundContent={<Empty description="No hay ningúna colección" image={Empty.PRESENTED_IMAGE_SIMPLE} className="ant-empty-small" />}
                        className="w-100"
                        suffixIcon={<Chevron />}
                        dropdownRender={menu => (
                          <>
                            {menu}
                            <Divider style={{ margin: '8px 0' }} />
                            <Space align="center" style={{ padding: '0 8px 4px' }}>
                              <Input placeholder="Nombre de la colección" value={collection} onChange={onNameChange} />
                              {/* eslint-disable-next-line */}
                              <a onClick={addItem} style={{ whiteSpace: 'nowrap' }}>
                                <PlusOutlined /> Agregar
                              </a>
                            </Space>
                          </>
                        )}
                      >
                        {collections.map(item => (
                          <Select.Option key={item} value={item}>{item}</Select.Option>
                        ))}
                      </Select>
                    </Form.Item>
                  </div>
                </Col>
                <Col sm={12}>
                  <div className={styles.collections_container}>
                    <Form.Item
                      hasFeedback
                      name="ancestry"
                      label="Descendencia"
                    >
                      <Select
                        size="large"
                        notFoundContent={<Empty description="No hay ningún NFT" image={Empty.PRESENTED_IMAGE_SIMPLE} className="ant-empty-small" />}
                        placeholder="Selecciona la descendencia del NFT"
                        className="w-100"
                        suffixIcon={<Chevron />}
                      />
                    </Form.Item>
                  </div>
                </Col>
              </Row>
              <Row gutter={[16, 0]}>
                <Col sm={24}>
                  <Form.Item
                    hasFeedback
                    label={<LabelTip label="Enlace externo" tip="Neon Savage incluirá un enlace a esta URL en los detalles de este NFT, para poder conocer más información sobre el." />}
                    name="link"
                  >
                    <Input size="large" placeholder="https://ejemplo/item/123" />
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={[16, 0]}>
                <Col sm={24}>
                  <Form.Item
                    label="Descripción (opcional)"
                    name="description"
                  >
                    <Input.TextArea size="large" rows={4} placeholder="Historia o caracteristicas del NFT" />
                  </Form.Item>
                </Col>
              </Row>
            </div>
          </div>

          <div>
            <div className={styles.title}>
              Características y rasgos
            </div>
            <div className={styles.section}>
              <Row gutter={[16, 0]}>
                <Col sm={12}>
                  <div className={styles.collections_container}>
                    <Form.Item
                      hasFeedback
                      name="tags"
                      label={<LabelTip label="Tags" tip="Identificadores del NFT los cuales ayudan a que sea encontrado más facilmente" />}
                      rules={[
                        {
                          required: true,
                          message: "Debes seleccionar al menos una etiqueta"
                        }
                      ]}
                    >
                      <Select
                        mode="tags"
                        size="large"
                        placeholder="Ej. #Aventura"
                        className="w-100"
                        notFoundContent={<Empty description="No hay ningún tag" image={Empty.PRESENTED_IMAGE_SIMPLE} className="ant-empty-small" />}
                        suffixIcon={<Chevron />}
                      />
                    </Form.Item>
                  </div>
                </Col>
                <Col sm={12}>
                  <div className={styles.collections_container}>
                    <Form.Item
                      hasFeedback
                      name="rarity"
                      label={<LabelTip label="Tipo de token" tip="Clasificación y valor del NFT" />}
                      rules={[
                        {
                          required: true,
                          message: "La rareza es obligatoria"
                        }
                      ]}
                    >
                      <Select
                        size="large"
                        placeholder="Ej. Ultra raro"
                        className="w-100"
                        suffixIcon={<Chevron />}
                      >
                        <Select.Option value="common">Común</Select.Option>
                        <Select.Option value="very-common">Poco común</Select.Option>
                        <Select.Option value="rare">Raros</Select.Option>
                        <Select.Option value="epic">Épico</Select.Option>
                        <Select.Option value="legendary">Legendario</Select.Option>
                        <Select.Option value="mythical">Mítico</Select.Option>
                      </Select>
                    </Form.Item>
                  </div>
                </Col>
              </Row>
              <Row gutter={[16, 0]}>
                <Col sm={12}>
                  <div className={styles.collections_container}>
                    <Form.Item
                      name="unlockable"
                      valuePropName="checked"
                      className={styles.form_item_horizontal}
                      label={
                        <div>
                          <strong>Contenido desbloqueable</strong>
                          <p>Solamente por el propietario del NFT</p>
                        </div>
                      }
                    >
                      <Switch />
                    </Form.Item>
                  </div>
                </Col>
                <Col sm={12}>
                  <div className={styles.collections_container}>
                    <Form.Item
                      name="explicit"
                      valuePropName="checked"
                      className={styles.form_item_horizontal}
                      label={
                        <div>
                          <strong>Contenido explicito y sensible</strong>
                          <p>Solamente se mostrará con autorización</p>
                        </div>
                      }
                    >
                      <Switch />
                    </Form.Item>
                  </div>
                </Col>
              </Row>
            </div>
          </div>
          {/** END FEATURES CONTAINER */}
          <div>
            <div className={styles.title}>
              Preferencia y configuración
            </div>

            <div className={styles.section}>
              <Row gutter={[16, 0]}>
                <Col sm={12}>
                  <Form.Item
                    hasFeedback
                    label={<LabelTip label="Suministro" tip="El número de copias que se pueden replicar, sin costo de gas, (cantidades superiores a uno)" />}
                    name="supply"
                    rules={[{ required: true, message: "El suministro es obligatorio" },
                      {
                        validator: async (_, value) => {
                          // eslint-disable-next-line no-restricted-globals
                          if (isNaN(value)) throw new Error('Debe ser un número')
                          if (!Number.isInteger(parseFloat(value))) throw new Error('Debe ser un número entero')
                          if (value <= 0) throw new Error('Debe ser un número mayor a 0')
                          return value
                        }
                      }]}
                  >
                    <Input type="number" size="large" placeholder="Ej. 5" />
                  </Form.Item>
                </Col>
                <Col sm={12}>
                  <div className={styles.collections_container}>
                    <Form.Item
                      hasFeedback
                      name="chain"
                      label="Blockchain"
                      rules={[
                        {
                          required: true,
                          message: "El blockchain es obligatorio"
                        }
                      ]}
                    >
                      <Select
                        size="large"
                        placeholder="Ej. Neon Savage"
                        className="w-100"
                        suffixIcon={<Chevron />}
                      >
                        <Select.Option value="savage" disabled>Neon Savage</Select.Option>
                        <Select.Option value="ethereum">Ethereum (Ropsten)</Select.Option>
                      </Select>
                    </Form.Item>
                  </div>
                </Col>
              </Row>

              <Row gutter={[16, 0]}>
                <Col sm={24}>
                  <Form.Item
                    label="Porcentaje de regalias por próximas ventas del NFT (Se recomienda no exceder del 10%)"
                    name="royalties"
                    rules={[{ required: true, message: "El porcentaje de regalias es obligatorio" }]}
                  >
                    <Slider min={1} max={100} />
                  </Form.Item>
                </Col>
              </Row>
            </div>

            <div className={styles.create_nfts_btn_container}>
              <Button htmlType="submit" className={styles.create_nfts_button}>Crear NFT</Button>
            </div>
          </div>
        </Form>
      </div>
    </section>
  );
};

export default CreateNFT;
