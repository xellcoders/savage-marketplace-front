import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import cn from "classnames";
import { Button } from "antd";
import Price from "components/Price";
import moment from "moment";
import styles from "./styles.module.sass";

export function Offer( { item } ){
  const history = useHistory();
  return (
    <div className={styles.offer}>
      {/* eslint-disable-next-line */}
      <Button onClick={() => history.push(`/nft/${item._id}/sell`)}>Vender</Button>
      {/* eslint-disable-next-line */}
      <Button onClick={() => history.push(`/nft/${item._id}/auction`)}>Subastar</Button>
      {/* eslint-disable-next-line */}
      <Button onClick={() => history.push(`/nft/${item._id}/transfer`)}>Transferir</Button>
    </div>
  )
}

export function Buy( { item } ){
  const history = useHistory();
  return (
    <div className={cn(styles.block, styles.buy)}>
      <h5>Compra este NFT</h5>
      <div className={styles.price}>
        Precio del NFT: <b><Price item={item} fixed={item.price || 0} /></b>
      </div>
      {/* eslint-disable-next-line */}
      <Button onClick={() => history.push(`/nft/${item._id}/buy`)}>Comprar</Button>
    </div>
  )
}

export function Bid( { item } ){
  const [days, setDays] = useState(0);
  const [hours, setHours] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [seconds, setSeconds] = useState(-1);
  const history = useHistory();

  useEffect(() => {
    const currentTime = moment().valueOf();
    const eventTime = moment( item.deadline ).valueOf();
    const diffTime = eventTime - currentTime;
    let duration = moment.duration(diffTime, 'milliseconds');

    setInterval(() => {
      duration = moment.duration(duration - 1000, 'milliseconds');
      setDays( duration.days() );
      setHours( duration.hours() );
      setMinutes( duration.minutes() );
      setSeconds( duration.seconds() );
    }, 1000);
  }, [item]);

  const finished = days < 0 || hours < 0 || seconds < 0;

  return (
    <div className={cn(styles.block, styles.bid)}>
      <h5>Obten el NFT por subasta</h5>
      <div className={styles.prices}>
        <div><span>Precio de subasta</span><b><Price item={item} fixed={item.start || 0} /></b></div>
        <div><span>Mejor oferta</span><b><Price item={item} fixed={item.bid || 0} /></b></div>
      </div>
      {finished ? <h5>La subasta terminó hace:</h5> : <h5>La subasta termina en:</h5>}
      <div className={styles.countdown}>
        <div><span>Días</span><b>{Math.abs(days)}</b></div>
        <div><span>Horas</span><b>{Math.abs(hours)}</b></div>
        <div><span>Minutos</span><b>{Math.abs(minutes)}</b></div>
        <div><span>Seg.</span><b>{Math.abs(seconds)}</b></div>
      </div>
      {/* eslint-disable-next-line */}
      {!finished && <Button onClick={() => history.push(`/nft/${item._id}/bid`)}>Ofertar</Button>}
    </div>
  )
}

export function Play( { item } ){
  console.log( item.tokenId )
  return (
    <div className={cn(styles.block, styles.play)}>
      <h5>Obten el NFT por una misión en su juego</h5>
      <div className={styles.instructions}>
        Cruza los 10 mundos que se interponen entre Mario Bros y el castillo de Bowser para rescatar a la princesa peach.
      </div>
      <Button>Ir al juego</Button>
    </div>
  )
}