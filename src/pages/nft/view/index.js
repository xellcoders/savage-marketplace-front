import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShareNodes } from "@fortawesome/free-solid-svg-icons";
import Raking from "components/Raking";
import BackButton2 from "components/BackButton2";
import Chevron from "components/Chevron";
import Loading from "components/Loading";
import Share from "components/Share";
import { getItem, toggleLikeItem } from "services/api.item.service";
import { TokenType } from "util/constants";
import { getItemImage } from "util/files";
import moment from "moment";
import cn from "classnames";
import styles from "./styles.module.sass";
import { history } from '../../../index';
import { Bid, Buy, Offer, Play } from "./blocks";

const Biography = ({item, block = true}) => {
  return (
    <div className={cn({ [styles.block]: block }, styles.biography)}>
      <div className={styles.description}>
        <h5>Biografía</h5>
        <p>{item.nft.description}</p>
      </div>
      <div className={styles.features}>
        <div className={styles.feature}>
          <p><img src="/images/features_icon.svg" alt="" height={14} /> Características:</p>
          <b>{item.nft.features}</b>
        </div>
        <div className={styles.feature}>
          <p><img src="/images/hability_icon.svg" alt="" height={14} /> Habilidad principal:</p>
          <b>{item.nft.skill}</b>
        </div>
        <div className={styles.feature}>
          <p><img src="/images/token_type_icon.svg" alt="" height={14} /> Tipo de token:</p>
          <b>{TokenType[item.nft.rarity]}</b>
        </div>
        <div className={styles.feature}>
          <p><img src="/images/rating_icon.svg" alt="" height={14} /> Rating:</p>
          <Raking raking={item.rating} />
        </div>
      </div>
      <h5>Descendencia del NFT</h5>
      <div className={styles.descent}>
        {item.nft.descendants?.map( descendant => (
          <div><img src={descendant.image} alt={descendant.name} /><div>{descendant.name}</div></div>
        ) )}
      </div>
    </div>
  )
}

const View = ({ user, match: { params: { id } }}) => {
  const [item, setItem] = useState({ nft: {} });
  const [loading, setLoading] = useState(true);
  const [liked, setLiked] = useState( false );
  const [showShareModal, setShowShareModal] = useState( false );

  useEffect(() => {
    setLoading( true );
    getItem( id ).then( data => {
      setItem( data );
      setLoading( false );
      setLiked( data.liked );
    } ).catch( () => {
      history.push( '/signup' );
      setLoading( false );
    } );
  }, [id]);

  return (
    <section className={styles.wrapper}>
      {loading && <Loading status="Cargando..." /> }
      <div className={styles.backButton}>
        <BackButton2 redirectTo="/" />
      </div>
      <div className={styles.container}>
        <div className={styles.center}>
          <div className={styles.image}>
            <figure>
              <img src={getItemImage(item.nft)} alt={item.nft.name} />
            </figure>
            <div className={styles.share}>
              <span
                className="button"
                role="button"
                tabIndex={0}
                onKeyDown={()=>{}}
                onClick={event => {
                  event.preventDefault();
                  setShowShareModal( true );
                }}
              >
                <FontAwesomeIcon icon={faShareNodes} color="#00B7C4" />
              </span>
              <span
                className="button"
                role="button"
                tabIndex={0}
                onKeyDown={()=>{}}
                onClick={event => {
                  event.preventDefault();
                  toggleLikeItem( id ).then( result => setLiked( result ) );
                }}
              >
                {liked ?
                  <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd" clipRule="evenodd" d="M11.0019 0.398682C9.72954 0.398682 8.8218 0.853724 8.12213 1.6525C8.09025 1.6889 8.04831 1.7408 8.00108 1.80134C7.95385 1.7408 7.91191 1.6889 7.88004 1.6525C7.18036 0.853724 6.27262 0.398682 5.00025 0.398682C2.55542 0.398682 0.882568 1.98206 0.882568 4.9989C0.882568 5.05766 0.883791 5.11656 0.886227 5.17559C0.964595 7.07474 2.2531 9.00992 4.33499 10.9305C5.06737 11.6061 5.84982 12.2325 6.63222 12.7952C6.90618 12.9923 7.1609 13.1675 7.39006 13.3191C7.47081 13.3725 7.54278 13.4192 7.60517 13.459L7.66347 13.4959L7.68578 13.5098L8.00108 13.7046L8.31638 13.5098C8.37243 13.4752 8.47311 13.411 8.6121 13.3191C8.84127 13.1675 9.09599 12.9923 9.36995 12.7952C10.1523 12.2325 10.9348 11.6061 11.6672 10.9305C13.7491 9.00992 15.0376 7.07474 15.1159 5.17559C15.1184 5.11656 15.1196 5.05766 15.1196 4.9989C15.1196 1.98206 13.4467 0.398682 11.0019 0.398682ZM10.8535 10.0484C10.16 10.6882 9.41463 11.2849 8.66925 11.8211C8.42872 11.9941 8.20432 12.149 8.00108 12.2844C7.79784 12.149 7.57344 11.9941 7.33291 11.8211C6.58753 11.2849 5.8422 10.6882 5.14866 10.0484C3.28055 8.32513 2.14696 6.62261 2.08521 5.12612C2.08345 5.08351 2.08257 5.04111 2.08257 4.9989C2.08257 2.66 3.20386 1.59868 5.00025 1.59868C5.9084 1.59868 6.49537 1.89293 6.97736 2.44318C7.13263 2.62045 7.68637 3.40068 7.51851 3.1735L8.00108 3.82662L8.48365 3.1735C8.31579 3.40068 8.86953 2.62045 9.0248 2.44318C9.50679 1.89293 10.0938 1.59868 11.0019 1.59868C12.7983 1.59868 13.9196 2.66 13.9196 4.9989C13.9196 5.04111 13.9187 5.08351 13.917 5.12612C13.8552 6.62261 12.7216 8.32513 10.8535 10.0484Z" fill="#00B7C4" />
                  </svg> :
                  <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd" clipRule="evenodd" d="M11.0019 0.398682C9.72954 0.398682 8.8218 0.853724 8.12213 1.6525C8.09025 1.6889 8.04831 1.7408 8.00108 1.80134C7.95385 1.7408 7.91191 1.6889 7.88004 1.6525C7.18036 0.853724 6.27262 0.398682 5.00025 0.398682C2.55542 0.398682 0.882568 1.98206 0.882568 4.9989C0.882568 5.05766 0.883791 5.11656 0.886227 5.17559C0.964595 7.07474 2.2531 9.00992 4.33499 10.9305C5.06737 11.6061 5.84982 12.2325 6.63222 12.7952C6.90618 12.9923 7.1609 13.1675 7.39006 13.3191C7.47081 13.3725 7.54278 13.4192 7.60517 13.459L7.66347 13.4959L7.68578 13.5098L8.00108 13.7046L8.31638 13.5098C8.37243 13.4752 8.47311 13.411 8.6121 13.3191C8.84127 13.1675 9.09599 12.9923 9.36995 12.7952C10.1523 12.2325 10.9348 11.6061 11.6672 10.9305C13.7491 9.00992 15.0376 7.07474 15.1159 5.17559C15.1184 5.11656 15.1196 5.05766 15.1196 4.9989C15.1196 1.98206 13.4467 0.398682 11.0019 0.398682ZM10.8535 10.0484C10.16 10.6882 9.41463 11.2849 8.66925 11.8211C8.42872 11.9941 8.20432 12.149 8.00108 12.2844C7.79784 12.149 7.57344 11.9941 7.33291 11.8211C6.58753 11.2849 5.8422 10.6882 5.14866 10.0484C3.28055 8.32513 2.14696 6.62261 2.08521 5.12612C2.08345 5.08351 2.08257 5.04111 2.08257 4.9989C2.08257 2.66 3.20386 1.59868 5.00025 1.59868C5.9084 1.59868 6.49537 1.89293 6.97736 2.44318C7.13263 2.62045 7.68637 3.40068 7.51851 3.1735L8.00108 3.82662L8.48365 3.1735C8.31579 3.40068 8.86953 2.62045 9.0248 2.44318C9.50679 1.89293 10.0938 1.59868 11.0019 1.59868C12.7983 1.59868 13.9196 2.66 13.9196 4.9989C13.9196 5.04111 13.9187 5.08351 13.917 5.12612C13.8552 6.62261 12.7216 8.32513 10.8535 10.0484Z" fill="white" fillOpacity="0.9" />
                  </svg>
                }
              </span>
            </div>
          </div>
          <Biography item={item} />
        </div>
        <div className={styles.right}>
          <div className={cn(styles.block, styles.info)}>
            <p className={styles.name}>{item.nft.name || 'NFT'}</p>
            <div className={styles.items}>
              <div className={styles.item}><span>Videojuego</span><b>{item.nft.videogame || 'N/A'}</b></div>
              <div className={styles.item}><span>Creados | Disponibles</span><b>{item.nft.supply} | {item.available}</b></div>
              <div className={styles.item}><span>ID</span><b>{item.tokenId || 0}</b></div>
              <div className={styles.item}><span>Creador</span><Link to={`/user/${item.nft.creator?._id}`}><b>{item.nft.creator?.username}</b></Link></div>
              <div className={styles.item}><span>Owners totales</span><b>1</b></div>
              <div className={styles.item}><span>Owner actual</span><Link to={`/user/${item.owner?._id}`}><b>{item.owner?.username}</b></Link></div>
              <div className={styles.item}><span>Fecha de creación</span><b>{moment(item.nft.createdAt).format('DD/MM/YYYY')}</b></div>
              <div className={styles.item}><span>Dirección</span><a href={`https://ropsten.etherscan.io/token/${item.nft.address}`} rel="noopener noreferrer" target="_blank"><b>{item.nft.address}</b></a></div>
            </div>
            <Biography item={item} block={false} />
          </div>
          {/* eslint-disable-next-line */}
          {user.id === item.owner?._id && item.status === 'owned' ? <Offer item={item} /> : ''}
          {item.status === 'onsale' && user.id !== item.owner?._id ? <Buy item={item} /> : ''}
          {item.status === 'onauction' && user.id !== item.owner?._id ? <Bid item={item} /> : ''}
          {item.status === 'ongame' ? <Play item={item} /> : ''}
          <div
            className={cn(styles.block, styles.collapsed)}
            role="button"
            tabIndex={0}
            onKeyDown={()=>{}}
            onClick={() => history.push(`/nft/${id}/comment`)}
          >
            <h5>Califica y comenta sobre este NFT</h5>
            <Chevron />
          </div>
          <div className={cn(styles.block, styles.collapsed)}>
            <h5>Otros de esta colección</h5>
            <Chevron />
          </div>
        </div>
      </div>
      {showShareModal && <Share id={id} onCancel={() => setShowShareModal( false )} /> }
    </section>
  );
};

export default connect(({ user }) => ({ user }))( withRouter(View) );
