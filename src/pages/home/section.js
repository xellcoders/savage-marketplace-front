import React, { useEffect, useState } from "react";
import Loader from "components/Loader";
import Card from "components/Card";
import styles from "./home.module.sass";

export default function({ title, load, onChange = () => {}, filters }){

  const [loading, setLoading] = useState(true);
  const [items, setItems] = useState([]);

  const container = React.createRef();

  useEffect(() => {
    load( filters ? Buffer.from(JSON.stringify(filters)).toString('base64') : null ).then( results => {
      setLoading( false );
      setItems( results );
    } ).catch( () => {
      setLoading( false );
    } );
  // eslint-disable-next-line
  }, [filters]);

  const scrollLeft = () => {
    const div = container.current;
    div.scrollLeft -= 100;
  }

  const scrollRight = () => {
    const div = container.current;
    div.scrollLeft += 100;
  }

  return (
    <section className={styles.section}>
      <h3>{title}</h3>
      {loading ? <Loader className={styles.loader} /> :
      <>
        {items.length > 0 &&
          <div className={styles.backward} role="button" tabIndex={0} onKeyDown={() => {}} onClick={scrollLeft}>
            <svg width="34" height="68" viewBox="0 0 34 68" fill="none" xmlns="http://www.w3.org/2000/svg">
              <g filter="url(#filter0_d_2421_103396)">
                <path d="M0 1C17.6731 1 32 15.3269 32 33C32 50.6731 17.6731 65 0 65V1Z" fill="#1D1D29" />
                <path d="M17.0656 25.9325L18.1262 26.9931L12.1225 32.9969L18.1325 39.0069L17.0718 40.0675L10.0012 32.9969L17.0656 25.9325Z" fill="#D1D5DA" />
              </g>
              <defs>
                <filter id="filter0_d_2421_103396" x="-2" y="0" width="36" height="68" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
                  <feFlood floodOpacity="0" result="BackgroundImageFix" />
                  <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
                  <feOffset dy="1" />
                  <feGaussianBlur stdDeviation="1" />
                  <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.12 0" />
                  <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_2421_103396" />
                  <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_2421_103396" result="shape" />
                </filter>
              </defs>
            </svg>
          </div>
        }
        <div className={styles.items} ref={container}>
          {/* eslint-disable-next-line no-underscore-dangle */}
          {items.map( item => <Card key={item._id} item={item} onChange={onChange} /> )}
        </div>
        {items.length > 0 ?
          <div className={styles.forward} role="button" tabIndex={0} onKeyDown={()=>{}} onClick={scrollRight}>
            <svg width="34" height="68" viewBox="0 0 34 68" fill="none" xmlns="http://www.w3.org/2000/svg">
              <g filter="url(#filter0_d_2421_103393)">
                <path d="M2 33C2 15.3269 16.3269 1 34 1V65C16.3269 65 2 50.6731 2 33Z" fill="#1D1D29" />
                <path d="M13.8025 26.9931L19.8063 32.9969L13.7963 39.0069L14.8569 40.0675L21.9276 32.9969L14.8632 25.9325L13.8025 26.9931Z" fill="#D1D5DA" />
              </g>
              <defs>
                <filter id="filter0_d_2421_103393" x="0" y="0" width="36" height="68" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
                  <feFlood floodOpacity="0" result="BackgroundImageFix" />
                  <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
                  <feOffset dy="1" />
                  <feGaussianBlur stdDeviation="1" />
                  <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.12 0" />
                  <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_2421_103393" />
                  <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_2421_103393" result="shape" />
                </filter>
              </defs>
            </svg>
          </div> :
          <div style={{ textAlign: 'center' }}>{filters ? 'Disculpa, no se han encontrado resultados para tu búsqueda' : ' '}</div>
        }
      </>
      }
    </section>
  )
}