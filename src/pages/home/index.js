import React, { useState } from "react";
import { connect } from "react-redux";
import cn from "classnames";

import Image from "components/Image";
import TagButton from "components/TagButton";
import IconSearch from "components/Icons/IconSearch";
import IconFilter from "components/Icons/IconFilter";
import { getActiveItems, getLikedItems, getOnAuction, getOnSale, getPreferredItems } from "services/api.item.service";
import TextInput from "components/TextInput";
import Account from "components/Account";
import Filters from "components/Filters";

import styles from "./home.module.sass";
import Section from "./section";
import { history } from "../../index"

function Home({ user }) {
  const [isShowAccount, setShowAccount] = useState(false);
  const [isVisibleFilters, setIsVisibleFilters] = useState(false);
  const [loadLike, setLoadLike] = useState( () => getLikedItems );
  const [filters, setFilters] = useState();
  const [showSearch, setShowSearch] = useState( false );

  const tags = [{
    option: "showAll",
    text: "Ver todo"
  }, ...(user.authorized ? [{
    option: "preferences",
    text: "Tus preferencias"
  }, {
    option: "likes",
    text: "Tus me gusta"
  }] : []), {
    option: "new",
    text: "Lo más nuevo"
  }, {
    option: "onsale",
    text: "En venta"
  }, {
    option: "onauction",
    text: "En subasta"
  }];

  const [selectedTag, setSelectedTag] = useState("showAll");

  const updateLikedItems = () => setLoadLike(() => () => getLikedItems());

  return (
    <>
      {isShowAccount && <Account onClose={() => setShowAccount(false)} />}
      <div className={styles.wrapper}>
        <div className={styles.header}>
          <div className={cn(styles.logo, { [styles.hidden]: showSearch })}>
            <Image src="/images/logo.svg" srcDark="/images/logo.svg" alt="Neon Savage" />
            <p className={styles.title}>Marketplace</p>
          </div>
          <nav className={cn(styles.tags)}>
            {tags.map(tag => (
              <TagButton
                key={tag.text}
                className={styles.tag}
                text={tag.text}
                isActivate={selectedTag === tag.option}
                onClick={() => setSelectedTag(tag.option)}
              />
            ))}
          </nav>
          <div className={styles.buttons}>
            <div className={styles.search}>
              {showSearch &&
                <TextInput
                  Icon={<IconSearch />}
                  name="hash"
                  type="text"
                  placeholder="Buscar en marketplace"
                  required
                  onChange={e => {
                    if( filters ) {
                      setFilters( {
                        rarity: filters.rarity,
                        // eslint-disable-next-line no-restricted-globals
                        minPrice: isNaN( filters.minPrice ) ? 0 : filters.minPrice,
                        // eslint-disable-next-line no-restricted-globals
                        maxPrice: isNaN( filters.maxPrice ) ? Number.MAX_VALUE : filters.maxPrice,
                        game: filters.game || 'all',
                        creator: filters.creator || 'all',
                        collection: filters.collection || 'all',
                        tags: filters.tags || [],
                        rating: filters.rating || 'all',
                        search: e.target.value
                      } );
                    } else {
                      setFilters( {
                        rarity: null,
                        // eslint-disable-next-line no-restricted-globals
                        minPrice: 0,
                        // eslint-disable-next-line no-restricted-globals
                        maxPrice: Number.MAX_VALUE,
                        game: 'all',
                        creator: 'all',
                        collection: 'all',
                        tags: [],
                        rating: 'all',
                        search: e.target.value
                      } );
                    }
                  }}
                />
              }
              <button type="button" className={styles.button} onClick={() => setShowSearch( !showSearch )}>
                {showSearch ? 'Cancelar' : <IconSearch />}
              </button>
            </div>
            {!showSearch &&
              <div className={styles.filters}>
                {/* eslint-disable-next-line */}
                <div onClick={() => setIsVisibleFilters(true)}>
                  <IconFilter />
                </div>
                <Filters
                  close={() => setIsVisibleFilters(false)}
                  handleFilter={values => setFilters( values )}
                  isVisible={isVisibleFilters}
                />
              </div>
            }
            {!showSearch ?
              /* eslint-disable-next-line */
              <div onClick={() => history.push( "/dashboard" )} style={{ cursor: 'pointer' }}>
                <svg width="28" height="21" viewBox="0 0 28 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M27.125 18.7345V20.1408H0.875V18.7345H27.125ZM19.5027 0.942383C20.8028 0.942383 21.8567 1.99628 21.8567 3.29632C21.8567 3.69312 21.7585 4.06699 21.5851 4.39495L23.8733 6.73472C24.2092 6.55011 24.5951 6.4451 25.0054 6.4451C26.3055 6.4451 27.3594 7.499 27.3594 8.79904C27.3594 10.0991 26.3055 11.153 25.0054 11.153C23.7054 11.153 22.6515 10.0991 22.6515 8.79904C22.6515 8.42222 22.74 8.06607 22.8975 7.75028L20.5864 5.38655C20.262 5.55505 19.8935 5.65026 19.5027 5.65026C19.1093 5.65026 18.7384 5.55376 18.4125 5.38312L10.5631 13.1724C10.7468 13.5077 10.8512 13.8925 10.8512 14.3018C10.8512 15.6018 9.79733 16.6557 8.49728 16.6557C7.19724 16.6557 6.14334 15.6018 6.14334 14.3018C6.14334 13.908 6.24003 13.5368 6.41097 13.2106L4.08196 10.8873C3.7567 11.057 3.38684 11.153 2.99457 11.153C1.69452 11.153 0.640625 10.0991 0.640625 8.79904C0.640625 7.499 1.69452 6.4451 2.99457 6.4451C4.29461 6.4451 5.34851 7.499 5.34851 8.79904C5.34851 9.19471 5.25089 9.56757 5.07842 9.89487L7.4052 12.2159C7.73159 12.0447 8.10312 11.9478 8.49728 11.9478C8.87562 11.9478 9.2331 12.0371 9.54983 12.1957L17.4169 4.38841C17.2457 4.06201 17.1488 3.69048 17.1488 3.29632C17.1488 1.99628 18.2027 0.942383 19.5027 0.942383ZM8.49728 13.3541C7.97389 13.3541 7.54959 13.7784 7.54959 14.3018C7.54959 14.8252 7.97389 15.2494 8.49728 15.2494C9.02068 15.2494 9.44497 14.8252 9.44497 14.3018C9.44497 13.7784 9.02068 13.3541 8.49728 13.3541ZM2.99457 7.85135C2.47117 7.85135 2.04688 8.27565 2.04688 8.79904C2.04688 9.32244 2.47117 9.74673 2.99457 9.74673C3.51796 9.74673 3.94226 9.32244 3.94226 8.79904C3.94226 8.27565 3.51796 7.85135 2.99457 7.85135ZM25.0054 7.85135C24.482 7.85135 24.0577 8.27565 24.0577 8.79904C24.0577 9.32244 24.482 9.74673 25.0054 9.74673C25.5288 9.74673 25.9531 9.32244 25.9531 8.79904C25.9531 8.27565 25.5288 7.85135 25.0054 7.85135ZM19.5027 2.34863C18.9793 2.34863 18.555 2.77293 18.555 3.29632C18.555 3.81972 18.9793 4.24401 19.5027 4.24401C20.0261 4.24401 20.4504 3.81972 20.4504 3.29632C20.4504 2.77293 20.0261 2.34863 19.5027 2.34863Z" fill="white" />
                </svg>
              </div> : ''
            }
            {user.authorized ?
              <>
                <button type="button" className={styles.signup} onClick={() => setShowAccount( true )}>
                  Mi cuenta
                </button>
              </> :
              <button type="button" className={styles.signup} onClick={() => history.push('/signup')}>
                Registrarse
              </button>
            }
          </div>
        </div>
        <nav className={cn(styles.tags)}>
          {tags.map(tag => (
            <TagButton
              key={tag.text}
              className={styles.tag}
              text={tag.text}
              isActivate={selectedTag === tag.option}
              onClick={() => setSelectedTag(tag.option)}
            />
          ))}
        </nav>
        <div className={styles.main}>
          {user.authorized &&
          <>
            {(selectedTag === 'showAll' || selectedTag === 'preferences') && <Section title="Tus preferencias" load={getPreferredItems} filters={filters} onChange={updateLikedItems} />}
            {(selectedTag === 'showAll' || selectedTag === 'likes') && <Section title="Tus me gusta" load={loadLike} filters={filters} onChange={updateLikedItems} />}
          </>
          }
          {(selectedTag === 'showAll' || selectedTag === 'new') && <Section title="Lo más nuevo" load={getActiveItems} filters={filters} onChange={updateLikedItems} />}
          {(selectedTag === 'showAll' || selectedTag === 'onsale') && <Section title="En venta" load={getOnSale} filters={filters} onChange={updateLikedItems} />}
          {(selectedTag === 'showAll' || selectedTag === 'onauction') && <Section title="En subasta" load={getOnAuction} filters={filters} onChange={updateLikedItems} />}
        </div>

        {/* <div className={styles.next_previous_buttons_container}>
          <button type="button" className={cn("button-stroke", styles.control_button_previous)} disabled={loading}>
            <span>Anterior</span>
          </button>
          <button type="button" className={cn("button-stroke", styles.control_button_next)} disabled={loading}>
            <span>Siguiente</span>
          </button>
        </div> */}

      </div>
    </>
  );
}

export default connect(({ user }) => ({ user }))( Home );
