import React, { useState, useEffect } from "react";
import "rc-slider/assets/index.css";
import List from "components/List";
import Loading from "components/Loading";
import { getOnGame } from "services/api.item.service";
import styles from "./dashboard.module.sass";

export default function () {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const init = () => {
    setLoading(false);
    getOnGame().then( results => {
      setItems( results );
      setLoading( false );
    } ).catch( () => {
      setLoading( false );
    } )
  }

  useEffect(() => {
    init();
    // eslint-disable-next-line
  }, []);

  return (
    <>
      {loading && <Loading status="Cargando..." /> }
      <div className={styles.title}>NFTs en Marketplace</div>
      <List items={items} prefixDate="En juego desde: " suffixTitle="en juego" />
    </>
  );
};