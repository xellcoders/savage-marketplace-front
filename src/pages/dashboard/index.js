import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Modal, Button, Layout } from "antd";
import "rc-slider/assets/index.css";
import cn from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleRight } from "@fortawesome/free-solid-svg-icons";
import Balance from "components/Balance";
import EtherPrice from "components/EtherPrice";
import Movements from "components/Movements";
import Loading from "components/Loading";
import { getCurrentUserStats } from "services/api.stats.service";
import { requestDeveloper } from "services/api.user.service";
import styles from "./dashboard.module.sass";
import Summary from "./summary";
import Onsale from "./onsale";
import Onauction from "./onauction";
import Ongame from "./ongame";
import Bought from "./bought";
import Sold from "./sold";
import Collection from "./collection";
import { history } from "../../index";

const { Sider, Content } = Layout;

function Dashboard({ user, match: { params: { section } } }) {
  const [loading, setLoading] = useState(true);
  const [userStats, setUserStats] = useState({});
  const [view, setView] = useState("metrics");

  const childrens = {
    "": <Summary section={view} />,
    onsale: <Onsale />,
    onauction: <Onauction />,
    ongame: <Ongame />,
    bought: <Bought />,
    sold: <Sold />,
    collection: <Collection />
  };

  const init = () => {
    getCurrentUserStats().then(stats => {
      setUserStats(stats || {});
      setLoading(false);
    });
  };

  useEffect(() => {
    init();
    // eslint-disable-next-line
  }, []);

  const MenuItem = ({ image, title, subtitle, path, style = {} }) => (
    <Button
      className={cn(styles.button, {
        [styles.active]: `/dashboard/${section || ""}` === path
      })}
      onClick={() => {
        if (path) history.push(path);
      }}
    >
      {image && <img src={image} alt={title} style={style} />}
      <span>
        {title} <small>{subtitle}</small>
      </span>
      <FontAwesomeIcon icon={faAngleRight} color="#01B7C3" />
    </Button>
  );

  if (loading) return <Loading status="Cargando..." />;

  return (
    <section className={styles.container}>
      <div className={cn(styles.user, styles.header)}>
        {/* eslint-disable-next-line jsx-a11y/no-noninteractive-element-to-interactive-role */}
        <figure role="button" tabIndex={0} onKeyDown={()=>{}} onClick={() => history.push('/dashboard')}>
          <img src={user.avatar || "/images/icons/avatar.png"} alt="Usuario" />
        </figure>
        <span>{user.username}</span>
        <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fillRule="evenodd" clipRule="evenodd" d="M17.3954 0.194824V6.43545H16.8V15.7998H10.549L10.549 9.34148H7.50064V15.7998H1.19999V6.43545H0.604553V0.194824H17.3954ZM12.0248 1.39482H9.6248V5.23545H12.0248V1.39482ZM13.2248 5.23545V1.39482H16.1954V5.23545H13.2248ZM2.39999 6.43545V14.5998H6.30064V8.14148H11.749V14.5998H15.6V6.43545H2.39999ZM5.97518 1.39482H8.4248V5.23545H5.97518V1.39482ZM4.77518 1.39482H1.80455V5.23545H4.77518V1.39482Z" fill="white" />
        </svg>
      </div>
      <div className={styles.options}>
        <Button className={cn( { [styles.active]: view === 'metrics' } )} onClick={() => setView('metrics')}>Metricas</Button>
        <Button className={cn( { [styles.active]: view === 'featured' } )} onClick={() => setView('featured')}>Destacados</Button>
      </div>
      <div className={cn( styles.total, { [styles.hide]: view !== 'metrics' })}>
        <span>{userStats.total || 0} NFTs</span> Activos
      </div>
      <div className={cn(styles.menu, { [styles.hide]: view !== 'metrics' })}>
        <MenuItem image="/images/icons/on_sale.svg" title={`${userStats.onSale || 0} NFTs`} path="/dashboard/onsale" subtitle="A la venta" />
        <MenuItem image="/images/icons/on_auction.svg" title={`${userStats.onAuction || 0} NFTs`} path="/dashboard/onauction" subtitle="En subasta" />
        <MenuItem image="/images/icons/on_game.svg" title={`${userStats.onGame || 0} NFTs`} path="/dashboard/ongame" subtitle="En juego" />
        <MenuItem image="/images/icons/bought.svg" title={`${userStats.bought || 0} NFTs`} path="/dashboard/bought" subtitle="Comprados" />
        <MenuItem image="/images/icons/sold.svg" title={`${userStats.sold || 0} NFTs`} path="/dashboard/sold" subtitle="Vendidos" />
        <MenuItem image="/images/icons/collection.svg" title={`${userStats.total || 4} NFTs`} path="/dashboard/collection" subtitle="Colección" />
      </div>
      <div className={cn(styles.create, { [styles.hide]: view !== 'metrics' })}>
        <MenuItem image="/images/icons/create_nft.svg" title="Crear NFT" path="/nft/create" />
      </div>
      <Layout>
        <Sider className={styles.left} width={288}>
          <div className={styles.user}>
            {/* eslint-disable-next-line jsx-a11y/no-noninteractive-element-to-interactive-role */}
            <figure role="button" tabIndex={0} onKeyDown={()=>{}} onClick={() => history.push('/dashboard')}>
              <img src={user.avatar || "/images/icons/avatar.png"} alt="Usuario" />
            </figure>
            <span>{user.username}</span>
          </div>
          {user.role !== 'developer' &&
            <Button
              className={styles.developer}
              onClick={() => {
                requestDeveloper( user.email ).then( () => {
                  Modal.success( { title: <div style={{ color: '#FFF' }}>Se ha enviado correctamente el correo</div> } );
                } );
              }}
            >
              Soy desarrollador
            </Button>
          }
          <div className={styles.h1}>
            <span>{userStats.total || 0} NFTs</span> Activos
          </div>
          {user.role === 'admin' &&
            <MenuItem
              title="Super Admin."
              path="/admin"
              style={{ marginTop: -8 }}
            />
          }
          <div className={styles.divider}> </div>
          <MenuItem image="/images/icons/create_nft.svg" title="Crear NFT" path="/nft/create" style={{ marginTop: -8 }} />
          <div className={styles.divider}> </div>
          <MenuItem image="/images/icons/on_sale.svg" title={`${userStats.onSale || 0} NFTs`} path="/dashboard/onsale" subtitle="A la venta" />
          <MenuItem image="/images/icons/on_auction.svg" title={`${userStats.onAuction || 0} NFTs`} path="/dashboard/onauction" subtitle="En subasta" />
          <MenuItem image="/images/icons/on_game.svg" title={`${userStats.onGame || 0} NFTs`} path="/dashboard/ongame" subtitle="En juego" />
          <div className={styles.divider}> </div>
          <MenuItem image="/images/icons/bought.svg" title={`${userStats.bought || 0} NFTs`} path="/dashboard/bought" subtitle="Comprados" />
          <MenuItem image="/images/icons/sold.svg" title={`${userStats.sold || 0} NFTs`} path="/dashboard/sold" subtitle="Vendidos" />
          <MenuItem image="/images/icons/collection.svg" title={`${userStats.total || 0} NFTs`} path="/dashboard/collection" subtitle="Colección" />
          <div className={styles.divider}> </div>
          <MenuItem title="Ir a marketplace" path="/" />
        </Sider>
        <Content className={styles.main}>{childrens[section || ""]}</Content>
        <Sider className={styles.right} width={380}>
          <div className={styles.title}>Tus cuentas</div>
          <Balance color="#5806AB" title="Total disponible" currency="eth" network="ethereum" />
          <Balance color="#06BBC8" title={<>Savage <b>NSC</b></>} currency="nsc" network="savage" image="/images/icons/nsc.png" />
          <Balance color="#06BBC8" title={<>Savage <b>ETH</b></>} currency="eth" network="savage" image="/images/icons/eth.png" />
          <Balance color="#EA33EA" title={<>Ethereum <b>ETH</b></>} currency="eth" network="ethereum" image="/images/icons/eth.png" />
          <EtherPrice />
          <Movements />
        </Sider>
      </Layout>
    </section>
  );
}

export default connect(({ user }) => ({ user }))(withRouter(Dashboard));
