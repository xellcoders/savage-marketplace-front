import React, { useState, useEffect } from "react";
import "rc-slider/assets/index.css";
import { getOwned } from "services/api.item.service";
import Loading from "components/Loading";
import Grid from "components/Grid";

export default function () {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const load = () => {
    setLoading(false);
    getOwned().then( results => {
      setItems( results );
      setLoading( false );
    } ).catch( () => {
      setLoading( false );
    } )
  }

  useEffect(() => {
    load();
    // eslint-disable-next-line
  }, []);

  return (
    <>
      {loading && <Loading status="Cargando..." /> }
      <Grid items={items} suffixTitle="en tu colección" />
    </>
  );
};