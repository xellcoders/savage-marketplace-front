import React, { useState, useEffect } from "react";
import "rc-slider/assets/index.css";
import cn from "classnames";
import Chart from "components/Chart";
import Carousel from "components/Carousel";
import { getItemsStats } from "services/api.stats.service";
import { getTopItems } from "services/api.item.service";
import { getTopCreators, getTopUsers } from "services/api.user.service";
import styles from "./dashboard.module.sass";
import { getItemImage } from "../../util/files";

export default function ({ section }) {
  const [loadingTopItems, setLoadingTopItems] = useState(true);
  const [topItems, setTopItems] = useState([]);
  const [loadingTopCreators, setLoadingTopCreators] = useState(true);
  const [topCreators, setTopCreators] = useState([]);
  const [loadingTopUsers, setLoadingTopUsers] = useState(true);
  const [topUsers, setTopUsers] = useState([]);
  const [itemsStats, setItemsStats] = useState({ marketplace: [], moving: [] });
  const [loadingItemsStats, setLoadingItemsStats] = useState(true);

  const init = () => {
    getItemsStats().then(stats => {
      setItemsStats( {
        marketplace: [{
          label: 'A la venta',
          value: stats.onSale,
          color: '#E92EE9'
        }, {
          label: 'En subasta',
          value: stats.onAuction,
          color: '#01B7C3'
        }, {
          label: 'En juego',
          value: stats.onGame,
          color: '#AC5DD9'
        }, {
          label: 'Inactivos',
          value: stats.inactive,
          color: '#d9b25d'
        }, {
          label: 'HODL',
          value: stats.hodl,
          color: '#6ed95d'
        }],
        moving: [{
          label: 'Comprados',
          value: stats.bought,
          color: '#E92EE9'
        }, {
          label: 'Vendidos',
          value: stats.sold,
          color: '#01B7C3'
        }, {
          label: 'Subasta',
          value: stats.auction,
          color: '#AC5DD9'
        }],
        hodl: [{
          label: 'En Juego',
          value: stats.onGame,
          color: '#E92EE9'
        }, {
          label: 'HODL',
          value: stats.found || 0,
          color: '#01B7C3'
        }]
      } );
      setLoadingItemsStats(false);
    }).catch(() => {
      setLoadingItemsStats(false);
    });
    getTopItems().then( items => {
      setTopItems( items.map( item => ({
        // eslint-disable-next-line no-underscore-dangle
        id: item._id,
        name: item.nft.name,
        image: getItemImage(item.nft)
      }) ) );
      setLoadingTopItems( false );
    } ).catch(() => {
      setLoadingTopItems( false );
    });
    getTopCreators().then( items => {
      setTopCreators( items.map( item => ({
        // eslint-disable-next-line no-underscore-dangle
        id: item._id,
        name: item.name || item.email.substring( 0, item.email.indexOf('@') ),
        image: item.avatar || '/images/icons/avatar.png'
      }) ) );
      setLoadingTopCreators( false );
    } ).catch(() => {
      setLoadingTopCreators( false );
    });
    getTopUsers().then( items => {
      setTopUsers( items.map( item => ({
        // eslint-disable-next-line no-underscore-dangle
        id: item._id,
        name: item.name || item.email.substring( 0, item.email.indexOf('@') ),
        image: item.avatar || '/images/icons/avatar.png'
      }) ) );
      setLoadingTopUsers( false );
    } ).catch(() => {
      setLoadingTopUsers( false );
    });
  }

  useEffect(() => {
    init();
    // eslint-disable-next-line
  }, []);

  return (
    <>
      <div className={cn( { [styles.hide]: section !== 'metrics' } )}>
        <div className={styles.title}>Métricas</div>
        <div className={styles.charts}>
          <Chart title="NFTs en Marketplace" items={itemsStats.marketplace} loading={loadingItemsStats} />
          <Chart title="NFTs en Movimiento" items={itemsStats.moving} loading={loadingItemsStats} />
        </div>
        <div className={styles.charts} style={{ marginTop: 20 }}>
          <Chart title="NFTs en juegos: Detalle de NFTs en los videojuegos" items={itemsStats.hodl} loading={loadingItemsStats} />
          <div> </div>
        </div>
      </div>
      <div className={cn( styles.featured, { [styles.hide]: section !== 'featured' } )}>
        <div className={styles.title}>Destacados</div>
        <Carousel title="Top 10 NFTs más vendidos" link="/top/items" items={topItems} path="/nft/" loading={loadingTopItems} />
        <Carousel title="Top 10 creadores" link="/top/creators" items={topCreators} path="/user/" showNumbers loading={loadingTopCreators} />
        <Carousel title="Top 10 usuarios" link="/top/users" items={topUsers} path="/user/" showNumbers loading={loadingTopUsers} />
      </div>
    </>
  );
};