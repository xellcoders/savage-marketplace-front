import React, { useState, useEffect } from "react";
import cn from "classnames";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Dropdown, Button, Layout, Table, Menu, notification, Col, Form, Input, Row, Select } from "antd";
import "rc-slider/assets/index.css";
import Loader from "components/Loader";
import { getUsers, updateRole, updateStatus } from "services/api.user.service";
import { getUsersStats } from "services/api.stats.service";
import Chevron from "components/Chevron";
import ChevronDown from "components/Dropdown/chevron";
import styles from "./admin.module.sass";
import { history } from "../../index";

const { Content } = Layout;

const roles = {
  consumer: 'Gamer',
  developer: 'Desarrollador',
  creator: 'Creador',
  admin: 'Admin'
};

const statuses = {
  active: 'Activo',
  inactive: 'Inactivo'
};

function Admin({ user }) {
  const [loadingStats, setLoadingStats] = useState(true);
  const [stats, setStats] = useState([]);
  const [loadingUsers, setLoadingUsers] = useState(true);
  const [users, setUsers] = useState([]);
  const [filters, setFilters] = useState({
    role: "consumer,developer,creator,admin",
    status: "active,inactive"
  });

  const loadStats = () => {
    getUsersStats().then( results => {
      setLoadingStats( false );
      setStats( results )
    } ).catch( () => {
      setLoadingStats( false );
    })
  }

  const loadUsers = () => {
    getUsers( filters ).then( results => {
      setLoadingUsers( false );
      setUsers( results )
    } ).catch( () => {
      setLoadingUsers( false );
    })
  }

  useEffect(() => {
    loadStats();
  }, []);

  useEffect(() => {
    loadUsers();
    // eslint-disable-next-line
  }, [filters]);

  const changeRole = (id, role) => {
    updateRole( id, role ).then( () => {
      notification.success({
        message: 'Tipo de usuario cambiado',
        description: `Se ha cambiado el tipo de usuario correctamente`,
      });
      loadUsers();
      loadStats();
    } ).catch( error => {
      notification.error({
        message: 'Error al cambiar el tipo de usuario',
        description: `${error.message} (code: ${error.code})`,
      });
    } )
  }

  const changeStatus = (id, status) => {
    updateStatus( id, status ).then( () => {
      notification.success({
        message: status === 'active' ? 'Usuario activado' : 'Usuario inactivado',
        description:  status === 'active' ? 'Se ha activado al usuario correctamente' : 'Se ha inactivado al usuario correctamente',
      });
      loadUsers();
      loadStats();
    } ).catch( error => {
      notification.error({
        message: `Error al ${status === 'active' ? 'activar' : 'inactivar'} al usuario`,
        description: `${error.message} (code: ${error.code})`,
      });
    } )
  }

  const headers = [
    {
      title: '',
      dataIndex: 'avatar',
      key: 'avatar',
      width: '30px',
      render: avatar => <figure><img src={avatar} alt="" /></figure>
    },
    {
      title: 'Nombre del usuario',
      dataIndex: 'username',
      key: 'username',
    },
    {
      title: 'Correo electrónico',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Tipo de usuario',
      dataIndex: 'role',
      key: 'role',
      render: (role, item) => {
        return (
          <Dropdown.Button
            overlay={
              <Menu className={styles.role_menu}>
                <Menu.Item onClick={() => item.role !== 'consumer' && changeRole( item.id, 'consumer' )}>Gamer</Menu.Item>
                <Menu.Item onClick={() => item.role !== 'developer' && changeRole( item.id, 'developer' )}>Desarrollador</Menu.Item>
                <Menu.Item onClick={() => item.role !== 'admin' && changeRole( item.id, 'admin' )}>Admin</Menu.Item>
              </Menu>
            }
            icon={
              <svg width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M8.35229 0.704498L5.00004 4.05675L1.64779 0.704498L0.852295 1.49999L5.00004 5.64774L9.14779 1.49999L8.35229 0.704498Z" fill="#00B7C4" />
              </svg>
            }
            className={cn(styles.role)}
          >
            {roles[role]}
          </Dropdown.Button>
        )
      }
    },
    {
      title: 'No. de NFTs',
      dataIndex: 'total',
      key: 'total',
      render: total => total || 0
    },
    {
      title: 'Comprados',
      dataIndex: 'bought',
      key: 'bought',
      render: bought => bought || 0
    },
    {
      title: 'Vendidos',
      dataIndex: 'sold',
      key: 'sold',
      render: sold => sold || 0
    },
    {
      title: 'Estatus',
      dataIndex: 'status',
      key: 'status',
      render: status => (
        <span
          className={cn( styles.status, { [styles.active]: status === 'active', [styles.inactive]: status === 'inactive' } )}
        >
          {statuses[status]}
        </span>
      )
    },
    {
      title: 'Más',
      dataIndex: 'status',
      key: 'more',
      render: (status, item) => {
        return (
          <Dropdown.Button
            overlay={
              <Menu className={styles.status_menu}>
                <Menu.Item onClick={() => item.status !== 'active' && changeStatus( item.id, 'active' )}>Activo</Menu.Item>
                <Menu.Item onClick={() => item.status !== 'inactive' && changeStatus( item.id, 'inactive' )}>Inactivo</Menu.Item>
              </Menu>
            }
            icon={
              <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 10C0 4.47715 4.47715 0 10 0C15.5228 0 20 4.47715 20 10C20 15.5228 15.5228 20 10 20C4.47715 20 0 15.5228 0 10Z" fill="#28293D" />
                <path d="M9.99479 6.4004C10.6575 6.4004 11.1947 5.8632 11.1947 5.20054C11.1947 4.53787 10.6575 4.00067 9.99479 4.00067C9.33212 4.00067 8.79492 4.53787 8.79492 5.20054C8.79492 5.8632 9.33212 6.4004 9.99479 6.4004Z" fill="#00B7C4" />
                <path d="M11.1947 10C11.1947 10.6627 10.6575 11.1999 9.99479 11.1999C9.33212 11.1999 8.79492 10.6627 8.79492 10C8.79492 9.33733 9.33212 8.80014 9.99479 8.80014C10.6575 8.80014 11.1947 9.33733 11.1947 10Z" fill="#00B7C4" />
                <path d="M11.1947 14.7995C11.1947 15.4621 10.6575 15.9993 9.99479 15.9993C9.33212 15.9993 8.79492 15.4621 8.79492 14.7995C8.79492 14.1368 9.33212 13.5996 9.99479 13.5996C10.6575 13.5996 11.1947 14.1368 11.1947 14.7995Z" fill="#00B7C4" />
              </svg>
            }
            className={styles.more}
          />
        )
      }
    }
  ];

  return (
    <section className={styles.container}>
      <Layout>
        <Layout.Header className={styles.header}>
          <figure>
            <img src={user.avatar || "/images/icons/avatar.png"} alt="Usuario" />
          </figure>
          <span className={styles.user}>{user.username}</span>
          <div className={styles.buttons}>
            <Button className={styles.button} onClick={() => history.push(`/dashboard`)}>
              Ir al perfíl de creador <Chevron />
            </Button>
            <Button className={styles.button} onClick={() => history.push(`/`)}>
              Ir a marketplace <Chevron />
            </Button>
            <Button className={styles.button} onClick={() => history.push(`/`)}>
              Ir a wallet <Chevron />
            </Button>
          </div>
        </Layout.Header>
        <Content className={styles.main}>
          <div className={styles.summary}>
            <div>Total de usuarios <b>{loadingStats ? <Loader className={styles.loader} /> : stats.members}</b></div>
            <div>Usuarios activos <b>{loadingStats ? <Loader className={styles.loader} /> : stats.active}</b></div>
            <div>Usuarios inactivos <b>{loadingStats ? <Loader className={styles.loader} /> : stats.inactive}</b></div>
            <div>Gamers <b>{loadingStats ? <Loader className={styles.loader} /> : stats.consumers}</b></div>
            <div>Desarrolladores <b>{loadingStats ? <Loader className={styles.loader} /> : stats.developer}</b></div>
            <div>Admins <b>{loadingStats ? <Loader className={styles.loader} /> : stats.admins}</b></div>
          </div>

          <div className={styles.users}>
            <div className={styles.filters}>
              <Form
                layout="horizontal"
                onValuesChange={(_,values) => setFilters( { ...values } )}
                hideRequiredMark
                initialValues={{ ...filters }}
              >
                <Row gutter={[24, 0]}>
                  <Col sm={8}>
                    <Form.Item name="username" className={styles.filter}>
                      <Input size="large" placeholder="Nombre de usuario" />
                    </Form.Item>
                  </Col>
                  <Col sm={8}>
                    <Form.Item name="role">
                      <Select
                        size="large"
                        placeholder="Tipo de Usuario"
                        className="w-100"
                        suffixIcon={<ChevronDown />}
                      >
                        <Select.Option value="consumer,developer,creator,admin">Todos</Select.Option>
                        <Select.Option value="consumer">Gamer</Select.Option>
                        <Select.Option value="developer">Desarrollador</Select.Option>
                        <Select.Option value="creator">Creador</Select.Option>
                        <Select.Option value="admin">Admin</Select.Option>
                      </Select>
                    </Form.Item>
                  </Col>
                  <Col sm={8}>
                    <Form.Item name="status">
                      <Select
                        size="large"
                        placeholder="Status"
                        className="w-100"
                        suffixIcon={<ChevronDown />}
                      >
                        <Select.Option value="active,inactive">Todos</Select.Option>
                        <Select.Option value="active">Activo</Select.Option>
                        <Select.Option value="inactive">Inactivo</Select.Option>
                      </Select>
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </div>
            <Table loading={loadingUsers} columns={headers} dataSource={users} pagination={{ position: ['none', 'bottomCenter'], pageSize: 10 }} />
          </div>
        </Content>
      </Layout>
    </section>
  );
}

export default connect(({ user }) => ({ user }))(withRouter(Admin));
