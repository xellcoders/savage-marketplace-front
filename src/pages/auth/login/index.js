import React from "react";
import cn from "classnames";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Button, Col, Form, Input, Row } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleRight, faEye } from "@fortawesome/free-solid-svg-icons";
import Footer from "components/Footer";
import Image from "components/Image";
import ArrowIcon from "components/Icons/arrow";
import { GoogleLogin } from "@react-oauth/google";
import axios from "axios";
import { history } from "../../../index";
import config from "../../../config";
import styles from "../style.module.sass";

function Login({ dispatch }) {

  const login = values => {
    dispatch({
      type: 'user/LOGIN',
      payload: values
    })
  }

  const handleLogin = async googleData => {
    const result = await axios.post(`${config.api.url}/auth/google`, { idToken: googleData.credential },
        {
          headers: {
            "Content-Type": "application/json"
          }
        } ).then( res => res.data );
    localStorage.setItem( "token", result.token );
    dispatch({ type: "user/LOAD_CURRENT_ACCOUNT" });
    if( result.username ){
      history.push( '/' );
    } else {
      history.push( '/signup/username' );
    }
  }

  return (
    <>
      <section className={cn(styles.container)}>
        <div>
          <Form layout="vertical" onFinish={login} className={cn(styles.form, 'auth-form')} hideRequiredMark>
            <img className={styles.logo_icon} src="/images/Neon-Savage.png" alt="Neon Savage" height={64} />
            <h4 className={cn("h4", styles.title)}>
              Ingresa en el marketplace
            </h4>
            {/* <Button>
              <Image
                className={styles.google_icon}
                src="/images/google-icon.png"
                srcDark="/images/google-icon.png"
              />
              Ingresa con Google
            </Button> */}
            <GoogleLogin
              theme="filled_blue"
              width="327px"
              onSuccess={data => handleLogin( data )}
              cookiePolicy="single_host_origin"
            />
            <Image
              className={styles.line}
              src="/images/line.png"
              srcDark="/images/line.png"
            />
            <Row>
              <Col sm={24}>
                <Form.Item
                  hasFeedback
                  label="Ingresa tu correo electrónico"
                  name="email"
                  rules={[{ required: true, message: "El correo es obligatorio" }, { type: 'email', message: 'Debe introducir un correo válido' }]}
                >
                  <Input size="large" type="email" placeholder="" className={styles.input} />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col sm={24}>
                <Form.Item
                  hasFeedback
                  label="Escribe una contraseña"
                  name="password"
                  rules={[{ required: true, message: "La contraseña es obligatoria" }]}
                >
                  <Input size="large" placeholder="" type="password" className={styles.input} />
                </Form.Item>
              </Col>
            </Row>

            <Button htmlType="submit" className={styles.bubble}>
              <span>Ingresar</span> <div><ArrowIcon /></div>
            </Button>
          </Form>
          <Link to="/signup" className={styles.link}>
            No tengo cuenta de marketplace
          </Link>
          <div className={styles.buttons}>
            <button type="button" onClick={() => history.push( '/' )}>
              <FontAwesomeIcon icon={faEye} color="#fff" />
              <span>Ingresar como invitado </span>
              <FontAwesomeIcon icon={faAngleRight} color="#fff" />
            </button>
          </div>
        </div>
      </section>
      <div className={styles.footer}>
        <Footer />
      </div>
    </>
  );
}

export default connect(() => ({}))(Login);
