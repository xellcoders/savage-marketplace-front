import React, { useState } from "react";
import { connect } from "react-redux";
import cn from "classnames";
import { Button, notification } from "antd";
import ArrowIcon from "components/Icons/arrow";
import Cards from "components/Cards";
import Footer from "components/Footer";
import { setInterest } from "services/api.user.service";
import styles from "../style.module.sass";
import { history } from "../../../index";

function Interests({ dispatch }) {
  const items = [{
    name: 'accion',
    image: '/images/cards/action.png',
    label: 'Acción'
  }, {
    name: 'aventura',
    image: '/images/cards/adventure.png',
    label: 'Aventura'
  }, {
    name: 'anime',
    image: '/images/cards/anime.png',
    label: 'Anime'
  }, {
    name: 'deportes',
    image: '/images/cards/sports.png',
    label: 'Deportes'
  }];
  const [selected, setSelected] = useState('');

  const submit = () => {
    setInterest( selected ).then( () => {
      dispatch({ type: "user/LOAD_CURRENT_ACCOUNT" });
      history.push( '/' );
    } ).catch( error => {
      notification.error({
        message: 'Error al establecer el nombre de usuario',
        description: `${error.message} (code: ${error.code})`,
      })
    } );
  }

  return (
    <>
      <section className={cn(styles.container)}>
        <div className={styles.cards}>
          <h4 className={cn("h4", styles.title)}>
            ¿Cuál de los siguientes temas te es más interesante?
          </h4>
          <Cards items={items} onChange={index => setSelected( items[index].name )} />
          <Button onClick={submit} className={styles.bubble} style={{ marginTop: 40 }}>
            <span>Continuar</span> <div><ArrowIcon /></div>
          </Button>
        </div>
      </section>
      <div className={styles.footer}>
        <Footer />
      </div>
    </>
  );
}

export default connect(() => ({}))( Interests );
