import React from "react";
import cn from "classnames";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import { Button, Checkbox, Col, Form, Input, notification, Row } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleRight, faEye } from "@fortawesome/free-solid-svg-icons";
import Image from "components/Image";
import Footer from "components/Footer";
import ArrowIcon from "components/Icons/arrow";
import { registerUser } from "services/api.user.service";
import axios from "axios";
import { GoogleLogin } from '@react-oauth/google';
import styles from "../style.module.sass";
import { history } from "../../../index";
import config from "../../../config";

function Signup({ dispatch, location }) {
  const query = new URLSearchParams( location.search );
  const isDeveloper = query.has('developer');

  const handleLogin = async googleData => {
    const result = await axios.post(`${config.api.url}/auth/google`, { idToken: googleData.credential },
    {
      headers: {
        "Content-Type": "application/json"
      }
    } ).then( res => res.data );
    localStorage.setItem( "token", result.token );
    dispatch({ type: "user/LOAD_CURRENT_ACCOUNT" });
    if( result.username ){
      history.push( '/' );
    } else {
      history.push( '/signup/username' );
    }
  }

  const register = values => {
    registerUser( values, isDeveloper ).then( user => {
      notification.success({
        message: 'Nuevo usuario',
        description: `Se ha creado correctamente la cuenta`,
      })
      localStorage.setItem( "token", user.token );
      dispatch({ type: "user/LOAD_CURRENT_ACCOUNT" });
      history.push( '/signup/username' );
    } ).catch( error => {
      notification.error({
        message: 'Error al registrar el usuario',
        description: `${error.message} (code: ${error.code})`,
      })
    } );
  }

  return (
    <>
      <section className={cn(styles.container)}>
        <div>
          <Form layout="vertical" onFinish={register} className={cn(styles.form, 'auth-form')} hideRequiredMark>
            <img className={styles.logo_icon} src="/images/Neon-Savage.png" alt="Neon Savage" height={64} />
            <h4 className={cn("h4", styles.title)}>
              Regístrate en el marketplace
            </h4>
            {/* <Button>
              <Image
                className={styles.google_icon}
                src="/images/google-icon.png"
                srcDark="/images/google-icon.png"
              />
              Regístrate con Google
            </Button> */}
            <GoogleLogin
              theme="filled_blue"
              width="327px"
              onSuccess={data => handleLogin( data )}
              cookiePolicy="single_host_origin"
            />
            <Image
              className={styles.line}
              src="/images/line.png"
              srcDark="/images/line.png"
            />
            <Row>
              <Col sm={24}>
                <Form.Item
                  hasFeedback
                  label="Ingresa tu correo electrónico"
                  name="email"
                  rules={[{ required: true, message: "El correo es obligatorio" }, { type: 'email', message: 'Debe introducir un correo válido' }]}
                >
                  <Input size="large" type="email" placeholder="" className={styles.input} />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col sm={24}>
                <Form.Item
                  hasFeedback
                  label="Escribe una contraseña"
                  name="password"
                  rules={[{ required: true, message: "La contraseña es obligatoria" }]}
                >
                  <Input size="large" placeholder="" type="password" className={styles.input} />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col sm={24}>
                <Form.Item
                  hasFeedback
                  name="terms"
                  valuePropName="checked"
                  className={styles.terms}
                  rules={[{ required: true, message: "Los términos y condiciones son obligatorios" }]}
                >
                  <Checkbox className={styles.checkbox}>
                    He leído y acepto los <a href="https://neonsavagestudios.com/es/terminos-condiciones/"><u>Términos y Condiciones</u></a> así como el <a href="https://neonsavagestudios.com/es/aviso-privacidad/"><u>Aviso de Privacidad</u></a> de Neon Savage Studios
                  </Checkbox>
                </Form.Item>
              </Col>
            </Row>

            <Button htmlType="submit" className={styles.bubble}>
              <span>Registrarme</span> <div><ArrowIcon /></div>
            </Button>
          </Form>
          <Link to="/login" className={styles.link}>
            Ya tengo cuenta de marketplace
          </Link>
          <div className={styles.buttons}>
            <button type="button" onClick={() => history.push( '/' )}>
              <FontAwesomeIcon icon={faEye} color="#fff" />
              <span>Ingresar como invitado </span>
              <FontAwesomeIcon icon={faAngleRight} color="#fff" />
            </button>
          </div>
        </div>
      </section>
      <div className={styles.footer}>
        <Footer />
      </div>
    </>
  );
}

export default connect(() => ({}))(withRouter(Signup));
