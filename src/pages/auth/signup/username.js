import React, { useState } from "react";
import { connect } from "react-redux";
import cn from "classnames";
import { Button, Col, Form, Input, notification, Row, Upload } from "antd";
import ArrowIcon from "components/Icons/arrow";
import Footer from "components/Footer";
import { LoadingOutlined } from "@ant-design/icons";
import { getBase64 } from "util/files";
import { setUsername } from "services/api.user.service";
import styles from "../style.module.sass";
import { history } from "../../../index";

function Username({ dispatch }) {
  const [uploading, setUploading] = useState(false);
  const [imageUrl, setImageUrl] = useState('/images/icons/avatar.png');

  const uploadButton = (
    <div className={styles.preview}>
      {uploading ?
        <LoadingOutlined /> :
        <img src="/images/icons/img.svg" width={50} style={{ marginTop: 25, marginLeft: 4 }} alt="Upload" />
      }
    </div>
  );

  const handleChange = info => {
    setUploading( true );
    getBase64(info.file, url => {
      setImageUrl(url);
      setUploading( false );
    });
  };

  const submit = ({ username }) => {
    setUsername( username, imageUrl ).then( () => {
      dispatch({ type: "user/LOAD_CURRENT_ACCOUNT" });
      history.push( '/signup/interests' );
    } ).catch( error => {
      notification.error({
        message: 'Error al establecer el nombre de usuario',
        description: `${error.message} (code: ${error.code})`,
      })
    } );
  }

  return (
    <>
      <section className={cn(styles.container)}>
        <Form
          layout="vertical"
          onFinish={submit}
          className={cn(styles.form, 'auth-form')}
          hideRequiredMark
          initialValues={{
            username: 'usuario_123!'
          }}
        >
          <h4 className={cn("h4", styles.title)}>
            ¡Ya tienes tu cuenta!, elige un
            avatar y tu nombre de usuario
          </h4>
          <Row gutter={[16, 0]}>
            <Col className={styles.upload}>
              <Upload
                accept=".jpg,.jpeg,.png"
                name="avatar"
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                beforeUpload={() => false}
                onChange={handleChange}
              >
                {imageUrl ?
                  <div className={styles.image}>
                    <img src={imageUrl} alt="avatar" style={{ width: '100%' }} />
                  </div> : uploadButton
                }
                {imageUrl ?
                  <div className={styles.edit} role="button" tabIndex={0} onKeyDown={()=>{}} onClick={e => { setImageUrl( null ); e.stopPropagation(); }}>
                    Editar <img src="/images/icons/edit.svg" alt="Editar" />
                  </div> : <a>Subir imagen</a>
                }
              </Upload>
            </Col>
            <Col sm={24}>
              <Form.Item
                hasFeedback
                label="Nombre de usuario"
                name="username"
                rules={[{ required: true, message: "El nombre de usuario es obligatorio" }]}
              >
                <Input size="large" placeholder="" className={styles.input} />
              </Form.Item>
            </Col>
          </Row>
          <Button htmlType="submit" className={styles.bubble} style={{marginTop: 20}}>
            <span>Continuar</span> <div><ArrowIcon /></div>
          </Button>
        </Form>
      </section>
      <div className={styles.footer}>
        <Footer />
      </div>
    </>
  );
}

export default connect(() => ({}))( Username );
