import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import cn from "classnames";
import { Button } from "antd";
import { withRouter } from "react-router-dom";
import BackButton2 from "components/BackButton2";
import Loader from "components/Loader";
import Loading from "components/Loading";
import { getTopCreators } from "services/api.user.service";
import { getUserStats } from "services/api.stats.service";
import styles from "./styles.module.sass";
import { history } from "../../index";

const Creator = ({ creator, index }) => {
  const [userStats, setUserStats] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect( () => {
    // eslint-disable-next-line no-underscore-dangle
    getUserStats( creator._id ).then(stats => {
      setUserStats(stats || {});
      setLoading(false);
    });
  }, [creator] )

  return (
    <div className={styles.creator}>
      <div className={styles.title}>
        <div className={styles.avatar}>
          <figure>
            {creator.avatar && <img src={creator.avatar} alt={creator.name} />}
          </figure>
        </div>
        <div className={styles.name}>{creator.username}</div>
        <div className={styles.count}>{index + 1}</div>
      </div>
      <div
        className={styles.subtitle}
        role="button"
        tabIndex={0}
        onKeyDown={()=>{}}
        /* eslint-disable-next-line no-underscore-dangle */
        onClick={() => history.push(`/user/${creator._id}`)}
      >
        {userStats.total || 0} NFTs activos
        <svg width="10" height="16" viewBox="0 0 10 16" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fillRule="evenodd" clipRule="evenodd" d="M6.80639 7.99702L0.80264 1.99328L1.8633 0.932617L8.92771 7.99702L1.85705 15.0677L0.796387 14.007L6.80639 7.99702Z" fill="#01B7C3" />
          <path fillRule="evenodd" clipRule="evenodd" d="M6.80639 7.99702L0.80264 1.99328L1.8633 0.932617L8.92771 7.99702L1.85705 15.0677L0.796387 14.007L6.80639 7.99702Z" fill="#01B7C3" />
          <path fillRule="evenodd" clipRule="evenodd" d="M6.80639 7.99702L0.80264 1.99328L1.8633 0.932617L8.92771 7.99702L1.85705 15.0677L0.796387 14.007L6.80639 7.99702Z" stroke="#00B7C4" />
          <path fillRule="evenodd" clipRule="evenodd" d="M6.80639 7.99702L0.80264 1.99328L1.8633 0.932617L8.92771 7.99702L1.85705 15.0677L0.796387 14.007L6.80639 7.99702Z" stroke="#00B7C4" />
        </svg>
      </div>
      {loading &&
        <div className={styles.loading}>
          <Loader />
        </div>
      }
      <div className={styles.content}>
        <span>{userStats.onAuction || 0} en subasta</span>
        <span>{userStats.onSale || 0} en venta</span>
        <span>{userStats.onCollection || 0} en tu colección</span>
      </div>
    </div>
  )
}

const TopCreators = ({ user }) => {
  const [creators, setCreators] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading( true );

    getTopCreators().then( results => {
      setCreators( results );
      setLoading( false );
    } ).catch(() => {
      setLoading( false );
    });
  }, []);

  return (
    <section className={styles.wrapper}>
      {loading && <Loading status="Cargando..." />}
      <div className={styles.backButton}>
        <BackButton2 redirectTo="/" />
      </div>

      <div className={cn(styles.user2, styles.header2)}>
        {/* eslint-disable-next-line jsx-a11y/no-noninteractive-element-to-interactive-role */}
        <figure role="button" tabIndex={0} onKeyDown={()=>{}} onClick={() => history.push('/dashboard')}>
          <img src={user.avatar || "/images/icons/avatar.png"} alt="Usuario" />
        </figure>
        <span>{user.username}</span>
        <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fillRule="evenodd" clipRule="evenodd" d="M17.3954 0.194824V6.43545H16.8V15.7998H10.549L10.549 9.34148H7.50064V15.7998H1.19999V6.43545H0.604553V0.194824H17.3954ZM12.0248 1.39482H9.6248V5.23545H12.0248V1.39482ZM13.2248 5.23545V1.39482H16.1954V5.23545H13.2248ZM2.39999 6.43545V14.5998H6.30064V8.14148H11.749V14.5998H15.6V6.43545H2.39999ZM5.97518 1.39482H8.4248V5.23545H5.97518V1.39482ZM4.77518 1.39482H1.80455V5.23545H4.77518V1.39482Z" fill="white" />
        </svg>
      </div>
      <div className={styles.options}>
        <Button onClick={() => history.push('/dashboard')}>Metricas</Button>
        <Button className={styles.active}>Destacados</Button>
      </div>

      <div className={styles.container}>
        <div className={styles.header}>Top 10 creadores</div>
        {creators.map( (creator, index) => <Creator creator={creator} index={index} /> )}
      </div>
    </section>
  );
};

export default connect(({ user }) => ({ user }))( withRouter( TopCreators ) );
