import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { useHistory, withRouter } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShareNodes } from "@fortawesome/free-solid-svg-icons";
import BackButton2 from "components/BackButton2";
import Loading from "components/Loading";
import Share from "components/Share";
import Price from "components/Price";
import { getItemImage } from "util/files";
import { getTopItems, toggleLikeItem } from "services/api.item.service";
import moment from "moment";
import cn from "classnames";
import styles2 from "pages/nft/view/styles.module.sass";
import { Button } from "antd";
import styles from "./styles.module.sass";

const Buy = ( { item } ) => {
  const history = useHistory();
  return (
    <div className={cn(styles.block, styles2.buy)}>
      <h5>Venta</h5>
      <div className={styles.price}>
        Precio del NFT: <b><Price item={item} fixed={item.price || 0} /></b>
      </div>
      {/* eslint-disable-next-line */}
      <Button onClick={() => history.push(`/nft/${item._id}/buy`)}>Comprar</Button>
    </div>
  )
}

const Bid = ( { item } ) => {
  const [days, setDays] = useState(0);
  const [hours, setHours] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [seconds, setSeconds] = useState(-1);
  const history = useHistory();

  useEffect(() => {
    const currentTime = moment().valueOf();
    const eventTime = moment( item.deadline ).valueOf();
    const diffTime = eventTime - currentTime;
    let duration = moment.duration(diffTime, 'milliseconds');

    setInterval(() => {
      duration = moment.duration(duration - 1000, 'milliseconds');
      setDays( duration.days() );
      setHours( duration.hours() );
      setMinutes( duration.minutes() );
      setSeconds( duration.seconds() );
    }, 1000);
  }, [item]);

  const finished = days < 0 || hours < 0 || seconds < 0;

  return (
    <div className={cn(styles.block, styles2.bid)}>
      <h5>Subasta</h5>
      <div className={styles2.prices}>
        <div><span>Precio del subasta</span><b><Price item={item} fixed={item.start || 0} /></b></div>
        <div><span>Mejor oferta</span><b><Price item={item} fixed={item.bid || 0} /></b></div>
      </div>
      {finished ? <h4>La subasta terminó hace:</h4> : <h4>La subasta termina en:</h4>}
      <div className={styles2.countdown}>
        <div><span>Días</span><b>{Math.abs(days)}</b></div>
        <div><span>Horas</span><b>{Math.abs(hours)}</b></div>
        <div><span>Minutos</span><b>{Math.abs(minutes)}</b></div>
        <div><span>Seg.</span><b>{Math.abs(seconds)}</b></div>
      </div>
      {/* eslint-disable-next-line */}
      {!finished && <Button onClick={() => history.push(`/nft/${item._id}/bid`)}>Ofertar</Button>}
    </div>
  )
}

const Play = ( { item } ) => {
  console.log( item.tokenId )
  return (
    <div className={cn(styles.block, styles2.play)}>
      <h5>Obtener en juego</h5>
      <div className={styles2.instructions}>
        Cruza los 10 mundos que se interponen entre Mario Bros y el castillo de Bowser para rescatar a la princesa peach.
      </div>
      <Button>Ir al juego</Button>
    </div>
  )
}

const Item = ({ item }) => {
  const [liked, setLiked] = useState( item.liked );
  const [showShareModal, setShowShareModal] = useState( false );
  const history = useHistory();

  return (
    <div className={styles.item}>
      <div className={styles.center}>
        <div className={styles.image}>
          <figure>
            {item.nft.image && <img src={getItemImage(item.nft)} alt={item.nft.name} />}
          </figure>
          <div className={styles.share}>
            <span
              className="button"
              role="button"
              tabIndex={0}
              onKeyDown={()=>{}}
              onClick={event => {
                event.preventDefault();
                setShowShareModal( true );
              }}
            >
              <FontAwesomeIcon icon={faShareNodes} color="#00B7C4" />
            </span>
            <span
              className="button"
              role="button"
              tabIndex={0}
              onKeyDown={()=>{}}
              onClick={event => {
                event.preventDefault();
                // eslint-disable-next-line no-underscore-dangle
                toggleLikeItem( item._id ).then( result => setLiked( result ) );
              }}
            >
              {liked ?
                <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path fillRule="evenodd" clipRule="evenodd" d="M11.0019 0.398682C9.72954 0.398682 8.8218 0.853724 8.12213 1.6525C8.09025 1.6889 8.04831 1.7408 8.00108 1.80134C7.95385 1.7408 7.91191 1.6889 7.88004 1.6525C7.18036 0.853724 6.27262 0.398682 5.00025 0.398682C2.55542 0.398682 0.882568 1.98206 0.882568 4.9989C0.882568 5.05766 0.883791 5.11656 0.886227 5.17559C0.964595 7.07474 2.2531 9.00992 4.33499 10.9305C5.06737 11.6061 5.84982 12.2325 6.63222 12.7952C6.90618 12.9923 7.1609 13.1675 7.39006 13.3191C7.47081 13.3725 7.54278 13.4192 7.60517 13.459L7.66347 13.4959L7.68578 13.5098L8.00108 13.7046L8.31638 13.5098C8.37243 13.4752 8.47311 13.411 8.6121 13.3191C8.84127 13.1675 9.09599 12.9923 9.36995 12.7952C10.1523 12.2325 10.9348 11.6061 11.6672 10.9305C13.7491 9.00992 15.0376 7.07474 15.1159 5.17559C15.1184 5.11656 15.1196 5.05766 15.1196 4.9989C15.1196 1.98206 13.4467 0.398682 11.0019 0.398682ZM10.8535 10.0484C10.16 10.6882 9.41463 11.2849 8.66925 11.8211C8.42872 11.9941 8.20432 12.149 8.00108 12.2844C7.79784 12.149 7.57344 11.9941 7.33291 11.8211C6.58753 11.2849 5.8422 10.6882 5.14866 10.0484C3.28055 8.32513 2.14696 6.62261 2.08521 5.12612C2.08345 5.08351 2.08257 5.04111 2.08257 4.9989C2.08257 2.66 3.20386 1.59868 5.00025 1.59868C5.9084 1.59868 6.49537 1.89293 6.97736 2.44318C7.13263 2.62045 7.68637 3.40068 7.51851 3.1735L8.00108 3.82662L8.48365 3.1735C8.31579 3.40068 8.86953 2.62045 9.0248 2.44318C9.50679 1.89293 10.0938 1.59868 11.0019 1.59868C12.7983 1.59868 13.9196 2.66 13.9196 4.9989C13.9196 5.04111 13.9187 5.08351 13.917 5.12612C13.8552 6.62261 12.7216 8.32513 10.8535 10.0484Z" fill="#00B7C4" />
                </svg> :
                <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path fillRule="evenodd" clipRule="evenodd" d="M11.0019 0.398682C9.72954 0.398682 8.8218 0.853724 8.12213 1.6525C8.09025 1.6889 8.04831 1.7408 8.00108 1.80134C7.95385 1.7408 7.91191 1.6889 7.88004 1.6525C7.18036 0.853724 6.27262 0.398682 5.00025 0.398682C2.55542 0.398682 0.882568 1.98206 0.882568 4.9989C0.882568 5.05766 0.883791 5.11656 0.886227 5.17559C0.964595 7.07474 2.2531 9.00992 4.33499 10.9305C5.06737 11.6061 5.84982 12.2325 6.63222 12.7952C6.90618 12.9923 7.1609 13.1675 7.39006 13.3191C7.47081 13.3725 7.54278 13.4192 7.60517 13.459L7.66347 13.4959L7.68578 13.5098L8.00108 13.7046L8.31638 13.5098C8.37243 13.4752 8.47311 13.411 8.6121 13.3191C8.84127 13.1675 9.09599 12.9923 9.36995 12.7952C10.1523 12.2325 10.9348 11.6061 11.6672 10.9305C13.7491 9.00992 15.0376 7.07474 15.1159 5.17559C15.1184 5.11656 15.1196 5.05766 15.1196 4.9989C15.1196 1.98206 13.4467 0.398682 11.0019 0.398682ZM10.8535 10.0484C10.16 10.6882 9.41463 11.2849 8.66925 11.8211C8.42872 11.9941 8.20432 12.149 8.00108 12.2844C7.79784 12.149 7.57344 11.9941 7.33291 11.8211C6.58753 11.2849 5.8422 10.6882 5.14866 10.0484C3.28055 8.32513 2.14696 6.62261 2.08521 5.12612C2.08345 5.08351 2.08257 5.04111 2.08257 4.9989C2.08257 2.66 3.20386 1.59868 5.00025 1.59868C5.9084 1.59868 6.49537 1.89293 6.97736 2.44318C7.13263 2.62045 7.68637 3.40068 7.51851 3.1735L8.00108 3.82662L8.48365 3.1735C8.31579 3.40068 8.86953 2.62045 9.0248 2.44318C9.50679 1.89293 10.0938 1.59868 11.0019 1.59868C12.7983 1.59868 13.9196 2.66 13.9196 4.9989C13.9196 5.04111 13.9187 5.08351 13.917 5.12612C13.8552 6.62261 12.7216 8.32513 10.8535 10.0484Z" fill="white" fillOpacity="0.9" />
                </svg>
              }
            </span>
          </div>
        </div>
        <div className={styles.title}>
          {item.nft.name}
          <span className={styles.subtitle}>Ningúno</span>
          {/* eslint-disable-next-line no-underscore-dangle */}
          <Button className={styles.more} onClick={() => history.push(`/nft/${item._id}`)}>
            Ver más
            <svg width="9" height="16" viewBox="0 0 9 16" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd" clipRule="evenodd" d="M6.80639 7.99702L0.80264 1.99328L1.8633 0.932617L8.92771 7.99702L1.85705 15.0677L0.796387 14.007L6.80639 7.99702Z" fill="#01B7C3" />
              <path fillRule="evenodd" clipRule="evenodd" d="M6.80639 7.99702L0.80264 1.99328L1.8633 0.932617L8.92771 7.99702L1.85705 15.0677L0.796387 14.007L6.80639 7.99702Z" fill="#01B7C3" />
            </svg>
          </Button>
          <div className={styles.action}>
            {/* eslint-disable-next-line */}
            {item.status === 'onsale' ? <Buy item={item} /> : ''}
            {item.status === 'onauction' ? <Bid item={item} /> : ''}
            {item.status === 'ongame' ? <Play item={item} /> : ''}
          </div>
        </div>
      </div>
      <div className={styles.right}>
        {/* eslint-disable-next-line */}
        {item.status === 'onsale' ? <Buy item={item} /> : ''}
        {item.status === 'onauction' ? <Bid item={item} /> : ''}
        {item.status === 'ongame' ? <Play item={item} /> : ''}
      </div>
      {/* eslint-disable-next-line no-underscore-dangle */}
      {showShareModal && <Share id={item._id} onCancel={() => setShowShareModal( false )} /> }
    </div>
  )
}

const TopItems = ({ user }) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);
  const history = useHistory();

  useEffect(() => {
    setLoading( true );

    getTopItems().then( results => {
      setItems( [...results, ...results] );
      setLoading( false );
    } ).catch(() => {
      setLoading( false );
    });
  }, []);

  return (
    <section className={styles.wrapper}>
      {loading && <Loading status="Cargando..." />}
      <div className={styles.backButton}>
        <BackButton2 redirectTo="/" />
      </div>
      <div className={cn(styles.user2, styles.header2)}>
        {/* eslint-disable-next-line jsx-a11y/no-noninteractive-element-to-interactive-role */}
        <figure role="button" tabIndex={0} onKeyDown={()=>{}} onClick={() => history.push('/dashboard')}>
          <img src={user.avatar || "/images/icons/avatar.png"} alt="Usuario" />
        </figure>
        <span>{user.username}</span>
        <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fillRule="evenodd" clipRule="evenodd" d="M17.3954 0.194824V6.43545H16.8V15.7998H10.549L10.549 9.34148H7.50064V15.7998H1.19999V6.43545H0.604553V0.194824H17.3954ZM12.0248 1.39482H9.6248V5.23545H12.0248V1.39482ZM13.2248 5.23545V1.39482H16.1954V5.23545H13.2248ZM2.39999 6.43545V14.5998H6.30064V8.14148H11.749V14.5998H15.6V6.43545H2.39999ZM5.97518 1.39482H8.4248V5.23545H5.97518V1.39482ZM4.77518 1.39482H1.80455V5.23545H4.77518V1.39482Z" fill="white" />
        </svg>
      </div>
      <div className={styles.options}>
        <Button onClick={() => history.push('/dashboard')}>Metricas</Button>
        <Button className={styles.active}>Destacados</Button>
      </div>
      <div className={styles.container}>
        <div className={styles.header}>Top 10 NFTs más vendidos</div>
        {items.map( item => <Item item={item} /> )}
      </div>
    </section>
  );
};

export default connect(({ user }) => ({ user }))( withRouter( TopItems ) );
