import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import BackButton2 from "components/BackButton2";
import styles from "./styles.module.sass";
import Movements from "../../components/Movements";

const MyMovements = () => {
  return (
    <section className={styles.wrapper}>
      <div className={styles.backButton}>
        <BackButton2 redirectTo="/" />
      </div>
      <div className={styles.container}>
        <div className={styles.header3}>Tus movimientos NFTs</div>
        <div className={styles.movements}>
          <h4>Últimos</h4>
          <Movements showTitle={false} />
        </div>
      </div>
    </section>
  );
};

export default connect(({ user }) => ({ user }))(withRouter(MyMovements));
