import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { useHistory, withRouter } from "react-router-dom";
import cn from "classnames";
import BackButton2 from "components/BackButton2";
import { Button } from "antd";
import Loading from "components/Loading";
import { getTopUsers } from "services/api.user.service";
import styles from "./styles.module.sass";

const User = ({ user, index }) => {
  const history = useHistory();

  return (
    <div className={styles.user}>
      <div className={styles.index}>{index + 1}</div>
      <div className={styles.avatar}>
        <figure>
          {user.avatar && <img src={user.avatar} alt={user.name} />}
        </figure>
      </div>
      <div className={styles.name}>{user.username}</div>
      <div className={styles.count}>{user.onCollection} NFTs en su colección</div>
      {/* eslint-disable-next-line no-underscore-dangle */}
      <Button className={styles.more} onClick={() => history.push(`/user/${user._id}`)}>
        <span>Ver Colección</span>
        <svg width="9" height="16" viewBox="0 0 9 16" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fillRule="evenodd" clipRule="evenodd" d="M6.80639 7.99702L0.80264 1.99328L1.8633 0.932617L8.92771 7.99702L1.85705 15.0677L0.796387 14.007L6.80639 7.99702Z" fill="#01B7C3" />
          <path fillRule="evenodd" clipRule="evenodd" d="M6.80639 7.99702L0.80264 1.99328L1.8633 0.932617L8.92771 7.99702L1.85705 15.0677L0.796387 14.007L6.80639 7.99702Z" fill="#01B7C3" />
        </svg>
      </Button>
    </div>
  )
}

const TopUsers = ({ user }) => {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(true);
  const history = useHistory();

  useEffect(() => {
    setLoading( true );

    getTopUsers().then( results => {
      setUsers( results );
      setLoading( false );
    } ).catch(() => {
      setLoading( false );
    });
  }, []);

  return (
    <section className={styles.wrapper}>
      {loading && <Loading status="Cargando..." />}
      <div className={styles.backButton}>
        <BackButton2 redirectTo="/" />
      </div>
      <div className={cn(styles.user2, styles.header2)}>
        {/* eslint-disable-next-line jsx-a11y/no-noninteractive-element-to-interactive-role */}
        <figure role="button" tabIndex={0} onKeyDown={()=>{}} onClick={() => history.push('/dashboard')}>
          <img src={user.avatar || "/images/icons/avatar.png"} alt="Usuario" />
        </figure>
        <span>{user.username}</span>
        <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fillRule="evenodd" clipRule="evenodd" d="M17.3954 0.194824V6.43545H16.8V15.7998H10.549L10.549 9.34148H7.50064V15.7998H1.19999V6.43545H0.604553V0.194824H17.3954ZM12.0248 1.39482H9.6248V5.23545H12.0248V1.39482ZM13.2248 5.23545V1.39482H16.1954V5.23545H13.2248ZM2.39999 6.43545V14.5998H6.30064V8.14148H11.749V14.5998H15.6V6.43545H2.39999ZM5.97518 1.39482H8.4248V5.23545H5.97518V1.39482ZM4.77518 1.39482H1.80455V5.23545H4.77518V1.39482Z" fill="white" />
        </svg>
      </div>
      <div className={styles.options}>
        <Button onClick={() => history.push('/dashboard')}>Metricas</Button>
        <Button className={styles.active}>Destacados</Button>
      </div>
      <div className={styles.container}>
        <div className={styles.header}>
          <svg width="14" height="13" viewBox="0 0 14 13" fill="none" xmlns="http://www.w3.org/2000/svg" onClick={() => history.push('/dashboard')}>
            <path d="M2.44554 5.71336L7.32796 0.850204L6.48111 0L0.148438 6.30769L6.48111 12.6154L7.32796 11.7652L2.45693 6.91336H13.5986V5.71336H2.44554Z" fill="white" />
          </svg>
          Top 10 usuarios
        </div>
        {users.map( (u, index) => <User user={u} index={index} /> )}
      </div>
    </section>
  );
};

export default connect(({ user }) => ({ user }))( withRouter( TopUsers ) );
