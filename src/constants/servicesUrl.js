export const API = "https://savage-api.xellcoders.com/";
// http://34.136.48.7/
// http://34.136.48.7/

export const SERVICES_USER = {
  USER_SEARCH_OR_GENERATION: {
    METHOD: "post",
    URL: `${API}users/`
  },
  USER_UPDATE: {
    METHOD: "put",
    URL: `${API}users/`
  },
  PASSWORD_UPDATE: {
    METHOD: "put",
    URL: `${API}users/password`
  },
  GET_USER_TOKEN: {
    METHOD: "get",
    URL: `${API}users`
  },
  GET_USER_BY_ADDRESS: {
    METHOD: "get",
    URL: `${API}users/`
  },
  ACTIVATE_USER: {
    METHOD: "put",
    URL: `${API}users/activate`
  },
  INACTIVATE_USER: {
    METHOD: "put",
    URL: `${API}users/inactivate`
  },
  BLOCK_USER: {
    METHOD: "put",
    URL: `${API}users/lock`
  },
  SET_USER_IMAGE: {
    POST: "post",
    URL: `${API}user/images`
  },
  UPDATE_USER_IMAGE: {
    PUT: "put",
    URL: `${API}user/images`
  },
  GET_USER_IMAGE: {
    GET: "get",
    URL: `${API}user_images`
  }
};

export const SERVICES_NFT = {
  GET_NFT: {
    METHOD: "get",
    URL: `${API}nfts/`
  },
  GET_NFTS_LIST: {
    METHOD: "get",
    URL: `${API}nfts/`
  },
  UPDATE_NFTS_IMAGE: {
    METHOD: "put",
    URL: `${API}nft_images`
  },
  SET_NFTS_IMAGE: {
    METHOD: "post",
    URL: `${API}nft_images`
  },
  ADD_COMMENT: {
    METHOD: "post",
    URL: `${API}nft_comments/`
  },
  GET_COMMENT_ID: {
    METHOD: "post",
    URL: `${API}nfts_comment/`
  },
  GET_COMMENT_ADDRESS: {
    METHOD: "get",
    URL: `${API}nft_comments/nft/`
  },
  GET_COMMENT_USER: {
    METHOD: "get",
    URL: `${API}nfts_comment/user/`
  },
  UPDATE_NFT: {
    METHOD: "put",
    URL: `${API}nfts/`
  },
  CREATE_NFT: {
    METHOD: "post",
    URL: `${API}nfts/`
  }
};

export const SERVICES_CATALOGS = {
  CATALOG_OF_FEATURE: {
    METHOD: "get",
    URL: `${API}catalogs/features`
  },
  CATALOG_OF_ABILITIES: {
    METHOD: "get",
    URL: `${API}catalogs/abilities`
  },
  CATALOG_OF_COLLECTIONS: {
    METHOD: "get",
    URL: `${API}catalogs/collections`
  },
  CATALOG_OF_TOKEN_TYPES: {
    METHOD: "get",
    URL: `${API}catalogs/token_types`
  }
};

export const SERVICES_IMAGES = {
  GET_IMAGES_AVATARS: {
    GET: "get",
    URL: `${API}/images/avatars`
  },
  GET_IMAGES_CATEGORIES: {
    GET: "get",
    URL: `${API}/images/categories`
  }
};
