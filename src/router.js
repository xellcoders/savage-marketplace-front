import React, { lazy, Suspense } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { ConnectedRouter } from "connected-react-router";
import { CSSTransition, SwitchTransition } from "react-transition-group";
import { connect } from "react-redux";

import Layout from "layouts";

export const routes = [
  // Screens
  {
    path: "/home",
    Component: lazy(() => import("pages/home")),
    exact: true
  },
  // Admin users
  {
    path: "/admin",
    Component: lazy(() => import("pages/admin")),
    exact: true,
    roles: ['developer', 'creator', 'admin']
  },
  // Dashbiard
  {
    path: "/dashboard",
    Component: lazy(() => import("pages/dashboard")),
    exact: true,
    roles: ['developer', 'creator', 'consumer', 'admin'],
    useWallet: true
  },
  {
    path: "/dashboard/:section",
    Component: lazy(() => import("pages/dashboard")),
    exact: false,
    roles: ['developer', 'creator', 'consumer', 'admin']
  },
  {
    path: "/top/items",
    Component: lazy(() => import("pages/top/items")),
    exact: true,
    roles: ['developer', 'creator', 'consumer', 'admin']
  },
  {
    path: "/top/creators",
    Component: lazy(() => import("pages/top/creators")),
    exact: true,
    roles: ['developer', 'creator', 'consumer', 'admin']
  },
  {
    path: "/top/users",
    Component: lazy(() => import("pages/top/users")),
    exact: true,
    roles: ['developer', 'creator', 'consumer', 'admin']
  },
  {
    path: "/movements",
    Component: lazy(() => import("pages/top/movements")),
    exact: true,
    roles: ['developer', 'creator', 'consumer', 'admin']
  },
  // Create NFT
  {
    path: "/nft/create",
    Component: lazy(() => import("pages/nft/create")),
    exact: true,
    roles: ['developer', 'creator', 'consumer', 'admin']
  },
  {
    path: "/nft/:id/sell",
    Component: lazy(() => import("pages/nft/offer/sell")),
    exact: true,
    roles: ['developer', 'creator', 'consumer', 'admin'],
    useWallet: true
  },
  {
    path: "/nft/:id/buy",
    Component: lazy(() => import("pages/nft/acquire/buy")),
    exact: true,
    roles: ['developer', 'creator', 'consumer', 'admin'],
    useWallet: true
  },
  {
    path: "/nft/:id/bid",
    Component: lazy(() => import("pages/nft/acquire/bid")),
    exact: true,
    roles: ['developer', 'creator', 'consumer', 'admin'],
    useWallet: true
  },
  {
    path: "/nft/:id/auction",
    Component: lazy(() => import("pages/nft/offer/auction")),
    exact: true,
    roles: ['developer', 'creator', 'consumer', 'admin'],
    useWallet: true
  },
  {
    path: "/nft/:id/transfer",
    Component: lazy(() => import("pages/nft/offer/transfer")),
    exact: true,
    roles: ['developer', 'creator', 'consumer', 'admin']
  },
  {
    path: "/nft/:id/game",
    Component: lazy(() => import("pages/nft/offer/game")),
    exact: true,
    roles: ['developer']
  },
  {
    path: "/nft/:id/comment",
    Component: lazy(() => import("pages/nft/comment")),
    exact: true,
    roles: ['developer', 'creator', 'consumer', 'admin']
  },
  {
    path: "/nft/:id",
    Component: lazy(() => import("pages/nft/view")),
    exact: true
  },
  {
    path: "/user/:id",
    Component: lazy(() => import("pages/user")),
    exact: true,
    roles: ['developer', 'creator', 'consumer', 'admin']
  },
  // Auth Pages
  {
    path: "/signup",
    Component: lazy(() => import("pages/auth/signup")),
    exact: true
  },
  {
    path: "/signup/username",
    Component: lazy(() => import("pages/auth/signup/username")),
    exact: true
  },
  {
    path: "/signup/interests",
    Component: lazy(() => import("pages/auth/signup/interests")),
    exact: true
  },
  {
    path: "/login",
    Component: lazy(() => import("pages/auth/login")),
    exact: true
  },
  {
    path: "/404",
    Component: lazy(() => import("pages/extra/404")),
    exact: true
  },
  {
    path: "/500",
    Component: lazy(() => import("pages/extra/500")),
    exact: true
  }
];

const Router = ({ history }) => {
  return (
    <ConnectedRouter history={history}>
      <Layout>
        <Route
          render={state => {
            const { location } = state;
            return (
              <SwitchTransition>
                <CSSTransition
                  key={location.pathname}
                  appear
                  classNames="fadein"
                  timeout={300}
                >
                  <Switch location={location}>
                    <Route exact path="/" render={() => <Redirect to="/home" />} />
                    {routes.map(({ path, Component, exact }) => (
                      <Route
                        path={path}
                        key={path}
                        exact={exact}
                        render={() => {
                          return (
                            <div className="fadein">
                              <Suspense fallback={null}>
                                <Component />
                              </Suspense>
                            </div>
                          );
                        }}
                      />
                    ))}
                    <Redirect to="/404" />
                  </Switch>
                </CSSTransition>
              </SwitchTransition>
            );
          }}
        />
      </Layout>
    </ConnectedRouter>
  );
};

const mapStateToProps = () => ({});

export default connect(mapStateToProps)(Router);
