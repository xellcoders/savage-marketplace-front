import { all, takeEvery, put, call } from "redux-saga/effects";
import { notification } from "antd";
import { history } from "index";
import { getETHPrice } from "services/api.util.service";
import { login, currentAccount, logout, exchangeRates } from "services/api.auth.service";
import actions from "./actions";

export function* LOGIN({ payload }) {
  const { email, password } = payload;
  yield put({
    type: "user/SET_STATE",
    payload: {
      loading: true
    }
  });
  const { token, error } = yield call(login, email, password);
  if( token ) {
    localStorage.setItem("token", token);
    notification.success({
      message: "Iniciar sesión",
      description: "Has iniciado sesión correctamente"
    });
    yield put({
      type: "user/LOAD_CURRENT_ACCOUNT"
    });
    yield history.push("/");
  } else {
    yield put({
      type: "user/LOAD_CURRENT_ACCOUNT"
    });
    notification.warning({
      message: "Login Error",
      description: `${error.message} (code: ${error.code})`
    });
  }
}

export function* LOAD_CURRENT_ACCOUNT() {
  yield put({
    type: "user/SET_STATE",
    payload: {
      loading: true
    }
  });
  const dollar = yield call(exchangeRates);
  const ethereum = yield call(getETHPrice);
  const response = yield call(currentAccount);
  const { _id: id, name, email, username, role, avatar, wallet, currency } = response;
  if( !id ) return yield put({
    type: "user/SET_STATE",
    payload: {
      loading: false,
      dollar,
      eth: ethereum.price
    }
  });

  yield put({
    type: "user/SET_STATE",
    payload: {
      id,
      name,
      role,
      email,
      username,
      avatar,
      currency,
      wallet,
      dollar,
      eth: ethereum.price,
      nsc: 1,
      authorized: !!id,
      loading: false
    }
  });
}

export function* LOGOUT() {
  yield call(logout);
  yield put({
    type: "user/SET_STATE",
    payload: {
      id: "",
      name: "",
      role: "",
      email: "",
      username: "",
      wallet: "",
      avatar: "",
      currency: "",
      authorized: false,
      loading: false
    }
  });
}

export default function* rootSaga() {
  yield all([
    takeEvery(actions.LOGIN, LOGIN),
    takeEvery(actions.LOAD_CURRENT_ACCOUNT, LOAD_CURRENT_ACCOUNT),
    takeEvery(actions.LOGOUT, LOGOUT),
    LOAD_CURRENT_ACCOUNT() // run once on app load to check user auth
  ]);
}
