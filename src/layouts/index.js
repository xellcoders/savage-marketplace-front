import React, { Fragment } from "react";
import { withRouter, Redirect, matchPath, Link } from "react-router-dom";
import { connect } from "react-redux";
import NProgress from "nprogress";
import { Layout } from 'antd'
import { Helmet } from "react-helmet";
import { useWalletProvider } from "providers/wallet";
import Connect from "components/Connect";
import { routes } from "../router";
import styles from "./style.module.sass";

const { Header, Content } = Layout;

const mapStateToProps = ({ user }) => ({ user });
let previousPath = "";

const MainLayout = ({ user, children, location: { pathname, search } }) => {

  const currentPath = pathname + search;
  if (currentPath !== previousPath) NProgress.start();

  setTimeout(() => {
    NProgress.done();
    previousPath = currentPath;
    window.scrollTo(0, 0);
  }, 300);

  const isUserLoading = user.loading;
  const route = routes.find(r =>
    matchPath(pathname, { path: r.path, exact: r.exact, strict: false })
  );
  const { connected } = useWalletProvider();

  const BootstrappedLayout = () => {
    if(isUserLoading) return null;
    if(route && route.roles && !route.roles.includes(user?.role)) return <Redirect to="/login" />;
    if(route && route.useWallet && !connected ) return <Connect />;
    return (
      <Layout>
        <Header className={styles.header}>
          <Link to="/">Neon Savage</Link>
        </Header>
        <Content>
          {children}
        </Content>
      </Layout>
    )
  };

  return (
    <Fragment>
      <Helmet titleTemplate="%s" title="Savage Marketplace" />
      {BootstrappedLayout()}
    </Fragment>
  );
};

export default withRouter( connect( mapStateToProps )( MainLayout ) );
