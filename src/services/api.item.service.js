import { get, put } from "../util/api";

export async function getActiveItems( filters ) {
  if( !filters ) return get( `/item` );
  return get( `/item?filters=${filters}` );
}

export async function getUserItems( user ) {
  return get( `/item/user/${user}` );
}

export async function getPreferredItems( filters ) {
  if( !filters ) return get( `/item/preferred` );
  return get( `/item/preferred?filters=${filters}` );
}

export async function getLikedItems( filters ) {
  if( !filters ) return get( `/item/liked` );
  return get( `/item/liked?filters=${filters}` );
}

export async function getItem( id ) {
  return get( `/item/${id}` );
}

export async function putOnSale( id, values ) {
  return put( `/item/${id}/sell`, values );
}

export async function putOnAuction( id, values ) {
  return put( `/item/${id}/auction`, values );
}

export async function putOnGame( id, values ) {
  return put( `/item/${id}/game`, values );
}

export async function transferItem( id, values ) {
  return put( `/item/${id}/transfer`, values );
}

export async function buyItem( id, wallet, address ) {
  return put( `/item/${id}/buy`, { wallet, address } );
}

export async function bidItem( id, values, wallet, address ) {
  return put( `/item/${id}/bid`, { ...values, wallet, address } );
}

export async function getComments( id ) {
  return get( `/item/${id}/comments` );
}

export async function getRating( id ) {
  return get( `/item/${id}/rating` );
}

export async function addComment( id, comment ) {
  return put( `/item/${id}/comment`, { comment } );
}

export async function rateItem( id, score ) {
  return put( `/item/${id}/rate`, { score } );
}


export async function toggleLikeItem( id ) {
  return put( `/item/${id}/like` );
}

export async function getOwned() {
  return get( `/item/owned` );
}

export async function getOnSale( filters ) {
  if( !filters ) return get( `/item/onsale` );
  return get( `/item/onsale?filters=${filters}` );
}

export async function getOnAuction( filters ) {
  if( !filters ) return get( `/item/onauction` );
  return get( `/item/onauction?filters=${filters}` );
}

export async function getUserOnSale( filters ) {
  if( !filters ) return get( `/item/onsale/user` );
  return get( `/item/onsale/user?filters=${filters}` );
}

export async function getUserOnAuction( filters ) {
  if( !filters ) return get( `/item/onauction/user` );
  return get( `/item/onauction/user?filters=${filters}` );
}

export async function getOnGame() {
  return get( `/item/ongame` );
}

export async function getSold() {
  return get( `/item/sold` );
}

export async function getBought() {
  return get( `/item/bought` );
}

export async function getTopItems() {
  return get( `/item/top` );
}