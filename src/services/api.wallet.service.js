import { get } from "../util/api";

export async function getBalance( address, currency, network ) {
  return get( `/wallet/balance/${address}?currency=${currency}&network=${network}` );
}

export const a = 3;