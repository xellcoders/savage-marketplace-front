import axios from "axios";
import { get, post } from "../util/api";

export async function login( email, password ) {
  return post( '/auth/login', { email, password } ).catch( error => ({ error }) );
}

export async function currentAccount() {
  return get( '/auth/current' ).catch( () => ({}) );
}

export async function exchangeRates() {
  return axios.get( 'https://api.apilayer.com/exchangerates_data/convert?to=MXN&from=USD&amount=1', { headers: { apikey: '1iESVc01vfgCpjcnjzn93Yjb561WQCcX' } } )
      .then( result => result.data.info.rate )
      .catch( () => 20 );
}

export async function logout() {
  localStorage.removeItem("token");
  return true;
}
