import { upload, get } from "../util/api";

export async function createNFT( file, data, onCreateProgress ) {
  delete data.file;
  return upload( '/nft', [file], data, onCreateProgress );
}

export async function getCollections() {
  return get( '/nft/collections' );
}

export const a = 3;