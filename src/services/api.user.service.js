import { get, post, put } from "util/api";

export async function getUsers( filters={ role: 'all', status: 'all' } ) {
  return get( `/user/?filters=${Buffer.from(JSON.stringify(filters)).toString('base64')}` );
}

export async function registerUser( data, isDeveloper ) {
  return post( `/user/`, {...data, isDeveloper } );
}

export async function requestDeveloper( email ) {
  return put( `/user/developer`, { email } );
}

export async function setUsername( username, avatar ) {
  return put( `/user/username`, { username, avatar } );
}

export async function updatePassword( password ) {
  return put( `/user/password`, { password } );
}

export async function updateCurrency( currency ) {
  return put( `/user/currency`, { currency } );
}

export async function updateRole( id, role ) {
  return put( `/user/role/${id}`, { role } );
}

export async function updateStatus( id, status ) {
  return put( `/user/status/${id}`, { status } );
}

export async function setInterest( interest ) {
  return put( `/user/interest`, { interest } );
}

export async function getUser( id ) {
  return get( `/user/${id}` );
}

export async function loadUsers() {
  return get( "/user" );
}

export async function getTopCreators() {
  return get( `/user/topcreators` );
}

export async function getTopUsers() {
  return get( `/user/topusers` );
}
