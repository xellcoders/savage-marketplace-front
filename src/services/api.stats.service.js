import { get } from "../util/api";

export async function getUserStats( id ) {
  return get( `/stats/user/${id}` );
}

export async function getCurrentUserStats() {
  return get( `/stats/user` );
}

export async function getItemsStats() {
  return get( `/stats/items` );
}

export async function getUsersStats() {
  return get( `/stats/users` );
}