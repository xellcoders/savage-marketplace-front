import React from "react";

const BlueArrowIcon = () => (
  <svg
    width="12"
    height="7"
    viewBox="0 0 12 7"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M10.8048 0.428336L11.6533 1.27686L6.00916 6.92102L0.36001 1.27187L1.20854 0.423339L6.00916 5.22396L10.8048 0.428336Z"
      fill="#00B7C4"
    />
  </svg>
);

export default BlueArrowIcon;
