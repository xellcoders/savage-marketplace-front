import React from "react";

const ArrowIcon = ({ className }) => (
  <svg
    className={className}
    width="10"
    height="16"
    viewBox="0 0 10 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M7.70371 7.99648L0.957223 1.24999L1.75272 0.454498L9.2947 7.99648L1.74569 15.5455L0.950195 14.75L7.70371 7.99648Z"
      fill="#E5FFFF"
    />
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M7.70371 7.99648L0.957223 1.24999L1.75272 0.454498L9.2947 7.99648L1.74569 15.5455L0.950195 14.75L7.70371 7.99648Z"
      fill="#E5FFFF"
    />
  </svg>
);

export default ArrowIcon;
