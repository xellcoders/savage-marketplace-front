import React from "react";
import styles from "./PurpleHeader.module.sass";

const PurpleHeader = ({ title }) => {
  return (
    <header className={styles.header}>
      <p>{title || "Marketplace"}</p>
    </header>
  );
};

export default PurpleHeader;
