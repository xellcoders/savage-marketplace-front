import React from "react";
import cn from "classnames";
import styles from "./Input.module.sass";

function Input({
  children,
  name,
  onChange,
  disabled,
  onPaste,
  onBlur,
  placeholder,
  id,
  value,
  className,
  type = "text",
  label
}) {
  return (
    // eslint-disable-next-line
    <label className={styles.label}>
      {label}
      {/* eslint-disable-next-line */}
      <input
        className={cn(styles.Input, className)}
        name={name}
        onChange={onChange}
        disabled={disabled}
        onPaste={onPaste}
        onBlur={onBlur || 0}
        placeholder={placeholder}
        id={id}
        value={value}
        type={type}
      >
        {children}
      </input>
    </label>
  );
}
export default Input;
