import React from "react";
import { useHistory } from "react-router-dom";
import cn from "classnames";
import { Button, Modal, Progress } from "antd";
import LoaderCircle from "../LoaderCircle";
import style from "./Loading.module.sass";

export default function( { status, percent, error=false } ) {
  return (
    <Modal className={style.modal} centered visible closable={false} footer={null}>
      <div className={style.content}>
        {percent ?
          <Progress className={style.percent} type="circle" status={error ? "exception" : ""} percent={percent} trailColor="rgba(0,0,0,0.2)" /> :
          <LoaderCircle className={style.loader} />
        }
        <div className={style.status}>{status || 'Cargando...'}</div>
      </div>
    </Modal>
  )
}

export function Success( { text, link } ) {
  const history = useHistory();
  return (
    <Modal className={style.modal} centered visible closable={false} footer={null}>
      <div className={cn(style.content, style.success)}>
        <Progress className={style.percent} width={60} type="circle" percent={100} trailColor="rgba(0,0,0,0.2)" /> :
        <div className={style.status}>{text}</div>
        <Button
          onClick={() => {
            history.push( '/dashboard' )
            history.push( link )
          }}
          className={style.button}
        >
          Salir
        </Button>
      </div>
    </Modal>
  )
}