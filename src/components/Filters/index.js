import React, { useState } from "react";
import cn from "classnames";
import { Button, Checkbox, Col, Empty, Input, Rate, Row, Select } from "antd";
import Modal from "react-modal";
import OutsideClickHandler from "react-outside-click-handler";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import BlueArrowIcon from "../Icons/BlueArrowIcon";
import styles from "./Filters.module.sass";
import Chevron from "../Dropdown/chevron";

Modal.setAppElement("#root");

function Filter({ title, children }) {
  const [open, setOpen] = useState(false);

  return (
    <div className={styles.filter}>
      <button
        type="button"
        className={styles.title}
        onClick={() => setOpen(!open)}
      >
        {title}
        <div
          className={
            open ? styles.arrowIconActivate : styles.arrowIconDeactivate
          }
        >
          <BlueArrowIcon />
        </div>
      </button>
      <div className={cn(styles.content, { [styles.show]: open })}>{children}</div>
    </div>
  );
}

const Filters = ({ close, isVisible, handleFilter = () => {} }) => {
  const [rarity, setRarity] = useState(["common", "very-common", "rare", "epic", "legendary", "mythical"]);
  const [minPrice, setMinPrice] = useState(0);
  const [maxPrice, setMaxPrice] = useState();
  const [game, setGame] = useState();
  const [creator, setCreator] = useState();
  const [collection, setCollection] = useState();
  const [tags, setTags] = useState();
  const [rating, setRating] = useState();
  const [loading, setLoading] = useState( false );

  const handleClose = () => {
    setLoading( false );
    close();
  };

  const filter = () => {
    const filters = {
      rarity,
      // eslint-disable-next-line no-restricted-globals
      minPrice: isNaN(minPrice) ? 0 : minPrice,
      // eslint-disable-next-line no-restricted-globals
      maxPrice: isNaN(maxPrice) ? Number.MAX_VALUE : maxPrice,
      game: game || 'all',
      creator: creator || 'all',
      collection: collection || 'all',
      tags: tags || [],
      rating: rating || 'all'
    };
    handleFilter( filters );
    setLoading( true );
    setTimeout(() => handleClose(), 500);
  }

  if (!isVisible) return <div> </div>;

  const rarities = [
    { label: "Común", value: "common" },
    { label: "Poco común", value: "very-common" },
    { label: "Raro", value: "rare" },
    { label: "Épico", value: "epic" },
    { label: "Legendario", value: "legendary" },
    { label: "Mítico", value: "mythical" }
  ];

  return (
    <Modal
      isOpen={isVisible}
      onRequestClose={handleClose}
      className={styles.modal}
      overlayClassName={styles.overlay}
    >
      <OutsideClickHandler onOutsideClick={close}>
        <div className={cn(styles.filters)}>
          <h5 className={styles.title}>
            <FontAwesomeIcon
              icon={faArrowLeft}
              className={styles.left_icon}
              color="#fff"
              onClick={close}
            />{" "}
            Filtrar
          </h5>
          <Filter title="Por rareza">
            <Checkbox.Group options={rarities} value={rarity} onChange={values => setRarity(values)} />
          </Filter>
          <Filter title="Por precio">
            <Row>
              <Col sm={24}>
                <Input type="number" placeholder="Mínimo" value={minPrice} onChange={e => setMinPrice( e.target.value )} className={styles.input} />
              </Col>
              <Col sm={24}>
                <Input type="number" placeholder="Máximo" value={maxPrice} onChange={e => setMaxPrice( e.target.value )} className={styles.input} />
              </Col>
            </Row>
          </Filter>
          <Filter title="Por juego">
            <Row>
              <Col sm={24}>
                <Input placeholder="Nombre del juego" value={game} onChange={e => setGame( e.target.value )} className={styles.input} />
              </Col>
            </Row>
          </Filter>
          <Filter title="Por creador">
            <Row>
              <Col sm={24}>
                <Input placeholder="Nombre del creador" value={creator} onChange={e => setCreator( e.target.value )} className={styles.input} />
              </Col>
            </Row>
          </Filter>
          <Filter title="Por nombre de la colección">
            <Row>
              <Col sm={24}>
                <Input placeholder="Nombre de la colección" value={collection} onChange={e => setCollection( e.target.value )} className={styles.input} />
              </Col>
            </Row>
          </Filter>
          <Filter title="Por tags">
            <Select
              mode="tags"
              placeholder="Ej. #Aventura"
              className="w-100"
              notFoundContent={<Empty description="No hay ningún tag" image={Empty.PRESENTED_IMAGE_SIMPLE} className="ant-empty-small" />}
              suffixIcon={<Chevron />}
              value={tags}
              onChange={value => setTags( value )}
            />
          </Filter>
          <Filter title="Por rating">
            <Rate allowHalf className={styles.rate} value={rating} onChange={value => setRating( value )} />
          </Filter>
          <Button loading={loading} className={styles.button} onClick={() => filter()}>
            Aplicar
          </Button>
        </div>
      </OutsideClickHandler>
    </Modal>
  );
};

export default Filters;
