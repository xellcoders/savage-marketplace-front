import React, { useMemo, useState } from 'react';
import { connect } from "react-redux";
import cn from "classnames";
import numeral from "numeral";
import styles from './Payment.module.sass'
import { getBalance } from "../../services/api.wallet.service";
import { useWalletProvider } from "../../providers/wallet";
import Loading from "../Loading";

export function Payment({ user, selectedWallet = 'nsc', setSelectedWallet, nsc = 0, eth = 0 }){
  const { account } = useWalletProvider();
  const [loading, setLoading] = useState( true );
  const [balance, setBalance] = useState( 0 );

  useMemo( () => {
    getBalance(account, 'eth', 'ethereum').then( result => {
      setBalance( result );
      setLoading( false );
    } ).catch( () => setLoading( false ) )
  }, [account] );

  if( loading ) return <Loading status="Cargando..." />;

  return (
    <div className={styles.container}>
      <div
        className={cn(styles.wallet, { [styles.active]: selectedWallet === 'nsc' })}
        role="button"
        tabIndex={0}
        onKeyDown={() => {}}
        onClick={() => setSelectedWallet( 'nsc' )}
      >
        Savage <b>NSC</b>
        <b className={styles.balance}>${numeral(nsc * user.nsc).format('##,###.00')} MXN</b>
        <img src="/images/icons/nsc_circle.png" alt="NSC" />
      </div>
      <div
        className={cn(styles.wallet, { [styles.active]: selectedWallet === 'eth' })}
        role="button"
        tabIndex={0}
        onKeyDown={()=>{}}
        onClick={() => setSelectedWallet( 'eth' )}
      >
        Ethereum <b>ETH</b>
        <b className={styles.balance}>${numeral((eth || balance) * user.eth * user.dollar).format('##,###.00')} MXN</b>
        <img src="/images/icons/eth_circle.png" alt="ETH" />
      </div>
    </div>
  )
}

export default connect(({ user }) => ({ user }))( Payment );