import React, { useEffect } from "react";
import { Button, Modal, Progress } from "antd";
import { useWalletProvider } from "providers/wallet";
import { useHistory } from "react-router-dom";
import LoaderCircle from "../LoaderCircle";
import style from "./Connect.module.sass";

export default function() {
  const history = useHistory();
  const {connecting, connected, error, connect} = useWalletProvider();

  useEffect( () => {
    connect();
  // eslint-disable-next-line
  }, [] );
  return (
    <Modal className={style.modal} centered visible onCancel={() => history.push('/login')} footer={null}>
      <div className={style.content}>
        {!connecting && error && error.code > 0 &&
          <>
            <Progress width={80} className={style.error} type="circle" status="exception" percent={100} trailColor="rgba(0,0,0,0.2)" />
            <div className={style.status}>
              Error al conectar
              <span>El intento de conexión falló. Haga clic en intentarlo de nuevo y siga los pasos para conectarse en su billetera.</span>
            </div>
            <Button onClick={() => connect()} className={style.button}>Voler a intentar</Button>
          </>
        }
        {(connecting || error?.code === -32002) &&
          <>
            <LoaderCircle className={style.loader} />
            <div className={style.status}>Conectando...</div>
          </>
        }
        {!connected && !connecting && !error &&
          <>
            <img src="/metamask.svg" alt="Metamask" width={100} />
            <div className={style.status}>
              Conectar Wallet
              <span>Haga click en el siguiente boton para conectar su wallet (MetaMask)</span>
            </div>
            <Button onClick={() => connect()} className={style.button}>Conectar</Button>
          </>
        }
      </div>
    </Modal>
  )
}