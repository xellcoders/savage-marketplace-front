import React, { useEffect, useState } from 'react';
import moment from "moment";
import { Link } from "react-router-dom";
import { getUserTransactions } from "services/api.tx.service";
import Loader from "components/Loader";
import styles from './Movements.module.sass'
import Price from "../Price";

const types = {
  onauction: {
    icon: 'auction',
    title: 'Mandaste a subasta'
  },
  onsale: {
    icon: 'sell',
    title: 'Pusiste en venta'
  },
  transfer: {
    icon: 'sold',
    title: 'Transferiste'
  },
  receive: {
    icon: 'sold',
    title: 'Recibiste'
  },
  ongame: {
    icon: 'game',
    title: 'Colocaste en juego'
  },
  buy: {
    icon: 'sold',
    title: 'Compraste'
  },
  bid: {
    icon: 'offer',
    title: 'Recibiste oferta de'
  },
  sell: {
    icon: 'sold',
    title: 'Vendiste'
  },
  win: {
    icon: 'mission',
    title: 'Completaron misión'
  }
}

export default function({ showTitle = true }) {
  const [loading, setLoading] = useState(true);
  const [movements, setMovements] = useState([]);

  const init = () => {
    getUserTransactions().then(transactions => {
      setMovements(transactions);
      setLoading(false);
    });
  };

  useEffect(() => {
    init();
    // eslint-disable-next-line
  }, []);

  return (
    <div className={styles.wrapper}>
      {showTitle &&
        <div className={styles.header}>
          <span>Tus movimientos NFTs</span>
          <Link to="/movements">Ver todo <small>&rsaquo;</small></Link>
        </div>
      }
      {loading && <Loader className={styles.loading} />}
      {!loading && movements.map( move => (
        <div className={styles.item} key={move.created_at}>
          <div className={styles.icon}><img src={`/images/icons/moves/${types[move.operation].icon}.svg`} alt="" /></div>
          <div className={styles.title}>
            {types[move.operation].title}
            <div className={styles.subtitle}>{move.item.nft.name}</div>
          </div>
          <div className={styles.price}>
            <Price item={{ ...move, currency: move.item.currency }} />
            <div className={styles.date}>{moment(move.created_at).format('DD/MM/YYYY')}</div>
          </div>
        </div>
      ) )}
    </div>
  )
}