import React from "react";
import { Doughnut } from "react-chartjs-2";
import Loader from "components/Loader";
import styles from "./Chart.module.sass"

export default function({ title, items=[], loading=false }) {
  const total = items.reduce( (a, i) => a + i.value, 0 );
  const doughnutData = {
    labels: items.map( item => item.label ),
    datasets: [
      {
        borderWidth: 0,
        data: items.map( item => item.value ),
        backgroundColor: items.map( item => item.color ),
        hoverBackgroundColor: items.map( item => item.color ),
      },
    ],
  }
  const doughnutOptions = {
    cutoutPercentage: 80,
    rotation: 1.7 * Math.PI,
    legend: {
      display: false,
    }
  }

  return (
    <div className={styles.wrapper}>
      {loading &&
        <div className={styles.loading}>
          <Loader />
        </div>
      }
      <div className={styles.title}>{title}</div>
      <div className={styles.divider}> </div>
      <div className={styles.chart}>
        <Doughnut data={doughnutData} options={doughnutOptions} width={182} height={173} />
      </div>
      <div className={styles.total}><b>{total}</b> NFTs</div>
      {items.map(item => (
        <div className={styles.item} key={item.label}>
          <svg width="18" height="17" viewBox="0 0 18 17" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M15.7172 8.99316C15.7172 12.2045 12.9333 14.9932 9.26435 14.9932C5.59537 14.9932 2.81152 12.2045 2.81152 8.99316C2.81152 5.78178 5.59537 2.99316 9.26435 2.99316C12.9333 2.99316 15.7172 5.78178 15.7172 8.99316Z"
              stroke={item.color}
              strokeWidth="4"
            />
          </svg>
          <div>{item.label}</div>
          <div>{item.value}</div>
        </div>
      ))}
    </div>
  )
}