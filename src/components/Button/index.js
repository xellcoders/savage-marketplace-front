import React from "react";
import cn from "classnames";
import { Button } from "antd";
import styles from "./Button.module.sass";

function StyledButton({ onClick, children, text, name, disabled, className = "" }) {
  return (
    <Button
      type="primary"
      size="large"
      htmlType="submit"
      name={name || "name"}
      onClick={onClick}
      disabled={disabled}
      className={cn(className, styles.button)}
    >
      {children}
      {text}
    </Button>
  );
}

export default StyledButton;
