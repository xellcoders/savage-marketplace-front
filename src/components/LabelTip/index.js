import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleInfo } from "@fortawesome/free-solid-svg-icons";
import styles from "./LabelTip.module.sass";

export default function( {label, tip} ) {
  return (
    <>
      {label}{" "}
      <p
        className={styles.collection_tooltip}
        data-tip={tip}
      >
        <FontAwesomeIcon
          icon={faCircleInfo}
          className={styles.info_icon}
          color="#00B7C4"
        />
      </p>
    </>
  )
}