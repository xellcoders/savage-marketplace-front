import React from "react";
import cn from "classnames";
import styles from "./Checkbox.module.sass";

const Checkbox = ({ className, content, value, onChange, children }) => {
  return (
    <label className={cn(styles.checkbox, className)} htmlFor="checkbox">
      <input
        name="checkbox"
        className={styles.input}
        type="checkbox"
        onChange={onChange}
        checked={value}
      />
      <span className={styles.inner}>
        <span className={styles.tick} />
        <span className={styles.text}>
          {content}
          {children}
        </span>
      </span>
    </label>
  );
};

export default Checkbox;
