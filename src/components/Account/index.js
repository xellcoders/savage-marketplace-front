import React, { useEffect, useState } from "react";
import { Alert, Button, Col, Form, Input, Modal, Row, Select, Upload } from "antd";
import { LoadingOutlined } from "@ant-design/icons";
import { connect } from "react-redux";
import cn from "classnames";
import { getBase64 } from "util/files";
import validators from "util/validators";
import { setUsername, updateCurrency, updatePassword } from "services/api.user.service";
import styles from "./Account.module.sass";
import Chevron from "../Dropdown/chevron";

function Account({ dispatch, user, onClose }) {
  const [error, setError] = useState();
  const [success, setSuccess] = useState();
  const [editAvatar, setEditAvatar] = useState(false);
  const [editCurrency, setEditCurrency] = useState(false);
  const [editUsername, setEditUsername] = useState(false);
  const [editPassword, setEditPassword] = useState(false);
  const [uploading, setUploading] = useState(false);
  const [imageUrl, setImageUrl] = useState();

  const uploadButton = (
    <div className={styles.preview}>
      {uploading ?
        <LoadingOutlined /> :
        <img src="/images/icons/img.svg" width={50} style={{ marginTop: 25, marginLeft: 4 }} alt="Upload" />
      }
    </div>
  );

  const handleChange = info => {
    setUploading( true );
    getBase64(info.file, url => {
      setImageUrl(url);
      setUploading( false );
      setEditAvatar( true );
    });
  };

  const submit = ({ username, password, currency }) => {
    if( editAvatar ) {
      setUsername( username, imageUrl ).then( () => {
        setSuccess( 'Cambiaste tu avatar, exitosamente' )
        dispatch( { type: "user/LOAD_CURRENT_ACCOUNT" } );
      } ).catch( e => {
        setError( {
          message: 'Error al establecer el avatar',
          description: `${e.message} (code: ${e.code})`,
        } )
      } );
    } else if( editUsername && username ) {
      setUsername( username, imageUrl ).then( () => {
        setSuccess( 'Cambiaste tu nombre de usuario, exitosamente' )
        dispatch( { type: "user/LOAD_CURRENT_ACCOUNT" } );
      } ).catch( e => {
        setError( {
          message: 'Error al establecer el nombre de usuario',
          description: `${e.message} (code: ${e.code})`,
        } )
      } );
    }
    if( editPassword && password ) {
      updatePassword( password ).then( () => {
        setSuccess( 'Cambiaste tu contraseña, exitosamente' )
      } ).catch( e => {
        setError( {
          message: 'Error al cambiar la contraseña',
          description: `${e.message} (code: ${e.code})`,
        } )
      } );
    }
    if( editCurrency && currency ){
      updateCurrency( currency ).then( () => {
        setSuccess( 'Cambiaste la conversión de moneda, exitosamente' )
        dispatch( { type: "user/LOAD_CURRENT_ACCOUNT" } );
      } ).catch( e => {
        setError( {
          message: 'Error al actualizar la conversión de moneda',
          description: `${e.message} (code: ${e.code})`,
        } )
      } );
    }
  }

  const logout = () => {
    localStorage.removeItem('token');
    window.location = '/';
  }

  useEffect( () => {
    setImageUrl( user.avatar );
  }, [user] )

  return (
    <Modal className={styles.modal} onCancel={onClose} centered visible footer={null}>
      <div className={styles.content}>
        <h5>Tu cuenta</h5>
        <div style={{ color: '#FFF' }}>{user.wallet}</div>
        <Form
          layout="vertical"
          onFinish={submit}
          className={cn(styles.form, 'auth-form')}
          hideRequiredMark
          initialValues={{
            username: user.username,
            currency: user.currency
          }}
        >
          <Row gutter={[16, 0]}>
            <Col className={styles.upload}>
              <Upload
                accept=".jpg,.jpeg,.png"
                name="avatar"
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                beforeUpload={() => false}
                onChange={handleChange}
              >
                {imageUrl ?
                  <div className={styles.image}>
                    <figure>
                      <img src={imageUrl} alt="avatar" style={{ width: '100%' }} />
                    </figure>
                  </div> : uploadButton
                }
                {imageUrl ?
                  <div className={styles.edit} role="button" tabIndex={0} onKeyDown={()=>{}} onClick={e => { setImageUrl( null ); e.stopPropagation(); }}>
                    Editar
                    <svg width="14" height="14" viewBox="0 0 14 14" fill="#00B7C4" xmlns="http://www.w3.org/2000/svg">
                      <path fillRule="evenodd" clipRule="evenodd" d="M13.8475 3.40053L10.6 0.152122L0.398926 10.351V13.5993H3.6454L10.5991 6.64782L10.6 6.64866L13.8475 3.40053ZM10.5988 4.95048L12.1499 3.4L10.5989 1.849L9.04784 3.39974L10.5988 4.95048ZM8.19922 4.24817L1.59893 10.847V12.399H3.14693L9.75004 5.79882L8.19922 4.24817Z" fill="#00B7C4" />
                    </svg>

                  </div> :
                  <div className={styles.edit} role="button" tabIndex={0} onKeyDown={()=>{}} onClick={e => { setImageUrl( user.avatar ); setEditAvatar( false ); e.stopPropagation(); }}>
                    Cancelar
                  </div>
                }
              </Upload>
            </Col>
            <Col sm={24}>
              <Form.Item
                hasFeedback
                label="Nombre de usuario"
                name="username"
              >
                <Input size="large" placeholder="" className={styles.input} disabled={!editUsername} />
              </Form.Item>
              <span
                className={cn(styles.edit, styles.right)}
                tabIndex={0}
                role="button"
                onKeyDown={()=>{}}
                onClick={() => setEditUsername( !editUsername )}
              >
                {editUsername ? 'Cancelar' : 'Editar'}
              </span>
            </Col>
            <Col sm={24}>
              <Form.Item
                hasFeedback
                label="Contraseña"
                name="password"
                rules={[editPassword ? { required: true, message: 'La contraseña es obligatoria' } : {}]}
              >
                <Input type="password" size="large" placeholder="*********" disabled={!editPassword} className={styles.input} />
              </Form.Item>
              <span
                className={cn(styles.edit, styles.right)}
                tabIndex={0}
                role="button"
                onKeyDown={()=>{}}
                onClick={() => setEditPassword( !editPassword )}
              >
                {editPassword ? 'Cancelar' : 'Editar'}
              </span>
            </Col>
            {editPassword &&
              <Col sm={24}>
                <Form.Item
                  hasFeedback
                  label="Contraseña"
                  name="confirm"
                  rules={[{ required: true, message: 'Por favor confirme su contraseña' }, validators.password]}
                >
                  <Input type="password" size="large" placeholder="*********" disabled={!editPassword} className={styles.input} />
                </Form.Item>
              </Col>
            }
          </Row>
          <div className={styles.divider}> </div>
          <Row gutter={[16, 0]}>
            <Col sm={24}>
              <Form.Item hasFeedback label="Conversión de moneda" name="currency">
                <Select size="large" className="w-100" suffixIcon={<Chevron />} onChange={() => setEditCurrency(true)}>
                  <Select.Option value="mxn">Peso Mexicano</Select.Option>
                  <Select.Option value="usd">Dolar Americano</Select.Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Button htmlType="submit" disabled={!editUsername && !editPassword && !editCurrency && !editAvatar} className={styles.button}>
            Guardar cambios
          </Button>
          <Button onClick={() => logout()} className={styles.cancel}>Cerrar sesión</Button>
          {error &&
            <Alert message={error.message} description={error.description} type="error" className={styles.error} showIcon closable />
          }
          {success &&
            <Alert message={success} type="success" className={styles.success} showIcon />
          }
        </Form>
      </div>
    </Modal>
  )
}

export default connect(({ user }) => ({ user }))( Account );