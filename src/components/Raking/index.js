import React from "react";
import styles from "./Raking.module.sass";

const Raking = ({ raking }) => {
  switch (raking) {
    case 5:
      return (
        <div>
          <Start fill />
          <Start fill />
          <Start fill />
          <Start fill />
          <Start fill />
        </div>
      );
    case 4:
      return (
        <div>
          <Start fill />
          <Start fill />
          <Start fill />
          <Start fill />
          <Start />
        </div>
      );
    case 3:
      return (
        <div>
          <Start fill />
          <Start fill />
          <Start fill />
          <Start />
          <Start />
        </div>
      );
    case 2:
      return (
        <div>
          <Start fill />
          <Start fill />
          <Start />
          <Start />
          <Start />
        </div>
      );
    case 1:
      return (
        <div>
          <Start fill />
          <Start />
          <Start />
          <Start />
          <Start />
        </div>
      );
    default:
      return (
        <div>
          <Start />
          <Start />
          <Start />
          <Start />
          <Start />
        </div>
      );
  }
};

export default Raking;

function Start({ fill = false }) {
  return (
    <>
      {fill ? (
        <svg
          width="14"
          height="13"
          viewBox="0 0 14 13"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
          className={styles.svg}
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M3.78509 8.56595L3.02639 12.9071L6.99902 10.8575L10.9717 12.9071L10.2129 8.56595L13.4269 5.49153L8.98534 4.85816L6.99902 0.908447L5.0127 4.85816L0.571167 5.49153L3.78509 8.56595ZM6.99902 9.50719L4.62469 10.7322L5.07745 8.14158L3.18469 6.33098L5.80299 5.95761L6.99902 3.57935L8.19505 5.95761L10.8133 6.33098L8.92059 8.14158L9.37335 10.7322L6.99902 9.50719Z"
            fill="#00B7C4"
          />
        </svg>
      ) : (
        <svg
          width="14"
          height="13"
          viewBox="0 0 14 13"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
          className={styles.svg}
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M3.78509 8.56595L3.02639 12.9071L6.99902 10.8575L10.9717 12.9071L10.2129 8.56595L13.4269 5.49153L8.98534 4.85816L6.99902 0.908447L5.0127 4.85816L0.571167 5.49153L3.78509 8.56595ZM6.99902 9.50719L4.62469 10.7322L5.07745 8.14158L3.18469 6.33098L5.80299 5.95761L6.99902 3.57935L8.19505 5.95761L10.8133 6.33098L8.92059 8.14158L9.37335 10.7322L6.99902 9.50719Z"
            fill="#F2F2F5"
          />
        </svg>
      )}
    </>
  );
}
