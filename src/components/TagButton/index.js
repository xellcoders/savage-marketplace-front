import React from "react";
import cn from "classnames";
import styles from "./TagButton.module.sass";

function TagButton({ text, isActivate, className, onClick }) {
  return (
    <div className={cn(styles.container, className)}>
      <button
        type="button"
        className={cn({
          [styles.button]: true,
          [styles.activate]: isActivate
        })}
        onClick={onClick}
      >
        {text}
      </button>
    </div>
  );
}

export default TagButton;
