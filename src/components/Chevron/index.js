import React from 'react';

export default function(){
  return (
    <svg width="9" height="16" viewBox="0 0 9 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd" clipRule="evenodd" d="M6.55846 7.99687L0.554715 1.99312L1.61538 0.932465L8.67978 7.99687L1.60912 15.0675L0.548462 14.0069L6.55846 7.99687Z" fill="#01B7C3" />
      <path fillRule="evenodd" clipRule="evenodd" d="M6.55846 7.99687L0.554715 1.99312L1.61538 0.932465L8.67978 7.99687L1.60912 15.0675L0.548462 14.0069L6.55846 7.99687Z" fill="#01B7C3" />
    </svg>
  )
}