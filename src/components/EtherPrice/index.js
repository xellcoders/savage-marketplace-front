import React, { useEffect, useState } from 'react';
import { connect } from "react-redux";
import numeral from 'numeral';
import styles from './Price.module.sass'
import { getETHPrice } from "../../services/api.util.service";

export function EtherPrice( { user } ) {
  const [price, setPrice] = useState( 0 );
  const [change, setChange] = useState( 0 );

  useEffect(() => {
    getETHPrice().then( response => {
      setPrice( response.price );
      setChange( response.change );
    } );
  }, []);

  return (
    <div className={styles.wrapper}>
      <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M0 8C0 3.58172 3.58172 0 8 0C12.4183 0 16 3.58172 16 8C16 12.4183 12.4183 16 8 16C3.58172 16 0 12.4183 0 8Z" fill="#01B7C4" />
        <path d="M7.88495 5.44154C8.36014 5.44154 8.74536 5.05631 8.74536 4.58112C8.74536 4.10592 8.36014 3.7207 7.88495 3.7207C7.40975 3.7207 7.02453 4.10592 7.02453 4.58112C7.02453 5.05631 7.40975 5.44154 7.88495 5.44154Z" fill="white" />
        <path d="M10.2604 11.1446V12.1446H5.80225V11.1446H7.27079V7.42657H6.16045V6.42657H8.72912V11.1446H10.2604Z" fill="white" />
      </svg>
      <div className={styles.value}>Valor ETH hoy <b>${numeral(price * user.dollar).format('##,###.00')}</b></div>
      <div className={styles.percent}>{numeral(change).format('##.00')}%</div>
    </div>
  )
}

export default connect(({ user }) => ({ user }))( EtherPrice );