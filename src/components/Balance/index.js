import React, { useMemo, useState } from 'react';
import numeral from 'numeral';
import { connect } from "react-redux";
import { getBalance } from "services/api.wallet.service";
import styles from './Balance.module.sass'
import { useWalletProvider } from "../../providers/wallet";

export function Balance({ user, color, title, network, currency, image }){
  const { account } = useWalletProvider();
  const [loading, setLoading] = useState( true );
  const [balance, setBalance] = useState( 0 );

  useMemo( () => {
    getBalance(account, currency, network).then( result => {
      setBalance( result );
      setLoading( false );
    } ).catch( () => {
      setLoading( false );
    } )
  // eslint-disable-next-line
  }, [account] );
  console.log(loading);
  return (
    <div className={styles.wrapper} style={{background: color}}>
      <div className={styles.title}>{title}</div>
      <div className={styles.amount}>${numeral(balance * user.eth * user.dollar).format('##,###.00')}<small>MXN</small></div>
      {image && <img src={image} alt="" />}
    </div>
  )
}

export default connect(({ user }) => ({ user }))( Balance );