import React from 'react';
import { getItemImage } from "util/files";
import styles from './Grid.module.sass'
import { history } from '../../index'

export default function({items, suffixTitle}){
  return (
    <>
      <div className={styles.header}>
        <span>{items.length} NFTs </span>{suffixTitle}
      </div>
      <div className={styles.grid}>
        {items.map( item => (
          <div
            /* eslint-disable-next-line */
            key={item._id}
            className={styles.item}
            role="button"
            tabIndex={0}
            onKeyDown={()=>{}}
            /* eslint-disable-next-line */
            onClick={() => history.push( `/nft/${item._id}` )}
          >
            <figure>
              {item.nft.image && <img src={getItemImage(item.nft)} alt={item.nft.name} />}
            </figure>
            <div>
              <div>
                <span className={styles.title}>{item.nft.name}</span>
                {/* eslint-disable-next-line no-underscore-dangle */}
                <span className={styles.subtitle}>{item.nft.videogame || 'N/A'}</span>
              </div>
              <svg width="7" height="10" viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fillRule="evenodd" clipRule="evenodd" d="M4.54667 4.79062L0.950279 1.19423L1.79881 0.345703L6.24372 4.79062L1.79506 9.23928L0.946533 8.39075L4.54667 4.79062Z" fill="#01B4C3" />
                <path fillRule="evenodd" clipRule="evenodd" d="M4.54667 4.79062L0.950279 1.19423L1.79881 0.345703L6.24372 4.79062L1.79506 9.23928L0.946533 8.39075L4.54667 4.79062Z" fill="#01B4C3" />
              </svg>
            </div>
          </div>
        ) )}
      </div>
    </>
  )
}