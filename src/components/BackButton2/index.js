import React from "react";
import cn from "classnames";
import { useHistory } from "react-router-dom";
import styles from "./BackButton2.module.sass";

const BackButton2 = ({ children, text = "Regresar", to }) => {
  const history = useHistory();

  return (
    <button
      className={cn(styles.button)}
      type="button"
      onClick={() => {
        if( to ) history.push( to );
        else history.goBack();
      }}
    >
      <svg
        width="12"
        height="12"
        viewBox="0 0 14 14"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M2.44591 6.39988L7.32833 1.53673L6.48148 0.686523L0.148804 6.99421L6.48148 13.3019L7.32833 12.4517L2.45729 7.59988H13.599V6.39988H2.44591Z"
          fill="white"
        />
      </svg>
      <p className={styles.text}>{text}</p>
      {children}
    </button>
  );
};

export default BackButton2;
