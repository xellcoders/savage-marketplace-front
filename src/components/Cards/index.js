import { Col, Row } from "antd";
import React, { useState } from "react";
import cn from "classnames";
import styles from "./Cards.module.sass";

export default function Cards({items=[], onChange = () => {}}) {
  const [selectedIndex, setSelectedIndex] = useState( -1 );

  const changeSelectedIndex = index => {
    setSelectedIndex( index );
    onChange( index );
  }
  const onKeyDown = e => {
    if(e.key === 'ArrowRight')
      changeSelectedIndex( selectedIndex < items.length - 1 ? selectedIndex + 1 : 0 )
    if(e.key === 'ArrowLeft')
      changeSelectedIndex( selectedIndex > 0 ? selectedIndex - 1 : items.length - 1 )
  }

  return (
    <Row className={styles.wrapper}>
      {items.map( (item, index) => (
        <Col xs={12} md={6} key={item.label}>
          <div
            role="button"
            tabIndex={0}
            onKeyDown={onKeyDown}
            className={cn(styles.card, {[styles.active]: selectedIndex === index})}
            onClick={() => changeSelectedIndex(index)}
          >
            <img src={item.image} alt={item.label} />
            <div>{item.label}</div>
          </div>
        </Col>
      ) )}
    </Row>
  )
}