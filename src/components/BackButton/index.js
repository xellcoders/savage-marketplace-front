import React from "react";
import { useHistory } from "react-router-dom";
import cn from "classnames";
import styles from "./BackButton.module.sass";

const BackButton = ({ children, text = "Regresar", redirectTo, className }) => {
  const history = useHistory();

  return (
    <button
      className={cn(styles.button, className)}
      type="button"
      onClick={() => {
        history.push(redirectTo);
      }}
    >
      <svg
        width="14"
        height="14"
        viewBox="0 0 14 14"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M2.44591 6.39988L7.32833 1.53673L6.48148 0.686523L0.148804 6.99421L6.48148 13.3019L7.32833 12.4517L2.45729 7.59988H13.599V6.39988H2.44591Z"
          fill="white"
        />
      </svg>
      <p>{text}</p>
      {children}
    </button>
  );
};

export default BackButton;
