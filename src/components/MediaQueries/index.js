// Packages
import { useMediaQuery } from "react-responsive";

export const MediaQuery = ({ children, minWidth, maxWidth }) => {
  const isMediaQuery = useMediaQuery({ minWidth, maxWidth });
  return isMediaQuery ? children : null;
};

export const MediaQueryMinWidth = ({ children, minWidth }) => {
  const isMediaQuery = useMediaQuery({ minWidth });
  return isMediaQuery ? children : null;
};
