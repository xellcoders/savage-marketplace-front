import React, { useEffect, useState } from "react";
import { Encoder, QRByte } from '@nuintun/qrcode';
import { Modal } from "antd";
import style from "./Share.module.sass";

export default function( { id, onCancel } ) {
  const [qrCode, setQRCode] = useState('');

  useEffect( () => {
    const qrcode = new Encoder();
    qrcode.setEncodingHint( true );
    qrcode.write( new QRByte( `https://savage-front.xellcoders.com/#/nft/${id}` ) );
    qrcode.make();
    setQRCode(qrcode.toDataURL());
  }, [id]);

  return (
    <Modal className={style.modal} centered visible closable={false} onCancel={onCancel} footer={null}>
      <div className={style.content}>
        <div>Comparte a un el QR de este NFT para que vea su detalle.</div>
        <img src={qrCode} width={300} alt="QR Code" />
        <div style={{ border: '1px dashed #CCC', background: 'rgba(0,0,0,0.5)', fontSize: 12, fontWeight: 500, marginTop: 15 }}>
          https://savage-front.xellcoders.com/#/nft/${id}
        </div>
      </div>
    </Modal>
  )
}