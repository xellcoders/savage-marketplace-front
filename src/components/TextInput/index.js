import React from "react";
import cn from "classnames";
import styles from "./TextInput.module.sass";

const TextInput = ({ className, label, Icon, classNameInput, ...props }) => {
  return (
    <div className={cn(styles.field, className)}>
      {label && <div className={styles.label}>{label}</div>}
      <div className={cn(styles.inputContainer, styles.wrap)}>
        {Icon}
        <input className={cn(styles.input, classNameInput)} {...props} />
      </div>
    </div>
  );
};

export default TextInput;
