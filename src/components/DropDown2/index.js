import React from "react";
import styles from "./DropDown2.module.sass";

const Dropdown = ({ label, value, options, onChange }) => {
  return (
    // eslint-disable-next-line
    <label className={styles.dropdown}>
      {label}
      <select value={value} onChange={onChange}>
        {options.map(option => (
          <option value={option.value}>{option.label}</option>
        ))}
      </select>
    </label>
  );
};

export default Dropdown;
