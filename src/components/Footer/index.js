import * as React from "react"

// eslint-disable
const SvgComponent = (props) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
    viewBox="0 0 1257.4 151.55"
    style={{ enableBackground: "new 0 0 1257.4 151.55" }}
    xmlSpace="preserve"
    {...props}
  >
    <style>{".st170{fill:#00fecf}.st921{fill:none}"}</style>
    <g id="layer_1">
      <image
        style={{ overflow: "visible", opacity: 0.4 }}
        width={1840}
        height={41}
        xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABzAAAAApCAYAAABa4L9XAAAACXBIWXMAAAsSAAALEgHS3X78AAAA GXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAFGJJREFUeNrsndty4zgOQEU5jpMP 2P//wHnfji8Sd1w1mXJ5bYkXAASlc566YwoEQfAKkRoGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIBOCJgAAAAAwDcx /sWcbe+T9vCf+LcfjFiiv+a7obKs+d9MdVfbcGs+Q5tzMHZgBQAAAADodj6LCQAAALbNDoMea5t1 BIGYQ/bCPSD0gRlM+wfKWMbdT2+41yY4bMhf5877hNGpnaPDuvY6N9pTAJlg+bND8AIBAACAygQL AACgbvXKabFUPAXSWi+wg7I9e91ACM7qe3Tur1sJTh8NfTk6rMctBG33OA5eFGR+ep3q9DQt2/OU 9OHf007mhq3GiHmnfmXR38eNt3dtnTV9M2zZjgRgEw3/z94H9gIAYGENAOBmcgrF48zoWLetj589 BnaeNxw4iaYPNl4YAhRlHzvWP3ZWP5/GZYqNbWqV/92ulw214ZMDX79stK2W9nvS+l8d2vbDIM9o 2Lamztp9ydy65dganbT9uLI2ih3U95xQ16370ehcprR+ocM+ob7QBEEBAMwGBACA/Jmkn+BgMJSt vajuAY/XYwUhfT0s5EYjXUPnfhsd6xk23AbHTm0mwUnIhlG4TqTkHZR8pqYsXxl5xooy7Zmfhd++ tjBddWLTaGzf2IntSvO4OrLLh7EdLOt2MvYVq/ZaGtzSKmeLF2dixlzMi36pPnsQlqtxSjomyLe+ qSY6GUM9juv/BiqlTm/e5RD8BICtQgATAPRnhOmBSYs+KSilldbBcvIZhr4CJBYcGtZNENI5OtLt HUfDvLTbvqcTvcGRLWvlWPRPFjb43Rw/SQ6vPU8NjPM7Ceqy59OZa+nvAbSfDfnfyZnePxtoqzHB 5paBipQN9tiwrYUKuS36Nss8Y8M81gKxo8PyzA7qNOcboV6Cwdr5WrxsML95dnTWJ8zKa6TaU+tr zwcrPQlUAsDeIIAJsBOMTzdab/5r9mmt+skeT3d5CVKGjDKOTvTzZGeN9us1gB069uvguF1K95uH DvxkiXtQRytQpSE3V+ZHpT7S5Xvku7C8pTZKec5bUEza3/8k1IlGXZtMp53k9SfD/yXKYb2J7Sm/ KKBXVCzzVmXHzmSmXj/aSs+lsU77KuKcgGasbKPW+rfKQ6ptzi/SHhrOP3N8wNOcISr4TrP5Sk1w NGfPkSAsAFguUAGgZlbh93uKQaDPsDyh0zJPzfRBSf/gzO7BQI9SHUdlHwqV+kRB/Uts9Grj5qjc frTk9frtx5BZhuBE1y3MZd/l/TlAa04Z/WRqP6c2Hdy4ftb6amLdtpdsdW5UHy394bNCB4kN+Yti Gb0FGdee+SjMI3ej/yak77vfD0plmAV0y00/CvjFXKFfWEgfBcr6Sr7mixFRwd+8teOlNL+nNg+C dhmUZM2Z6wLrU7Oxcg0TFXWTWGupz+sIdgIAAUwA7Z0H3SClh1OLmhvS1tcUSjJ2outYITs48uuD oE9pB7BHZZuMRm14rvR16bZTWp6D0GJsNGgvewiY5di9VdD5XT2clMursUlQk7/XoP9XZjlrNw61 Nu7X0n07XQ/+yUj7bejrQ+O2piXzT2E+35V6eTpF1kLuj5ANvgT6B+m+xbJvzMl7GmQ252tf/ss9 tT8p1c/ab2Ohvr+/xwobrgUza8sehvSgqMfx+51v1PYpB+W2O1XKXfJVaRuXBM1z0obKcpde6awx r3DzIt1joJJvegKA5oIVYPdUBiytvwGpddVry5ODNc/VnED7nXy3ODEYBGWHRnWWeup3flqgSZWh VM/c57QCxJZBy6Ognw4Kdtm6TvfNg0/h8cPqxYUt5vMxbOMbnVtYR6QE8jUCxh/D+omQpd8f/38U 0i23nIcV3WJGeSTtX1P3JXldlfV8p9+nQL4XBd/WvjpRWv4xUa7UtY73329GbVXiisKoaI+40K/k 2jTlb5OQ7Fpfrbm6tUWeY0LatZOZJXqFoS6YuWTDUGDnnDxD5phXc8W8Vn/a64sQUs+mvHSrMWbl 1HnMmGdv5QruIaENv3+I4CbAriCACVCzcs4LVoZG7Voz6FdTplGhrNp2fmVbieCoth8dHPtQDh8Z +QZF/5H49mBQlP1OZumbos9BS8k67jmgNSr6zrtnjsZtzuOcMQzy38QsKZvWdwx7WYzHwdcJzNMb O44ZNm59EuP+93ensUJn7fdn4bcv520jVffYuAyvZJ0HuU3Yk1A5ooEcDzqdFfuLk4BMz8+8+z3l metg/x3jO58FdohCZY9Gad/9bRaqvxS5Ej4i+UzIkFli60f5U4E9Y8KcTUrX57rTCqCOCnU9F+qS kiZUyHxMKzWWp5yWrj3larXuSJ/0EtwE2BwEMGF3CF3pGhq3w95PK0nKspDzbiM0CNvc8jnrAF/u c0Gg7MHIVhbPWJd/Gt6fLNuCf/fQlsKgd8Vz6ExGTZ1J6nLf8JC4rlf7mmpP+WrnmRpE1d7IOAz1 m4KSgfma8qZ8R63VJlbMrJNSJoPyRWH944L+qXm2Ciha6nYYdE9E5pwMtD7JpRHsKX0uZvQ3KX3O XPFsTXqNYJBkei3Zv0GPFrp4tYm07OffrYK1kra3eqYmj3nI/35siT6DoG+FxrpIzj2l5ldJcn4D m497wgQ7AfqAACbsDmdXvLa6wtV603gtvVdZzwtrqXoIijb38Gxt+Ubn9vRU96FC7rtnDpltqbaP qG3PmkG2kNkvSNrko7Ls2uNIK/mhsg5KnzkOOowV+s2C9aFdBq0Xvz6EbWD5LaHndEsnemKhz7Ra S6ZeZ3ocfJJzHetRwYdK5Dynv1bIS9n4/UxI73UjWFvGNVOORJDhWFiGHoNnS3+7De2DYR8FNuk1 3aRQ31JpDxuw/atrUKNB/1Kbx2hQ99J9jsbV+JZjj5bsmudbfmKgSB7BTAC/EMCETSJ0ylKyjYSG 6QMyim2bewVoIH1V+t+rc4KATKu0OTbwnDb8szA+CNVprb94SNuqfPd6+BDKt+Y5TpinjQVS5URm /jNagbwW6yNPQclaO6Refea5ruaKZ0sCj6OCzFfPWW+mWm3srl0x2Oo7kTnfxk35TTtwoRHoCMLl igV+n5uvdKBKQ6ZV3iEx/dYCs72lq02b2/f1qMdQ6cvaY9penpeeS5jJILgJ0OfCFMA1zgKYmsFL go7yMn7Tpm5ubzmoaK3TWpCnh+CThmyrsoXOfTZspF2Ow76vRvZWlt6vR5eQ5bFMccg7+W+xDvKg g7f1YO+bPdFhmaWuctvaqce1fkJbH6nvRe79BKVU+kmgnFLpgnF+OemkZMXBZ/AvNLbLq7/XypoF 2oxm3yDZr7QIAGuPnRLPh8q6lniu9tlaPzKflxLABNjughXAfpfBT+DS4jtTGleF1j7f20ZoaWAt N3/rgFeJ7FaBhNKgpcapSe10EjIl/CB0WO6W9f0ubVRqr8GorUn3RTV5xKHvly96Gctq5cRGuoWK MlnM2aKAXl7WZN7WhnEj+cZG+XO1q40MvqnoV7bHYMme0lEGvXQtruXdWlrpl1L2+KzUiczcZzSv 1k2fNBPUBNjtIhVAbqeAU5ierqHt6RSmdBCh5Dnr9F500Q6s1eiqdW2rp/xHgzx+/z8b2cnaphJp PbXPVnmEFwvMliczS0/6eTu52CLQKT2OS857NNZCIXFzJBjo4mWd2fPmjuTGXE1+Ejp4uhZWSk4P ZZLYSCd9WXoCMXppU+zt6epdizxblKW361m1/VWyDlpeyWpxklNLd835TPNTmP9OcAlcAjRbWAJs GoFgpmQQMw56AU3vV7lKby5abNyGQS5YkZve45WyEjLW/CMU6L6FgJZGfeReXToY2LW3tLl1Owu1 xVGgXe3xylepsXvJDyzGJasxrpVOErLiUH7Vr8a8r8W6yutabimQ3Ww54EieZQC01+Bkb3Je/X4f xycBOSVl6DEPDb00glEeg4ct83+c//ZU5pa+IV2e3k9KW8hOrQ/Nb2D2HnSUGkulx14TGQQtAdov LgF2idAJzdC4PS5twOdc9caJkrwgZk6eMSGfngMAWs+MhXmkBJ/mQt17Dj7n6n4QLHtu2tuwve/N lr7Y8KGYR22Zb8p9gvVzKePFscNx7iY0B2kVuExJfxz0Ao5X4/ngp/L09yooK6VekqflheXQvCmg 1l5Luk1G86xrhY1Ln5GQN21kuVlq/y1du6upS0wc9ySvRtS6TjF2pF9cabO9Bds96ZRSP7Mz/+m5 HcWFfYKW/V6rl240dWrxvctqWQQtAfxAABN2y28Ac2lQSghyBqftsWRTN3dzMlbKCpX6lpYtCthg zHhecgM+Cti5ZZDynf3XbDBm5FNyZd+Y+Uzu9YiHAlvl5DEOZYGoWUh+iY9s5bRgTR4xox1IXl8t 3S+Rp9y4rjHn8C5Tw44Sz2qskYJzedLcg7GXjv38VjjvalGfr569dL5UmzPmkcnLP61l5Yu/XZXk apZvHvRO2OScRJUObmzpO6GtdIlv1jfxRd3WBqy07CEZ/IoCPhuF2oRl0PwxjZVOWs/FhHVxTX+r df24tR5asrTG5SJ5BCoB/EMAE0BilHwf6AwdtG+tzb1auWFhgilth1qZ704Lzpl5WX9r9JCQZhLK NwjW0ZgpI3cDsuT05zyUf2syNa85IW2oaJ+vFm/P6Y4Lv0len5yS/lZQPq22Jd02lp77VCyrVjmv jfoN6b7n1W+fiuOitbxLg7mCtOyTwnzs3bNnxXnayXCe+KO4ftQoR3xTFynf1M75+9nIp29D+vfA U387G/iP5WbfTbh9SYwrMfM3Dxv6nvIqeW4twFBy4pVrk8vr5yboNzUnuKKyTw7CbfpVmriyHp0K 6kwyGDUktkuLlw+kx6lZQI7GizWWJx6l/CEq+ZjofINgJUCfEMAE0FzZL5/gTP0+kNXb3pIbiKUB R+3vNaY8V/O2fRDQNTTwhZDhD9rlKLH/mJlPzinQsbD8sUL3VNvHjHYdBNryq3SjkF9IBrNyr20O ynpLlTtW9NvagUBLXbx+6/k3UHNWHJ9qxgLNl5qsg5SpdfFYH9ZrJInx+uvh3z8O9Kktw3M5PF/Z fq6Ym7QeU9Z8JWbOpbzsPUwFZbKm9tSrl3JMCnpqnbK5KsvXlDdX5hkb1FMc3p8u1gha1zwnHWAM ynk+9sPT0Md10Z6vqo4LY9ysoKtle9XQNyqVq+mYR7ASYDsQwASQWJHIfE+zp/YbjPVc2+iJgvlp lS31lKaHq9NSTmi+019bp7XfS4LHMUOnsULPlMBpGOo2RuOK7Fz751xlXLvhXJI2txwxw2+0dJEu 0yOnyvy9BEQvDvvFHJmWpwBb5HMedG5DkJZzMpwreVhTebwy9GtlDBSfkjco47myj5QMznrwgT1s FsanOdtPY7tFx/WppdulU7++dtreJPT+/c59bhlqg4JaJ9kkgkMlpyxz1uNSzELlbnUib3bQnqKS D/XQn6vZlQAlwH4ggAmgMTuxDWjmnDKT7hOkTnREJf2eFwYebPxKZu4pubnSRhIb96NwPlFJzxR/ 0ApkzEL1Ewx01WBU0LXGBiXfX22ts5Zcj1dxe5VZ24a+Fn47C5et1RjvLc9nOd8Zz/1xtG76Lijv fxvpGgr8P7UOcsrypTBfS+HmoN//MapTy/nEs7xp8MN58EttPUzG7admrZCqo8XVrEu/R+W8NYJ5 NyE/vCn57FTho61Orw5GaTTketZNoy0Pxm1TWm+rfrv5+EAQE2AfEMAE8LIiWwl6rg3MjU6Beu1v gmN9JYOwz7JmgTylbCd1sm02LMsoJM8yYD8q+2HOlbmp6YOz9vvqat5glJdluaSeaXk16iHxmcmR zo+cFH3eqv5TuTgcmy2+K+lt3dX6RN6azXvfdIpPdtY6ebn0ss2Ztb3L9tT6VOSsINNC71TdPW7O zw10lpD5O7e6dtYeb871i2/8IzbUYXDozxKnAmOj/mEpLw9XbbvLl2AjAPS0kAaAnZERXPXcJ0l/ GygK22DMWHxobubX6BsqbadZltHAXyWvQh4NbBIa+LkH3+1Vj1dpHwOCk1GekmU8FMiZnJTt/nzK 6bQfw/HR88s/knl8FzzzM/SHlznVd+Fzf5Tb6Nrvx0x9JfrQIOiXnq6dlshjatg2ZoN2onE13+xI z1QZUvKtgoXaV/9aBYm0r4asPRnn6fT1mq6zcl1KMhu3/VJZrfuy2MCPJMa4LLkEFwGAhS0AbJoO Tn/SJ/5/QCs6t9tYkP/cqH5HRVtp1JPlCwUS1y+3Dhb2dnrcQmcCyTIyH585Oh2bNOrs4qTOPnc0 T7k01OXYgV1727S7DvvCug+8vxzzM8De/ZjN/DI71Nit5anKnDXL3FF9Raft4p2e0bB+Y8/t/zfg mLoXR4ASAFgMAACUzly3E/ykf99HOaVPKYRObRsd1F3YqJ/t5TpwAsPLpJzOPG/Ux6z1/CrQheBG Ot/KY4kHJgH/TT2ZKXHqUvOKaUnZU6Hc0r7DKigRncqy0Ck2LnN0Zg8v+r+Se+vYD+MG2lHvfhw6 tnF3cxOCkwCwdQgcAADsHIsg8qtJ9caC1xYny6ztFTu1naXNmEdt63SgZ/0+8MPu6u9G21j13R7G KPNpWcO8b0Y+5uU78/eTlpcBNPBw5WfAvpDAvJFyxD3kS6AOAICFOQAAgP/VGad2gfkNdsNGa9dy XrE1fuoEjat67xuYF6c+ciwoiyfmjn0xCJUxFPpkl9PqTvK7Ddtk6VveEz4FWR1gYXDvcW1NgBAA AFjcAgAAAAAkwgsLAF2s3UaqBnofbpzrVxVYJijRbA4zLtTJjIUAAAAA+l0EAwAAAAAAQOfwIgIB JAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA AAAAAAAAAA3+J8AA/4szmXRnKx8AAAAASUVORK5CYII="
        transform="translate(-303.677 92.457)"
      />
      <linearGradient
        id="SVGID_1_"
        gradientUnits="userSpaceOnUse"
        x1={1382.033}
        y1={100.347}
        x2={1382.033}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_1_)" }}
        d="M1382.89 135.52v-.21h-1.71z"
      />
      <linearGradient
        id="SVGID_2_"
        gradientUnits="userSpaceOnUse"
        x1={1336.314}
        y1={100.347}
        x2={1336.314}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_2_)" }}
        d="m1289.74 123.86 87.2 10.91h5.95v-10.83l-.82-.09z"
      />
      <linearGradient
        id="SVGID_3_"
        gradientUnits="userSpaceOnUse"
        x1={1341.22}
        y1={100.347}
        x2={1341.22}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_3_)" }}
        d="M1382.55 173.66h.34v-14.55l-83.34.01z"
      />
      <linearGradient
        id="SVGID_4_"
        gradientUnits="userSpaceOnUse"
        x1={1327.651}
        y1={100.347}
        x2={1327.651}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_4_)" }}
        d="m1272.42 135.31 110.47 16.13V136l-5.54-.7z"
      />
      <linearGradient
        id="SVGID_5_"
        gradientUnits="userSpaceOnUse"
        x1={-82.055}
        y1={100.347}
        x2={-82.055}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_5_)" }}
        d="M-130.9 135.31v14.27l97.69-14.27z"
      />
      <linearGradient
        id="SVGID_6_"
        gradientUnits="userSpaceOnUse"
        x1={159}
        y1={100.347}
        x2={159}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_6_)" }}
        d="M81.43 134.77h105.33l49.81-10.91h-93.48z"
      />
      <linearGradient
        id="SVGID_7_"
        gradientUnits="userSpaceOnUse"
        x1={362.378}
        y1={100.347}
        x2={362.378}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_7_)" }}
        d="M427.92 123.86H333.8l-36.97 10.91h106.14z"
      />
      <linearGradient
        id="SVGID_8_"
        gradientUnits="userSpaceOnUse"
        x1={-27.676}
        y1={100.347}
        x2={-27.676}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_8_)" }}
        d="M75.54 135.31H-29.92l-100.98 14.88v8.12h75.16z"
      />
      <linearGradient
        id="SVGID_9_"
        gradientUnits="userSpaceOnUse"
        x1={480.618}
        y1={100.347}
        x2={480.618}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_9_)" }}
        d="m524.01 123.46 7.13-6.21H444.1l-14 6.21z"
      />
      <linearGradient
        id="SVGID_10_"
        gradientUnits="userSpaceOnUse"
        x1={260.69}
        y1={100.347}
        x2={260.69}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_10_)" }}
        d="M332.26 123.86h-93.82l-49.32 10.91h105.76z"
      />
      <linearGradient
        id="SVGID_11_"
        gradientUnits="userSpaceOnUse"
        x1={1354.012}
        y1={100.347}
        x2={1354.012}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_11_)" }}
        d="M1381.82 123.46h1.07v-6.21h-57.75z"
      />
      <linearGradient
        id="SVGID_12_"
        gradientUnits="userSpaceOnUse"
        x1={1273.271}
        y1={100.347}
        x2={1273.271}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_12_)" }}
        d="m1163.66 135.31 131.28 23h87.95v-6.24l-113.84-16.77z"
      />
      <linearGradient
        id="SVGID_13_"
        gradientUnits="userSpaceOnUse"
        x1={399.919}
        y1={100.347}
        x2={399.919}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_13_)" }}
        d="m349.62 159.12-32.82 14.54h149.53l16.71-14.54z"
      />
      <linearGradient
        id="SVGID_14_"
        gradientUnits="userSpaceOnUse"
        x1={256.14}
        y1={100.347}
        x2={256.14}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_14_)" }}
        d="m214.3 159.12-49.29 14.54H314l33.27-14.54z"
      />
      <linearGradient
        id="SVGID_15_"
        gradientUnits="userSpaceOnUse"
        x1={543.682}
        y1={100.347}
        x2={543.682}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_15_)" }}
        d="m485.01 159.12-16.34 14.54h149.84l.18-14.54z"
      />
      <linearGradient
        id="SVGID_16_"
        gradientUnits="userSpaceOnUse"
        x1={695.522}
        y1={100.347}
        x2={695.522}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_16_)" }}
        d="m620.51 159.12.18 14.54h149.84l-16.33-14.54z"
      />
      <linearGradient
        id="SVGID_17_"
        gradientUnits="userSpaceOnUse"
        x1={839.283}
        y1={100.347}
        x2={839.283}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_17_)" }}
        d="m756.16 159.12 16.71 14.54H922.4l-32.82-14.54z"
      />
      <linearGradient
        id="SVGID_18_"
        gradientUnits="userSpaceOnUse"
        x1={112.35}
        y1={100.347}
        x2={112.35}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_18_)" }}
        d="m79.02 159.12-65.75 14.54h148.32l49.84-14.54z"
      />
      <linearGradient
        id="SVGID_19_"
        gradientUnits="userSpaceOnUse"
        x1={-95.624}
        y1={100.347}
        x2={-95.624}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_19_)" }}
        d="M-130.9 159.12v12.36l70.55-12.36z"
      />
      <linearGradient
        id="SVGID_20_"
        gradientUnits="userSpaceOnUse"
        x1={1126.852}
        y1={100.347}
        x2={1126.852}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_20_)" }}
        d="m1027.77 159.12 49.84 14.54h148.32l-65.75-14.54z"
      />
      <linearGradient
        id="SVGID_21_"
        gradientUnits="userSpaceOnUse"
        x1={1270.65}
        y1={100.347}
        x2={1270.65}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_21_)" }}
        d="m1163.65 159.12 66.42 14.54h147.58l-82.22-14.54z"
      />
      <linearGradient
        id="SVGID_22_"
        gradientUnits="userSpaceOnUse"
        x1={-27.673}
        y1={100.347}
        x2={-27.673}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_22_)" }}
        d="m-56.23 159.12-74.67 13.21v1.33H9.13l66.42-14.54z"
      />
      <linearGradient
        id="SVGID_23_"
        gradientUnits="userSpaceOnUse"
        x1={983.061}
        y1={100.347}
        x2={983.061}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_23_)" }}
        d="m891.93 159.12 33.27 14.54h148.99l-49.29-14.54z"
      />
      <linearGradient
        id="SVGID_24_"
        gradientUnits="userSpaceOnUse"
        x1={850.098}
        y1={100.347}
        x2={850.098}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_24_)" }}
        d="m904.03 123.46-21.02-6.21h-86.84l14.19 6.21z"
      />
      <linearGradient
        id="SVGID_25_"
        gradientUnits="userSpaceOnUse"
        x1={758.584}
        y1={100.347}
        x2={758.584}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_25_)" }}
        d="m809.1 123.46-13.99-6.21h-87.05l7.13 6.21z"
      />
      <linearGradient
        id="SVGID_26_"
        gradientUnits="userSpaceOnUse"
        x1={667.077}
        y1={100.347}
        x2={667.077}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_26_)" }}
        d="M707.17 117.25h-87.16l.08 6.21h94.05z"
      />
      <linearGradient
        id="SVGID_27_"
        gradientUnits="userSpaceOnUse"
        x1={686.758}
        y1={100.347}
        x2={686.758}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_27_)" }}
        d="m620.23 135.31.27 23h132.79l-25.84-23z"
      />
      <linearGradient
        id="SVGID_28_"
        gradientUnits="userSpaceOnUse"
        x1={673.469}
        y1={100.347}
        x2={673.469}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_28_)" }}
        d="M714.6 123.86h-94.51l.13 10.91h106.63z"
      />
      <linearGradient
        id="SVGID_29_"
        gradientUnits="userSpaceOnUse"
        x1={941.617}
        y1={100.347}
        x2={941.617}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_29_)" }}
        d="m998.93 123.46-28.04-6.21H884.3l21.26 6.21z"
      />
      <linearGradient
        id="SVGID_30_"
        gradientUnits="userSpaceOnUse"
        x1={1080.202}
        y1={100.347}
        x2={1080.202}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_30_)" }}
        d="m1157.77 134.77-61.66-10.91h-93.48l49.82 10.91z"
      />
      <linearGradient
        id="SVGID_31_"
        gradientUnits="userSpaceOnUse"
        x1={1033.14}
        y1={100.347}
        x2={1033.14}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_31_)" }}
        d="m1093.82 123.46-35.06-6.21h-86.3l28.32 6.21z"
      />
      <linearGradient
        id="SVGID_32_"
        gradientUnits="userSpaceOnUse"
        x1={464.059}
        y1={100.347}
        x2={464.059}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_32_)" }}
        d="M523.55 123.86h-94.36l-24.62 10.91h106.45z"
      />
      <linearGradient
        id="SVGID_33_"
        gradientUnits="userSpaceOnUse"
        x1={1181.894}
        y1={100.347}
        x2={1181.894}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_33_)" }}
        d="m1098.33 123.86 62.25 10.91h104.88l-74.08-10.92z"
      />
      <linearGradient
        id="SVGID_34_"
        gradientUnits="userSpaceOnUse"
        x1={1283.586}
        y1={100.347}
        x2={1283.586}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_34_)" }}
        d="m1194.03 123.86 74.76 10.92 104.35-.01-86.41-10.92z"
      />
      <linearGradient
        id="SVGID_35_"
        gradientUnits="userSpaceOnUse"
        x1={978.512}
        y1={100.347}
        x2={978.512}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_35_)" }}
        d="m1050.08 134.77-49.32-10.91h-93.82l37.38 10.91z"
      />
      <linearGradient
        id="SVGID_36_"
        gradientUnits="userSpaceOnUse"
        x1={808.283}
        y1={100.347}
        x2={808.283}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_36_)" }}
        d="m728.81 135.31 26.43 23h132.52l-51.91-23z"
      />
      <linearGradient
        id="SVGID_37_"
        gradientUnits="userSpaceOnUse"
        x1={1345.522}
        y1={100.347}
        x2={1345.522}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_37_)" }}
        d="M1321.5 116.85h61.39v-1.46h-74.73z"
      />
      <linearGradient
        id="SVGID_38_"
        gradientUnits="userSpaceOnUse"
        x1={929.813}
        y1={100.347}
        x2={929.813}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_38_)" }}
        d="m837.47 135.31 52.61 23h132.08l-77.96-23z"
      />
      <linearGradient
        id="SVGID_39_"
        gradientUnits="userSpaceOnUse"
        x1={1172.884}
        y1={100.347}
        x2={1172.884}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_39_)" }}
        d="m1054.91 135.31 105.05 23h130.9l-130.04-23z"
      />
      <linearGradient
        id="SVGID_40_"
        gradientUnits="userSpaceOnUse"
        x1={1051.347}
        y1={100.347}
        x2={1051.347}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_40_)" }}
        d="m946.17 135.31 78.83 23h131.52l-104-23z"
      />
      <linearGradient
        id="SVGID_41_"
        gradientUnits="userSpaceOnUse"
        x1={1307.715}
        y1={100.347}
        x2={1307.715}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_41_)" }}
        d="M1286.57 123.46h91.9l-56.18-6.21h-85.33z"
      />
      <linearGradient
        id="SVGID_42_"
        gradientUnits="userSpaceOnUse"
        x1={57.308}
        y1={100.347}
        x2={57.308}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_42_)" }}
        d="M140.87 123.86H47.75l-74.01 10.91H78.62z"
      />
      <linearGradient
        id="SVGID_43_"
        gradientUnits="userSpaceOnUse"
        x1={23.015}
        y1={100.347}
        x2={23.015}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_43_)" }}
        d="M90.42 117.25H4.72l-49.11 6.21h92.4z"
      />
      <linearGradient
        id="SVGID_44_"
        gradientUnits="userSpaceOnUse"
        x1={66.318}
        y1={100.347}
        x2={66.318}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_44_)" }}
        d="m78.38 135.31-130.04 23h130.9l105.06-23z"
      />
      <linearGradient
        id="SVGID_45_"
        gradientUnits="userSpaceOnUse"
        x1={187.855}
        y1={100.347}
        x2={187.855}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_45_)" }}
        d="m186.68 135.31-104 23H214.2l78.83-23z"
      />
      <linearGradient
        id="SVGID_46_"
        gradientUnits="userSpaceOnUse"
        x1={309.39}
        y1={100.347}
        x2={309.39}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_46_)" }}
        d="m295.01 135.31-77.97 23h132.08l52.62-23z"
      />
      <linearGradient
        id="SVGID_47_"
        gradientUnits="userSpaceOnUse"
        x1={389.105}
        y1={100.347}
        x2={389.105}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_47_)" }}
        d="m428.85 123.46 14.19-6.21H356.2l-21.03 6.21z"
      />
      <linearGradient
        id="SVGID_48_"
        gradientUnits="userSpaceOnUse"
        x1={297.585}
        y1={100.347}
        x2={297.585}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_48_)" }}
        d="m333.64 123.46 21.26-6.21h-86.59l-28.04 6.21z"
      />
      <linearGradient
        id="SVGID_49_"
        gradientUnits="userSpaceOnUse"
        x1={114.538}
        y1={100.347}
        x2={114.538}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_49_)" }}
        d="m143.18 123.46 35.4-6.21h-86l-42.09 6.21z"
      />
      <linearGradient
        id="SVGID_50_"
        gradientUnits="userSpaceOnUse"
        x1={552.445}
        y1={100.347}
        x2={552.445}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_50_)" }}
        d="M485.92 158.31H618.7l.27-23H511.75z"
      />
      <linearGradient
        id="SVGID_51_"
        gradientUnits="userSpaceOnUse"
        x1={1124.665}
        y1={100.347}
        x2={1124.665}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_51_)" }}
        d="m1188.71 123.46-42.15-6.22-85.94.01 35.4 6.21z"
      />
      <linearGradient
        id="SVGID_52_"
        gradientUnits="userSpaceOnUse"
        x1={876.825}
        y1={100.347}
        x2={876.825}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_52_)" }}
        d="m942.37 134.77-36.97-10.91h-94.12l24.95 10.91z"
      />
      <linearGradient
        id="SVGID_53_"
        gradientUnits="userSpaceOnUse"
        x1={1216.19}
        y1={100.347}
        x2={1216.19}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_53_)" }}
        d="m1283.59 123.46-49.17-6.22-85.63.01 42.54 6.22z"
      />
      <linearGradient
        id="SVGID_54_"
        gradientUnits="userSpaceOnUse"
        x1={775.143}
        y1={100.347}
        x2={775.143}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_54_)" }}
        d="m715.65 123.86 12.54 10.91h106.44l-24.61-10.91z"
      />
      <linearGradient
        id="SVGID_55_"
        gradientUnits="userSpaceOnUse"
        x1={430.92}
        y1={100.347}
        x2={430.92}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_55_)" }}
        d="m403.35 135.31-51.91 23h132.53l26.43-23z"
      />
      <linearGradient
        id="SVGID_56_"
        gradientUnits="userSpaceOnUse"
        x1={565.733}
        y1={100.347}
        x2={565.733}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_56_)" }}
        d="M512.36 134.77h106.62l.13-10.91h-94.5z"
      />
      <linearGradient
        id="SVGID_57_"
        gradientUnits="userSpaceOnUse"
        x1={572.125}
        y1={100.347}
        x2={572.125}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_57_)" }}
        d="M525.06 123.46h94.05l.08-6.21h-87.16z"
      />
      <linearGradient
        id="SVGID_58_"
        gradientUnits="userSpaceOnUse"
        x1={206.062}
        y1={100.347}
        x2={206.062}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_58_)" }}
        d="m238.42 123.46 28.33-6.21h-86.31l-35.06 6.21z"
      />
      <linearGradient
        id="SVGID_59_"
        gradientUnits="userSpaceOnUse"
        x1={1183.669}
        y1={100.347}
        x2={1183.669}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_59_)" }}
        d="m1146.09 116.86 85.2-.01-11.53-1.46h-83.71z"
      />
      <linearGradient
        id="SVGID_60_"
        gradientUnits="userSpaceOnUse"
        x1={923.494}
        y1={100.347}
        x2={923.494}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_60_)" }}
        d="M882.92 116.85h86.14l-6.58-1.46h-84.55z"
      />
      <linearGradient
        id="SVGID_61_"
        gradientUnits="userSpaceOnUse"
        x1={-31.178}
        y1={100.347}
        x2={-31.178}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_61_)" }}
        d="M-79.48 116.85H5.53l11.6-1.46h-83.44z"
      />
      <linearGradient
        id="SVGID_62_"
        gradientUnits="userSpaceOnUse"
        x1={750.061}
        y1={100.347}
        x2={750.061}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_62_)" }}
        d="M707.6 116.85h86.59l-3.28-1.46h-84.98z"
      />
      <linearGradient
        id="SVGID_63_"
        gradientUnits="userSpaceOnUse"
        x1={663.356}
        y1={100.347}
        x2={663.356}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_63_)" }}
        d="M620.01 116.85h86.71l-1.64-1.46h-85.09z"
      />
      <linearGradient
        id="SVGID_64_"
        gradientUnits="userSpaceOnUse"
        x1={1096.942}
        y1={100.347}
        x2={1096.942}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_64_)" }}
        d="M1058.31 116.85h85.57l-9.88-1.46h-84z"
      />
      <linearGradient
        id="SVGID_65_"
        gradientUnits="userSpaceOnUse"
        x1={55.551}
        y1={100.347}
        x2={55.551}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_65_)" }}
        d="M7.92 116.85h85.34l9.92-1.46H19.44z"
      />
      <linearGradient
        id="SVGID_66_"
        gradientUnits="userSpaceOnUse"
        x1={836.774}
        y1={100.347}
        x2={836.774}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_66_)" }}
        d="M795.24 116.85h86.4l-4.94-1.46h-84.79z"
      />
      <linearGradient
        id="SVGID_67_"
        gradientUnits="userSpaceOnUse"
        x1={1270.396}
        y1={100.347}
        x2={1270.396}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_67_)" }}
        d="m1233.79 116.86 84.9-.01-13.18-1.46h-83.41z"
      />
      <linearGradient
        id="SVGID_68_"
        gradientUnits="userSpaceOnUse"
        x1={228.986}
        y1={100.347}
        x2={228.986}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_68_)" }}
        d="M182.73 116.85h85.86l6.65-1.46h-84.28z"
      />
      <linearGradient
        id="SVGID_69_"
        gradientUnits="userSpaceOnUse"
        x1={-99.913}
        y1={100.347}
        x2={-99.913}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_69_)" }}
        d="M-68.93 115.39h-61.97v1.46h48.71z"
      />
      <linearGradient
        id="SVGID_70_"
        gradientUnits="userSpaceOnUse"
        x1={1010.217}
        y1={100.347}
        x2={1010.217}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_70_)" }}
        d="M970.61 116.85h85.86l-8.23-1.46h-84.28z"
      />
      <linearGradient
        id="SVGID_71_"
        gradientUnits="userSpaceOnUse"
        x1={575.847}
        y1={100.347}
        x2={575.847}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_71_)" }}
        d="M532.48 116.85h86.71l.02-1.46h-85.09z"
      />
      <linearGradient
        id="SVGID_72_"
        gradientUnits="userSpaceOnUse"
        x1={-64.327}
        y1={100.347}
        x2={-64.327}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_72_)" }}
        d="M2.24 117.25h-85.38l-47.76 5.28v.93h83.65z"
      />
      <linearGradient
        id="SVGID_73_"
        gradientUnits="userSpaceOnUse"
        x1={402.43}
        y1={100.347}
        x2={402.43}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_73_)" }}
        d="M357.57 116.85h86.39l3.33-1.46H362.5z"
      />
      <linearGradient
        id="SVGID_74_"
        gradientUnits="userSpaceOnUse"
        x1={142.261}
        y1={100.347}
        x2={142.261}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_74_)" }}
        d="M95.32 116.85h85.57l8.31-1.46h-84z"
      />
      <linearGradient
        id="SVGID_75_"
        gradientUnits="userSpaceOnUse"
        x1={489.143}
        y1={100.347}
        x2={489.143}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_75_)" }}
        d="M445.01 116.85h86.59l1.68-1.46h-84.99z"
      />
      <linearGradient
        id="SVGID_76_"
        gradientUnits="userSpaceOnUse"
        x1={-90.717}
        y1={100.347}
        x2={-90.717}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_76_)" }}
        d="M-130.9 123.86v10.07l80.36-10.07z"
      />
      <linearGradient
        id="SVGID_77_"
        gradientUnits="userSpaceOnUse"
        x1={315.709}
        y1={100.347}
        x2={315.709}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_77_)" }}
        d="M270.14 116.85h86.15l4.99-1.46h-84.55z"
      />
      <linearGradient
        id="SVGID_78_"
        gradientUnits="userSpaceOnUse"
        x1={-42.862}
        y1={100.347}
        x2={-42.862}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_78_)" }}
        d="M45.17 123.86h-92.76l-83.31 10.52v.39h101.45z"
      />
      <linearGradient
        id="SVGID_79_"
        gradientUnits="userSpaceOnUse"
        x1={-108.415}
        y1={100.347}
        x2={-108.415}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_79_)" }}
        d="M-130.9 117.25v4.93l44.97-4.93z"
      />
      <linearGradient
        id="SVGID_80_"
        gradientUnits="userSpaceOnUse"
        x1={234.228}
        y1={115.392}
        x2={234.228}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_80_)" }}
        d="m275.24 115.39 2.26-.49-83.62-.02-2.92.51z"
      />
      <linearGradient
        id="SVGID_81_"
        gradientUnits="userSpaceOnUse"
        x1={234.228}
        y1={100.347}
        x2={234.228}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_81_)" }}
        d="m275.24 115.39 2.26-.49-83.62-.02-2.92.51z"
      />
      <linearGradient
        id="SVGID_82_"
        gradientUnits="userSpaceOnUse"
        x1={919.422}
        y1={115.392}
        x2={919.422}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_82_)" }}
        d="m962.48 115.39-2.16-.48-83.95.03 1.56.45z"
      />
      <linearGradient
        id="SVGID_83_"
        gradientUnits="userSpaceOnUse"
        x1={919.422}
        y1={100.347}
        x2={919.422}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_83_)" }}
        d="m962.48 115.39-2.16-.48-83.95.03 1.56.45z"
      />
      <linearGradient
        id="SVGID_84_"
        gradientUnits="userSpaceOnUse"
        x1={833.811}
        y1={115.392}
        x2={833.811}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_84_)" }}
        d="m876.7 115.39-1.54-.45-84.24.02.99.43z"
      />
      <linearGradient
        id="SVGID_85_"
        gradientUnits="userSpaceOnUse"
        x1={833.811}
        y1={100.347}
        x2={833.811}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_85_)" }}
        d="m876.7 115.39-1.54-.45-84.24.02.99.43z"
      />
      <linearGradient
        id="SVGID_86_"
        gradientUnits="userSpaceOnUse"
        x1={148.673}
        y1={115.392}
        x2={148.673}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_86_)" }}
        d="m189.2 115.39 2.95-.51-18.99-.01-64.15-.04-3.81.56z"
      />
      <linearGradient
        id="SVGID_87_"
        gradientUnits="userSpaceOnUse"
        x1={148.673}
        y1={100.347}
        x2={148.673}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_87_)" }}
        d="m189.2 115.39 2.95-.51-18.99-.01-64.15-.04-3.81.56z"
      />
      <linearGradient
        id="SVGID_88_"
        gradientUnits="userSpaceOnUse"
        x1={63.234}
        y1={115.392}
        x2={63.234}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_88_)" }}
        d="m103.18 115.39 3.85-.56-82.71-.05-4.88.61z"
      />
      <linearGradient
        id="SVGID_89_"
        gradientUnits="userSpaceOnUse"
        x1={63.234}
        y1={100.347}
        x2={63.234}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_89_)" }}
        d="m103.18 115.39 3.85-.56-82.71-.05-4.88.61z"
      />
      <linearGradient
        id="SVGID_90_"
        gradientUnits="userSpaceOnUse"
        x1={748.182}
        y1={115.392}
        x2={748.182}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_90_)" }}
        d="m790.91 115.39-.98-.43-84.48.02.48.41z"
      />
      <linearGradient
        id="SVGID_91_"
        gradientUnits="userSpaceOnUse"
        x1={748.182}
        y1={100.347}
        x2={748.182}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_91_)" }}
        d="m790.91 115.39-.98-.43-84.48.02.48.41z"
      />
      <linearGradient
        id="SVGID_92_"
        gradientUnits="userSpaceOnUse"
        x1={1176.101}
        y1={115.392}
        x2={1176.101}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_92_)" }}
        d="m1219.76 115.39-4.59-.58-82.73.05 3.61.53z"
      />
      <linearGradient
        id="SVGID_93_"
        gradientUnits="userSpaceOnUse"
        x1={1176.101}
        y1={100.347}
        x2={1176.101}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_93_)" }}
        d="m1219.76 115.39-4.59-.58-82.73.05 3.61.53z"
      />
      <linearGradient
        id="SVGID_94_"
        gradientUnits="userSpaceOnUse"
        x1={576.667}
        y1={115.392}
        x2={576.667}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_94_)" }}
        d="m619.21 115.39.01-.4-84.62-.02-.48.42z"
      />
      <linearGradient
        id="SVGID_95_"
        gradientUnits="userSpaceOnUse"
        x1={576.667}
        y1={100.347}
        x2={576.667}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_95_)" }}
        d="m619.21 115.39.01-.4-84.62-.02-.48.42z"
      />
      <linearGradient
        id="SVGID_96_"
        gradientUnits="userSpaceOnUse"
        x1={662.535}
        y1={115.392}
        x2={662.535}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_96_)" }}
        d="m705.08 115.39-.46-.41-57.84.02-26.79-.01v.4z"
      />
      <linearGradient
        id="SVGID_97_"
        gradientUnits="userSpaceOnUse"
        x1={662.535}
        y1={100.347}
        x2={662.535}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_97_)" }}
        d="m705.08 115.39-.46-.41-57.84.02-26.79-.01v.4z"
      />
      <linearGradient
        id="SVGID_98_"
        gradientUnits="userSpaceOnUse"
        x1={1090.576}
        y1={115.392}
        x2={1090.576}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_98_)" }}
        d="m1134 115.39-3.56-.52h-10.04l-73.25.02 2.85.5z"
      />
      <linearGradient
        id="SVGID_99_"
        gradientUnits="userSpaceOnUse"
        x1={1090.576}
        y1={100.347}
        x2={1090.576}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_99_)" }}
        d="m1134 115.39-3.56-.52h-10.04l-73.25.02 2.85.5z"
      />
      <linearGradient
        id="SVGID_100_"
        gradientUnits="userSpaceOnUse"
        x1={-22.133}
        y1={115.392}
        x2={-22.133}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_100_)" }}
        d="m17.13 115.39 4.91-.62-82.29-.05-6.06.67z"
      />
      <linearGradient
        id="SVGID_101_"
        gradientUnits="userSpaceOnUse"
        x1={-22.133}
        y1={100.347}
        x2={-22.133}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_101_)" }}
        d="m17.13 115.39 4.91-.62-82.29-.05-6.06.67z"
      />
      <linearGradient
        id="SVGID_102_"
        gradientUnits="userSpaceOnUse"
        x1={491.03}
        y1={115.392}
        x2={491.03}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_102_)" }}
        d="m533.28 115.39.49-.42-84.46-.03-1.02.45z"
      />
      <linearGradient
        id="SVGID_103_"
        gradientUnits="userSpaceOnUse"
        x1={491.03}
        y1={100.347}
        x2={491.03}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_103_)" }}
        d="m533.28 115.39.49-.42-84.46-.03-1.02.45z"
      />
      <linearGradient
        id="SVGID_104_"
        gradientUnits="userSpaceOnUse"
        x1={319.807}
        y1={115.392}
        x2={319.807}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_104_)" }}
        d="m361.28 115.39 1.61-.47-83.93-.02-2.23.49z"
      />
      <linearGradient
        id="SVGID_105_"
        gradientUnits="userSpaceOnUse"
        x1={319.807}
        y1={100.347}
        x2={319.807}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_105_)" }}
        d="m361.28 115.39 1.61-.47-83.93-.02-2.23.49z"
      />
      <linearGradient
        id="SVGID_106_"
        gradientUnits="userSpaceOnUse"
        x1={1342.621}
        y1={115.392}
        x2={1342.621}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_106_)" }}
        d="m1302.36 114.76 5.8.63h74.73v-.7l-25.68.03z"
      />
      <linearGradient
        id="SVGID_107_"
        gradientUnits="userSpaceOnUse"
        x1={1342.621}
        y1={100.347}
        x2={1342.621}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_107_)" }}
        d="m1302.36 114.76 5.8.63h74.73v-.7l-25.68.03z"
      />
      <linearGradient
        id="SVGID_108_"
        gradientUnits="userSpaceOnUse"
        x1={405.409}
        y1={115.392}
        x2={405.409}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_108_)" }}
        d="m447.29 115.39 1.03-.45-84.22-.02-1.6.47z"
      />
      <linearGradient
        id="SVGID_109_"
        gradientUnits="userSpaceOnUse"
        x1={405.409}
        y1={100.347}
        x2={405.409}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_109_)" }}
        d="m447.29 115.39 1.03-.45-84.22-.02-1.6.47z"
      />
      <linearGradient
        id="SVGID_110_"
        gradientUnits="userSpaceOnUse"
        x1={-96.856}
        y1={115.392}
        x2={-96.856}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_110_)" }}
        d="M-62.81 114.72h-.84l-67.25-.07v.74h61.97z"
      />
      <linearGradient
        id="SVGID_111_"
        gradientUnits="userSpaceOnUse"
        x1={-96.856}
        y1={100.347}
        x2={-96.856}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_111_)" }}
        d="M-62.81 114.72h-.84l-67.25-.07v.74h61.97z"
      />
      <linearGradient
        id="SVGID_112_"
        gradientUnits="userSpaceOnUse"
        x1={1261.486}
        y1={115.392}
        x2={1261.486}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_112_)" }}
        d="m1305.51 115.39-5.73-.63-82.32.05 4.64.58z"
      />
      <linearGradient
        id="SVGID_113_"
        gradientUnits="userSpaceOnUse"
        x1={1261.486}
        y1={100.347}
        x2={1261.486}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_113_)" }}
        d="m1305.51 115.39-5.73-.63-82.32.05 4.64.58z"
      />
      <linearGradient
        id="SVGID_114_"
        gradientUnits="userSpaceOnUse"
        x1={1005.01}
        y1={115.392}
        x2={1005.01}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_114_)" }}
        d="m1048.24 115.39-2.82-.5-83.64.02 2.18.48z"
      />
      <linearGradient
        id="SVGID_115_"
        gradientUnits="userSpaceOnUse"
        x1={1005.01}
        y1={100.347}
        x2={1005.01}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_115_)" }}
        d="m1048.24 115.39-2.82-.5-83.64.02 2.18.48z"
      />
      <linearGradient
        id="SVGID_116_"
        gradientUnits="userSpaceOnUse"
        x1={625.995}
        y1={100.347}
        x2={625.995}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        d="M1308.16 115.39h-2.64l13.18 1.46-84.9.01-11.69-1.46h-2.34l11.53 1.46-85.2.01-10.04-1.47H1134l9.88 1.46h-85.57l-8.31-1.46h-1.76l8.23 1.46h-85.86l-6.65-1.46h-1.48l6.58 1.46h-86.14l-4.99-1.46h-1.23l4.93 1.46h-86.39l-3.33-1.46h-1l3.29 1.46h-86.6l-1.67-1.46h-.84l1.64 1.46h-86.71l-.02-1.46h-.78l-.02 1.46h-86.71l1.64-1.46h-.84l-1.67 1.46h-86.59l3.29-1.46h-1l-3.33 1.46H357.6l4.93-1.46h-1.23l-4.99 1.46h-86.14l6.58-1.46h-1.48l-6.65 1.46h-85.86l8.23-1.46h-1.76l-8.31 1.46h-85.6l9.88-1.46h-2.01l-9.93 1.46H7.92l11.52-1.46h-2.32l-11.59 1.46h-85.02l13.17-1.46h-2.62l-13.26 1.46h-48.71v.4h44.96l-44.96 4.93v.35l47.75-5.28h85.4l-49.49 6.2h-83.65v.4h80.36l-80.36 10.07v.46l83.31-10.52h92.76l-74.62 10.91H-130.9v.54h97.68l-97.68 14.27v.61l100.98-14.88H75.54l-131.28 23h-75.16v.81h70.54l-70.54 12.36v.85l74.67-13.21H75.55L9.13 173.66h4.14l65.75-14.54h132.41l-49.84 14.54h3.42l49.29-14.54h132.97L314 173.66h2.8l32.82-14.54h133.42l-16.71 14.54h2.35l16.33-14.54h133.68l-.17 14.54h2.17l-.17-14.54H754.2l16.33 14.54h2.34l-16.71-14.54h133.42l32.82 14.54h2.8l-33.27-14.54h132.97l49.29 14.54h3.42l-49.84-14.54h132.41l65.75 14.54h4.14l-66.42-14.54h131.78l82.21 14.54h4.9l-83-14.54 83.33-.01v-.8h-87.95l-131.28-23 105.39-.01 113.84 16.78v-.64l-110.47-16.13 104.93-.01 5.54.7v-.48l-1.7-.21h1.7v-.53h-5.95l-87.19-10.92 92.33-.01.81.09v-.48h-1.07l-56.68-6.21h57.75v-.4h-61.39l-13.32-1.46zm-864.06 1.86h87.04l-7.13 6.2H430.1l14-6.2zm-205.66 6.61h93.82l-37.38 10.91H189.12l49.32-10.91zm-51.68 10.91H81.43l61.66-10.91h93.48l-49.81 10.91zm147.04-10.91h94.12l-24.95 10.91H296.83l36.97-10.91zm95.39 0h94.36l-12.53 10.91H404.57l24.62-10.91zm13.85-6.61-14.19 6.2h-93.67l21.02-6.2h86.84zm-88.14 0-21.26 6.2h-93.37l28.04-6.2h86.59zm-88.15 0-28.33 6.2h-93.04l35.07-6.2h86.3zm-88.17 0-35.4 6.2H50.49l42.09-6.2h86zm-222.97 6.21 49.1-6.2h85.7l-42.4 6.2h-92.4zm18.13 11.31 74.01-10.91h93.12l-62.25 10.91H-26.26zm-25.4 23.54 130.04-23H184.3l-105.05 23H-51.66zm265.86 0H82.68l104-23h106.35l-78.83 23zm134.92 0H217.04l77.96-23h106.73l-52.61 23zm134.85 0H351.44l51.91-23H510.4l-26.43 23zm134.73 0H485.92l25.84-23h107.22l-.28 23zm.28-23.54H512.36l12.25-10.91h94.5l-.13 10.91zm.13-11.31h-94.05l6.97-6.2h87.16l-.08 6.2zm615.31-6.22 49.17 6.21-92.26.01-42.54-6.21 85.63-.01zm-87.86 0 42.16 6.21h-92.69l-35.4-6.2 85.93-.01zm-335.28 6.62h94.12l36.97 10.91H836.23l-24.95-10.91zm23.35 10.91H728.19l-12.53-10.91h94.36l24.61 10.91zm72.31-10.91h93.82l49.32 10.91H944.32l-37.38-10.91zm95.69 0h93.48l61.66 10.91h-105.33l-49.81-10.91zm56.13-6.61 35.07 6.2h-93.04l-28.33-6.2h86.3zm-87.87 0 28.05 6.2h-93.38l-21.26-6.2h86.59zm-87.88 0 21.02 6.2h-93.67l-14.19-6.2h86.84zm-87.9 0 14 6.2H715.2l-7.13-6.2h87.04zm-175.1 0h87.16l6.97 6.2h-94.05l-.08-6.2zm.08 6.61h94.5l12.25 10.91H620.22l-.13-10.91zm.41 34.45-.28-23h107.22l25.83 23H620.5zm134.74 0-26.43-23h107.04l51.91 23H755.24zm134.84 0-52.62-23H944.2l77.96 23H890.08zm134.92 0-78.83-23h106.35l104 23H1025zm265.86 0h-130.9l-105.05-23h105.92l130.03 23zm-130.28-23.54-62.25-10.91 93.06-.01 74.08 10.92h-104.89zm212.56 0-104.35.01-74.76-10.92 92.7-.01 86.41 10.92zm5.33-11.31-91.9.01-49.61-6.21 85.33-.01 56.18 6.21z"
        style={{ fill: "url(#SVGID_116_)" }}
      />
      <linearGradient
        id="SVGID_117_"
        gradientUnits="userSpaceOnUse"
        x1={625.995}
        y1={111.907}
        x2={625.995}
        y2={159.426}
      >
        <stop offset={0} style={{ stopColor: "#15fffb" }} />
        <stop offset={0.072} style={{ stopColor: "#11dbfc" }} />
        <stop offset={0.198} style={{ stopColor: "#09a3fd" }} />
        <stop offset={0.306} style={{ stopColor: "#047afe" }} />
        <stop offset={0.39} style={{ stopColor: "#0160ff" }} />
        <stop offset={0.441} style={{ stopColor: "#0057ff" }} />
        <stop offset={1} style={{ stopColor: "#008391" }} />
      </linearGradient>
      <path
        d="M1308.16 115.39h-2.64l13.18 1.46-84.9.01-11.69-1.46h-2.34l11.53 1.46-85.2.01-10.04-1.47H1134l9.88 1.46h-85.57l-8.31-1.46h-1.76l8.23 1.46h-85.86l-6.65-1.46h-1.48l6.58 1.46h-86.14l-4.99-1.46h-1.23l4.93 1.46h-86.39l-3.33-1.46h-1l3.29 1.46h-86.6l-1.67-1.46h-.84l1.64 1.46h-86.71l-.02-1.46h-.78l-.02 1.46h-86.71l1.64-1.46h-.84l-1.67 1.46h-86.59l3.29-1.46h-1l-3.33 1.46H357.6l4.93-1.46h-1.23l-4.99 1.46h-86.14l6.58-1.46h-1.48l-6.65 1.46h-85.86l8.23-1.46h-1.76l-8.31 1.46h-85.6l9.88-1.46h-2.01l-9.93 1.46H7.92l11.52-1.46h-2.32l-11.59 1.46h-85.02l13.17-1.46h-2.62l-13.26 1.46h-48.71v.4h44.96l-44.96 4.93v.35l47.75-5.28h85.4l-49.49 6.2h-83.65v.4h80.36l-80.36 10.07v.46l83.31-10.52h92.76l-74.62 10.91H-130.9v.54h97.68l-97.68 14.27v.61l100.98-14.88H75.54l-131.28 23h-75.16v.81h70.54l-70.54 12.36v.85l74.67-13.21H75.55L9.13 173.66h4.14l65.75-14.54h132.41l-49.84 14.54h3.42l49.29-14.54h132.97L314 173.66h2.8l32.82-14.54h133.42l-16.71 14.54h2.35l16.33-14.54h133.68l-.17 14.54h2.17l-.17-14.54H754.2l16.33 14.54h2.34l-16.71-14.54h133.42l32.82 14.54h2.8l-33.27-14.54h132.97l49.29 14.54h3.42l-49.84-14.54h132.41l65.75 14.54h4.14l-66.42-14.54h131.78l82.21 14.54h4.9l-83-14.54 83.33-.01v-.8h-87.95l-131.28-23 105.39-.01 113.84 16.78v-.64l-110.47-16.13 104.93-.01 5.54.7v-.48l-1.7-.21h1.7v-.53h-5.95l-87.19-10.92 92.33-.01.81.09v-.48h-1.07l-56.68-6.21h57.75v-.4h-61.39l-13.32-1.46zm-864.06 1.86h87.04l-7.13 6.2H430.1l14-6.2zm-205.66 6.61h93.82l-37.38 10.91H189.12l49.32-10.91zm-51.68 10.91H81.43l61.66-10.91h93.48l-49.81 10.91zm147.04-10.91h94.12l-24.95 10.91H296.83l36.97-10.91zm95.39 0h94.36l-12.53 10.91H404.57l24.62-10.91zm13.85-6.61-14.19 6.2h-93.67l21.02-6.2h86.84zm-88.14 0-21.26 6.2h-93.37l28.04-6.2h86.59zm-88.15 0-28.33 6.2h-93.04l35.07-6.2h86.3zm-88.17 0-35.4 6.2H50.49l42.09-6.2h86zm-222.97 6.21 49.1-6.2h85.7l-42.4 6.2h-92.4zm18.13 11.31 74.01-10.91h93.12l-62.25 10.91H-26.26zm-25.4 23.54 130.04-23H184.3l-105.05 23H-51.66zm265.86 0H82.68l104-23h106.35l-78.83 23zm134.92 0H217.04l77.96-23h106.73l-52.61 23zm134.85 0H351.44l51.91-23H510.4l-26.43 23zm134.73 0H485.92l25.84-23h107.22l-.28 23zm.28-23.54H512.36l12.25-10.91h94.5l-.13 10.91zm.13-11.31h-94.05l6.97-6.2h87.16l-.08 6.2zm615.31-6.22 49.17 6.21-92.26.01-42.54-6.21 85.63-.01zm-87.86 0 42.16 6.21h-92.69l-35.4-6.2 85.93-.01zm-335.28 6.62h94.12l36.97 10.91H836.23l-24.95-10.91zm23.35 10.91H728.19l-12.53-10.91h94.36l24.61 10.91zm72.31-10.91h93.82l49.32 10.91H944.32l-37.38-10.91zm95.69 0h93.48l61.66 10.91h-105.33l-49.81-10.91zm56.13-6.61 35.07 6.2h-93.04l-28.33-6.2h86.3zm-87.87 0 28.05 6.2h-93.38l-21.26-6.2h86.59zm-87.88 0 21.02 6.2h-93.67l-14.19-6.2h86.84zm-87.9 0 14 6.2H715.2l-7.13-6.2h87.04zm-175.1 0h87.16l6.97 6.2h-94.05l-.08-6.2zm.08 6.61h94.5l12.25 10.91H620.22l-.13-10.91zm.41 34.45-.28-23h107.22l25.83 23H620.5zm134.74 0-26.43-23h107.04l51.91 23H755.24zm134.84 0-52.62-23H944.2l77.96 23H890.08zm134.92 0-78.83-23h106.35l104 23H1025zm265.86 0h-130.9l-105.05-23h105.92l130.03 23zm-130.28-23.54-62.25-10.91 93.06-.01 74.08 10.92h-104.89zm212.56 0-104.35.01-74.76-10.92 92.7-.01 86.41 10.92zm5.33-11.31-91.9.01-49.61-6.21 85.33-.01 56.18 6.21z"
        style={{ fill: "url(#SVGID_117_)" }}
      />
      <linearGradient
        id="SVGID_118_"
        gradientUnits="userSpaceOnUse"
        x1={20.723}
        y1={115.392}
        x2={20.723}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_118_)" }}
        d="m19.44 115.39 4.88-.61-2.28-.01-4.91.62z"
      />
      <linearGradient
        id="SVGID_119_"
        gradientUnits="userSpaceOnUse"
        x1={20.723}
        y1={100.347}
        x2={20.723}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_119_)" }}
        d="m19.44 115.39 4.88-.61-2.28-.01-4.91.62z"
      />
      <linearGradient
        id="SVGID_120_"
        gradientUnits="userSpaceOnUse"
        x1={20.723}
        y1={111.907}
        x2={20.723}
        y2={159.426}
      >
        <stop offset={0} style={{ stopColor: "#15fffb" }} />
        <stop offset={0.072} style={{ stopColor: "#11dbfc" }} />
        <stop offset={0.198} style={{ stopColor: "#09a3fd" }} />
        <stop offset={0.306} style={{ stopColor: "#047afe" }} />
        <stop offset={0.39} style={{ stopColor: "#0160ff" }} />
        <stop offset={0.441} style={{ stopColor: "#0057ff" }} />
        <stop offset={1} style={{ stopColor: "#008391" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_120_)" }}
        d="m19.44 115.39 4.88-.61-2.28-.01-4.91.62z"
      />
      <linearGradient
        id="SVGID_121_"
        gradientUnits="userSpaceOnUse"
        x1={1303.967}
        y1={115.392}
        x2={1303.967}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_121_)" }}
        d="m1308.16 115.39-5.8-.63h-2.58l5.73.63z"
      />
      <linearGradient
        id="SVGID_122_"
        gradientUnits="userSpaceOnUse"
        x1={1303.967}
        y1={100.347}
        x2={1303.967}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_122_)" }}
        d="m1308.16 115.39-5.8-.63h-2.58l5.73.63z"
      />
      <linearGradient
        id="SVGID_123_"
        gradientUnits="userSpaceOnUse"
        x1={1303.967}
        y1={111.907}
        x2={1303.967}
        y2={159.426}
      >
        <stop offset={0} style={{ stopColor: "#15fffb" }} />
        <stop offset={0.072} style={{ stopColor: "#11dbfc" }} />
        <stop offset={0.198} style={{ stopColor: "#09a3fd" }} />
        <stop offset={0.306} style={{ stopColor: "#047afe" }} />
        <stop offset={0.39} style={{ stopColor: "#0160ff" }} />
        <stop offset={0.441} style={{ stopColor: "#0057ff" }} />
        <stop offset={1} style={{ stopColor: "#008391" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_123_)" }}
        d="m1308.16 115.39-5.8-.63h-2.58l5.73.63z"
      />
      <linearGradient
        id="SVGID_124_"
        gradientUnits="userSpaceOnUse"
        x1={362.686}
        y1={115.392}
        x2={362.686}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_124_)" }}
        d="m362.5 115.39 1.6-.47h-1.21l-1.61.47z"
      />
      <linearGradient
        id="SVGID_125_"
        gradientUnits="userSpaceOnUse"
        x1={362.686}
        y1={100.347}
        x2={362.686}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_125_)" }}
        d="m362.5 115.39 1.6-.47h-1.21l-1.61.47z"
      />
      <linearGradient
        id="SVGID_126_"
        gradientUnits="userSpaceOnUse"
        x1={362.686}
        y1={111.907}
        x2={362.686}
        y2={159.426}
      >
        <stop offset={0} style={{ stopColor: "#15fffb" }} />
        <stop offset={0.072} style={{ stopColor: "#11dbfc" }} />
        <stop offset={0.198} style={{ stopColor: "#09a3fd" }} />
        <stop offset={0.306} style={{ stopColor: "#047afe" }} />
        <stop offset={0.39} style={{ stopColor: "#0160ff" }} />
        <stop offset={0.441} style={{ stopColor: "#0057ff" }} />
        <stop offset={1} style={{ stopColor: "#008391" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_126_)" }}
        d="m362.5 115.39 1.6-.47h-1.21l-1.61.47z"
      />
      <linearGradient
        id="SVGID_127_"
        gradientUnits="userSpaceOnUse"
        x1={1133.244}
        y1={115.392}
        x2={1133.244}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_127_)" }}
        d="m1136.05 115.39-3.61-.53-2 .01 3.56.52z"
      />
      <linearGradient
        id="SVGID_128_"
        gradientUnits="userSpaceOnUse"
        x1={1133.244}
        y1={100.347}
        x2={1133.244}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_128_)" }}
        d="m1136.05 115.39-3.61-.53-2 .01 3.56.52z"
      />
      <linearGradient
        id="SVGID_129_"
        gradientUnits="userSpaceOnUse"
        x1={1133.244}
        y1={111.907}
        x2={1133.244}
        y2={159.426}
      >
        <stop offset={0} style={{ stopColor: "#15fffb" }} />
        <stop offset={0.072} style={{ stopColor: "#11dbfc" }} />
        <stop offset={0.198} style={{ stopColor: "#09a3fd" }} />
        <stop offset={0.306} style={{ stopColor: "#047afe" }} />
        <stop offset={0.39} style={{ stopColor: "#0160ff" }} />
        <stop offset={0.441} style={{ stopColor: "#0057ff" }} />
        <stop offset={1} style={{ stopColor: "#008391" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_129_)" }}
        d="m1136.05 115.39-3.61-.53-2 .01 3.56.52z"
      />
      <linearGradient
        id="SVGID_130_"
        gradientUnits="userSpaceOnUse"
        x1={1218.637}
        y1={115.392}
        x2={1218.637}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_130_)" }}
        d="m1222.1 115.39-4.64-.58h-2.29l4.59.58z"
      />
      <linearGradient
        id="SVGID_131_"
        gradientUnits="userSpaceOnUse"
        x1={1218.637}
        y1={100.347}
        x2={1218.637}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_131_)" }}
        d="m1222.1 115.39-4.64-.58h-2.29l4.59.58z"
      />
      <linearGradient
        id="SVGID_132_"
        gradientUnits="userSpaceOnUse"
        x1={1218.637}
        y1={111.907}
        x2={1218.637}
        y2={159.426}
      >
        <stop offset={0} style={{ stopColor: "#15fffb" }} />
        <stop offset={0.072} style={{ stopColor: "#11dbfc" }} />
        <stop offset={0.198} style={{ stopColor: "#09a3fd" }} />
        <stop offset={0.306} style={{ stopColor: "#047afe" }} />
        <stop offset={0.39} style={{ stopColor: "#0160ff" }} />
        <stop offset={0.441} style={{ stopColor: "#0057ff" }} />
        <stop offset={1} style={{ stopColor: "#008391" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_132_)" }}
        d="m1222.1 115.39-4.64-.58h-2.29l4.59.58z"
      />
      <linearGradient
        id="SVGID_133_"
        gradientUnits="userSpaceOnUse"
        x1={191.538}
        y1={115.392}
        x2={191.538}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_133_)" }}
        d="m190.96 115.39 2.92-.51h-1.73l-2.95.51z"
      />
      <linearGradient
        id="SVGID_134_"
        gradientUnits="userSpaceOnUse"
        x1={191.538}
        y1={100.347}
        x2={191.538}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_134_)" }}
        d="m190.96 115.39 2.92-.51h-1.73l-2.95.51z"
      />
      <linearGradient
        id="SVGID_135_"
        gradientUnits="userSpaceOnUse"
        x1={191.538}
        y1={111.907}
        x2={191.538}
        y2={159.426}
      >
        <stop offset={0} style={{ stopColor: "#15fffb" }} />
        <stop offset={0.072} style={{ stopColor: "#11dbfc" }} />
        <stop offset={0.198} style={{ stopColor: "#09a3fd" }} />
        <stop offset={0.306} style={{ stopColor: "#047afe" }} />
        <stop offset={0.39} style={{ stopColor: "#0160ff" }} />
        <stop offset={0.441} style={{ stopColor: "#0057ff" }} />
        <stop offset={1} style={{ stopColor: "#008391" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_135_)" }}
        d="m190.96 115.39 2.92-.51h-1.73l-2.95.51z"
      />
      <linearGradient
        id="SVGID_136_"
        gradientUnits="userSpaceOnUse"
        x1={277.099}
        y1={115.392}
        x2={277.099}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_136_)" }}
        d="m276.73 115.39 2.23-.49h-1.46l-2.26.49z"
      />
      <linearGradient
        id="SVGID_137_"
        gradientUnits="userSpaceOnUse"
        x1={277.099}
        y1={100.347}
        x2={277.099}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_137_)" }}
        d="m276.73 115.39 2.23-.49h-1.46l-2.26.49z"
      />
      <linearGradient
        id="SVGID_138_"
        gradientUnits="userSpaceOnUse"
        x1={277.099}
        y1={111.907}
        x2={277.099}
        y2={159.426}
      >
        <stop offset={0} style={{ stopColor: "#15fffb" }} />
        <stop offset={0.072} style={{ stopColor: "#11dbfc" }} />
        <stop offset={0.198} style={{ stopColor: "#09a3fd" }} />
        <stop offset={0.306} style={{ stopColor: "#047afe" }} />
        <stop offset={0.39} style={{ stopColor: "#0160ff" }} />
        <stop offset={0.441} style={{ stopColor: "#0057ff" }} />
        <stop offset={1} style={{ stopColor: "#008391" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_138_)" }}
        d="m276.73 115.39 2.23-.49h-1.46l-2.26.49z"
      />
      <linearGradient
        id="SVGID_139_"
        gradientUnits="userSpaceOnUse"
        x1={705.275}
        y1={115.392}
        x2={705.275}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_139_)" }}
        d="m705.93 115.39-.48-.41h-.83l.46.41z"
      />
      <linearGradient
        id="SVGID_140_"
        gradientUnits="userSpaceOnUse"
        x1={705.275}
        y1={100.347}
        x2={705.275}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_140_)" }}
        d="m705.93 115.39-.48-.41h-.83l.46.41z"
      />
      <linearGradient
        id="SVGID_141_"
        gradientUnits="userSpaceOnUse"
        x1={705.275}
        y1={111.907}
        x2={705.275}
        y2={159.426}
      >
        <stop offset={0} style={{ stopColor: "#15fffb" }} />
        <stop offset={0.072} style={{ stopColor: "#11dbfc" }} />
        <stop offset={0.198} style={{ stopColor: "#09a3fd" }} />
        <stop offset={0.306} style={{ stopColor: "#047afe" }} />
        <stop offset={0.39} style={{ stopColor: "#0160ff" }} />
        <stop offset={0.441} style={{ stopColor: "#0057ff" }} />
        <stop offset={1} style={{ stopColor: "#008391" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_141_)" }}
        d="m705.93 115.39-.48-.41h-.83l.46.41z"
      />
      <linearGradient
        id="SVGID_142_"
        gradientUnits="userSpaceOnUse"
        x1={876.543}
        y1={115.392}
        x2={876.543}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_142_)" }}
        d="m877.93 115.39-1.56-.45h-1.21l1.54.45z"
      />
      <linearGradient
        id="SVGID_143_"
        gradientUnits="userSpaceOnUse"
        x1={876.543}
        y1={100.347}
        x2={876.543}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_143_)" }}
        d="m877.93 115.39-1.56-.45h-1.21l1.54.45z"
      />
      <linearGradient
        id="SVGID_144_"
        gradientUnits="userSpaceOnUse"
        x1={876.543}
        y1={111.907}
        x2={876.543}
        y2={159.426}
      >
        <stop offset={0} style={{ stopColor: "#15fffb" }} />
        <stop offset={0.072} style={{ stopColor: "#11dbfc" }} />
        <stop offset={0.198} style={{ stopColor: "#09a3fd" }} />
        <stop offset={0.306} style={{ stopColor: "#047afe" }} />
        <stop offset={0.39} style={{ stopColor: "#0160ff" }} />
        <stop offset={0.441} style={{ stopColor: "#0057ff" }} />
        <stop offset={1} style={{ stopColor: "#008391" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_144_)" }}
        d="m877.93 115.39-1.56-.45h-1.21l1.54.45z"
      />
      <linearGradient
        id="SVGID_145_"
        gradientUnits="userSpaceOnUse"
        x1={448.298}
        y1={115.392}
        x2={448.298}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_145_)" }}
        d="m448.29 115.39 1.02-.45h-.99l-1.03.45z"
      />
      <linearGradient
        id="SVGID_146_"
        gradientUnits="userSpaceOnUse"
        x1={448.298}
        y1={100.347}
        x2={448.298}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_146_)" }}
        d="m448.29 115.39 1.02-.45h-.99l-1.03.45z"
      />
      <linearGradient
        id="SVGID_147_"
        gradientUnits="userSpaceOnUse"
        x1={448.298}
        y1={111.907}
        x2={448.298}
        y2={159.426}
      >
        <stop offset={0} style={{ stopColor: "#15fffb" }} />
        <stop offset={0.072} style={{ stopColor: "#11dbfc" }} />
        <stop offset={0.198} style={{ stopColor: "#09a3fd" }} />
        <stop offset={0.306} style={{ stopColor: "#047afe" }} />
        <stop offset={0.39} style={{ stopColor: "#0160ff" }} />
        <stop offset={0.441} style={{ stopColor: "#0057ff" }} />
        <stop offset={1} style={{ stopColor: "#008391" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_147_)" }}
        d="m448.29 115.39 1.02-.45h-.99l-1.03.45z"
      />
      <linearGradient
        id="SVGID_148_"
        gradientUnits="userSpaceOnUse"
        x1={-64.589}
        y1={115.392}
        x2={-64.589}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_148_)" }}
        d="m-66.31 115.39 6.06-.67h-2.56l-6.12.67z"
      />
      <linearGradient
        id="SVGID_149_"
        gradientUnits="userSpaceOnUse"
        x1={-64.589}
        y1={100.347}
        x2={-64.589}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_149_)" }}
        d="m-66.31 115.39 6.06-.67h-2.56l-6.12.67z"
      />
      <linearGradient
        id="SVGID_150_"
        gradientUnits="userSpaceOnUse"
        x1={-64.589}
        y1={111.907}
        x2={-64.589}
        y2={159.426}
      >
        <stop offset={0} style={{ stopColor: "#15fffb" }} />
        <stop offset={0.072} style={{ stopColor: "#11dbfc" }} />
        <stop offset={0.198} style={{ stopColor: "#09a3fd" }} />
        <stop offset={0.306} style={{ stopColor: "#047afe" }} />
        <stop offset={0.39} style={{ stopColor: "#0160ff" }} />
        <stop offset={0.441} style={{ stopColor: "#0057ff" }} />
        <stop offset={1} style={{ stopColor: "#008391" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_150_)" }}
        d="m-66.31 115.39 6.06-.67h-2.56l-6.12.67z"
      />
      <linearGradient
        id="SVGID_151_"
        gradientUnits="userSpaceOnUse"
        x1={1047.71}
        y1={115.392}
        x2={1047.71}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_151_)" }}
        d="m1050 115.39-2.85-.5h-1.73l2.82.5z"
      />
      <linearGradient
        id="SVGID_152_"
        gradientUnits="userSpaceOnUse"
        x1={1047.71}
        y1={100.347}
        x2={1047.71}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_152_)" }}
        d="m1050 115.39-2.85-.5h-1.73l2.82.5z"
      />
      <linearGradient
        id="SVGID_153_"
        gradientUnits="userSpaceOnUse"
        x1={1047.71}
        y1={111.907}
        x2={1047.71}
        y2={159.426}
      >
        <stop offset={0} style={{ stopColor: "#15fffb" }} />
        <stop offset={0.072} style={{ stopColor: "#11dbfc" }} />
        <stop offset={0.198} style={{ stopColor: "#09a3fd" }} />
        <stop offset={0.306} style={{ stopColor: "#047afe" }} />
        <stop offset={0.39} style={{ stopColor: "#0160ff" }} />
        <stop offset={0.441} style={{ stopColor: "#0057ff" }} />
        <stop offset={1} style={{ stopColor: "#008391" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_153_)" }}
        d="m1050 115.39-2.85-.5h-1.73l2.82.5z"
      />
      <linearGradient
        id="SVGID_154_"
        gradientUnits="userSpaceOnUse"
        x1={619.601}
        y1={115.392}
        x2={619.601}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_154_)" }}
        d="M619.99 115.39v-.4h-.77l-.01.4z"
      />
      <linearGradient
        id="SVGID_155_"
        gradientUnits="userSpaceOnUse"
        x1={619.601}
        y1={100.347}
        x2={619.601}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_155_)" }}
        d="M619.99 115.39v-.4h-.77l-.01.4z"
      />
      <linearGradient
        id="SVGID_156_"
        gradientUnits="userSpaceOnUse"
        x1={619.601}
        y1={111.907}
        x2={619.601}
        y2={159.426}
      >
        <stop offset={0} style={{ stopColor: "#15fffb" }} />
        <stop offset={0.072} style={{ stopColor: "#11dbfc" }} />
        <stop offset={0.198} style={{ stopColor: "#09a3fd" }} />
        <stop offset={0.306} style={{ stopColor: "#047afe" }} />
        <stop offset={0.39} style={{ stopColor: "#0160ff" }} />
        <stop offset={0.441} style={{ stopColor: "#0057ff" }} />
        <stop offset={1} style={{ stopColor: "#008391" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_156_)" }}
        d="M619.99 115.39v-.4h-.77l-.01.4z"
      />
      <linearGradient
        id="SVGID_157_"
        gradientUnits="userSpaceOnUse"
        x1={106.099}
        y1={115.392}
        x2={106.099}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_157_)" }}
        d="m105.2 115.39 3.81-.56h-1.98l-3.85.56z"
      />
      <linearGradient
        id="SVGID_158_"
        gradientUnits="userSpaceOnUse"
        x1={106.099}
        y1={100.347}
        x2={106.099}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_158_)" }}
        d="m105.2 115.39 3.81-.56h-1.98l-3.85.56z"
      />
      <linearGradient
        id="SVGID_159_"
        gradientUnits="userSpaceOnUse"
        x1={106.099}
        y1={111.907}
        x2={106.099}
        y2={159.426}
      >
        <stop offset={0} style={{ stopColor: "#15fffb" }} />
        <stop offset={0.072} style={{ stopColor: "#11dbfc" }} />
        <stop offset={0.198} style={{ stopColor: "#09a3fd" }} />
        <stop offset={0.306} style={{ stopColor: "#047afe" }} />
        <stop offset={0.39} style={{ stopColor: "#0160ff" }} />
        <stop offset={0.441} style={{ stopColor: "#0057ff" }} />
        <stop offset={1} style={{ stopColor: "#008391" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_159_)" }}
        d="m105.2 115.39 3.81-.56h-1.98l-3.85.56z"
      />
      <linearGradient
        id="SVGID_160_"
        gradientUnits="userSpaceOnUse"
        x1={962.139}
        y1={115.392}
        x2={962.139}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_160_)" }}
        d="m963.96 115.39-2.18-.48h-1.46l2.16.48z"
      />
      <linearGradient
        id="SVGID_161_"
        gradientUnits="userSpaceOnUse"
        x1={962.139}
        y1={100.347}
        x2={962.139}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_161_)" }}
        d="m963.96 115.39-2.18-.48h-1.46l2.16.48z"
      />
      <linearGradient
        id="SVGID_162_"
        gradientUnits="userSpaceOnUse"
        x1={962.139}
        y1={111.907}
        x2={962.139}
        y2={159.426}
      >
        <stop offset={0} style={{ stopColor: "#15fffb" }} />
        <stop offset={0.072} style={{ stopColor: "#11dbfc" }} />
        <stop offset={0.198} style={{ stopColor: "#09a3fd" }} />
        <stop offset={0.306} style={{ stopColor: "#047afe" }} />
        <stop offset={0.39} style={{ stopColor: "#0160ff" }} />
        <stop offset={0.441} style={{ stopColor: "#0057ff" }} />
        <stop offset={1} style={{ stopColor: "#008391" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_162_)" }}
        d="m963.96 115.39-2.18-.48h-1.46l2.16.48z"
      />
      <linearGradient
        id="SVGID_163_"
        gradientUnits="userSpaceOnUse"
        x1={790.922}
        y1={115.392}
        x2={790.922}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_163_)" }}
        d="m791.91 115.39-.99-.43h-.99l.98.43z"
      />
      <linearGradient
        id="SVGID_164_"
        gradientUnits="userSpaceOnUse"
        x1={790.922}
        y1={100.347}
        x2={790.922}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_164_)" }}
        d="m791.91 115.39-.99-.43h-.99l.98.43z"
      />
      <linearGradient
        id="SVGID_165_"
        gradientUnits="userSpaceOnUse"
        x1={790.922}
        y1={111.907}
        x2={790.922}
        y2={159.426}
      >
        <stop offset={0} style={{ stopColor: "#15fffb" }} />
        <stop offset={0.072} style={{ stopColor: "#11dbfc" }} />
        <stop offset={0.198} style={{ stopColor: "#09a3fd" }} />
        <stop offset={0.306} style={{ stopColor: "#047afe" }} />
        <stop offset={0.39} style={{ stopColor: "#0160ff" }} />
        <stop offset={0.441} style={{ stopColor: "#0057ff" }} />
        <stop offset={1} style={{ stopColor: "#008391" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_165_)" }}
        d="m791.91 115.39-.99-.43h-.99l.98.43z"
      />
      <linearGradient
        id="SVGID_166_"
        gradientUnits="userSpaceOnUse"
        x1={533.936}
        y1={115.392}
        x2={533.936}
        y2={-100.084}
      >
        <stop offset={0} style={{ stopColor: "#0057bf" }} />
        <stop offset={0.087} style={{ stopColor: "#2748c3" }} />
        <stop offset={0.214} style={{ stopColor: "#5a35c8" }} />
        <stop offset={0.344} style={{ stopColor: "#8625cd" }} />
        <stop offset={0.473} style={{ stopColor: "#aa18d0" }} />
        <stop offset={0.604} style={{ stopColor: "#c60dd3" }} />
        <stop offset={0.734} style={{ stopColor: "#d906d5" }} />
        <stop offset={0.866} style={{ stopColor: "#e501d7" }} />
        <stop offset={1} style={{ stopColor: "#e900d7" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_166_)" }}
        d="m534.12 115.39.48-.42h-.83l-.49.42z"
      />
      <linearGradient
        id="SVGID_167_"
        gradientUnits="userSpaceOnUse"
        x1={533.936}
        y1={100.347}
        x2={533.936}
        y2={177.17}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_167_)" }}
        d="m534.12 115.39.48-.42h-.83l-.49.42z"
      />
      <linearGradient
        id="SVGID_168_"
        gradientUnits="userSpaceOnUse"
        x1={533.936}
        y1={111.907}
        x2={533.936}
        y2={159.426}
      >
        <stop offset={0} style={{ stopColor: "#15fffb" }} />
        <stop offset={0.072} style={{ stopColor: "#11dbfc" }} />
        <stop offset={0.198} style={{ stopColor: "#09a3fd" }} />
        <stop offset={0.306} style={{ stopColor: "#047afe" }} />
        <stop offset={0.39} style={{ stopColor: "#0160ff" }} />
        <stop offset={0.441} style={{ stopColor: "#0057ff" }} />
        <stop offset={1} style={{ stopColor: "#008391" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_168_)" }}
        d="m534.12 115.39.48-.42h-.83l-.49.42z"
      />
      <path
        style={{ fill: "#4becf9" }}
        d="m1120.4 114.04-473.62-.12-473.62.12-236.81.16-67.25.07v.38l67.25.07h3.4l82.29.05 2.28.01 82.71.05h1.98l64.15.04 18.99.01h1.73l83.62.02h1.46l83.93.02h1.21l84.22.02h.99l84.46.03h.83l84.62.02h.77l26.79.01 57.84-.02h.83l84.48-.02h.99l84.24-.02h1.21l83.95-.03h1.46l83.64-.02h1.73l73.25-.02h10.04l2-.01 82.73-.05h2.29l82.32-.05h2.58l54.85-.04 25.68-.03v-.47l-25.68-.03z"
      />
    </g>
    <g id="Layer_7">
      <linearGradient
        id="SVGID_169_"
        gradientUnits="userSpaceOnUse"
        x1={961.917}
        y1={142.162}
        x2={961.917}
        y2={-63.003}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_169_)" }}
        d="m1257.3 114.66-16.73-1.86-15.38-1.65-16.4-5.84-5.56 2.37-14.89-8.48-15.59-8.29-13.58 8.89-21.7-3.78-16.43-8.78-6.71 5.12-11.8-5.98-10.06-7.27-7.71-.73-9.13 3.73-8.94.24-11.71-1.43-8.63-2.96-4.29 1.28-6.45 3.08-23.89-15.18-10.33-5.03-10.62 2.98-8.31 6.96-5.61 2.6-5.81-1.75-7.97-6.02-9.09-9.23-11.91 4.66-10.62 2.26-9.22-9.69-6.98-4.3-4.13 3.73-5.81-.02-6.82 2.44-16.73-3.11-15.53-2.5-12.06 5.4-19.09-3.03-14.56 6.19-16.73-.18L785 72.81l-14.06 7.69-17.91 4.34-11.41 9.14-20.23-.82-9.77 2.08-1.94 3.79-6.16 2.66-14.35 5.57-22.64 7.4h590.77z"
      />
      <path
        className="st170"
        d="M1257.3 115.16H663.39l25.62-8.38 14.32-5.56 5.98-2.58 1.96-3.85 10.07-2.14h.06l20.04.82 11.46-9.13 17.85-4.32 14-7.66L803.63 59l16.79.18 14.6-6.2 19.08 3.03 12.06-5.41 15.67 2.53 16.61 3.09 6.86-2.43 5.63.02 4.26-3.85 7.4 4.58 9.02 9.48 10.35-2.2 12.13-4.76 9.32 9.47 7.84 5.91 5.54 1.67 5.38-2.5 8.45-7.01 10.8-3.03 10.5 5.11 23.7 15.06 6.19-2.96 4.52-1.35 8.74 3 11.66 1.42 8.77-.24 9.16-3.74 7.97.75 10.17 7.35 11.46 5.8 6.71-5.13 16.64 8.9 21.42 3.73 13.66-8.95 15.85 8.42 14.69 8.37 5.51-2.35 16.53 5.88 15.31 1.65 16.74 1.86-.02 1.01zm-587.63-1h578.59l-23.23-2.54-16.22-5.77-5.61 2.39-15.11-8.61-15.32-8.14-13.5 8.84-22.04-3.87-16.15-8.63-6.7 5.12-12.09-6.12-10.02-7.23-7.45-.7-9.19 3.72-8.93.24-11.89-1.46-8.48-2.91-4.14 1.23-6.62 3.17-24.12-15.33-10.11-4.91-10.33 2.9-8.34 6.96-5.79 2.69-6.15-1.89-7.97-6.01-8.92-9.05-11.6 4.54-10.98 2.34-9.36-9.84-6.61-4.07-3.99 3.61-5.92-.03-6.86 2.46-.13-.03-16.74-3.12-15.37-2.48-12.06 5.41L835.13 54l-14.52 6.17-16.67-.17-18.65 13.22-14.23 7.77-17.81 4.31-11.47 9.19-.19-.01-20.16-.82-9.48 2.02-1.91 3.74-6.33 2.73-14.36 5.57-19.68 6.44z"
      />
      <path
        className="st170"
        d="m1208.58 114.8-2.46-3.85 2.44-5.73.46.19-2.33 5.49 2.31 3.63z"
      />
      <path
        className="st170"
        d="m1224.01 114.95-17.56-3.76-5.61 3.68-.28-.42 5.79-3.79.11.02 17.23 3.69 1.27-3.31.47.17zM1194.08 111.19l-.07-.02-23.46-5.67 1.95-14.62.49.07-1.88 14.17 22.94 5.55 9.1-3.22.17.47z"
      />
      <path
        className="st170"
        d="m1194.33 114.69-.51-3.73.49-.07.51 3.74zM1153.32 114.82l-.38-.32 5.33-6.18.07-.02 31.8-8.29.12.49-31.72 8.27z"
      />
      <path
        className="st170"
        d="m1141.72 110.25-.08-.08-8.09-7.66.34-.36 8.01 7.58 16.27-1.42.75-8.53.49.05-.78 8.95z"
      />
      <path
        className="st170"
        d="m1143.36 114.75-1.82-4.77 5.32-12.38.46.2-5.24 12.19 1.75 4.58z"
      />
      <path
        transform="rotate(-19.352 1135.258 112.398)"
        className="st170"
        d="M1128.09 112.07h14.12v.5h-14.12z"
      />
      <path
        className="st170"
        d="m1119.23 103.28.02-.29 1.54-15.78.5.05-1.51 15.49 13.8-.66 3.68-6.19.43.25-3.82 6.43h-.14z"
      />
      <path className="st170" d="m1121.18 87.79-.33-.39.38-.32.33.39z" />
      <path
        transform="rotate(-40.043 1127.454 94.987)"
        className="st170"
        d="M1127.3 85.38h.5v19.2h-.5z"
      />
      <path
        className="st170"
        d="m1120.79 114.69-1.53-11.6-5.05-10.42-10.96 4.14-.97-10.41.5-.05.91 9.76 10.77-4.07 5.29 10.94 1.54 11.65z"
      />
      <path
        className="st170"
        d="m1108.58 106.48-5.29-9.82-11.96-4.13.89-13.44.5.03-.86 13.06 11.78 4.08.05.08 5.14 9.54 10.6-3.11.14.48z"
      />
      <path
        className="st170"
        d="m1093.6 115.04-2.25-9.67.49-.11 2.09 9.02 15-8.51.53 8.88-.5.03-.48-8.08z"
      />
      <path
        transform="rotate(-31.077 1083.861 109.975)"
        className="st170"
        d="M1074.79 109.74h18.11v.5h-18.11z"
      />
      <path
        className="st170"
        d="M1091.35 105.81V92.57l-13.73-2.04 6.92-12.27.43.25-6.55 11.63 13.43 2v12.68l11.47-8.56.3.4zM1048.57 91.59l-2.47-13.58.5-.09 2.23 12.32 5.94-9.45.43.27z"
      />
      <path
        className="st170"
        d="m1091.54 105.56-21.83-5.15 8.03-10.13-2.35-8.1.48-.14 2.43 8.35-.08.1-7.63 9.61 21.06 4.97z"
      />
      <path
        className="st170"
        d="m1070.41 114.67-.5-14.12-18.1 2.91-.08-.49 18.66-3.01.52 14.69z"
      />
      <path
        className="st170"
        d="m1051.51 114.47-.5-.02.53-11.33 3.26-7.51.13-.03 23.03-5.49.12.49-22.92 5.46-3.15 7.23z"
      />
      <path
        className="st170"
        d="m746.77 110.74-.12-.01-13.05-1.4-7.89-1.14-13.56 1.6-10.18-3.38-12.78 1.1-.04-.5 12.88-1.1.05.01 10.12 3.36 13.52-1.6 7.94 1.15 12.92 1.39 5.75-6.1 18.81 1.79 10.51-2.32.06.01 15.49 3.14 6.35-2.39 10.48 2.38 19.94-6.74.08.03 10.97 3.32 16.48-4.83.07.02 22.28 5.37 10.7-1.35 15.35 1.58 6.61-.56 10 1.31 17.96 4.28 12.38-6.75.09.02 13.2 3.71 4.12-2.92.11.02 14.22 3.01 4.32-6.44 11.48.03 11.39 5.53 7.63-2.52.06.01 15.21 2.7 4.84-3.7 8.28 1.11-.07.49-8.08-1.07-4.84 3.7-.11-.02-15.26-2.71-7.69 2.55-.09-.04-11.44-5.56h-11.04l-4.36 6.5-14.44-3.05-4.15 2.93-.11-.03-13.21-3.72-12.37 6.75-.09-.02-18.03-4.3-9.92-1.3-6.6.56-15.34-1.58-10.73 1.35-.05-.01-22.27-5.36-16.49 4.83-11.04-3.34-19.93 6.73-10.47-2.37-6.34 2.37-.07-.01-15.51-3.14-10.49 2.31h-.04l-18.6-1.77z"
      />
      <path
        className="st170"
        d="m1002.48 114.69-.49-.06 2.04-14.6-.77-10.93-3.32-11.64v-.04l1.2-15.33.5.04-1.2 15.28 3.31 11.61.78 11.02zM1016.02 114.66h-.5v-9.02l-2.72-11.34.16-15.48-1.48-11.65.49-.07 1.49 11.69-.17 15.45 2.72 11.35zM1025.86 114.72l-2.7-11.64 5.27-11.54-6.97-16.95.46-.19 7.05 17.15-5.28 11.58 2.66 11.47zM1046.1 114.52l-2.75-12.3-3.65-5.42-4.32-14.41.47-.14 4.31 14.37 3.67 5.45 2.76 12.34zM1036.82 114.52l-.49-.12 2.17-8.63.49.12zM995.22 114.7l-2.42-14.6-4.34-11.29v-9.88l2.07-13.88.49.08-2.07 13.84.01 9.79 4.32 11.21 2.44 14.65zM990.57 114.72l-2.09-8.06-8.63-8.58-4.42-11.76 1.17-11.69.5.05-1.16 11.58 4.35 11.59 8.64 8.55.02.07 2.11 8.13z"
      />
      <path
        className="st170"
        d="m988.51 88.97-7.23-9.03.01-.11.93-7.81.49.06-.91 7.71 7.1 8.86zM957.56 114.48l-.94-11.84.02-.06 3.41-8.44 4.69-7.98 6.07-13.37.46.21-6.08 13.39-4.7 7.97-3.36 8.35.93 11.73zM968.52 114.72l-.49-.12 1.9-8.2-.9-10.54.49-.05.92 10.63-.01.04z"
      />
      <path
        className="st170"
        d="m978.51 114.55-4.42-10.97-5.08-7.69.02-.1 1.76-9.54.49.09-1.74 9.44 4.99 7.57 4.45 11.02zM975.59 83.16l-7.79-3.1-7.18-1.55-10.76-6.42-7.98-9.62.39-.32 7.95 9.59 10.62 6.32 7.1 1.53 7.83 3.1z"
      />
      <path
        className="st170"
        d="m945.65 114.72-1.37-5.15-11.2-9.49.19-.19 13.52-13.82 13.78-7.97 5.95-8.6.41.28-6.06 8.73-13.8 7.98-13.26 13.55 10.92 9.25.02.08 1.38 5.23zM916.78 114.66h-.5v-10.78l-5.79-11.33 13.54-13.51-4.18-14.54.48-.14 4.26 14.82-13.49 13.46 5.68 11.18z"
      />
      <path
        transform="rotate(-19.118 917.817 57.836)"
        className="st170"
        d="M917.51 50.16h.5v15.36h-.5z"
      />
      <path
        className="st170"
        d="m928.53 114.72-2.3-9.53-1.91-8.08.05-.09L933 80.87l-7.59-16.27-3.41-9.63.47-.17 3.4 9.61 7.69 16.48-.06.11-8.65 16.18 1.87 7.89 2.3 9.53zM896.03 114.7l-1.7-11.91.01-.05 2.24-8.64.06-.05 13.28-11.29 7.47-13.85-6.5-14.5.46-.2 6.59 14.72-7.66 14.18-13.26 11.27-2.18 8.44 1.68 11.81zM905.83 114.76l-.47-.2 4.27-10.24.92-11.74.49.04-.93 11.85z"
      />
      <path
        transform="rotate(-84.635 897.632 65.789)"
        className="st170"
        d="M888.58 65.51h18.13v.5h-18.13z"
      />
      <path
        className="st170"
        d="m863.28 114.69-2.03-15.96.03-.07 11.95-23.22 2.33-13.32.04-.06 6.99-8.41.38.32-6.94 8.36-2.34 13.31-11.92 23.18 2 15.81zM884.15 114.67l-.56-10.51 1.17-19.5.08-.07 11.94-10.11.15.09 13.23 8.08 14.14-3.8.06.01 17.61 3.48 7.92-10.61.07-.03 13.02-5.05.18.47-12.95 5.02-8.03 10.75-.15-.03-17.71-3.5-14.24 3.83-.09-.05-13.16-8.05-11.58 9.81-1.16 19.27.56 10.48zM875.29 114.71l-2.78-13.1.03-.07 8.24-19.83 3.52-14.31 5.51-12.34.46.2-5.5 12.3-3.52 14.31-8.22 19.79 2.75 12.95z"
      />
      <path
        className="st170"
        d="m909.87 82.94-1.05-15.78-3.76-12.8.49-.14 3.76 12.85 1.06 15.84zM842.41 114.72l-.49-.12 2.84-10.99-4.34-13.62 6.63-5.75 2.5-9.09 4.89-5.22 3.35-9.52 8.62-9.88.93 7.33-3.29 9.36-5.84 13.64-4.96 10.81.22 9.52 2.2 13.36-.49.08-2.21-13.39-.22-9.68.02-.05 4.98-10.85 5.83-13.62 3.24-9.23-.77-6.11-7.85 9-3.36 9.53-4.88 5.21-2.47 9.08-6.49 5.63 4.28 13.44-.02.07z"
      />
      <path
        className="st170"
        d="m761 90.34-8.74-5.7 21.28-1.41 14.64-5.97 15.33-10.01 10.39 1.87 20.21-10.57.09.02 18.4 3.29 5.31-1.53 9.1-2.73 14.68-4.22.13.48-14.67 4.21-9.1 2.74-5.42 1.56-.06-.01-18.38-3.29-20.21 10.58-.08-.02-10.28-1.85-15.21 9.92-14.8 6.02-19.82 1.32 7.31 4.76 13.61-3.06 7.14 1.54 12.51-9 .08.01 12.68.78 20.7-14 .1.01 26.72 3.73 9.1-2.91 12.07-4.95.11.06 20.88 12.48 12.22-7.62.1.02 8.41 1.75 2.41-4.46 11.65.14.07.12 10.61 18.01 4.73 3.59h28.57l5.84-6.41 18.75-2.44 12.99 1.37 8.41-5.3.26.42-8.55 5.4-.08-.01-13.03-1.38-18.5 2.41-5.87 6.44h-28.96l-4.97-3.79-10.56-17.94-11.06-.13-2.44 4.52-.19-.04-8.5-1.76-12.3 7.68-21.04-12.58-11.86 4.87-9.23 2.95-26.74-3.73-20.68 13.99h-.09l-12.67-.79-12.54 9.03-7.25-1.57zM803.64 104.85l-.16-.47 9.25-3.08 15.67-9.59 12.31-1.89h.03l12.36 1.56 8.57 7.2-.32.38-8.46-7.11-12.17-1.53-12.19 1.88-15.59 9.55z"
      />
      <path
        className="st170"
        d="m830.01 114.73-.48-.14 4.17-14.29-5.58-8.37 9.66-10.23v-8.24l5.61-5.13 9.01-6.37 1.53-5.5.48.13-1.58 5.67-.07.06-9.05 6.4-5.43 4.96v8.22l-9.52 10.09 5.49 8.22-.03.12z"
      />
      <path
        className="st170"
        d="m822.88 114.67-.49-10.57-1.56-7.83.06-.08 10.5-15.77-3.2-8.26-.57-5.91 6.3-7.54.92-5.26.49.08-.94 5.39-.04.05-6.21 7.44.55 5.67 3.26 8.4-10.59 15.9 1.53 7.66.49 10.61zM815.24 114.71l-1.45-7.67-1.2-5.35-6.13-3.56.33-9.87.15-8 .04-.06 6.75-10.95 6.58-9.71.42.28-6.58 9.71-6.71 10.87-.15 7.88-.32 9.57 6.06 3.52 1.25 5.57 1.45 7.67zM798.86 114.72l-1.89-7.71.01-.04 1.36-13.37-1.71-7.95-2.49-6.15 9.17-12.07.23-7.94.5.01-.23 8.1-9.1 11.98 2.4 5.92 1.73 8.08-1.36 13.4 1.87 7.62z"
      />
      <path
        className="st170"
        d="m778.59 94.12 3.1-5.69 6.29-10.93-3.18-4.54.41-.29 3.36 4.8-6.45 11.21-2.38 4.38 17.11-7.73L806.98 88l21.39-16.13.11.02 21.29 3.18 10.95-1.01 12.77 1.22-.05.49-12.72-1.21-10.97 1.01-21.24-3.17-21.42 16.14-.12-.03-10.07-2.65z"
      />
      <path
        className="st170"
        d="m782.92 114.7-1.51-10.82-2.49-10.23.49-.12 2.49 10.25 1.52 10.85zM770.56 114.68l-.49-.04.83-8.44-3.08-7.73 6.62-11.5-1.07-3.37-2.61-2.93.37-.34 2.7 3.07 1.15 3.62-.05.1-6.55 11.39 3.03 7.61zM820.9 96.48l-4.52-5.68-.25-9.69 1.54-8.44 2.87-6.95 4.68-8.15.43.25-4.66 8.12-2.84 6.87-1.53 8.34.26 9.47 4.41 5.55zM757.63 114.68l-.49-.04.87-9.72 2.28-3.61.51-11.2-1.92-5.48.48-.17 1.94 5.57v.05l-.52 11.39-2.31 3.67zM752.52 114.67l-.34-10.24-2.29-5.37.01-.08 2.88-14.19.49.1-2.86 14.12 2.27 5.36.34 10.28zM747.8 114.74l-1.35-4.14-4.34-5.71-.74-10.89.5-.03.73 10.74 4.31 5.69 1.36 4.18zM726.85 114.71l-1.41-6.88 5.13-3.8-.07-10.44h.5l.07 10.69-5.08 3.76 1.35 6.57zM712.16 114.67l-.24-5.21.06-.07 5.93-8.33 3.25-8 .46.19-3.27 8.05-5.92 8.31.23 5.04z"
      />
      <path
        className="st170"
        d="m696.95 114.79-.43-.26 5.31-8.55 7.78-7.22 21.27 5.15 11.4.63 7.81-5.77 10.42 2.36 18.66-7.79 19.54.03 7.99 4.32 24.92-17.51.12.03 15.52 3.91 26.25-8.88.11.09 23.26 18.65 13.94-1.64.05.02 22.59 7.44 26.81-5.82.05.01 19.67 3.73 8.61-9.16 15.03.31 9.46 5.11 15.65-2.69.07.03 11.1 5.12 8.8-5.83.15.11 6.09 4.76 11.56-13.29.37.33-11.86 13.64-6.33-4.94-8.73 5.79-.13-.06-11.16-5.14L1013 94.5l-9.56-5.16-14.63-.28-8.64 9.2-19.89-3.77-26.85 5.83-.06-.02-22.6-7.45-14.04 1.65-.08-.07-23.24-18.62-26.13 8.83-.07-.02-15.49-3.9-24.99 17.56-.14-.07-8.06-4.37h-19.31l-18.66 7.81-10.36-2.35-7.77 5.75-11.62-.65-21.05-5.09-7.56 7.01z"
      />
      <linearGradient
        id="SVGID_170_"
        gradientUnits="userSpaceOnUse"
        x1={621.308}
        y1={139.496}
        x2={621.308}
        y2={-65.669}
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_170_)" }}
        d="m916.69 111.99-16.73-1.85-15.38-1.66-16.4-5.83-5.56 2.37-14.89-8.49-15.59-8.28-13.58 8.89-21.7-3.78-16.43-8.79-6.7 5.12-11.81-5.98-10.06-7.27-7.71-.72-9.13 3.72-8.94.25-11.71-1.43-8.63-2.97-4.29 1.28-6.44 3.08-23.89-15.18-10.34-5.03-10.62 2.98-8.31 6.96-5.61 2.61-5.81-1.76-7.97-6.01-9.09-9.23-11.9 4.66-10.63 2.25-9.22-9.68-6.98-4.31-4.13 3.74-5.81-.03-6.82 2.45-16.73-3.12-15.53-2.5-12.06 5.41-19.09-3.04-14.56 6.19-16.73-.18-18.79 13.32-14.06 7.68-17.91 4.34-11.41 9.15-20.23-.83-9.77 2.08-1.94 3.8-6.16 2.66-14.35 5.56-22.64 7.4h590.77z"
      />
      <path
        className="st170"
        d="M916.69 112.49H322.78l25.62-8.38 14.32-5.56 5.98-2.58 1.96-3.85 10.07-2.14h.06l20.04.82 11.46-9.13 17.85-4.32 14-7.66 18.87-13.38 16.79.18 14.6-6.2 19.08 3.03 12.06-5.41 15.67 2.53 16.61 3.09 6.86-2.43 5.63.02 4.26-3.85 7.4 4.58 9.02 9.48 10.35-2.2 12.13-4.76 9.32 9.47 7.84 5.91 5.54 1.67 5.38-2.5 8.45-7.01 10.8-3.03 10.5 5.11L695 79.05l6.19-2.96 4.52-1.35 8.74 3 11.66 1.42 8.77-.24 9.16-3.74 7.97.75 10.17 7.35 11.46 5.8 6.71-5.13 16.64 8.9 21.42 3.73 13.66-8.95 15.85 8.42 14.69 8.37 5.51-2.35 16.53 5.88 15.31 1.65 16.74 1.86-.01 1.03zm-587.63-1h578.59l-23.23-2.54-16.22-5.77-5.61 2.39-15.11-8.61-15.32-8.14-13.5 8.84-22.04-3.87-16.15-8.63-6.7 5.12-12.09-6.12-10.02-7.23-7.46-.7-9.19 3.72-8.93.24-11.89-1.46-8.48-2.91-4.14 1.23-6.62 3.17-24.12-15.33-10.11-4.92-10.33 2.9-8.34 6.96-5.79 2.69-6.15-1.89-7.97-6.01-8.92-9.05-11.6 4.54-10.98 2.34-9.36-9.84-6.61-4.07-3.99 3.61-5.92-.03-6.86 2.46-.13-.03-16.74-3.12-15.37-2.48-12.06 5.41-19.09-3.04-14.52 6.17-16.68-.18-18.65 13.22-14.23 7.77-17.81 4.31-11.47 9.19-.19-.01-20.16-.82-9.48 2.02-1.91 3.74-6.33 2.73-14.36 5.57-19.66 6.46z"
      />
      <path
        className="st170"
        d="m867.97 112.13-2.46-3.84 2.44-5.74.46.2-2.33 5.49 2.31 3.62z"
      />
      <path
        className="st170"
        d="m883.4 112.28-17.56-3.75-5.61 3.67-.27-.41 5.78-3.79.11.02 17.23 3.69 1.27-3.32.47.18zM853.47 108.52l-.07-.02-23.46-5.66 1.95-14.63.5.07-1.89 14.18 22.94 5.54 9.1-3.22.17.47z"
      />
      <path
        className="st170"
        d="m853.72 112.03-.51-3.73.49-.07.51 3.73zM812.71 112.16l-.38-.33 5.33-6.17.07-.02 31.8-8.29.12.48-31.72 8.27z"
      />
      <path
        className="st170"
        d="m801.11 107.58-.08-.08-8.09-7.65.35-.37 8 7.58 16.27-1.41.75-8.53.5.04-.78 8.95z"
      />
      <path
        className="st170"
        d="m802.75 112.08-1.82-4.76 5.32-12.38.46.19-5.24 12.2 1.75 4.58z"
      />
      <path
        transform="rotate(-19.352 794.614 109.713)"
        className="st170"
        d="M787.48 109.41h14.12v.5h-14.12z"
      />
      <path
        className="st170"
        d="m778.62 100.61.03-.29 1.53-15.77.5.05-1.51 15.48 13.8-.66 3.68-6.19.43.26-3.82 6.42-.13.01z"
      />
      <path className="st170" d="m780.57 85.13-.33-.4.38-.32.33.4z" />
      <path
        transform="rotate(-40.043 786.873 92.317)"
        className="st170"
        d="M786.69 82.72h.5v19.2h-.5z"
      />
      <path
        className="st170"
        d="m780.18 112.03-1.53-11.61-5.05-10.41-10.96 4.13-.97-10.41.5-.04.91 9.75 10.77-4.06 5.29 10.93 1.54 11.65z"
      />
      <path
        className="st170"
        d="M767.97 103.81 762.69 94l-11.97-4.13.89-13.45.5.04-.86 13.06 11.78 4.07.05.09 5.14 9.54 10.6-3.11.14.48z"
      />
      <path
        className="st170"
        d="m752.99 112.38-2.25-9.68.49-.11 2.09 9.02 15-8.51.53 8.88-.5.03-.48-8.08z"
      />
      <path
        transform="rotate(-31.077 743.246 107.313)"
        className="st170"
        d="M734.18 107.07h18.11v.5h-18.11z"
      />
      <path
        className="st170"
        d="M750.74 103.15V89.91l-13.73-2.05 6.92-12.27.43.25-6.55 11.63 13.43 2.01v12.67l11.47-8.56.3.4zM707.96 88.92l-2.47-13.58.5-.09 2.23 12.32 5.94-9.44.43.26z"
      />
      <path
        className="st170"
        d="m750.93 102.89-21.83-5.15 8.03-10.13-2.35-8.1.48-.14 2.43 8.35-.08.1-7.63 9.62 21.06 4.96z"
      />
      <path
        className="st170"
        d="m729.8 112-.5-14.12-18.1 2.92-.08-.5 18.66-3 .52 14.69z"
      />
      <path
        className="st170"
        d="m710.9 111.81-.5-.03.53-11.33 3.26-7.51.13-.03 23.03-5.49.12.49-22.92 5.46-3.15 7.24z"
      />
      <path
        className="st170"
        d="m406.16 108.07-.12-.01-13.05-1.4-7.89-1.14-13.56 1.61-10.18-3.38-12.78 1.09-.04-.5 12.88-1.1.05.02 10.13 3.36 13.51-1.6 7.94 1.15 12.92 1.38 5.75-6.1 18.81 1.79 10.51-2.32.06.01 15.49 3.14 6.35-2.38 10.48 2.38 19.94-6.74.08.02 10.97 3.32 16.48-4.82.07.01 22.29 5.37 10.69-1.34 15.35 1.57 6.61-.55 10 1.31 17.96 4.28 12.38-6.76.09.03 13.2 3.71 4.12-2.93.11.03 14.22 3 4.32-6.44 11.48.03 11.39 5.54 7.63-2.53.06.02 15.21 2.69 4.84-3.69 8.28 1.1-.07.5-8.07-1.08-4.85 3.71-.11-.02-15.26-2.71-7.69 2.55-.09-.05-11.44-5.56h-11.04l-4.36 6.5-14.44-3.05-4.15 2.94-.11-.04-13.21-3.71-12.37 6.75-.09-.02-18.03-4.3-9.92-1.31-6.6.56-15.34-1.58-10.73 1.36-.05-.01-22.27-5.36-16.49 4.82-11.04-3.34-19.93 6.74-10.47-2.38-6.33 2.38-.07-.02-15.52-3.14-10.49 2.32-.04-.01-18.6-1.77z"
      />
      <path
        className="st170"
        d="m661.87 112.03-.49-.07 2.04-14.6-.77-10.92-3.32-11.64v-.05l1.2-15.33.5.04-1.2 15.29 3.31 11.6.78 11.03zM675.41 111.99h-.5v-9.01l-2.72-11.35.16-15.48-1.48-11.65.49-.06 1.49 11.68-.17 15.45 2.72 11.35zM685.25 112.05l-2.7-11.63 5.27-11.55-6.97-16.95.46-.19 7.05 17.15-5.28 11.59 2.66 11.47zM705.5 111.85l-2.76-12.29-3.65-5.43-4.32-14.4.47-.15 4.32 14.37 3.66 5.45 2.76 12.34zM696.21 111.85l-.49-.12 2.17-8.63.49.12zM654.61 112.04l-2.42-14.61-4.34-11.29v-9.88l2.07-13.87.49.07-2.07 13.84.01 9.8 4.32 11.2 2.44 14.65zM649.96 112.06l-2.09-8.07-8.63-8.58-4.42-11.75 1.17-11.7.5.05-1.16 11.58 4.36 11.59 8.63 8.55.02.07 2.11 8.13z"
      />
      <path
        className="st170"
        d="m647.9 86.3-7.23-9.03.01-.1.93-7.82.49.06-.91 7.71 7.1 8.87zM616.95 111.81l-.94-11.84.02-.05 3.41-8.44 4.69-7.98 6.07-13.37.46.2-6.08 13.4-4.7 7.97-3.36 8.35.93 11.72zM627.91 112.05l-.49-.11 1.9-8.21-.9-10.54.5-.04.91 10.62-.01.04z"
      />
      <path
        className="st170"
        d="m637.9 111.89-4.42-10.97-5.08-7.7.02-.1 1.76-9.54.5.09-1.75 9.44 4.99 7.57 4.45 11.02zM634.98 80.49l-7.79-3.09-7.18-1.56-10.76-6.42-7.98-9.62.39-.31 7.95 9.58 10.62 6.32 7.1 1.53 7.83 3.1z"
      />
      <path
        className="st170"
        d="m605.04 112.06-1.37-5.16-11.2-9.49.19-.19 13.52-13.81 13.78-7.97 5.95-8.61.41.28-6.06 8.73-13.8 7.98-13.26 13.56 10.92 9.24.02.08 1.38 5.23zM576.18 111.99h-.5v-10.77l-5.8-11.34 13.54-13.51-4.18-14.54.48-.14 4.26 14.82-13.49 13.47 5.69 11.18z"
      />
      <path
        transform="rotate(-19.118 577.189 55.17)"
        className="st170"
        d="M576.91 47.49h.5v15.36h-.5z"
      />
      <path
        className="st170"
        d="m587.92 112.05-2.3-9.53-1.9-8.08.04-.08 8.63-16.15-7.59-16.27-3.41-9.64.47-.17 3.41 9.62 7.68 16.47-.06.11-8.65 16.18 1.87 7.9 2.3 9.53zM555.42 112.03l-1.7-11.91.01-.05 2.24-8.64.06-.05 13.28-11.28 7.47-13.86-6.49-14.49.45-.21 6.59 14.72-7.66 14.19-13.25 11.26-2.19 8.44 1.68 11.81zM565.22 112.09l-.46-.19 4.26-10.25.92-11.74.5.04-.94 11.86z"
      />
      <path
        transform="rotate(-84.635 557.028 63.111)"
        className="st170"
        d="M547.97 62.84h18.13v.5h-18.13z"
      />
      <path
        className="st170"
        d="m522.67 112.03-2.03-15.97.04-.07 11.94-23.21 2.33-13.33.04-.05 6.99-8.42.38.32-6.94 8.37-2.34 13.3-11.92 23.18 2 15.81zM543.54 112.01l-.56-10.51 1.17-19.51.08-.07 11.94-10.11.15.1 13.23 8.08 14.14-3.81.06.02 17.61 3.48 7.92-10.62.07-.03 13.02-5.05.18.47-12.95 5.02-8.03 10.76-.15-.03-17.71-3.5-14.24 3.83-.09-.06-13.16-8.04-11.58 9.8-1.16 19.27.56 10.48zM534.68 112.05l-2.78-13.1.03-.07 8.24-19.83 3.52-14.32 5.51-12.34.46.21-5.5 12.29-3.52 14.31-8.22 19.8 2.75 12.94z"
      />
      <path
        className="st170"
        d="m569.26 80.27-1.05-15.78-3.75-12.8.48-.14 3.76 12.85 1.06 15.84zM501.8 112.06l-.49-.13 2.84-10.99-4.33-13.62 6.62-5.74 2.5-9.1 4.89-5.22 3.35-9.52 8.62-9.88.93 7.34-3.29 9.36-5.84 13.63L512.64 89l.22 9.53 2.2 13.35-.49.08-2.21-13.39-.22-9.67.02-.06 4.98-10.85 5.83-13.62 3.25-9.23-.78-6.11-7.85 9-3.36 9.54-4.88 5.21-2.47 9.07-6.49 5.63 4.28 13.45-.02.07z"
      />
      <path
        className="st170"
        d="m420.39 87.67-8.74-5.7 21.28-1.41 14.64-5.97 15.33-10.01 10.39 1.88 20.21-10.57.09.01 18.4 3.29 5.31-1.52 9.1-2.74 14.68-4.22.14.48-14.68 4.22-9.1 2.74-5.42 1.55-.06-.01-18.38-3.28-20.21 10.57-.08-.02-10.28-1.85-15.21 9.92-14.8 6.03-19.82 1.31 7.31 4.77 13.61-3.07 7.14 1.55 12.51-9h.09l12.67.79 20.7-14.01.1.02 26.72 3.72 9.1-2.91 12.07-4.95.11.06 20.88 12.49 12.22-7.62.1.02 8.41 1.74 2.41-4.46 11.65.14.07.12 10.61 18.01 4.73 3.6h28.57l5.85-6.42 18.74-2.44 12.99 1.38 8.41-5.31.26.42-8.55 5.4-.08-.01-13.02-1.38-18.51 2.41-5.87 6.45h-28.96l-4.97-3.8-10.56-17.93-11.06-.14-2.44 4.52-.18-.03-8.5-1.76-12.31 7.67-21.04-12.57-11.86 4.86-9.23 2.95-26.74-3.72-20.68 13.99-.09-.01-12.67-.78-12.54 9.02-7.24-1.57zM463.03 102.19l-.16-.48 9.25-3.08 15.67-9.58 12.31-1.9h.03l12.36 1.56 8.57 7.21-.32.38-8.45-7.11-12.18-1.54-12.19 1.88-15.59 9.55z"
      />
      <path
        className="st170"
        d="m489.4 112.06-.48-.14 4.17-14.29-5.58-8.36 9.66-10.23v-8.25l5.62-5.12 9-6.38 1.53-5.5.48.13-1.58 5.68-.07.05-9.05 6.4-5.43 4.96v8.22l-9.52 10.09 5.49 8.23-.03.11z"
      />
      <path
        className="st170"
        d="m482.27 112.01-.49-10.58-1.56-7.82.06-.09 10.5-15.77-3.2-8.25-.57-5.92 6.3-7.54.92-5.26.49.09-.94 5.39-.04.05-6.21 7.44.55 5.66 3.27 8.4-10.6 15.9 1.53 7.66.49 10.61zM474.63 112.04l-1.44-7.66-1.21-5.35-6.13-3.57.33-9.86.15-8.01.04-.06 6.75-10.94 6.59-9.72.41.28-6.58 9.71-6.71 10.88-.15 7.87-.32 9.57 6.06 3.53 1.26 5.56 1.45 7.68zM458.26 112.05l-1.9-7.7.01-.05 1.36-13.36-1.71-7.96-2.49-6.15 9.18-12.07.22-7.94.5.02-.23 8.09-9.09 11.98 2.39 5.93 1.73 8.07-1.36 13.4 1.87 7.62z"
      />
      <path
        className="st170"
        d="m437.98 91.46 3.1-5.69 6.29-10.94-3.18-4.54.41-.29 3.36 4.81-6.45 11.2-2.38 4.38 17.12-7.72 10.12 2.66 21.4-16.12.1.01 21.29 3.18 10.95-1.01 12.77 1.22-.05.5-12.72-1.22-10.96 1.01-21.25-3.17-21.42 16.15-.12-.03-10.07-2.65z"
      />
      <path
        className="st170"
        d="m442.31 112.03-1.51-10.82-2.48-10.23.48-.12 2.49 10.26 1.52 10.84zM429.96 112.02l-.5-.05.83-8.44-3.08-7.72 6.62-11.51-1.06-3.36-2.62-2.94.37-.33 2.71 3.06 1.15 3.63-.06.09-6.55 11.4 3.03 7.61zM480.29 93.82l-4.52-5.68-.25-9.7 1.54-8.43 2.87-6.96 4.68-8.14.43.25-4.66 8.11-2.84 6.88-1.52 8.33.25 9.48 4.41 5.54zM417.03 112.02l-.5-.05.87-9.71 2.28-3.61.51-11.21-1.92-5.48.48-.17 1.94 5.58v.04l-.52 11.39-2.31 3.67zM411.91 112l-.34-10.23-2.29-5.38.01-.07 2.88-14.2.49.1-2.86 14.12 2.27 5.36.34 10.29zM407.19 112.07l-1.35-4.14-4.34-5.71-.74-10.89.5-.03.73 10.74 4.31 5.7 1.36 4.18zM386.24 112.04l-1.41-6.88 5.13-3.8-.07-10.44h.5l.08 10.69-5.09 3.76 1.35 6.57zM371.55 112.01l-.23-5.22.05-.07 5.93-8.32 3.25-8 .46.18-3.27 8.06-5.92 8.31.23 5.03z"
      />
      <path
        className="st170"
        d="m356.34 112.13-.43-.27 5.31-8.55 7.78-7.22 21.27 5.15 11.4.64 7.81-5.78 10.42 2.36 18.66-7.79 19.54.03 7.99 4.33 24.93-17.52.11.03 15.52 3.91 26.25-8.87.11.08 23.26 18.65 13.94-1.64.05.02 22.59 7.44 26.81-5.81.05.01 19.67 3.72 8.61-9.16 15.03.31 9.46 5.11 15.65-2.69.07.03 11.11 5.13 8.79-5.84.15.11 6.09 4.76 11.56-13.29.37.33-11.86 13.65-6.33-4.95-8.73 5.8-.13-.06-11.16-5.15-15.67 2.69-9.55-5.16-14.64-.28-8.64 9.2-19.89-3.76-26.85 5.82-.06-.02-22.6-7.45-14.04 1.65-.08-.06-23.23-18.63-26.14 8.83-.07-.01-15.49-3.91-24.99 17.57-.14-.08-8.06-4.37h-19.31l-18.66 7.82-10.35-2.35-7.78 5.75-11.62-.66-21.05-5.09-7.56 7.01z"
      />
      <linearGradient
        id="SVGID_171_"
        gradientUnits="userSpaceOnUse"
        x1={458.319}
        y1={141.162}
        x2={458.319}
        y2={-64.003}
        gradientTransform="matrix(-1 0 0 1 759.52 0)"
      >
        <stop offset={0} style={{ stopColor: "#00cfa9" }} />
        <stop offset={0.08} style={{ stopColor: "#07a897" }} />
        <stop offset={0.18} style={{ stopColor: "#0e7f84" }} />
        <stop offset={0.285} style={{ stopColor: "#155c74" }} />
        <stop offset={0.396} style={{ stopColor: "#1a4066" }} />
        <stop offset={0.514} style={{ stopColor: "#1e2a5c" }} />
        <stop offset={0.644} style={{ stopColor: "#211a55" }} />
        <stop offset={0.791} style={{ stopColor: "#221150" }} />
        <stop offset={1} style={{ stopColor: "#230e4f" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_171_)" }}
        d="m5.82 113.66 16.73-1.86 15.38-1.65 16.4-5.84 5.56 2.37 14.89-8.48 15.59-8.29 13.58 8.89 21.7-3.78 16.43-8.78 6.7 5.12 11.81-5.98 10.06-7.27 7.71-.73 9.13 3.73 8.94.24 11.71-1.43 8.63-2.96 4.29 1.28 6.44 3.08 23.89-15.18 10.34-5.03 10.62 2.98 8.31 6.96 5.61 2.6 5.81-1.75 7.97-6.02 9.09-9.23 11.9 4.66 10.63 2.26 9.22-9.69 6.98-4.3 4.13 3.73 5.81-.02 6.82 2.44 16.73-3.11 15.53-2.5 12.06 5.4 19.09-3.03 14.56 6.19 16.73-.18 18.79 13.31 14.06 7.69 17.91 4.34 11.41 9.14 20.23-.82 9.77 2.08 1.94 3.79 6.16 2.66 14.35 5.57 22.64 7.4H5.82z"
      />
      <path
        className="st170"
        d="M599.73 114.16H5.82l-.06-1 32.05-3.51 16.53-5.88 5.51 2.35 14.68-8.36 15.87-8.43 13.66 8.95 21.42-3.73 16.64-8.9 6.71 5.13 11.53-5.84 10.1-7.31 7.97-.75 9.16 3.74 8.84.24 11.58-1.42 8.74-3 4.45 1.32 6.26 2.99 23.66-15.04 10.55-5.14 10.99 3.13 8.26 6.91 5.38 2.5 5.54-1.67 7.9-5.96 9.27-9.42 12.21 4.78 10.28 2.17 9.12-9.56 7.3-4.5 4.26 3.85 5.71-.02 6.77 2.43 16.6-3.09 15.69-2.53L409.01 55l19.08-3.03 14.59 6.2 16.81-.17 18.92 13.41 13.95 7.63 18.05 4.42 11.26 9.03 20.1-.82.06.01 10.01 2.13 1.96 3.85 6 2.59 14.33 5.56 25.6 8.35zm-584.87-1h578.59l-19.66-6.42-14.37-5.58-6.35-2.74-1.91-3.74-9.48-2.02-20.35.83-11.47-9.19-17.93-4.36-14.06-7.69L459.18 59l-16.68.18-14.52-6.17-19.09 3.04-.14-.06-11.92-5.35-15.38 2.48-16.86 3.14-6.86-2.46-5.92.03-3.99-3.61-6.61 4.07-9.36 9.84-10.9-2.31-11.67-4.56-8.86 9-8.18 6.14-5.99 1.81-5.9-2.76-8.23-6.89-10.33-2.9-10.16 4.94-24.07 15.31-6.69-3.2-4.07-1.2-8.59 2.94-11.71 1.43-9.18-.28-9.02-3.68-7.45.7-9.95 7.19-12.15 6.16-6.7-5.12-16.3 8.69-21.89 3.81-13.5-8.84-15.33 8.15-15.1 8.6-5.6-2.39L38 110.66l-23.14 2.5z"
      />
      <path
        className="st170"
        d="m54.54 113.8-.42-.27 2.31-3.63-2.33-5.49.46-.19 2.44 5.73z"
      />
      <path
        className="st170"
        d="m39.11 113.95-1.42-3.72.47-.17 1.27 3.31 17.34-3.71.08.06 5.7 3.73-.27.42-5.61-3.68zM69.04 110.19l-.07-.03-9.17-3.24.17-.47 9.1 3.22 22.94-5.55-1.89-14.17.5-.07 1.95 14.62z"
      />
      <path
        className="st170"
        d="m68.79 113.69-.49-.06.51-3.74.49.07zM109.8 113.82l-5.22-6.05-31.72-8.27.12-.49 31.87 8.31 5.33 6.18z"
      />
      <path
        className="st170"
        d="m121.4 109.25-16.92-1.47-.78-8.95.5-.05.75 8.53 16.27 1.42 8-7.58.35.36z"
      />
      <path
        className="st170"
        d="m119.76 113.75-.47-.18 1.75-4.58-5.24-12.19.46-.2 5.32 12.38z"
      />
      <path
        className="st170"
        d="m134.55 113.9-13.32-4.68.16-.47 13.33 4.67zM143.89 102.28l-14.64-.7-3.82-6.43.43-.25 3.68 6.19 13.8.66-1.51-15.49.5-.05z"
      />
      <path className="st170" d="m141.94 86.79-.38-.32.33-.39.38.32z" />
      <path
        transform="rotate(-49.912 135.568 93.978)"
        className="st170"
        d="M125.97 93.73h19.2v.5h-19.2z"
      />
      <path
        className="st170"
        d="m142.33 113.69-.5-.06 1.56-11.73 5.27-10.86 10.77 4.07.91-9.76.5.05-.97 10.41-10.96-4.14-5.05 10.42z"
      />
      <path
        className="st170"
        d="m154.54 105.48-10.99-3.23.14-.48 10.6 3.11 5.19-9.62.09-.04 11.69-4.04-.86-13.06.5-.03.89 13.44-11.96 4.13z"
      />
      <path
        className="st170"
        d="m169.52 114.04-14.88-8.44-.48 8.08-.5-.03.53-8.88.35.19 14.65 8.32 2.09-9.02.49.11z"
      />
      <path
        transform="rotate(-58.929 179.275 108.992)"
        className="st170"
        d="M179.03 99.93h.5v18.11h-.5z"
      />
      <path
        className="st170"
        d="m171.77 104.81-12.27-9.15.3-.4 11.47 8.56V91.14l13.43-2-6.55-11.63.43-.25 6.92 12.27-13.73 2.04zM214.55 90.59l-6.63-10.53.43-.27 5.94 9.45 2.23-12.32.5.09z"
      />
      <path
        className="st170"
        d="m171.58 104.56-.11-.49 21.06-4.97-7.71-9.71 2.43-8.35.48.14-2.35 8.1 8.03 10.13z"
      />
      <path
        className="st170"
        d="m192.71 113.67-.5-.02.52-14.69 18.66 3.01-.08.49-18.1-2.91z"
      />
      <path
        className="st170"
        d="m211.61 113.47-.5-11.2-3.15-7.23-22.92-5.46.12-.49 23.15 5.52 3.29 7.59.51 11.25z"
      />
      <path
        className="st170"
        d="m516.35 109.74-5.76-6.1-18.64 1.77-.04-.01-10.45-2.3-15.58 3.15-.07-.02-6.27-2.35-10.47 2.37-.07-.02-19.86-6.71-11.04 3.34-.07-.02-16.42-4.81-22.32 5.37h-.04l-10.75-1.35-15.23 1.58-6.71-.56-9.88 1.31-18.1 4.31-12.37-6.75-13.33 3.75-4.14-2.93-14.44 3.05-4.36-6.5H258.9l-11.53 5.6-7.69-2.55-15.37 2.73-4.85-3.7-8.07 1.07-.07-.49 8.28-1.11.09.07 4.75 3.63 15.27-2.71.06.02 7.57 2.5 11.5-5.56h11.37l4.32 6.44 14.32-3.03 4.13 2.92 13.29-3.73 12.38 6.75 17.94-4.27 9.96-1.32 6.71.56 15.25-1.58 10.76 1.35 22.35-5.39.06.02 16.42 4.81 11.05-3.35.07.03 19.87 6.71 10.48-2.38 6.35 2.39 15.55-3.15.05.01 10.46 2.31 18.81-1.79 5.75 6.1 12.93-1.39 7.93-1.15 13.52 1.6 10.17-3.37h.05l12.83 1.1-.04.5-12.78-1.1-10.18 3.38-.06-.01-13.5-1.59-7.88 1.14z"
      />
      <path
        className="st170"
        d="m260.64 113.69-2.04-14.6.76-11.02 3.32-11.66-1.2-15.28.5-.04 1.2 15.37-.01.05-3.32 11.65-.76 10.92 2.04 14.55zM247.6 113.66h-.5v-9.02l2.73-11.46-.17-15.36 1.49-11.72.49.07-1.49 11.68.18 15.38-2.73 11.47zM237.26 113.72l-.49-.12 2.66-11.47-5.29-11.58 7.06-17.15.46.19-6.97 16.95 5.27 11.54zM217.01 113.52l-.48-.11 2.8-12.43 3.63-5.36 4.31-14.37.47.14-4.35 14.48-3.62 5.35zM226.3 113.52l-2.17-8.63.49-.12 2.17 8.63zM267.9 113.7l-.5-.08 2.43-14.6 4.33-11.26v-9.83l-2.06-13.8.49-.08 2.07 13.84v9.97l-4.35 11.29zM272.55 113.72l-.49-.12 2.13-8.2.05-.05 8.59-8.5 4.35-11.59-1.16-11.58.5-.05 1.17 11.69-4.47 11.85-8.58 8.49z"
      />
      <path
        className="st170"
        d="m274.61 87.97-.39-.32 7.1-8.86-.91-7.71.49-.06.94 7.92zM305.56 113.48l-.5-.04.93-11.73-3.38-8.38-4.66-7.91-6.1-13.42.46-.21 6.08 13.4 4.67 7.92 3.44 8.53-.01.05zM294.6 113.72l-1.92-8.28.91-10.63.5.05-.91 10.54 1.91 8.2z"
      />
      <path
        className="st170"
        d="m284.61 113.55-.47-.18 4.43-10.98 5.01-7.61-1.74-9.44.49-.09 1.78 9.64-5.1 7.74zM287.53 82.16l-.18-.47 7.79-3.09 7.14-1.54 10.62-6.32 7.95-9.59.39.32-8.04 9.68-10.77 6.39-7.15 1.53z"
      />
      <path
        className="st170"
        d="m317.47 113.72-.48-.12 1.4-5.31 10.92-9.25-13.26-13.55-13.88-8.06-5.98-8.65.41-.28 5.95 8.6 13.84 8.02 13.65 13.96-.21.17-10.99 9.32zM346.83 113.66h-.5l.03-10.95 5.66-11.07-13.49-13.46 4.26-14.82.48.14-4.18 14.54 13.54 13.51-5.8 11.33z"
      />
      <path
        transform="rotate(-70.866 345.34 56.83)"
        className="st170"
        d="M337.68 56.58h15.36v.5h-15.36z"
      />
      <path
        className="st170"
        d="m334.59 113.72-.49-.12 2.3-9.53 1.87-7.89-8.71-16.29 7.69-16.5 3.4-9.59.47.17-3.4 9.61-7.6 16.29 8.68 16.24zM367.09 113.7l-.49-.07 1.68-11.81-2.19-8.44-13.31-11.34-7.6-14.11 6.59-14.72.45.2-6.49 14.5 7.47 13.85 13.34 11.34 2.25 8.69-.01.05zM357.29 113.76l-4.3-10.36-.91-11.78.49-.04.92 11.74 4.26 10.24z"
      />
      <path
        transform="rotate(-5.342 365.485 64.762)"
        className="st170"
        d="M365.22 55.7h.5v18.13h-.5z"
      />
      <path
        className="st170"
        d="m399.84 113.69-.49-.06 2-15.81-11.94-23.25-2.32-13.24-6.94-8.36.38-.32 7.03 8.47.01.06 2.32 13.26 11.98 23.29zM378.96 113.67l-.49-.02.56-10.51-1.16-19.24-11.58-9.81-13.25 8.1-.11-.03-14.13-3.8-17.86 3.53-8.03-10.75-12.95-5.02.18-.47 13.09 5.08 7.92 10.61 17.67-3.49.05.02 14.09 3.78 13.38-8.17.14.12 11.88 10.06 1.17 19.48zM387.83 113.71l-.49-.1 2.75-12.95-8.21-19.75-3.55-14.39-5.48-12.26.46-.2 5.49 12.29 3.55 14.4 8.26 19.86z"
      />
      <path
        className="st170"
        d="m353.25 81.94-.5-.03 1.05-15.79 3.77-12.9.48.14-3.76 12.85zM420.71 113.72l-2.87-11.13 4.28-13.44-6.49-5.63-2.47-9.08-4.94-5.29-3.3-9.45-7.85-9-.78 6.11 3.25 9.25 5.83 13.6 5 10.9-.22 9.65-2.21 13.42-.49-.08 2.2-13.39.22-9.49-4.96-10.8-5.83-13.63-3.3-9.38.93-7.33 8.67 9.96 3.3 9.44 4.95 5.32 2.44 8.99 6.63 5.75-4.34 13.62 2.84 10.99z"
      />
      <path
        className="st170"
        d="m502.12 89.34-.11-.03-13.6-3.06-7.25 1.57-.09-.07-12.45-8.96-12.76.79-20.68-13.99-26.74 3.73-.06-.02-9.15-2.92-11.88-4.88-21.04 12.58L354 66.4l-8.68 1.8-2.44-4.52-11.06.13-10.62 18.01-4.91 3.72h-28.96l-5.87-6.44-18.56-2.41-13.05 1.39-.08-.05-8.47-5.35.26-.42 8.41 5.3 12.93-1.37 18.81 2.44.06.06 5.78 6.35h28.57l4.73-3.59 10.68-18.13 11.65-.14 2.4 4.46 8.52-1.77 12.22 7.62 20.99-12.54.11.04 11.98 4.92 9.08 2.9 26.82-3.74 20.7 14 12.76-.79.08.06 12.43 8.94 7.14-1.54.05.01 13.56 3.05 7.31-4.76-19.9-1.33-14.68-5.99-15.25-9.94-10.36 1.87-.08-.04-20.13-10.54-18.44 3.3-.06-.02-5.36-1.54-9.1-2.74-14.67-4.21.13-.48 14.68 4.21 9.1 2.74 5.31 1.53 18.49-3.31.07.04 20.14 10.53 10.39-1.87.08.05 15.29 9.98 14.6 5.95 21.28 1.41zM459.48 103.85l-9.25-3.08-15.64-9.57-12.19-1.88-12.18 1.53-8.45 7.11-.32-.38 8.57-7.2.07-.01 12.32-1.55h.04l12.36 1.93 15.63 9.57 9.2 3.06z"
      />
      <path
        className="st170"
        d="m433.11 113.73-4.24-14.52 5.49-8.22-9.52-10.09v-8.22l-5.45-4.98-9.1-6.44-1.58-5.67.48-.13 1.53 5.5 8.98 6.35 5.64 5.15v8.24L435 90.93l-5.58 8.37 4.17 14.29z"
      />
      <path
        className="st170"
        d="m440.24 113.67-.5-.02.49-10.57 1.53-7.7-10.6-15.9 3.27-8.4.55-5.67-6.25-7.49-.94-5.39.49-.08.92 5.26 6.3 7.54-.59 5.98-3.18 8.19 10.56 15.85-1.56 7.87zM447.88 113.71l-.49-.1 1.44-7.66 1.26-5.58 6.06-3.52-.32-9.57-.15-7.88-6.72-10.88-6.57-9.7.41-.28 6.58 9.7 6.8 11.02.15 8.01.33 9.86-6.13 3.56-1.21 5.36zM464.25 113.72l-.48-.12 1.87-7.62-1.36-13.4 1.72-8.03 2.41-5.97-9.1-11.98-.23-8.1.5-.01.22 7.94 9.18 12.07-2.51 6.19-1.69 7.91 1.37 13.41z"
      />
      <path
        className="st170"
        d="m484.53 93.12-18.31-8.26-10.19 2.68-.1-.07-21.32-16.07-21.19 3.16-11.06-1-12.68 1.21-.05-.49 12.72-1.22 11.06 1.01 21.34-3.2.08.07L456.14 87l10.12-2.67.08.04 17.04 7.69-2.39-4.39-6.44-11.2 3.36-4.8.41.29-3.18 4.54 6.29 10.93z"
      />
      <path
        className="st170"
        d="m480.2 113.7-.5-.07 1.52-10.82 2.49-10.28.49.12-2.49 10.25zM492.55 113.68l-.84-8.56 3.03-7.61-6.61-11.49 1.21-3.71 2.65-2.98.37.34-2.61 2.93-1.07 3.37 6.62 11.5-3.08 7.73.83 8.44zM442.22 95.48l-.39-.31 4.41-5.55.25-9.52-1.53-8.34-2.85-6.85-4.64-8.09.43-.25 4.66 8.12 2.87 6.93 1.56 8.44-.25 9.74zM505.48 113.68l-.83-9.54-2.31-3.67-.52-11.44.01-.04 1.93-5.53.48.17-1.92 5.48.51 11.2 2.32 3.73.83 9.6zM510.6 113.67l-.5-.02.36-10.37 2.25-5.27-2.86-14.12.49-.1 2.89 14.27-2.29 5.37zM515.32 113.74l-.47-.16 1.4-4.25 4.27-5.62.73-10.74.5.03-.74 10.89-4.34 5.71zM536.27 113.71l-.49-.1 1.35-6.57-5.09-3.76.08-10.69h.5l-.07 10.44 5.13 3.8zM550.96 113.67l-.5-.02.23-5.04-5.89-8.25-3.3-8.11.46-.19 3.28 8.06 5.95 8.34z"
      />
      <path
        className="st170"
        d="m566.17 113.79-5.25-8.47-7.56-7.01-21.01 5.08-11.66.66-7.78-5.75-10.35 2.35-.07-.03-18.59-7.78h-19.31l-8.2 4.44-.13-.09-24.86-17.47-15.56 3.92-.07-.02-26.07-8.81-23.31 18.69-.1-.01-13.94-1.64-22.66 7.47-.07-.02-26.78-5.81-19.89 3.77-.09-.1-8.55-9.1-14.64.28-9.55 5.16-15.67-2.69-11.29 5.2-8.73-5.79-6.33 4.94-11.86-13.64.37-.33 11.56 13.29 6.24-4.87 8.8 5.83 11.17-5.15.08.01 15.57 2.68 9.57-5.14 14.92-.28.08.08 8.53 9.08 19.72-3.74.05.01 26.76 5.81 22.64-7.46.05.01 13.89 1.63 23.36-18.74.13.05 26.13 8.83 15.64-3.94.09.06 24.83 17.45 8.11-4.35 19.52.02 18.56 7.77 10.42-2.36 7.81 5.77 11.44-.64 21.23-5.14 7.82 7.27 5.27 8.5z"
      />
      <linearGradient
        id="SVGID_172_"
        gradientUnits="userSpaceOnUse"
        x1={779.848}
        y1={117.652}
        x2={779.848}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_172_)" }}
        d="M780.11 94.49h-.52l.26-9.13z"
      />
      <linearGradient
        id="SVGID_173_"
        gradientUnits="userSpaceOnUse"
        x1={780.59}
        y1={117.652}
        x2={780.59}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_173_)" }}
        d="M780.73 94.49h-.28l.14-4.87z"
      />
      <linearGradient
        id="SVGID_174_"
        gradientUnits="userSpaceOnUse"
        x1={763.195}
        y1={117.652}
        x2={763.195}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_174_)" }}
        d="M763.45 101.99h-.51l.25-9.13z"
      />
      <linearGradient
        id="SVGID_175_"
        gradientUnits="userSpaceOnUse"
        x1={766.575}
        y1={117.652}
        x2={766.575}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_175_)" }}
        d="M766.71 101.99h-.27l.14-4.87z"
      />
      <linearGradient
        id="SVGID_176_"
        gradientUnits="userSpaceOnUse"
        x1={733.422}
        y1={117.652}
        x2={733.422}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_176_)" }}
        d="M733.56 90.16h-.28l.14-4.86z"
      />
      <linearGradient
        id="SVGID_177_"
        gradientUnits="userSpaceOnUse"
        x1={753.776}
        y1={117.652}
        x2={753.776}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_177_)" }}
        d="M745.63 87.63h16.29V116h-16.29z"
      />
      <linearGradient
        id="SVGID_178_"
        gradientUnits="userSpaceOnUse"
        x1={782.096}
        y1={117.652}
        x2={782.096}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_178_)" }}
        d="M770.97 100.36h22.25v16.52h-22.25z"
      />
      <linearGradient
        id="SVGID_179_"
        gradientUnits="userSpaceOnUse"
        x1={793.146}
        y1={117.652}
        x2={793.146}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_179_)" }}
        d="M789.4 103.45h7.49v13.43h-7.49z"
      />
      <linearGradient
        id="SVGID_180_"
        gradientUnits="userSpaceOnUse"
        x1={769.098}
        y1={117.652}
        x2={769.098}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_180_)" }}
        d="M767.23 105.86h3.75v11.01h-3.75z"
      />
      <linearGradient
        id="SVGID_181_"
        gradientUnits="userSpaceOnUse"
        x1={759.257}
        y1={117.652}
        x2={759.257}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_181_)" }}
        d="M751.29 101.64h15.94v15.24h-15.94z"
      />
      <linearGradient
        id="SVGID_182_"
        gradientUnits="userSpaceOnUse"
        x1={785.092}
        y1={117.651}
        x2={785.092}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_182_)" }}
        d="M777.04 96.49h16.11v3.86h-16.11z"
      />
      <linearGradient
        id="SVGID_183_"
        gradientUnits="userSpaceOnUse"
        x1={728.722}
        y1={117.652}
        x2={728.722}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_183_)" }}
        d="M722.8 90.16h11.85v6.32H722.8z"
      />
      <linearGradient
        id="SVGID_184_"
        gradientUnits="userSpaceOnUse"
        x1={783.46}
        y1={117.652}
        x2={783.46}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_184_)" }}
        d="M777.04 94.43h12.85v2.12h-12.85z"
      />
      <linearGradient
        id="SVGID_185_"
        gradientUnits="userSpaceOnUse"
        x1={735.6}
        y1={117.652}
        x2={735.6}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_185_)" }}
        d="M727.12 106.53h16.96v2.12h-16.96z"
      />
      <linearGradient
        id="SVGID_186_"
        gradientUnits="userSpaceOnUse"
        x1={744.08}
        y1={117.652}
        x2={744.08}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_186_)" }}
        d="M734.65 108.61h18.86v8.26h-18.86z"
      />
      <linearGradient
        id="SVGID_187_"
        gradientUnits="userSpaceOnUse"
        x1={718.611}
        y1={117.652}
        x2={718.611}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_187_)" }}
        d="M712.34 102.14h12.54v14.73h-12.54z"
      />
      <linearGradient
        id="SVGID_188_"
        gradientUnits="userSpaceOnUse"
        x1={729.372}
        y1={117.652}
        x2={729.372}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_188_)" }}
        d="M719.94 96.49h18.86v20.39h-18.86z"
      />
      <linearGradient
        id="SVGID_189_"
        gradientUnits="userSpaceOnUse"
        x1={626.252}
        y1={117.652}
        x2={626.252}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_189_)" }}
        d="M626.51 94.49h-.52l.26-9.13z"
      />
      <linearGradient
        id="SVGID_190_"
        gradientUnits="userSpaceOnUse"
        x1={626.993}
        y1={117.652}
        x2={626.993}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_190_)" }}
        d="M627.13 94.49h-.27l.13-4.87z"
      />
      <linearGradient
        id="SVGID_191_"
        gradientUnits="userSpaceOnUse"
        x1={609.598}
        y1={117.652}
        x2={609.598}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_191_)" }}
        d="M609.86 101.99h-.52l.26-9.13z"
      />
      <linearGradient
        id="SVGID_192_"
        gradientUnits="userSpaceOnUse"
        x1={612.979}
        y1={117.652}
        x2={612.979}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_192_)" }}
        d="M613.12 101.99h-.28l.14-4.87z"
      />
      <linearGradient
        id="SVGID_193_"
        gradientUnits="userSpaceOnUse"
        x1={579.825}
        y1={117.652}
        x2={579.825}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_193_)" }}
        d="M579.96 90.16h-.27l.14-4.86z"
      />
      <linearGradient
        id="SVGID_194_"
        gradientUnits="userSpaceOnUse"
        x1={600.18}
        y1={117.652}
        x2={600.18}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_194_)" }}
        d="M592.03 87.63h16.29V116h-16.29z"
      />
      <linearGradient
        id="SVGID_195_"
        gradientUnits="userSpaceOnUse"
        x1={628.5}
        y1={117.652}
        x2={628.5}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_195_)" }}
        d="M617.37 100.36h22.25v16.52h-22.25z"
      />
      <linearGradient
        id="SVGID_196_"
        gradientUnits="userSpaceOnUse"
        x1={639.55}
        y1={117.652}
        x2={639.55}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_196_)" }}
        d="M635.8 103.45h7.49v13.43h-7.49z"
      />
      <linearGradient
        id="SVGID_197_"
        gradientUnits="userSpaceOnUse"
        x1={615.502}
        y1={117.652}
        x2={615.502}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_197_)" }}
        d="M613.63 105.86h3.75v11.01h-3.75z"
      />
      <linearGradient
        id="SVGID_198_"
        gradientUnits="userSpaceOnUse"
        x1={605.66}
        y1={117.652}
        x2={605.66}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_198_)" }}
        d="M597.69 101.64h15.94v15.24h-15.94z"
      />
      <linearGradient
        id="SVGID_199_"
        gradientUnits="userSpaceOnUse"
        x1={631.495}
        y1={117.651}
        x2={631.495}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_199_)" }}
        d="M623.44 96.49h16.11v3.86h-16.11z"
      />
      <linearGradient
        id="SVGID_200_"
        gradientUnits="userSpaceOnUse"
        x1={575.126}
        y1={117.652}
        x2={575.126}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_200_)" }}
        d="M569.2 90.16h11.85v6.32H569.2z"
      />
      <linearGradient
        id="SVGID_201_"
        gradientUnits="userSpaceOnUse"
        x1={629.863}
        y1={117.652}
        x2={629.863}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_201_)" }}
        d="M623.44 94.43h12.85v2.12h-12.85z"
      />
      <linearGradient
        id="SVGID_202_"
        gradientUnits="userSpaceOnUse"
        x1={582.003}
        y1={117.652}
        x2={582.003}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_202_)" }}
        d="M573.52 106.53h16.96v2.12h-16.96z"
      />
      <linearGradient
        id="SVGID_203_"
        gradientUnits="userSpaceOnUse"
        x1={590.483}
        y1={117.652}
        x2={590.483}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_203_)" }}
        d="M581.05 108.61h18.86v8.26h-18.86z"
      />
      <linearGradient
        id="SVGID_204_"
        gradientUnits="userSpaceOnUse"
        x1={565.014}
        y1={117.652}
        x2={565.014}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_204_)" }}
        d="M558.74 102.14h12.54v14.73h-12.54z"
      />
      <linearGradient
        id="SVGID_205_"
        gradientUnits="userSpaceOnUse"
        x1={575.776}
        y1={117.652}
        x2={575.776}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_205_)" }}
        d="M566.34 96.49h18.86v20.39h-18.86z"
      />
      <linearGradient
        id="SVGID_206_"
        gradientUnits="userSpaceOnUse"
        x1={567.538}
        y1={117.652}
        x2={567.538}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_206_)" }}
        d="M567.8 94.49h-.52l.26-9.13z"
      />
      <linearGradient
        id="SVGID_207_"
        gradientUnits="userSpaceOnUse"
        x1={568.279}
        y1={117.652}
        x2={568.279}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_207_)" }}
        d="M568.42 94.49h-.28l.14-4.87z"
      />
      <linearGradient
        id="SVGID_208_"
        gradientUnits="userSpaceOnUse"
        x1={550.884}
        y1={117.652}
        x2={550.884}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_208_)" }}
        d="M551.14 101.99h-.51l.25-9.13z"
      />
      <linearGradient
        id="SVGID_209_"
        gradientUnits="userSpaceOnUse"
        x1={554.265}
        y1={117.652}
        x2={554.265}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_209_)" }}
        d="M554.4 101.99h-.27l.13-4.87z"
      />
      <linearGradient
        id="SVGID_210_"
        gradientUnits="userSpaceOnUse"
        x1={521.111}
        y1={117.652}
        x2={521.111}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_210_)" }}
        d="M521.25 90.16h-.28l.14-4.86z"
      />
      <linearGradient
        id="SVGID_211_"
        gradientUnits="userSpaceOnUse"
        x1={541.466}
        y1={117.652}
        x2={541.466}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_211_)" }}
        d="M533.32 87.63h16.29V116h-16.29z"
      />
      <linearGradient
        id="SVGID_212_"
        gradientUnits="userSpaceOnUse"
        x1={569.785}
        y1={117.652}
        x2={569.785}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_212_)" }}
        d="M558.66 100.36h22.25v16.52h-22.25z"
      />
      <linearGradient
        id="SVGID_213_"
        gradientUnits="userSpaceOnUse"
        x1={580.836}
        y1={117.652}
        x2={580.836}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_213_)" }}
        d="M577.09 103.45h7.49v13.43h-7.49z"
      />
      <linearGradient
        id="SVGID_214_"
        gradientUnits="userSpaceOnUse"
        x1={556.788}
        y1={117.652}
        x2={556.788}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_214_)" }}
        d="M554.91 105.86h3.75v11.01h-3.75z"
      />
      <linearGradient
        id="SVGID_215_"
        gradientUnits="userSpaceOnUse"
        x1={546.946}
        y1={117.652}
        x2={546.946}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_215_)" }}
        d="M538.98 101.64h15.94v15.24h-15.94z"
      />
      <linearGradient
        id="SVGID_216_"
        gradientUnits="userSpaceOnUse"
        x1={572.781}
        y1={117.651}
        x2={572.781}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_216_)" }}
        d="M564.73 96.49h16.11v3.86h-16.11z"
      />
      <linearGradient
        id="SVGID_217_"
        gradientUnits="userSpaceOnUse"
        x1={516.412}
        y1={117.652}
        x2={516.412}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_217_)" }}
        d="M510.49 90.16h11.85v6.32h-11.85z"
      />
      <linearGradient
        id="SVGID_218_"
        gradientUnits="userSpaceOnUse"
        x1={571.149}
        y1={117.652}
        x2={571.149}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_218_)" }}
        d="M564.73 94.43h12.85v2.12h-12.85z"
      />
      <linearGradient
        id="SVGID_219_"
        gradientUnits="userSpaceOnUse"
        x1={523.289}
        y1={117.652}
        x2={523.289}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_219_)" }}
        d="M514.81 106.53h16.96v2.12h-16.96z"
      />
      <linearGradient
        id="SVGID_220_"
        gradientUnits="userSpaceOnUse"
        x1={531.769}
        y1={117.652}
        x2={531.769}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_220_)" }}
        d="M522.34 108.61h18.86v8.26h-18.86z"
      />
      <linearGradient
        id="SVGID_221_"
        gradientUnits="userSpaceOnUse"
        x1={506.3}
        y1={117.652}
        x2={506.3}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_221_)" }}
        d="M500.03 102.14h12.54v14.73h-12.54z"
      />
      <linearGradient
        id="SVGID_222_"
        gradientUnits="userSpaceOnUse"
        x1={517.062}
        y1={117.652}
        x2={517.062}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_222_)" }}
        d="M507.63 96.49h18.86v20.39h-18.86z"
      />
      <linearGradient
        id="SVGID_223_"
        gradientUnits="userSpaceOnUse"
        x1={707.966}
        y1={117.652}
        x2={707.966}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_223_)" }}
        d="M708.22 78.95h-.51l.26-15.46z"
      />
      <linearGradient
        id="SVGID_224_"
        gradientUnits="userSpaceOnUse"
        x1={708.707}
        y1={117.651}
        x2={708.707}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_224_)" }}
        d="M708.84 78.95h-.27l.14-8.25z"
      />
      <linearGradient
        id="SVGID_225_"
        gradientUnits="userSpaceOnUse"
        x1={691.312}
        y1={117.652}
        x2={691.312}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_225_)" }}
        d="M691.57 91.65h-.52l.26-15.46z"
      />
      <linearGradient
        id="SVGID_226_"
        gradientUnits="userSpaceOnUse"
        x1={694.693}
        y1={117.652}
        x2={694.693}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_226_)" }}
        d="M694.83 91.65h-.27l.13-8.24z"
      />
      <linearGradient
        id="SVGID_227_"
        gradientUnits="userSpaceOnUse"
        x1={661.539}
        y1={117.652}
        x2={661.539}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_227_)" }}
        d="M661.68 71.62h-.28l.14-8.24z"
      />
      <linearGradient
        id="SVGID_228_"
        gradientUnits="userSpaceOnUse"
        x1={681.894}
        y1={117.652}
        x2={681.894}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_228_)" }}
        d="M673.75 67.33h16.29v48.07h-16.29z"
      />
      <linearGradient
        id="SVGID_229_"
        gradientUnits="userSpaceOnUse"
        x1={710.214}
        y1={117.652}
        x2={710.214}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_229_)" }}
        d="M699.09 88.89h22.25v27.99h-22.25z"
      />
      <linearGradient
        id="SVGID_230_"
        gradientUnits="userSpaceOnUse"
        x1={721.264}
        y1={117.652}
        x2={721.264}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_230_)" }}
        d="M717.52 94.12h7.49v22.75h-7.49z"
      />
      <linearGradient
        id="SVGID_231_"
        gradientUnits="userSpaceOnUse"
        x1={697.216}
        y1={117.652}
        x2={697.216}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_231_)" }}
        d="M695.34 98.22h3.75v18.66h-3.75z"
      />
      <linearGradient
        id="SVGID_232_"
        gradientUnits="userSpaceOnUse"
        x1={687.374}
        y1={117.652}
        x2={687.374}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_232_)" }}
        d="M679.41 91.06h15.94v25.82h-15.94z"
      />
      <linearGradient
        id="SVGID_233_"
        gradientUnits="userSpaceOnUse"
        x1={713.209}
        y1={117.651}
        x2={713.209}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_233_)" }}
        d="M705.15 82.34h16.11v6.55h-16.11z"
      />
      <linearGradient
        id="SVGID_234_"
        gradientUnits="userSpaceOnUse"
        x1={656.84}
        y1={117.652}
        x2={656.84}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_234_)" }}
        d="M650.91 71.62h11.85v10.72h-11.85z"
      />
      <linearGradient
        id="SVGID_235_"
        gradientUnits="userSpaceOnUse"
        x1={711.577}
        y1={117.652}
        x2={711.577}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_235_)" }}
        d="M705.15 78.85H718v3.6h-12.85z"
      />
      <linearGradient
        id="SVGID_236_"
        gradientUnits="userSpaceOnUse"
        x1={663.717}
        y1={117.652}
        x2={663.717}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_236_)" }}
        d="M655.24 99.35h16.96v3.6h-16.96z"
      />
      <linearGradient
        id="SVGID_237_"
        gradientUnits="userSpaceOnUse"
        x1={672.197}
        y1={117.652}
        x2={672.197}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_237_)" }}
        d="M662.77 102.87h18.86v14h-18.86z"
      />
      <linearGradient
        id="SVGID_238_"
        gradientUnits="userSpaceOnUse"
        x1={646.728}
        y1={117.652}
        x2={646.728}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_238_)" }}
        d="M640.46 91.91H653v24.96h-12.54z"
      />
      <linearGradient
        id="SVGID_239_"
        gradientUnits="userSpaceOnUse"
        x1={657.49}
        y1={117.652}
        x2={657.49}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_239_)" }}
        d="M648.06 82.34h18.86v34.54h-18.86z"
      />
      <linearGradient
        id="SVGID_240_"
        gradientUnits="userSpaceOnUse"
        x1={519.893}
        y1={117.652}
        x2={519.893}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_240_)" }}
        d="M520.15 78.95h-.51l.25-15.46z"
      />
      <linearGradient
        id="SVGID_241_"
        gradientUnits="userSpaceOnUse"
        x1={520.634}
        y1={117.651}
        x2={520.634}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_241_)" }}
        d="M520.77 78.95h-.27l.13-8.25z"
      />
      <linearGradient
        id="SVGID_242_"
        gradientUnits="userSpaceOnUse"
        x1={503.239}
        y1={117.652}
        x2={503.239}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_242_)" }}
        d="M503.5 91.65h-.52l.26-15.46z"
      />
      <linearGradient
        id="SVGID_243_"
        gradientUnits="userSpaceOnUse"
        x1={506.619}
        y1={117.652}
        x2={506.619}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_243_)" }}
        d="M506.76 91.65h-.28l.14-8.24z"
      />
      <linearGradient
        id="SVGID_244_"
        gradientUnits="userSpaceOnUse"
        x1={493.821}
        y1={117.652}
        x2={493.821}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_244_)" }}
        d="M485.67 67.33h16.29v48.07h-16.29z"
      />
      <linearGradient
        id="SVGID_245_"
        gradientUnits="userSpaceOnUse"
        x1={522.14}
        y1={117.652}
        x2={522.14}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_245_)" }}
        d="M511.02 88.89h22.25v27.99h-22.25z"
      />
      <linearGradient
        id="SVGID_246_"
        gradientUnits="userSpaceOnUse"
        x1={533.191}
        y1={117.652}
        x2={533.191}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_246_)" }}
        d="M529.44 94.12h7.49v22.75h-7.49z"
      />
      <linearGradient
        id="SVGID_247_"
        gradientUnits="userSpaceOnUse"
        x1={509.142}
        y1={117.652}
        x2={509.142}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_247_)" }}
        d="M507.27 98.22h3.75v18.66h-3.75z"
      />
      <linearGradient
        id="SVGID_248_"
        gradientUnits="userSpaceOnUse"
        x1={499.301}
        y1={117.652}
        x2={499.301}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_248_)" }}
        d="M491.33 91.06h15.94v25.82h-15.94z"
      />
      <linearGradient
        id="SVGID_249_"
        gradientUnits="userSpaceOnUse"
        x1={525.136}
        y1={117.651}
        x2={525.136}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_249_)" }}
        d="M517.08 82.34h16.11v6.55h-16.11z"
      />
      <linearGradient
        id="SVGID_250_"
        gradientUnits="userSpaceOnUse"
        x1={523.504}
        y1={117.652}
        x2={523.504}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_250_)" }}
        d="M517.08 78.85h12.85v3.6h-12.85z"
      />
      <linearGradient
        id="SVGID_251_"
        gradientUnits="userSpaceOnUse"
        x1={469.35}
        y1={117.652}
        x2={469.35}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_251_)" }}
        d="M460.87 93.41h16.96v9.54h-16.96z"
      />
      <linearGradient
        id="SVGID_252_"
        gradientUnits="userSpaceOnUse"
        x1={476.995}
        y1={117.652}
        x2={476.995}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_252_)" }}
        d="M460.43 102.87h33.12v14h-33.12z"
      />
      <linearGradient
        id="SVGID_253_"
        gradientUnits="userSpaceOnUse"
        x1={779.848}
        y1={117.652}
        x2={779.848}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_253_)" }}
        d="M780.11 94.49h-.52l.26-9.13z"
      />
      <linearGradient
        id="SVGID_254_"
        gradientUnits="userSpaceOnUse"
        x1={780.59}
        y1={117.652}
        x2={780.59}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_254_)" }}
        d="M780.73 94.49h-.28l.14-4.87z"
      />
      <linearGradient
        id="SVGID_255_"
        gradientUnits="userSpaceOnUse"
        x1={763.195}
        y1={117.652}
        x2={763.195}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_255_)" }}
        d="M763.45 101.99h-.51l.25-9.13z"
      />
      <linearGradient
        id="SVGID_256_"
        gradientUnits="userSpaceOnUse"
        x1={766.575}
        y1={117.652}
        x2={766.575}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_256_)" }}
        d="M766.71 101.99h-.27l.14-4.87z"
      />
      <linearGradient
        id="SVGID_257_"
        gradientUnits="userSpaceOnUse"
        x1={733.422}
        y1={117.652}
        x2={733.422}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_257_)" }}
        d="M733.56 90.16h-.28l.14-4.86z"
      />
      <linearGradient
        id="SVGID_258_"
        gradientUnits="userSpaceOnUse"
        x1={753.776}
        y1={117.652}
        x2={753.776}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_258_)" }}
        d="M745.63 87.63h16.29V116h-16.29z"
      />
      <linearGradient
        id="SVGID_259_"
        gradientUnits="userSpaceOnUse"
        x1={782.096}
        y1={117.652}
        x2={782.096}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_259_)" }}
        d="M770.97 100.36h22.25v16.52h-22.25z"
      />
      <linearGradient
        id="SVGID_260_"
        gradientUnits="userSpaceOnUse"
        x1={793.146}
        y1={117.652}
        x2={793.146}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_260_)" }}
        d="M789.4 103.45h7.49v13.43h-7.49z"
      />
      <linearGradient
        id="SVGID_261_"
        gradientUnits="userSpaceOnUse"
        x1={769.098}
        y1={117.652}
        x2={769.098}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_261_)" }}
        d="M767.23 105.86h3.75v11.01h-3.75z"
      />
      <linearGradient
        id="SVGID_262_"
        gradientUnits="userSpaceOnUse"
        x1={759.257}
        y1={117.652}
        x2={759.257}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_262_)" }}
        d="M751.29 101.64h15.94v15.24h-15.94z"
      />
      <linearGradient
        id="SVGID_263_"
        gradientUnits="userSpaceOnUse"
        x1={785.092}
        y1={117.651}
        x2={785.092}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_263_)" }}
        d="M777.04 96.49h16.11v3.86h-16.11z"
      />
      <linearGradient
        id="SVGID_264_"
        gradientUnits="userSpaceOnUse"
        x1={728.722}
        y1={117.652}
        x2={728.722}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_264_)" }}
        d="M722.8 90.16h11.85v6.32H722.8z"
      />
      <linearGradient
        id="SVGID_265_"
        gradientUnits="userSpaceOnUse"
        x1={783.46}
        y1={117.652}
        x2={783.46}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_265_)" }}
        d="M777.04 94.43h12.85v2.12h-12.85z"
      />
      <linearGradient
        id="SVGID_266_"
        gradientUnits="userSpaceOnUse"
        x1={735.6}
        y1={117.652}
        x2={735.6}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_266_)" }}
        d="M727.12 106.53h16.96v2.12h-16.96z"
      />
      <linearGradient
        id="SVGID_267_"
        gradientUnits="userSpaceOnUse"
        x1={744.08}
        y1={117.652}
        x2={744.08}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_267_)" }}
        d="M734.65 108.61h18.86v8.26h-18.86z"
      />
      <linearGradient
        id="SVGID_268_"
        gradientUnits="userSpaceOnUse"
        x1={718.611}
        y1={117.652}
        x2={718.611}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_268_)" }}
        d="M712.34 102.14h12.54v14.73h-12.54z"
      />
      <linearGradient
        id="SVGID_269_"
        gradientUnits="userSpaceOnUse"
        x1={729.372}
        y1={117.652}
        x2={729.372}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_269_)" }}
        d="M719.94 96.49h18.86v20.39h-18.86z"
      />
      <linearGradient
        id="SVGID_270_"
        gradientUnits="userSpaceOnUse"
        x1={626.252}
        y1={117.652}
        x2={626.252}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_270_)" }}
        d="M626.51 94.49h-.52l.26-9.13z"
      />
      <linearGradient
        id="SVGID_271_"
        gradientUnits="userSpaceOnUse"
        x1={626.993}
        y1={117.652}
        x2={626.993}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_271_)" }}
        d="M627.13 94.49h-.27l.13-4.87z"
      />
      <linearGradient
        id="SVGID_272_"
        gradientUnits="userSpaceOnUse"
        x1={609.598}
        y1={117.652}
        x2={609.598}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_272_)" }}
        d="M609.86 101.99h-.52l.26-9.13z"
      />
      <linearGradient
        id="SVGID_273_"
        gradientUnits="userSpaceOnUse"
        x1={612.979}
        y1={117.652}
        x2={612.979}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_273_)" }}
        d="M613.12 101.99h-.28l.14-4.87z"
      />
      <linearGradient
        id="SVGID_274_"
        gradientUnits="userSpaceOnUse"
        x1={579.825}
        y1={117.652}
        x2={579.825}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_274_)" }}
        d="M579.96 90.16h-.27l.14-4.86z"
      />
      <linearGradient
        id="SVGID_275_"
        gradientUnits="userSpaceOnUse"
        x1={600.18}
        y1={117.652}
        x2={600.18}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_275_)" }}
        d="M592.03 87.63h16.29V116h-16.29z"
      />
      <linearGradient
        id="SVGID_276_"
        gradientUnits="userSpaceOnUse"
        x1={628.5}
        y1={117.652}
        x2={628.5}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_276_)" }}
        d="M617.37 100.36h22.25v16.52h-22.25z"
      />
      <linearGradient
        id="SVGID_277_"
        gradientUnits="userSpaceOnUse"
        x1={639.55}
        y1={117.652}
        x2={639.55}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_277_)" }}
        d="M635.8 103.45h7.49v13.43h-7.49z"
      />
      <linearGradient
        id="SVGID_278_"
        gradientUnits="userSpaceOnUse"
        x1={615.502}
        y1={117.652}
        x2={615.502}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_278_)" }}
        d="M613.63 105.86h3.75v11.01h-3.75z"
      />
      <linearGradient
        id="SVGID_279_"
        gradientUnits="userSpaceOnUse"
        x1={605.66}
        y1={117.652}
        x2={605.66}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_279_)" }}
        d="M597.69 101.64h15.94v15.24h-15.94z"
      />
      <linearGradient
        id="SVGID_280_"
        gradientUnits="userSpaceOnUse"
        x1={631.495}
        y1={117.651}
        x2={631.495}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_280_)" }}
        d="M623.44 96.49h16.11v3.86h-16.11z"
      />
      <linearGradient
        id="SVGID_281_"
        gradientUnits="userSpaceOnUse"
        x1={575.126}
        y1={117.652}
        x2={575.126}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_281_)" }}
        d="M569.2 90.16h11.85v6.32H569.2z"
      />
      <linearGradient
        id="SVGID_282_"
        gradientUnits="userSpaceOnUse"
        x1={629.863}
        y1={117.652}
        x2={629.863}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_282_)" }}
        d="M623.44 94.43h12.85v2.12h-12.85z"
      />
      <linearGradient
        id="SVGID_283_"
        gradientUnits="userSpaceOnUse"
        x1={582.003}
        y1={117.652}
        x2={582.003}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_283_)" }}
        d="M573.52 106.53h16.96v2.12h-16.96z"
      />
      <linearGradient
        id="SVGID_284_"
        gradientUnits="userSpaceOnUse"
        x1={590.483}
        y1={117.652}
        x2={590.483}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_284_)" }}
        d="M581.05 108.61h18.86v8.26h-18.86z"
      />
      <linearGradient
        id="SVGID_285_"
        gradientUnits="userSpaceOnUse"
        x1={565.014}
        y1={117.652}
        x2={565.014}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_285_)" }}
        d="M558.74 102.14h12.54v14.73h-12.54z"
      />
      <linearGradient
        id="SVGID_286_"
        gradientUnits="userSpaceOnUse"
        x1={575.776}
        y1={117.652}
        x2={575.776}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_286_)" }}
        d="M566.34 96.49h18.86v20.39h-18.86z"
      />
      <linearGradient
        id="SVGID_287_"
        gradientUnits="userSpaceOnUse"
        x1={567.538}
        y1={117.652}
        x2={567.538}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_287_)" }}
        d="M567.8 94.49h-.52l.26-9.13z"
      />
      <linearGradient
        id="SVGID_288_"
        gradientUnits="userSpaceOnUse"
        x1={568.279}
        y1={117.652}
        x2={568.279}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_288_)" }}
        d="M568.42 94.49h-.28l.14-4.87z"
      />
      <linearGradient
        id="SVGID_289_"
        gradientUnits="userSpaceOnUse"
        x1={550.884}
        y1={117.652}
        x2={550.884}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_289_)" }}
        d="M551.14 101.99h-.51l.25-9.13z"
      />
      <linearGradient
        id="SVGID_290_"
        gradientUnits="userSpaceOnUse"
        x1={554.265}
        y1={117.652}
        x2={554.265}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_290_)" }}
        d="M554.4 101.99h-.27l.13-4.87z"
      />
      <linearGradient
        id="SVGID_291_"
        gradientUnits="userSpaceOnUse"
        x1={521.111}
        y1={117.652}
        x2={521.111}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_291_)" }}
        d="M521.25 90.16h-.28l.14-4.86z"
      />
      <linearGradient
        id="SVGID_292_"
        gradientUnits="userSpaceOnUse"
        x1={541.466}
        y1={117.652}
        x2={541.466}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_292_)" }}
        d="M533.32 87.63h16.29V116h-16.29z"
      />
      <linearGradient
        id="SVGID_293_"
        gradientUnits="userSpaceOnUse"
        x1={569.785}
        y1={117.652}
        x2={569.785}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_293_)" }}
        d="M558.66 100.36h22.25v16.52h-22.25z"
      />
      <linearGradient
        id="SVGID_294_"
        gradientUnits="userSpaceOnUse"
        x1={580.836}
        y1={117.652}
        x2={580.836}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_294_)" }}
        d="M577.09 103.45h7.49v13.43h-7.49z"
      />
      <linearGradient
        id="SVGID_295_"
        gradientUnits="userSpaceOnUse"
        x1={556.788}
        y1={117.652}
        x2={556.788}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_295_)" }}
        d="M554.91 105.86h3.75v11.01h-3.75z"
      />
      <linearGradient
        id="SVGID_296_"
        gradientUnits="userSpaceOnUse"
        x1={546.946}
        y1={117.652}
        x2={546.946}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_296_)" }}
        d="M538.98 101.64h15.94v15.24h-15.94z"
      />
      <linearGradient
        id="SVGID_297_"
        gradientUnits="userSpaceOnUse"
        x1={572.781}
        y1={117.651}
        x2={572.781}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_297_)" }}
        d="M564.73 96.49h16.11v3.86h-16.11z"
      />
      <linearGradient
        id="SVGID_298_"
        gradientUnits="userSpaceOnUse"
        x1={516.412}
        y1={117.652}
        x2={516.412}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_298_)" }}
        d="M510.49 90.16h11.85v6.32h-11.85z"
      />
      <linearGradient
        id="SVGID_299_"
        gradientUnits="userSpaceOnUse"
        x1={571.149}
        y1={117.652}
        x2={571.149}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_299_)" }}
        d="M564.73 94.43h12.85v2.12h-12.85z"
      />
      <linearGradient
        id="SVGID_300_"
        gradientUnits="userSpaceOnUse"
        x1={523.289}
        y1={117.652}
        x2={523.289}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_300_)" }}
        d="M514.81 106.53h16.96v2.12h-16.96z"
      />
      <linearGradient
        id="SVGID_301_"
        gradientUnits="userSpaceOnUse"
        x1={531.769}
        y1={117.652}
        x2={531.769}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_301_)" }}
        d="M522.34 108.61h18.86v8.26h-18.86z"
      />
      <linearGradient
        id="SVGID_302_"
        gradientUnits="userSpaceOnUse"
        x1={506.3}
        y1={117.652}
        x2={506.3}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_302_)" }}
        d="M500.03 102.14h12.54v14.73h-12.54z"
      />
      <linearGradient
        id="SVGID_303_"
        gradientUnits="userSpaceOnUse"
        x1={517.062}
        y1={117.652}
        x2={517.062}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_303_)" }}
        d="M507.63 96.49h18.86v20.39h-18.86z"
      />
      <linearGradient
        id="SVGID_304_"
        gradientUnits="userSpaceOnUse"
        x1={707.966}
        y1={117.652}
        x2={707.966}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_304_)" }}
        d="M708.22 78.95h-.51l.26-15.46z"
      />
      <linearGradient
        id="SVGID_305_"
        gradientUnits="userSpaceOnUse"
        x1={708.707}
        y1={117.651}
        x2={708.707}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_305_)" }}
        d="M708.84 78.95h-.27l.14-8.25z"
      />
      <linearGradient
        id="SVGID_306_"
        gradientUnits="userSpaceOnUse"
        x1={691.312}
        y1={117.652}
        x2={691.312}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_306_)" }}
        d="M691.57 91.65h-.52l.26-15.46z"
      />
      <linearGradient
        id="SVGID_307_"
        gradientUnits="userSpaceOnUse"
        x1={694.693}
        y1={117.652}
        x2={694.693}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_307_)" }}
        d="M694.83 91.65h-.27l.13-8.24z"
      />
      <linearGradient
        id="SVGID_308_"
        gradientUnits="userSpaceOnUse"
        x1={661.539}
        y1={117.652}
        x2={661.539}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_308_)" }}
        d="M661.68 71.62h-.28l.14-8.24z"
      />
      <linearGradient
        id="SVGID_309_"
        gradientUnits="userSpaceOnUse"
        x1={681.894}
        y1={117.652}
        x2={681.894}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_309_)" }}
        d="M673.75 67.33h16.29v48.07h-16.29z"
      />
      <linearGradient
        id="SVGID_310_"
        gradientUnits="userSpaceOnUse"
        x1={710.214}
        y1={117.652}
        x2={710.214}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_310_)" }}
        d="M699.09 88.89h22.25v27.99h-22.25z"
      />
      <linearGradient
        id="SVGID_311_"
        gradientUnits="userSpaceOnUse"
        x1={721.264}
        y1={117.652}
        x2={721.264}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_311_)" }}
        d="M717.52 94.12h7.49v22.75h-7.49z"
      />
      <linearGradient
        id="SVGID_312_"
        gradientUnits="userSpaceOnUse"
        x1={697.216}
        y1={117.652}
        x2={697.216}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_312_)" }}
        d="M695.34 98.22h3.75v18.66h-3.75z"
      />
      <linearGradient
        id="SVGID_313_"
        gradientUnits="userSpaceOnUse"
        x1={687.374}
        y1={117.652}
        x2={687.374}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_313_)" }}
        d="M679.41 91.06h15.94v25.82h-15.94z"
      />
      <linearGradient
        id="SVGID_314_"
        gradientUnits="userSpaceOnUse"
        x1={713.209}
        y1={117.651}
        x2={713.209}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_314_)" }}
        d="M705.15 82.34h16.11v6.55h-16.11z"
      />
      <linearGradient
        id="SVGID_315_"
        gradientUnits="userSpaceOnUse"
        x1={656.84}
        y1={117.652}
        x2={656.84}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_315_)" }}
        d="M650.91 71.62h11.85v10.72h-11.85z"
      />
      <linearGradient
        id="SVGID_316_"
        gradientUnits="userSpaceOnUse"
        x1={711.577}
        y1={117.652}
        x2={711.577}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_316_)" }}
        d="M705.15 78.85H718v3.6h-12.85z"
      />
      <linearGradient
        id="SVGID_317_"
        gradientUnits="userSpaceOnUse"
        x1={663.717}
        y1={117.652}
        x2={663.717}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_317_)" }}
        d="M655.24 99.35h16.96v3.6h-16.96z"
      />
      <linearGradient
        id="SVGID_318_"
        gradientUnits="userSpaceOnUse"
        x1={672.197}
        y1={117.652}
        x2={672.197}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_318_)" }}
        d="M662.77 102.87h18.86v14h-18.86z"
      />
      <linearGradient
        id="SVGID_319_"
        gradientUnits="userSpaceOnUse"
        x1={646.728}
        y1={117.652}
        x2={646.728}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_319_)" }}
        d="M640.46 91.91H653v24.96h-12.54z"
      />
      <linearGradient
        id="SVGID_320_"
        gradientUnits="userSpaceOnUse"
        x1={657.49}
        y1={117.652}
        x2={657.49}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_320_)" }}
        d="M648.06 82.34h18.86v34.54h-18.86z"
      />
      <linearGradient
        id="SVGID_321_"
        gradientUnits="userSpaceOnUse"
        x1={519.893}
        y1={117.652}
        x2={519.893}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_321_)" }}
        d="M520.15 78.95h-.51l.25-15.46z"
      />
      <linearGradient
        id="SVGID_322_"
        gradientUnits="userSpaceOnUse"
        x1={520.634}
        y1={117.651}
        x2={520.634}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_322_)" }}
        d="M520.77 78.95h-.27l.13-8.25z"
      />
      <linearGradient
        id="SVGID_323_"
        gradientUnits="userSpaceOnUse"
        x1={503.239}
        y1={117.652}
        x2={503.239}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_323_)" }}
        d="M503.5 91.65h-.52l.26-15.46z"
      />
      <linearGradient
        id="SVGID_324_"
        gradientUnits="userSpaceOnUse"
        x1={506.619}
        y1={117.652}
        x2={506.619}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_324_)" }}
        d="M506.76 91.65h-.28l.14-8.24z"
      />
      <linearGradient
        id="SVGID_325_"
        gradientUnits="userSpaceOnUse"
        x1={493.821}
        y1={117.652}
        x2={493.821}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_325_)" }}
        d="M485.67 67.33h16.29v48.07h-16.29z"
      />
      <linearGradient
        id="SVGID_326_"
        gradientUnits="userSpaceOnUse"
        x1={522.14}
        y1={117.652}
        x2={522.14}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_326_)" }}
        d="M511.02 88.89h22.25v27.99h-22.25z"
      />
      <linearGradient
        id="SVGID_327_"
        gradientUnits="userSpaceOnUse"
        x1={533.191}
        y1={117.652}
        x2={533.191}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_327_)" }}
        d="M529.44 94.12h7.49v22.75h-7.49z"
      />
      <linearGradient
        id="SVGID_328_"
        gradientUnits="userSpaceOnUse"
        x1={509.142}
        y1={117.652}
        x2={509.142}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_328_)" }}
        d="M507.27 98.22h3.75v18.66h-3.75z"
      />
      <linearGradient
        id="SVGID_329_"
        gradientUnits="userSpaceOnUse"
        x1={499.301}
        y1={117.652}
        x2={499.301}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_329_)" }}
        d="M491.33 91.06h15.94v25.82h-15.94z"
      />
      <linearGradient
        id="SVGID_330_"
        gradientUnits="userSpaceOnUse"
        x1={525.136}
        y1={117.651}
        x2={525.136}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_330_)" }}
        d="M517.08 82.34h16.11v6.55h-16.11z"
      />
      <linearGradient
        id="SVGID_331_"
        gradientUnits="userSpaceOnUse"
        x1={523.504}
        y1={117.652}
        x2={523.504}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_331_)" }}
        d="M517.08 78.85h12.85v3.6h-12.85z"
      />
      <linearGradient
        id="SVGID_332_"
        gradientUnits="userSpaceOnUse"
        x1={469.35}
        y1={117.652}
        x2={469.35}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_332_)" }}
        d="M460.87 93.41h16.96v9.54h-16.96z"
      />
      <linearGradient
        id="SVGID_333_"
        gradientUnits="userSpaceOnUse"
        x1={476.995}
        y1={117.652}
        x2={476.995}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_333_)" }}
        d="M460.43 102.87h33.12v14h-33.12z"
      />
      <linearGradient
        id="SVGID_334_"
        gradientUnits="userSpaceOnUse"
        x1={779.848}
        y1={117.652}
        x2={779.848}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_334_)" }}
        d="M780.11 94.49h-.52l.26-9.13z"
      />
      <linearGradient
        id="SVGID_335_"
        gradientUnits="userSpaceOnUse"
        x1={780.59}
        y1={117.652}
        x2={780.59}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_335_)" }}
        d="M780.73 94.49h-.28l.14-4.87z"
      />
      <linearGradient
        id="SVGID_336_"
        gradientUnits="userSpaceOnUse"
        x1={763.195}
        y1={117.652}
        x2={763.195}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_336_)" }}
        d="M763.45 101.99h-.51l.25-9.13z"
      />
      <linearGradient
        id="SVGID_337_"
        gradientUnits="userSpaceOnUse"
        x1={766.575}
        y1={117.652}
        x2={766.575}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_337_)" }}
        d="M766.71 101.99h-.27l.14-4.87z"
      />
      <linearGradient
        id="SVGID_338_"
        gradientUnits="userSpaceOnUse"
        x1={733.422}
        y1={117.652}
        x2={733.422}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_338_)" }}
        d="M733.56 90.16h-.28l.14-4.86z"
      />
      <linearGradient
        id="SVGID_339_"
        gradientUnits="userSpaceOnUse"
        x1={753.776}
        y1={117.652}
        x2={753.776}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_339_)" }}
        d="M745.63 87.63h16.29V116h-16.29z"
      />
      <linearGradient
        id="SVGID_340_"
        gradientUnits="userSpaceOnUse"
        x1={782.096}
        y1={117.652}
        x2={782.096}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_340_)" }}
        d="M770.97 100.36h22.25v16.52h-22.25z"
      />
      <linearGradient
        id="SVGID_341_"
        gradientUnits="userSpaceOnUse"
        x1={793.146}
        y1={117.652}
        x2={793.146}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_341_)" }}
        d="M789.4 103.45h7.49v13.43h-7.49z"
      />
      <linearGradient
        id="SVGID_342_"
        gradientUnits="userSpaceOnUse"
        x1={769.098}
        y1={117.652}
        x2={769.098}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_342_)" }}
        d="M767.23 105.86h3.75v11.01h-3.75z"
      />
      <linearGradient
        id="SVGID_343_"
        gradientUnits="userSpaceOnUse"
        x1={759.257}
        y1={117.652}
        x2={759.257}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_343_)" }}
        d="M751.29 101.64h15.94v15.24h-15.94z"
      />
      <linearGradient
        id="SVGID_344_"
        gradientUnits="userSpaceOnUse"
        x1={785.092}
        y1={117.651}
        x2={785.092}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_344_)" }}
        d="M777.04 96.49h16.11v3.86h-16.11z"
      />
      <linearGradient
        id="SVGID_345_"
        gradientUnits="userSpaceOnUse"
        x1={728.722}
        y1={117.652}
        x2={728.722}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_345_)" }}
        d="M722.8 90.16h11.85v6.32H722.8z"
      />
      <linearGradient
        id="SVGID_346_"
        gradientUnits="userSpaceOnUse"
        x1={783.46}
        y1={117.652}
        x2={783.46}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_346_)" }}
        d="M777.04 94.43h12.85v2.12h-12.85z"
      />
      <linearGradient
        id="SVGID_347_"
        gradientUnits="userSpaceOnUse"
        x1={735.6}
        y1={117.652}
        x2={735.6}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_347_)" }}
        d="M727.12 106.53h16.96v2.12h-16.96z"
      />
      <linearGradient
        id="SVGID_348_"
        gradientUnits="userSpaceOnUse"
        x1={744.08}
        y1={117.652}
        x2={744.08}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_348_)" }}
        d="M734.65 108.61h18.86v8.26h-18.86z"
      />
      <linearGradient
        id="SVGID_349_"
        gradientUnits="userSpaceOnUse"
        x1={718.611}
        y1={117.652}
        x2={718.611}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_349_)" }}
        d="M712.34 102.14h12.54v14.73h-12.54z"
      />
      <linearGradient
        id="SVGID_350_"
        gradientUnits="userSpaceOnUse"
        x1={729.372}
        y1={117.652}
        x2={729.372}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_350_)" }}
        d="M719.94 96.49h18.86v20.39h-18.86z"
      />
      <linearGradient
        id="SVGID_351_"
        gradientUnits="userSpaceOnUse"
        x1={626.252}
        y1={117.652}
        x2={626.252}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_351_)" }}
        d="M626.51 94.49h-.52l.26-9.13z"
      />
      <linearGradient
        id="SVGID_352_"
        gradientUnits="userSpaceOnUse"
        x1={626.993}
        y1={117.652}
        x2={626.993}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_352_)" }}
        d="M627.13 94.49h-.27l.13-4.87z"
      />
      <linearGradient
        id="SVGID_353_"
        gradientUnits="userSpaceOnUse"
        x1={609.598}
        y1={117.652}
        x2={609.598}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_353_)" }}
        d="M609.86 101.99h-.52l.26-9.13z"
      />
      <linearGradient
        id="SVGID_354_"
        gradientUnits="userSpaceOnUse"
        x1={612.979}
        y1={117.652}
        x2={612.979}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_354_)" }}
        d="M613.12 101.99h-.28l.14-4.87z"
      />
      <linearGradient
        id="SVGID_355_"
        gradientUnits="userSpaceOnUse"
        x1={579.825}
        y1={117.652}
        x2={579.825}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_355_)" }}
        d="M579.96 90.16h-.27l.14-4.86z"
      />
      <linearGradient
        id="SVGID_356_"
        gradientUnits="userSpaceOnUse"
        x1={600.18}
        y1={117.652}
        x2={600.18}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_356_)" }}
        d="M592.03 87.63h16.29V116h-16.29z"
      />
      <linearGradient
        id="SVGID_357_"
        gradientUnits="userSpaceOnUse"
        x1={628.5}
        y1={117.652}
        x2={628.5}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_357_)" }}
        d="M617.37 100.36h22.25v16.52h-22.25z"
      />
      <linearGradient
        id="SVGID_358_"
        gradientUnits="userSpaceOnUse"
        x1={639.55}
        y1={117.652}
        x2={639.55}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_358_)" }}
        d="M635.8 103.45h7.49v13.43h-7.49z"
      />
      <linearGradient
        id="SVGID_359_"
        gradientUnits="userSpaceOnUse"
        x1={615.502}
        y1={117.652}
        x2={615.502}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_359_)" }}
        d="M613.63 105.86h3.75v11.01h-3.75z"
      />
      <linearGradient
        id="SVGID_360_"
        gradientUnits="userSpaceOnUse"
        x1={605.66}
        y1={117.652}
        x2={605.66}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_360_)" }}
        d="M597.69 101.64h15.94v15.24h-15.94z"
      />
      <linearGradient
        id="SVGID_361_"
        gradientUnits="userSpaceOnUse"
        x1={631.495}
        y1={117.651}
        x2={631.495}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_361_)" }}
        d="M623.44 96.49h16.11v3.86h-16.11z"
      />
      <linearGradient
        id="SVGID_362_"
        gradientUnits="userSpaceOnUse"
        x1={575.126}
        y1={117.652}
        x2={575.126}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_362_)" }}
        d="M569.2 90.16h11.85v6.32H569.2z"
      />
      <linearGradient
        id="SVGID_363_"
        gradientUnits="userSpaceOnUse"
        x1={629.863}
        y1={117.652}
        x2={629.863}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_363_)" }}
        d="M623.44 94.43h12.85v2.12h-12.85z"
      />
      <linearGradient
        id="SVGID_364_"
        gradientUnits="userSpaceOnUse"
        x1={582.003}
        y1={117.652}
        x2={582.003}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_364_)" }}
        d="M573.52 106.53h16.96v2.12h-16.96z"
      />
      <linearGradient
        id="SVGID_365_"
        gradientUnits="userSpaceOnUse"
        x1={590.483}
        y1={117.652}
        x2={590.483}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_365_)" }}
        d="M581.05 108.61h18.86v8.26h-18.86z"
      />
      <linearGradient
        id="SVGID_366_"
        gradientUnits="userSpaceOnUse"
        x1={565.014}
        y1={117.652}
        x2={565.014}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_366_)" }}
        d="M558.74 102.14h12.54v14.73h-12.54z"
      />
      <linearGradient
        id="SVGID_367_"
        gradientUnits="userSpaceOnUse"
        x1={575.776}
        y1={117.652}
        x2={575.776}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_367_)" }}
        d="M566.34 96.49h18.86v20.39h-18.86z"
      />
      <linearGradient
        id="SVGID_368_"
        gradientUnits="userSpaceOnUse"
        x1={567.538}
        y1={117.652}
        x2={567.538}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_368_)" }}
        d="M567.8 94.49h-.52l.26-9.13z"
      />
      <linearGradient
        id="SVGID_369_"
        gradientUnits="userSpaceOnUse"
        x1={568.279}
        y1={117.652}
        x2={568.279}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_369_)" }}
        d="M568.42 94.49h-.28l.14-4.87z"
      />
      <linearGradient
        id="SVGID_370_"
        gradientUnits="userSpaceOnUse"
        x1={550.884}
        y1={117.652}
        x2={550.884}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_370_)" }}
        d="M551.14 101.99h-.51l.25-9.13z"
      />
      <linearGradient
        id="SVGID_371_"
        gradientUnits="userSpaceOnUse"
        x1={554.265}
        y1={117.652}
        x2={554.265}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_371_)" }}
        d="M554.4 101.99h-.27l.13-4.87z"
      />
      <linearGradient
        id="SVGID_372_"
        gradientUnits="userSpaceOnUse"
        x1={521.111}
        y1={117.652}
        x2={521.111}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_372_)" }}
        d="M521.25 90.16h-.28l.14-4.86z"
      />
      <linearGradient
        id="SVGID_373_"
        gradientUnits="userSpaceOnUse"
        x1={541.466}
        y1={117.652}
        x2={541.466}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_373_)" }}
        d="M533.32 87.63h16.29V116h-16.29z"
      />
      <linearGradient
        id="SVGID_374_"
        gradientUnits="userSpaceOnUse"
        x1={569.785}
        y1={117.652}
        x2={569.785}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_374_)" }}
        d="M558.66 100.36h22.25v16.52h-22.25z"
      />
      <linearGradient
        id="SVGID_375_"
        gradientUnits="userSpaceOnUse"
        x1={580.836}
        y1={117.652}
        x2={580.836}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_375_)" }}
        d="M577.09 103.45h7.49v13.43h-7.49z"
      />
      <linearGradient
        id="SVGID_376_"
        gradientUnits="userSpaceOnUse"
        x1={556.788}
        y1={117.652}
        x2={556.788}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_376_)" }}
        d="M554.91 105.86h3.75v11.01h-3.75z"
      />
      <linearGradient
        id="SVGID_377_"
        gradientUnits="userSpaceOnUse"
        x1={546.946}
        y1={117.652}
        x2={546.946}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_377_)" }}
        d="M538.98 101.64h15.94v15.24h-15.94z"
      />
      <linearGradient
        id="SVGID_378_"
        gradientUnits="userSpaceOnUse"
        x1={572.781}
        y1={117.651}
        x2={572.781}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_378_)" }}
        d="M564.73 96.49h16.11v3.86h-16.11z"
      />
      <linearGradient
        id="SVGID_379_"
        gradientUnits="userSpaceOnUse"
        x1={516.412}
        y1={117.652}
        x2={516.412}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_379_)" }}
        d="M510.49 90.16h11.85v6.32h-11.85z"
      />
      <linearGradient
        id="SVGID_380_"
        gradientUnits="userSpaceOnUse"
        x1={571.149}
        y1={117.652}
        x2={571.149}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_380_)" }}
        d="M564.73 94.43h12.85v2.12h-12.85z"
      />
      <linearGradient
        id="SVGID_381_"
        gradientUnits="userSpaceOnUse"
        x1={523.289}
        y1={117.652}
        x2={523.289}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_381_)" }}
        d="M514.81 106.53h16.96v2.12h-16.96z"
      />
      <linearGradient
        id="SVGID_382_"
        gradientUnits="userSpaceOnUse"
        x1={531.769}
        y1={117.652}
        x2={531.769}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_382_)" }}
        d="M522.34 108.61h18.86v8.26h-18.86z"
      />
      <linearGradient
        id="SVGID_383_"
        gradientUnits="userSpaceOnUse"
        x1={506.3}
        y1={117.652}
        x2={506.3}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_383_)" }}
        d="M500.03 102.14h12.54v14.73h-12.54z"
      />
      <linearGradient
        id="SVGID_384_"
        gradientUnits="userSpaceOnUse"
        x1={517.062}
        y1={117.652}
        x2={517.062}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_384_)" }}
        d="M507.63 96.49h18.86v20.39h-18.86z"
      />
      <linearGradient
        id="SVGID_385_"
        gradientUnits="userSpaceOnUse"
        x1={707.966}
        y1={117.652}
        x2={707.966}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_385_)" }}
        d="M708.22 78.95h-.51l.26-15.46z"
      />
      <linearGradient
        id="SVGID_386_"
        gradientUnits="userSpaceOnUse"
        x1={708.707}
        y1={117.651}
        x2={708.707}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_386_)" }}
        d="M708.84 78.95h-.27l.14-8.25z"
      />
      <linearGradient
        id="SVGID_387_"
        gradientUnits="userSpaceOnUse"
        x1={691.312}
        y1={117.652}
        x2={691.312}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_387_)" }}
        d="M691.57 91.65h-.52l.26-15.46z"
      />
      <linearGradient
        id="SVGID_388_"
        gradientUnits="userSpaceOnUse"
        x1={694.693}
        y1={117.652}
        x2={694.693}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_388_)" }}
        d="M694.83 91.65h-.27l.13-8.24z"
      />
      <linearGradient
        id="SVGID_389_"
        gradientUnits="userSpaceOnUse"
        x1={661.539}
        y1={117.652}
        x2={661.539}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_389_)" }}
        d="M661.68 71.62h-.28l.14-8.24z"
      />
      <linearGradient
        id="SVGID_390_"
        gradientUnits="userSpaceOnUse"
        x1={681.894}
        y1={117.652}
        x2={681.894}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_390_)" }}
        d="M673.75 67.33h16.29v48.07h-16.29z"
      />
      <linearGradient
        id="SVGID_391_"
        gradientUnits="userSpaceOnUse"
        x1={710.214}
        y1={117.652}
        x2={710.214}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_391_)" }}
        d="M699.09 88.89h22.25v27.99h-22.25z"
      />
      <linearGradient
        id="SVGID_392_"
        gradientUnits="userSpaceOnUse"
        x1={721.264}
        y1={117.652}
        x2={721.264}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_392_)" }}
        d="M717.52 94.12h7.49v22.75h-7.49z"
      />
      <linearGradient
        id="SVGID_393_"
        gradientUnits="userSpaceOnUse"
        x1={697.216}
        y1={117.652}
        x2={697.216}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_393_)" }}
        d="M695.34 98.22h3.75v18.66h-3.75z"
      />
      <linearGradient
        id="SVGID_394_"
        gradientUnits="userSpaceOnUse"
        x1={687.374}
        y1={117.652}
        x2={687.374}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_394_)" }}
        d="M679.41 91.06h15.94v25.82h-15.94z"
      />
      <linearGradient
        id="SVGID_395_"
        gradientUnits="userSpaceOnUse"
        x1={713.209}
        y1={117.651}
        x2={713.209}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_395_)" }}
        d="M705.15 82.34h16.11v6.55h-16.11z"
      />
      <linearGradient
        id="SVGID_396_"
        gradientUnits="userSpaceOnUse"
        x1={656.84}
        y1={117.652}
        x2={656.84}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_396_)" }}
        d="M650.91 71.62h11.85v10.72h-11.85z"
      />
      <linearGradient
        id="SVGID_397_"
        gradientUnits="userSpaceOnUse"
        x1={711.577}
        y1={117.652}
        x2={711.577}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_397_)" }}
        d="M705.15 78.85H718v3.6h-12.85z"
      />
      <linearGradient
        id="SVGID_398_"
        gradientUnits="userSpaceOnUse"
        x1={663.717}
        y1={117.652}
        x2={663.717}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_398_)" }}
        d="M655.24 99.35h16.96v3.6h-16.96z"
      />
      <linearGradient
        id="SVGID_399_"
        gradientUnits="userSpaceOnUse"
        x1={672.197}
        y1={117.652}
        x2={672.197}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_399_)" }}
        d="M662.77 102.87h18.86v14h-18.86z"
      />
      <linearGradient
        id="SVGID_400_"
        gradientUnits="userSpaceOnUse"
        x1={646.728}
        y1={117.652}
        x2={646.728}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_400_)" }}
        d="M640.46 91.91H653v24.96h-12.54z"
      />
      <linearGradient
        id="SVGID_401_"
        gradientUnits="userSpaceOnUse"
        x1={657.49}
        y1={117.652}
        x2={657.49}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_401_)" }}
        d="M648.06 82.34h18.86v34.54h-18.86z"
      />
      <linearGradient
        id="SVGID_402_"
        gradientUnits="userSpaceOnUse"
        x1={519.893}
        y1={117.652}
        x2={519.893}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_402_)" }}
        d="M520.15 78.95h-.51l.25-15.46z"
      />
      <linearGradient
        id="SVGID_403_"
        gradientUnits="userSpaceOnUse"
        x1={520.634}
        y1={117.651}
        x2={520.634}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_403_)" }}
        d="M520.77 78.95h-.27l.13-8.25z"
      />
      <linearGradient
        id="SVGID_404_"
        gradientUnits="userSpaceOnUse"
        x1={503.239}
        y1={117.652}
        x2={503.239}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_404_)" }}
        d="M503.5 91.65h-.52l.26-15.46z"
      />
      <linearGradient
        id="SVGID_405_"
        gradientUnits="userSpaceOnUse"
        x1={506.619}
        y1={117.652}
        x2={506.619}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_405_)" }}
        d="M506.76 91.65h-.28l.14-8.24z"
      />
      <linearGradient
        id="SVGID_406_"
        gradientUnits="userSpaceOnUse"
        x1={493.821}
        y1={117.652}
        x2={493.821}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_406_)" }}
        d="M485.67 67.33h16.29v48.07h-16.29z"
      />
      <linearGradient
        id="SVGID_407_"
        gradientUnits="userSpaceOnUse"
        x1={522.14}
        y1={117.652}
        x2={522.14}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_407_)" }}
        d="M511.02 88.89h22.25v27.99h-22.25z"
      />
      <linearGradient
        id="SVGID_408_"
        gradientUnits="userSpaceOnUse"
        x1={533.191}
        y1={117.652}
        x2={533.191}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_408_)" }}
        d="M529.44 94.12h7.49v22.75h-7.49z"
      />
      <linearGradient
        id="SVGID_409_"
        gradientUnits="userSpaceOnUse"
        x1={509.142}
        y1={117.652}
        x2={509.142}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_409_)" }}
        d="M507.27 98.22h3.75v18.66h-3.75z"
      />
      <linearGradient
        id="SVGID_410_"
        gradientUnits="userSpaceOnUse"
        x1={499.301}
        y1={117.652}
        x2={499.301}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_410_)" }}
        d="M491.33 91.06h15.94v25.82h-15.94z"
      />
      <linearGradient
        id="SVGID_411_"
        gradientUnits="userSpaceOnUse"
        x1={525.136}
        y1={117.651}
        x2={525.136}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_411_)" }}
        d="M517.08 82.34h16.11v6.55h-16.11z"
      />
      <linearGradient
        id="SVGID_412_"
        gradientUnits="userSpaceOnUse"
        x1={523.504}
        y1={117.652}
        x2={523.504}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_412_)" }}
        d="M517.08 78.85h12.85v3.6h-12.85z"
      />
      <linearGradient
        id="SVGID_413_"
        gradientUnits="userSpaceOnUse"
        x1={469.35}
        y1={117.652}
        x2={469.35}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_413_)" }}
        d="M460.87 93.41h16.96v9.54h-16.96z"
      />
      <linearGradient
        id="SVGID_414_"
        gradientUnits="userSpaceOnUse"
        x1={476.995}
        y1={117.652}
        x2={476.995}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_414_)" }}
        d="M460.43 102.87h33.12v14h-33.12z"
      />
      <linearGradient
        id="SVGID_415_"
        gradientUnits="userSpaceOnUse"
        x1={980.079}
        y1={117.651}
        x2={980.079}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_415_)" }}
        d="M980.25 102.35h-.34l.17-5.92z"
      />
      <linearGradient
        id="SVGID_416_"
        gradientUnits="userSpaceOnUse"
        x1={980.56}
        y1={117.651}
        x2={980.56}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_416_)" }}
        d="M980.65 102.35h-.18l.09-3.15z"
      />
      <linearGradient
        id="SVGID_417_"
        gradientUnits="userSpaceOnUse"
        x1={969.276}
        y1={117.651}
        x2={969.276}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_417_)" }}
        d="M969.44 107.22h-.33l.17-5.92z"
      />
      <linearGradient
        id="SVGID_418_"
        gradientUnits="userSpaceOnUse"
        x1={971.469}
        y1={117.651}
        x2={971.469}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_418_)" }}
        d="M971.56 107.22h-.18l.09-3.16z"
      />
      <linearGradient
        id="SVGID_419_"
        gradientUnits="userSpaceOnUse"
        x1={949.961}
        y1={117.652}
        x2={949.961}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_419_)" }}
        d="M950.05 99.55h-.18l.09-3.16z"
      />
      <linearGradient
        id="SVGID_420_"
        gradientUnits="userSpaceOnUse"
        x1={963.166}
        y1={117.652}
        x2={963.166}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_420_)" }}
        d="M957.88 97.9h10.57v18.41h-10.57z"
      />
      <linearGradient
        id="SVGID_421_"
        gradientUnits="userSpaceOnUse"
        x1={981.537}
        y1={117.652}
        x2={981.537}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_421_)" }}
        d="M974.32 106.16h14.43v10.72h-14.43z"
      />
      <linearGradient
        id="SVGID_422_"
        gradientUnits="userSpaceOnUse"
        x1={988.706}
        y1={117.652}
        x2={988.706}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_422_)" }}
        d="M986.28 108.16h4.86v8.71h-4.86z"
      />
      <linearGradient
        id="SVGID_423_"
        gradientUnits="userSpaceOnUse"
        x1={973.105}
        y1={117.652}
        x2={973.105}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_423_)" }}
        d="M971.89 109.73h2.43v7.14h-2.43z"
      />
      <linearGradient
        id="SVGID_424_"
        gradientUnits="userSpaceOnUse"
        x1={966.721}
        y1={117.652}
        x2={966.721}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_424_)" }}
        d="M961.55 106.99h10.34v9.88h-10.34z"
      />
      <linearGradient
        id="SVGID_425_"
        gradientUnits="userSpaceOnUse"
        x1={983.481}
        y1={117.652}
        x2={983.481}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_425_)" }}
        d="M978.26 103.65h10.45v2.51h-10.45z"
      />
      <linearGradient
        id="SVGID_426_"
        gradientUnits="userSpaceOnUse"
        x1={946.913}
        y1={117.651}
        x2={946.913}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_426_)" }}
        d="M943.07 99.55h7.69v4.1h-7.69z"
      />
      <linearGradient
        id="SVGID_427_"
        gradientUnits="userSpaceOnUse"
        x1={982.422}
        y1={117.652}
        x2={982.422}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_427_)" }}
        d="M978.26 102.31h8.33v1.38h-8.33z"
      />
      <linearGradient
        id="SVGID_428_"
        gradientUnits="userSpaceOnUse"
        x1={951.374}
        y1={117.651}
        x2={951.374}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_428_)" }}
        d="M945.87 110.17h11v1.38h-11z"
      />
      <linearGradient
        id="SVGID_429_"
        gradientUnits="userSpaceOnUse"
        x1={956.875}
        y1={117.652}
        x2={956.875}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_429_)" }}
        d="M950.76 111.51H963v5.36h-12.24z"
      />
      <linearGradient
        id="SVGID_430_"
        gradientUnits="userSpaceOnUse"
        x1={940.353}
        y1={117.652}
        x2={940.353}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_430_)" }}
        d="M936.29 107.32h8.14v9.56h-8.14z"
      />
      <linearGradient
        id="SVGID_431_"
        gradientUnits="userSpaceOnUse"
        x1={947.335}
        y1={117.652}
        x2={947.335}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_431_)" }}
        d="M941.22 103.65h12.24v13.22h-12.24z"
      />
      <linearGradient
        id="SVGID_432_"
        gradientUnits="userSpaceOnUse"
        x1={880.438}
        y1={117.651}
        x2={880.438}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_432_)" }}
        d="M880.61 102.35h-.34l.17-5.92z"
      />
      <linearGradient
        id="SVGID_433_"
        gradientUnits="userSpaceOnUse"
        x1={880.919}
        y1={117.651}
        x2={880.919}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_433_)" }}
        d="M881.01 102.35h-.18l.09-3.15z"
      />
      <linearGradient
        id="SVGID_434_"
        gradientUnits="userSpaceOnUse"
        x1={869.635}
        y1={117.651}
        x2={869.635}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_434_)" }}
        d="M869.8 107.22h-.33l.17-5.92z"
      />
      <linearGradient
        id="SVGID_435_"
        gradientUnits="userSpaceOnUse"
        x1={871.828}
        y1={117.651}
        x2={871.828}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_435_)" }}
        d="M871.92 107.22h-.18l.09-3.16z"
      />
      <linearGradient
        id="SVGID_436_"
        gradientUnits="userSpaceOnUse"
        x1={850.321}
        y1={117.652}
        x2={850.321}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_436_)" }}
        d="M850.41 99.55h-.18l.09-3.16z"
      />
      <linearGradient
        id="SVGID_437_"
        gradientUnits="userSpaceOnUse"
        x1={863.525}
        y1={117.652}
        x2={863.525}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_437_)" }}
        d="M858.24 97.9h10.57v18.41h-10.57z"
      />
      <linearGradient
        id="SVGID_438_"
        gradientUnits="userSpaceOnUse"
        x1={881.897}
        y1={117.652}
        x2={881.897}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_438_)" }}
        d="M874.68 106.16h14.43v10.72h-14.43z"
      />
      <linearGradient
        id="SVGID_439_"
        gradientUnits="userSpaceOnUse"
        x1={889.065}
        y1={117.652}
        x2={889.065}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_439_)" }}
        d="M886.64 108.16h4.86v8.71h-4.86z"
      />
      <linearGradient
        id="SVGID_440_"
        gradientUnits="userSpaceOnUse"
        x1={873.465}
        y1={117.652}
        x2={873.465}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_440_)" }}
        d="M872.25 109.73h2.43v7.14h-2.43z"
      />
      <linearGradient
        id="SVGID_441_"
        gradientUnits="userSpaceOnUse"
        x1={867.08}
        y1={117.652}
        x2={867.08}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_441_)" }}
        d="M861.91 106.99h10.34v9.88h-10.34z"
      />
      <linearGradient
        id="SVGID_442_"
        gradientUnits="userSpaceOnUse"
        x1={883.84}
        y1={117.652}
        x2={883.84}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_442_)" }}
        d="M878.61 103.65h10.45v2.51h-10.45z"
      />
      <linearGradient
        id="SVGID_443_"
        gradientUnits="userSpaceOnUse"
        x1={847.272}
        y1={117.651}
        x2={847.272}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_443_)" }}
        d="M843.43 99.55h7.69v4.1h-7.69z"
      />
      <linearGradient
        id="SVGID_444_"
        gradientUnits="userSpaceOnUse"
        x1={882.781}
        y1={117.652}
        x2={882.781}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_444_)" }}
        d="M878.61 102.31h8.33v1.38h-8.33z"
      />
      <linearGradient
        id="SVGID_445_"
        gradientUnits="userSpaceOnUse"
        x1={851.734}
        y1={117.651}
        x2={851.734}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_445_)" }}
        d="M846.23 110.17h11v1.38h-11z"
      />
      <linearGradient
        id="SVGID_446_"
        gradientUnits="userSpaceOnUse"
        x1={857.235}
        y1={117.652}
        x2={857.235}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_446_)" }}
        d="M851.12 111.51h12.24v5.36h-12.24z"
      />
      <linearGradient
        id="SVGID_447_"
        gradientUnits="userSpaceOnUse"
        x1={840.712}
        y1={117.652}
        x2={840.712}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_447_)" }}
        d="M836.64 107.32h8.14v9.56h-8.14z"
      />
      <linearGradient
        id="SVGID_448_"
        gradientUnits="userSpaceOnUse"
        x1={847.694}
        y1={117.652}
        x2={847.694}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_448_)" }}
        d="M841.58 103.65h12.24v13.22h-12.24z"
      />
      <linearGradient
        id="SVGID_449_"
        gradientUnits="userSpaceOnUse"
        x1={842.675}
        y1={117.651}
        x2={842.675}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_449_)" }}
        d="M842.84 102.35h-.33l.17-5.92z"
      />
      <linearGradient
        id="SVGID_450_"
        gradientUnits="userSpaceOnUse"
        x1={843.156}
        y1={117.651}
        x2={843.156}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_450_)" }}
        d="M843.25 102.35h-.18l.09-3.15z"
      />
      <linearGradient
        id="SVGID_451_"
        gradientUnits="userSpaceOnUse"
        x1={831.872}
        y1={117.651}
        x2={831.872}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_451_)" }}
        d="M832.04 107.22h-.33l.16-5.92z"
      />
      <linearGradient
        id="SVGID_452_"
        gradientUnits="userSpaceOnUse"
        x1={834.065}
        y1={117.651}
        x2={834.065}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_452_)" }}
        d="M834.15 107.22h-.17l.08-3.16z"
      />
      <linearGradient
        id="SVGID_453_"
        gradientUnits="userSpaceOnUse"
        x1={812.558}
        y1={117.652}
        x2={812.558}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_453_)" }}
        d="M812.65 99.55h-.18l.09-3.16z"
      />
      <linearGradient
        id="SVGID_454_"
        gradientUnits="userSpaceOnUse"
        x1={825.762}
        y1={117.652}
        x2={825.762}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_454_)" }}
        d="M820.48 97.9h10.57v18.41h-10.57z"
      />
      <linearGradient
        id="SVGID_455_"
        gradientUnits="userSpaceOnUse"
        x1={844.134}
        y1={117.652}
        x2={844.134}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_455_)" }}
        d="M836.92 106.16h14.43v10.72h-14.43z"
      />
      <linearGradient
        id="SVGID_456_"
        gradientUnits="userSpaceOnUse"
        x1={851.302}
        y1={117.652}
        x2={851.302}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_456_)" }}
        d="M848.87 108.16h4.86v8.71h-4.86z"
      />
      <linearGradient
        id="SVGID_457_"
        gradientUnits="userSpaceOnUse"
        x1={835.702}
        y1={117.652}
        x2={835.702}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_457_)" }}
        d="M834.49 109.73h2.43v7.14h-2.43z"
      />
      <linearGradient
        id="SVGID_458_"
        gradientUnits="userSpaceOnUse"
        x1={829.317}
        y1={117.652}
        x2={829.317}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_458_)" }}
        d="M824.15 106.99h10.34v9.88h-10.34z"
      />
      <linearGradient
        id="SVGID_459_"
        gradientUnits="userSpaceOnUse"
        x1={846.077}
        y1={117.652}
        x2={846.077}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_459_)" }}
        d="M840.85 103.65h10.45v2.51h-10.45z"
      />
      <linearGradient
        id="SVGID_460_"
        gradientUnits="userSpaceOnUse"
        x1={809.509}
        y1={117.651}
        x2={809.509}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_460_)" }}
        d="M805.67 99.55h7.69v4.1h-7.69z"
      />
      <linearGradient
        id="SVGID_461_"
        gradientUnits="userSpaceOnUse"
        x1={845.018}
        y1={117.652}
        x2={845.018}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_461_)" }}
        d="M840.85 102.31h8.33v1.38h-8.33z"
      />
      <linearGradient
        id="SVGID_462_"
        gradientUnits="userSpaceOnUse"
        x1={813.971}
        y1={117.651}
        x2={813.971}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_462_)" }}
        d="M808.47 110.17h11v1.38h-11z"
      />
      <linearGradient
        id="SVGID_463_"
        gradientUnits="userSpaceOnUse"
        x1={819.472}
        y1={117.652}
        x2={819.472}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_463_)" }}
        d="M813.35 111.51h12.24v5.36h-12.24z"
      />
      <linearGradient
        id="SVGID_464_"
        gradientUnits="userSpaceOnUse"
        x1={802.949}
        y1={117.652}
        x2={802.949}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_464_)" }}
        d="M798.88 107.32h8.14v9.56h-8.14z"
      />
      <linearGradient
        id="SVGID_465_"
        gradientUnits="userSpaceOnUse"
        x1={809.931}
        y1={117.652}
        x2={809.931}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_465_)" }}
        d="M803.81 103.65h12.24v13.22h-12.24z"
      />
      <linearGradient
        id="SVGID_466_"
        gradientUnits="userSpaceOnUse"
        x1={933.448}
        y1={117.652}
        x2={933.448}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_466_)" }}
        d="M933.61 92.27h-.33l.17-10.03z"
      />
      <linearGradient
        id="SVGID_467_"
        gradientUnits="userSpaceOnUse"
        x1={933.929}
        y1={117.652}
        x2={933.929}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_467_)" }}
        d="M934.02 92.27h-.18l.09-5.35z"
      />
      <linearGradient
        id="SVGID_468_"
        gradientUnits="userSpaceOnUse"
        x1={922.644}
        y1={117.651}
        x2={922.644}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_468_)" }}
        d="M922.81 100.51h-.33l.16-10.03z"
      />
      <linearGradient
        id="SVGID_469_"
        gradientUnits="userSpaceOnUse"
        x1={924.837}
        y1={117.651}
        x2={924.837}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_469_)" }}
        d="M924.93 100.51h-.18l.09-5.35z"
      />
      <linearGradient
        id="SVGID_470_"
        gradientUnits="userSpaceOnUse"
        x1={903.33}
        y1={117.652}
        x2={903.33}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_470_)" }}
        d="M903.42 87.52h-.18l.09-5.35z"
      />
      <linearGradient
        id="SVGID_471_"
        gradientUnits="userSpaceOnUse"
        x1={916.534}
        y1={117.652}
        x2={916.534}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_471_)" }}
        d="M911.25 84.74h10.57v31.18h-10.57z"
      />
      <linearGradient
        id="SVGID_472_"
        gradientUnits="userSpaceOnUse"
        x1={934.906}
        y1={117.652}
        x2={934.906}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_472_)" }}
        d="M927.69 98.72h14.43v18.15h-14.43z"
      />
      <linearGradient
        id="SVGID_473_"
        gradientUnits="userSpaceOnUse"
        x1={942.074}
        y1={117.652}
        x2={942.074}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_473_)" }}
        d="M939.64 102.11h4.86v14.76h-4.86z"
      />
      <linearGradient
        id="SVGID_474_"
        gradientUnits="userSpaceOnUse"
        x1={926.474}
        y1={117.652}
        x2={926.474}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_474_)" }}
        d="M925.26 104.77h2.43v12.1h-2.43z"
      />
      <linearGradient
        id="SVGID_475_"
        gradientUnits="userSpaceOnUse"
        x1={920.09}
        y1={117.652}
        x2={920.09}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_475_)" }}
        d="M914.92 100.13h10.34v16.75h-10.34z"
      />
      <linearGradient
        id="SVGID_476_"
        gradientUnits="userSpaceOnUse"
        x1={936.849}
        y1={117.652}
        x2={936.849}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_476_)" }}
        d="M931.62 94.47h10.45v4.25h-10.45z"
      />
      <linearGradient
        id="SVGID_477_"
        gradientUnits="userSpaceOnUse"
        x1={900.282}
        y1={117.652}
        x2={900.282}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_477_)" }}
        d="M896.44 87.52h7.69v6.95h-7.69z"
      />
      <linearGradient
        id="SVGID_478_"
        gradientUnits="userSpaceOnUse"
        x1={935.79}
        y1={117.652}
        x2={935.79}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_478_)" }}
        d="M931.62 92.2h8.33v2.34h-8.33z"
      />
      <linearGradient
        id="SVGID_479_"
        gradientUnits="userSpaceOnUse"
        x1={904.743}
        y1={117.652}
        x2={904.743}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_479_)" }}
        d="M899.24 105.51h11v2.34h-11z"
      />
      <linearGradient
        id="SVGID_480_"
        gradientUnits="userSpaceOnUse"
        x1={910.244}
        y1={117.652}
        x2={910.244}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_480_)" }}
        d="M904.13 107.79h12.24v9.08h-12.24z"
      />
      <linearGradient
        id="SVGID_481_"
        gradientUnits="userSpaceOnUse"
        x1={893.722}
        y1={117.652}
        x2={893.722}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_481_)" }}
        d="M889.65 100.68h8.14v16.19h-8.14z"
      />
      <linearGradient
        id="SVGID_482_"
        gradientUnits="userSpaceOnUse"
        x1={900.703}
        y1={117.652}
        x2={900.703}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_482_)" }}
        d="M894.58 94.47h12.24v22.4h-12.24z"
      />
      <linearGradient
        id="SVGID_483_"
        gradientUnits="userSpaceOnUse"
        x1={810.236}
        y1={117.652}
        x2={810.236}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_483_)" }}
        d="M810.4 92.27h-.33l.17-10.03z"
      />
      <linearGradient
        id="SVGID_484_"
        gradientUnits="userSpaceOnUse"
        x1={810.717}
        y1={117.652}
        x2={810.717}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_484_)" }}
        d="M810.81 92.27h-.18l.09-5.35z"
      />
      <linearGradient
        id="SVGID_485_"
        gradientUnits="userSpaceOnUse"
        x1={799.432}
        y1={117.651}
        x2={799.432}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_485_)" }}
        d="M799.6 100.51h-.33l.16-10.03z"
      />
      <linearGradient
        id="SVGID_486_"
        gradientUnits="userSpaceOnUse"
        x1={801.625}
        y1={117.651}
        x2={801.625}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_486_)" }}
        d="M801.71 100.51h-.17l.09-5.35z"
      />
      <linearGradient
        id="SVGID_487_"
        gradientUnits="userSpaceOnUse"
        x1={793.322}
        y1={117.652}
        x2={793.322}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_487_)" }}
        d="M788.04 84.74h10.57v31.18h-10.57z"
      />
      <linearGradient
        id="SVGID_488_"
        gradientUnits="userSpaceOnUse"
        x1={811.694}
        y1={117.652}
        x2={811.694}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_488_)" }}
        d="M804.48 98.72h14.43v18.15h-14.43z"
      />
      <linearGradient
        id="SVGID_489_"
        gradientUnits="userSpaceOnUse"
        x1={818.862}
        y1={117.652}
        x2={818.862}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_489_)" }}
        d="M816.43 102.11h4.86v14.76h-4.86z"
      />
      <linearGradient
        id="SVGID_490_"
        gradientUnits="userSpaceOnUse"
        x1={803.262}
        y1={117.652}
        x2={803.262}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_490_)" }}
        d="M802.05 104.77h2.43v12.1h-2.43z"
      />
      <linearGradient
        id="SVGID_491_"
        gradientUnits="userSpaceOnUse"
        x1={796.878}
        y1={117.652}
        x2={796.878}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_491_)" }}
        d="M791.71 100.13h10.34v16.75h-10.34z"
      />
      <linearGradient
        id="SVGID_492_"
        gradientUnits="userSpaceOnUse"
        x1={813.637}
        y1={117.652}
        x2={813.637}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_492_)" }}
        d="M808.41 94.47h10.45v4.25h-10.45z"
      />
      <linearGradient
        id="SVGID_493_"
        gradientUnits="userSpaceOnUse"
        x1={812.579}
        y1={117.652}
        x2={812.579}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_493_)" }}
        d="M808.41 92.2h8.33v2.34h-8.33z"
      />
      <linearGradient
        id="SVGID_494_"
        gradientUnits="userSpaceOnUse"
        x1={777.448}
        y1={117.652}
        x2={777.448}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_494_)" }}
        d="M771.95 101.66h11v6.19h-11z"
      />
      <linearGradient
        id="SVGID_495_"
        gradientUnits="userSpaceOnUse"
        x1={782.408}
        y1={117.652}
        x2={782.408}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_495_)" }}
        d="M771.66 107.79h21.49v9.08h-21.49z"
      />
      <linearGradient
        id="SVGID_496_"
        gradientUnits="userSpaceOnUse"
        x1={980.079}
        y1={117.651}
        x2={980.079}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_496_)" }}
        d="M980.25 102.35h-.34l.17-5.92z"
      />
      <linearGradient
        id="SVGID_497_"
        gradientUnits="userSpaceOnUse"
        x1={980.56}
        y1={117.651}
        x2={980.56}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_497_)" }}
        d="M980.65 102.35h-.18l.09-3.15z"
      />
      <linearGradient
        id="SVGID_498_"
        gradientUnits="userSpaceOnUse"
        x1={969.276}
        y1={117.651}
        x2={969.276}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_498_)" }}
        d="M969.44 107.22h-.33l.17-5.92z"
      />
      <linearGradient
        id="SVGID_499_"
        gradientUnits="userSpaceOnUse"
        x1={971.469}
        y1={117.651}
        x2={971.469}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_499_)" }}
        d="M971.56 107.22h-.18l.09-3.16z"
      />
      <linearGradient
        id="SVGID_500_"
        gradientUnits="userSpaceOnUse"
        x1={949.961}
        y1={117.652}
        x2={949.961}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_500_)" }}
        d="M950.05 99.55h-.18l.09-3.16z"
      />
      <linearGradient
        id="SVGID_501_"
        gradientUnits="userSpaceOnUse"
        x1={963.166}
        y1={117.652}
        x2={963.166}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_501_)" }}
        d="M957.88 97.9h10.57v18.41h-10.57z"
      />
      <linearGradient
        id="SVGID_502_"
        gradientUnits="userSpaceOnUse"
        x1={981.537}
        y1={117.652}
        x2={981.537}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_502_)" }}
        d="M974.32 106.16h14.43v10.72h-14.43z"
      />
      <linearGradient
        id="SVGID_503_"
        gradientUnits="userSpaceOnUse"
        x1={988.706}
        y1={117.652}
        x2={988.706}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_503_)" }}
        d="M986.28 108.16h4.86v8.71h-4.86z"
      />
      <linearGradient
        id="SVGID_504_"
        gradientUnits="userSpaceOnUse"
        x1={973.105}
        y1={117.652}
        x2={973.105}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_504_)" }}
        d="M971.89 109.73h2.43v7.14h-2.43z"
      />
      <linearGradient
        id="SVGID_505_"
        gradientUnits="userSpaceOnUse"
        x1={966.721}
        y1={117.652}
        x2={966.721}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_505_)" }}
        d="M961.55 106.99h10.34v9.88h-10.34z"
      />
      <linearGradient
        id="SVGID_506_"
        gradientUnits="userSpaceOnUse"
        x1={983.481}
        y1={117.652}
        x2={983.481}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_506_)" }}
        d="M978.26 103.65h10.45v2.51h-10.45z"
      />
      <linearGradient
        id="SVGID_507_"
        gradientUnits="userSpaceOnUse"
        x1={946.913}
        y1={117.651}
        x2={946.913}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_507_)" }}
        d="M943.07 99.55h7.69v4.1h-7.69z"
      />
      <linearGradient
        id="SVGID_508_"
        gradientUnits="userSpaceOnUse"
        x1={982.422}
        y1={117.652}
        x2={982.422}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_508_)" }}
        d="M978.26 102.31h8.33v1.38h-8.33z"
      />
      <linearGradient
        id="SVGID_509_"
        gradientUnits="userSpaceOnUse"
        x1={951.374}
        y1={117.651}
        x2={951.374}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_509_)" }}
        d="M945.87 110.17h11v1.38h-11z"
      />
      <linearGradient
        id="SVGID_510_"
        gradientUnits="userSpaceOnUse"
        x1={956.875}
        y1={117.652}
        x2={956.875}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_510_)" }}
        d="M950.76 111.51H963v5.36h-12.24z"
      />
      <linearGradient
        id="SVGID_511_"
        gradientUnits="userSpaceOnUse"
        x1={940.353}
        y1={117.652}
        x2={940.353}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_511_)" }}
        d="M936.29 107.32h8.14v9.56h-8.14z"
      />
      <linearGradient
        id="SVGID_512_"
        gradientUnits="userSpaceOnUse"
        x1={947.335}
        y1={117.652}
        x2={947.335}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_512_)" }}
        d="M941.22 103.65h12.24v13.22h-12.24z"
      />
      <linearGradient
        id="SVGID_513_"
        gradientUnits="userSpaceOnUse"
        x1={880.438}
        y1={117.651}
        x2={880.438}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_513_)" }}
        d="M880.61 102.35h-.34l.17-5.92z"
      />
      <linearGradient
        id="SVGID_514_"
        gradientUnits="userSpaceOnUse"
        x1={880.919}
        y1={117.651}
        x2={880.919}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_514_)" }}
        d="M881.01 102.35h-.18l.09-3.15z"
      />
      <linearGradient
        id="SVGID_515_"
        gradientUnits="userSpaceOnUse"
        x1={869.635}
        y1={117.651}
        x2={869.635}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_515_)" }}
        d="M869.8 107.22h-.33l.17-5.92z"
      />
      <linearGradient
        id="SVGID_516_"
        gradientUnits="userSpaceOnUse"
        x1={871.828}
        y1={117.651}
        x2={871.828}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_516_)" }}
        d="M871.92 107.22h-.18l.09-3.16z"
      />
      <linearGradient
        id="SVGID_517_"
        gradientUnits="userSpaceOnUse"
        x1={850.321}
        y1={117.652}
        x2={850.321}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_517_)" }}
        d="M850.41 99.55h-.18l.09-3.16z"
      />
      <linearGradient
        id="SVGID_518_"
        gradientUnits="userSpaceOnUse"
        x1={863.525}
        y1={117.652}
        x2={863.525}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_518_)" }}
        d="M858.24 97.9h10.57v18.41h-10.57z"
      />
      <linearGradient
        id="SVGID_519_"
        gradientUnits="userSpaceOnUse"
        x1={881.897}
        y1={117.652}
        x2={881.897}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_519_)" }}
        d="M874.68 106.16h14.43v10.72h-14.43z"
      />
      <linearGradient
        id="SVGID_520_"
        gradientUnits="userSpaceOnUse"
        x1={889.065}
        y1={117.652}
        x2={889.065}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_520_)" }}
        d="M886.64 108.16h4.86v8.71h-4.86z"
      />
      <linearGradient
        id="SVGID_521_"
        gradientUnits="userSpaceOnUse"
        x1={873.465}
        y1={117.652}
        x2={873.465}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_521_)" }}
        d="M872.25 109.73h2.43v7.14h-2.43z"
      />
      <linearGradient
        id="SVGID_522_"
        gradientUnits="userSpaceOnUse"
        x1={867.08}
        y1={117.652}
        x2={867.08}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_522_)" }}
        d="M861.91 106.99h10.34v9.88h-10.34z"
      />
      <linearGradient
        id="SVGID_523_"
        gradientUnits="userSpaceOnUse"
        x1={883.84}
        y1={117.652}
        x2={883.84}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_523_)" }}
        d="M878.61 103.65h10.45v2.51h-10.45z"
      />
      <linearGradient
        id="SVGID_524_"
        gradientUnits="userSpaceOnUse"
        x1={847.272}
        y1={117.651}
        x2={847.272}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_524_)" }}
        d="M843.43 99.55h7.69v4.1h-7.69z"
      />
      <linearGradient
        id="SVGID_525_"
        gradientUnits="userSpaceOnUse"
        x1={882.781}
        y1={117.652}
        x2={882.781}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_525_)" }}
        d="M878.61 102.31h8.33v1.38h-8.33z"
      />
      <linearGradient
        id="SVGID_526_"
        gradientUnits="userSpaceOnUse"
        x1={851.734}
        y1={117.651}
        x2={851.734}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_526_)" }}
        d="M846.23 110.17h11v1.38h-11z"
      />
      <linearGradient
        id="SVGID_527_"
        gradientUnits="userSpaceOnUse"
        x1={857.235}
        y1={117.652}
        x2={857.235}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_527_)" }}
        d="M851.12 111.51h12.24v5.36h-12.24z"
      />
      <linearGradient
        id="SVGID_528_"
        gradientUnits="userSpaceOnUse"
        x1={840.712}
        y1={117.652}
        x2={840.712}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_528_)" }}
        d="M836.64 107.32h8.14v9.56h-8.14z"
      />
      <linearGradient
        id="SVGID_529_"
        gradientUnits="userSpaceOnUse"
        x1={847.694}
        y1={117.652}
        x2={847.694}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_529_)" }}
        d="M841.58 103.65h12.24v13.22h-12.24z"
      />
      <linearGradient
        id="SVGID_530_"
        gradientUnits="userSpaceOnUse"
        x1={842.675}
        y1={117.651}
        x2={842.675}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_530_)" }}
        d="M842.84 102.35h-.33l.17-5.92z"
      />
      <linearGradient
        id="SVGID_531_"
        gradientUnits="userSpaceOnUse"
        x1={843.156}
        y1={117.651}
        x2={843.156}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_531_)" }}
        d="M843.25 102.35h-.18l.09-3.15z"
      />
      <linearGradient
        id="SVGID_532_"
        gradientUnits="userSpaceOnUse"
        x1={831.872}
        y1={117.651}
        x2={831.872}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_532_)" }}
        d="M832.04 107.22h-.33l.16-5.92z"
      />
      <linearGradient
        id="SVGID_533_"
        gradientUnits="userSpaceOnUse"
        x1={834.065}
        y1={117.651}
        x2={834.065}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_533_)" }}
        d="M834.15 107.22h-.17l.08-3.16z"
      />
      <linearGradient
        id="SVGID_534_"
        gradientUnits="userSpaceOnUse"
        x1={812.558}
        y1={117.652}
        x2={812.558}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_534_)" }}
        d="M812.65 99.55h-.18l.09-3.16z"
      />
      <linearGradient
        id="SVGID_535_"
        gradientUnits="userSpaceOnUse"
        x1={825.762}
        y1={117.652}
        x2={825.762}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_535_)" }}
        d="M820.48 97.9h10.57v18.41h-10.57z"
      />
      <linearGradient
        id="SVGID_536_"
        gradientUnits="userSpaceOnUse"
        x1={844.134}
        y1={117.652}
        x2={844.134}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_536_)" }}
        d="M836.92 106.16h14.43v10.72h-14.43z"
      />
      <linearGradient
        id="SVGID_537_"
        gradientUnits="userSpaceOnUse"
        x1={851.302}
        y1={117.652}
        x2={851.302}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_537_)" }}
        d="M848.87 108.16h4.86v8.71h-4.86z"
      />
      <linearGradient
        id="SVGID_538_"
        gradientUnits="userSpaceOnUse"
        x1={835.702}
        y1={117.652}
        x2={835.702}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_538_)" }}
        d="M834.49 109.73h2.43v7.14h-2.43z"
      />
      <linearGradient
        id="SVGID_539_"
        gradientUnits="userSpaceOnUse"
        x1={829.317}
        y1={117.652}
        x2={829.317}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_539_)" }}
        d="M824.15 106.99h10.34v9.88h-10.34z"
      />
      <linearGradient
        id="SVGID_540_"
        gradientUnits="userSpaceOnUse"
        x1={846.077}
        y1={117.652}
        x2={846.077}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_540_)" }}
        d="M840.85 103.65h10.45v2.51h-10.45z"
      />
      <linearGradient
        id="SVGID_541_"
        gradientUnits="userSpaceOnUse"
        x1={809.509}
        y1={117.651}
        x2={809.509}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_541_)" }}
        d="M805.67 99.55h7.69v4.1h-7.69z"
      />
      <linearGradient
        id="SVGID_542_"
        gradientUnits="userSpaceOnUse"
        x1={845.018}
        y1={117.652}
        x2={845.018}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_542_)" }}
        d="M840.85 102.31h8.33v1.38h-8.33z"
      />
      <linearGradient
        id="SVGID_543_"
        gradientUnits="userSpaceOnUse"
        x1={813.971}
        y1={117.651}
        x2={813.971}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_543_)" }}
        d="M808.47 110.17h11v1.38h-11z"
      />
      <linearGradient
        id="SVGID_544_"
        gradientUnits="userSpaceOnUse"
        x1={819.472}
        y1={117.652}
        x2={819.472}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_544_)" }}
        d="M813.35 111.51h12.24v5.36h-12.24z"
      />
      <linearGradient
        id="SVGID_545_"
        gradientUnits="userSpaceOnUse"
        x1={802.949}
        y1={117.652}
        x2={802.949}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_545_)" }}
        d="M798.88 107.32h8.14v9.56h-8.14z"
      />
      <linearGradient
        id="SVGID_546_"
        gradientUnits="userSpaceOnUse"
        x1={809.931}
        y1={117.652}
        x2={809.931}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_546_)" }}
        d="M803.81 103.65h12.24v13.22h-12.24z"
      />
      <linearGradient
        id="SVGID_547_"
        gradientUnits="userSpaceOnUse"
        x1={933.448}
        y1={117.652}
        x2={933.448}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_547_)" }}
        d="M933.61 92.27h-.33l.17-10.03z"
      />
      <linearGradient
        id="SVGID_548_"
        gradientUnits="userSpaceOnUse"
        x1={933.929}
        y1={117.652}
        x2={933.929}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_548_)" }}
        d="M934.02 92.27h-.18l.09-5.35z"
      />
      <linearGradient
        id="SVGID_549_"
        gradientUnits="userSpaceOnUse"
        x1={922.644}
        y1={117.651}
        x2={922.644}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_549_)" }}
        d="M922.81 100.51h-.33l.16-10.03z"
      />
      <linearGradient
        id="SVGID_550_"
        gradientUnits="userSpaceOnUse"
        x1={924.837}
        y1={117.651}
        x2={924.837}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_550_)" }}
        d="M924.93 100.51h-.18l.09-5.35z"
      />
      <linearGradient
        id="SVGID_551_"
        gradientUnits="userSpaceOnUse"
        x1={903.33}
        y1={117.652}
        x2={903.33}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_551_)" }}
        d="M903.42 87.52h-.18l.09-5.35z"
      />
      <linearGradient
        id="SVGID_552_"
        gradientUnits="userSpaceOnUse"
        x1={916.534}
        y1={117.652}
        x2={916.534}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_552_)" }}
        d="M911.25 84.74h10.57v31.18h-10.57z"
      />
      <linearGradient
        id="SVGID_553_"
        gradientUnits="userSpaceOnUse"
        x1={934.906}
        y1={117.652}
        x2={934.906}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_553_)" }}
        d="M927.69 98.72h14.43v18.15h-14.43z"
      />
      <linearGradient
        id="SVGID_554_"
        gradientUnits="userSpaceOnUse"
        x1={942.074}
        y1={117.652}
        x2={942.074}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_554_)" }}
        d="M939.64 102.11h4.86v14.76h-4.86z"
      />
      <linearGradient
        id="SVGID_555_"
        gradientUnits="userSpaceOnUse"
        x1={926.474}
        y1={117.652}
        x2={926.474}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_555_)" }}
        d="M925.26 104.77h2.43v12.1h-2.43z"
      />
      <linearGradient
        id="SVGID_556_"
        gradientUnits="userSpaceOnUse"
        x1={920.09}
        y1={117.652}
        x2={920.09}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_556_)" }}
        d="M914.92 100.13h10.34v16.75h-10.34z"
      />
      <linearGradient
        id="SVGID_557_"
        gradientUnits="userSpaceOnUse"
        x1={936.849}
        y1={117.652}
        x2={936.849}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_557_)" }}
        d="M931.62 94.47h10.45v4.25h-10.45z"
      />
      <linearGradient
        id="SVGID_558_"
        gradientUnits="userSpaceOnUse"
        x1={900.282}
        y1={117.652}
        x2={900.282}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_558_)" }}
        d="M896.44 87.52h7.69v6.95h-7.69z"
      />
      <linearGradient
        id="SVGID_559_"
        gradientUnits="userSpaceOnUse"
        x1={935.79}
        y1={117.652}
        x2={935.79}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_559_)" }}
        d="M931.62 92.2h8.33v2.34h-8.33z"
      />
      <linearGradient
        id="SVGID_560_"
        gradientUnits="userSpaceOnUse"
        x1={904.743}
        y1={117.652}
        x2={904.743}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_560_)" }}
        d="M899.24 105.51h11v2.34h-11z"
      />
      <linearGradient
        id="SVGID_561_"
        gradientUnits="userSpaceOnUse"
        x1={910.244}
        y1={117.652}
        x2={910.244}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_561_)" }}
        d="M904.13 107.79h12.24v9.08h-12.24z"
      />
      <linearGradient
        id="SVGID_562_"
        gradientUnits="userSpaceOnUse"
        x1={893.722}
        y1={117.652}
        x2={893.722}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_562_)" }}
        d="M889.65 100.68h8.14v16.19h-8.14z"
      />
      <linearGradient
        id="SVGID_563_"
        gradientUnits="userSpaceOnUse"
        x1={900.703}
        y1={117.652}
        x2={900.703}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_563_)" }}
        d="M894.58 94.47h12.24v22.4h-12.24z"
      />
      <linearGradient
        id="SVGID_564_"
        gradientUnits="userSpaceOnUse"
        x1={810.236}
        y1={117.652}
        x2={810.236}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_564_)" }}
        d="M810.4 92.27h-.33l.17-10.03z"
      />
      <linearGradient
        id="SVGID_565_"
        gradientUnits="userSpaceOnUse"
        x1={810.717}
        y1={117.652}
        x2={810.717}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_565_)" }}
        d="M810.81 92.27h-.18l.09-5.35z"
      />
      <linearGradient
        id="SVGID_566_"
        gradientUnits="userSpaceOnUse"
        x1={799.432}
        y1={117.651}
        x2={799.432}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_566_)" }}
        d="M799.6 100.51h-.33l.16-10.03z"
      />
      <linearGradient
        id="SVGID_567_"
        gradientUnits="userSpaceOnUse"
        x1={801.625}
        y1={117.651}
        x2={801.625}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_567_)" }}
        d="M801.71 100.51h-.17l.09-5.35z"
      />
      <linearGradient
        id="SVGID_568_"
        gradientUnits="userSpaceOnUse"
        x1={793.322}
        y1={117.652}
        x2={793.322}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_568_)" }}
        d="M788.04 84.74h10.57v31.18h-10.57z"
      />
      <linearGradient
        id="SVGID_569_"
        gradientUnits="userSpaceOnUse"
        x1={811.694}
        y1={117.652}
        x2={811.694}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_569_)" }}
        d="M804.48 98.72h14.43v18.15h-14.43z"
      />
      <linearGradient
        id="SVGID_570_"
        gradientUnits="userSpaceOnUse"
        x1={818.862}
        y1={117.652}
        x2={818.862}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_570_)" }}
        d="M816.43 102.11h4.86v14.76h-4.86z"
      />
      <linearGradient
        id="SVGID_571_"
        gradientUnits="userSpaceOnUse"
        x1={803.262}
        y1={117.652}
        x2={803.262}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_571_)" }}
        d="M802.05 104.77h2.43v12.1h-2.43z"
      />
      <linearGradient
        id="SVGID_572_"
        gradientUnits="userSpaceOnUse"
        x1={796.878}
        y1={117.652}
        x2={796.878}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_572_)" }}
        d="M791.71 100.13h10.34v16.75h-10.34z"
      />
      <linearGradient
        id="SVGID_573_"
        gradientUnits="userSpaceOnUse"
        x1={813.637}
        y1={117.652}
        x2={813.637}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_573_)" }}
        d="M808.41 94.47h10.45v4.25h-10.45z"
      />
      <linearGradient
        id="SVGID_574_"
        gradientUnits="userSpaceOnUse"
        x1={812.579}
        y1={117.652}
        x2={812.579}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_574_)" }}
        d="M808.41 92.2h8.33v2.34h-8.33z"
      />
      <linearGradient
        id="SVGID_575_"
        gradientUnits="userSpaceOnUse"
        x1={777.448}
        y1={117.652}
        x2={777.448}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_575_)" }}
        d="M771.95 101.66h11v6.19h-11z"
      />
      <linearGradient
        id="SVGID_576_"
        gradientUnits="userSpaceOnUse"
        x1={782.408}
        y1={117.652}
        x2={782.408}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_576_)" }}
        d="M771.66 107.79h21.49v9.08h-21.49z"
      />
      <linearGradient
        id="SVGID_577_"
        gradientUnits="userSpaceOnUse"
        x1={980.079}
        y1={117.651}
        x2={980.079}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_577_)" }}
        d="M980.25 102.35h-.34l.17-5.92z"
      />
      <linearGradient
        id="SVGID_578_"
        gradientUnits="userSpaceOnUse"
        x1={980.56}
        y1={117.651}
        x2={980.56}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_578_)" }}
        d="M980.65 102.35h-.18l.09-3.15z"
      />
      <linearGradient
        id="SVGID_579_"
        gradientUnits="userSpaceOnUse"
        x1={969.276}
        y1={117.651}
        x2={969.276}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_579_)" }}
        d="M969.44 107.22h-.33l.17-5.92z"
      />
      <linearGradient
        id="SVGID_580_"
        gradientUnits="userSpaceOnUse"
        x1={971.469}
        y1={117.651}
        x2={971.469}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_580_)" }}
        d="M971.56 107.22h-.18l.09-3.16z"
      />
      <linearGradient
        id="SVGID_581_"
        gradientUnits="userSpaceOnUse"
        x1={949.961}
        y1={117.652}
        x2={949.961}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_581_)" }}
        d="M950.05 99.55h-.18l.09-3.16z"
      />
      <linearGradient
        id="SVGID_582_"
        gradientUnits="userSpaceOnUse"
        x1={963.166}
        y1={117.652}
        x2={963.166}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_582_)" }}
        d="M957.88 97.9h10.57v18.41h-10.57z"
      />
      <linearGradient
        id="SVGID_583_"
        gradientUnits="userSpaceOnUse"
        x1={981.537}
        y1={117.652}
        x2={981.537}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_583_)" }}
        d="M974.32 106.16h14.43v10.72h-14.43z"
      />
      <linearGradient
        id="SVGID_584_"
        gradientUnits="userSpaceOnUse"
        x1={988.706}
        y1={117.652}
        x2={988.706}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_584_)" }}
        d="M986.28 108.16h4.86v8.71h-4.86z"
      />
      <linearGradient
        id="SVGID_585_"
        gradientUnits="userSpaceOnUse"
        x1={973.105}
        y1={117.652}
        x2={973.105}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_585_)" }}
        d="M971.89 109.73h2.43v7.14h-2.43z"
      />
      <linearGradient
        id="SVGID_586_"
        gradientUnits="userSpaceOnUse"
        x1={966.721}
        y1={117.652}
        x2={966.721}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_586_)" }}
        d="M961.55 106.99h10.34v9.88h-10.34z"
      />
      <linearGradient
        id="SVGID_587_"
        gradientUnits="userSpaceOnUse"
        x1={983.481}
        y1={117.652}
        x2={983.481}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_587_)" }}
        d="M978.26 103.65h10.45v2.51h-10.45z"
      />
      <linearGradient
        id="SVGID_588_"
        gradientUnits="userSpaceOnUse"
        x1={946.913}
        y1={117.651}
        x2={946.913}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_588_)" }}
        d="M943.07 99.55h7.69v4.1h-7.69z"
      />
      <linearGradient
        id="SVGID_589_"
        gradientUnits="userSpaceOnUse"
        x1={982.422}
        y1={117.652}
        x2={982.422}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_589_)" }}
        d="M978.26 102.31h8.33v1.38h-8.33z"
      />
      <linearGradient
        id="SVGID_590_"
        gradientUnits="userSpaceOnUse"
        x1={951.374}
        y1={117.651}
        x2={951.374}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_590_)" }}
        d="M945.87 110.17h11v1.38h-11z"
      />
      <linearGradient
        id="SVGID_591_"
        gradientUnits="userSpaceOnUse"
        x1={956.875}
        y1={117.652}
        x2={956.875}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_591_)" }}
        d="M950.76 111.51H963v5.36h-12.24z"
      />
      <linearGradient
        id="SVGID_592_"
        gradientUnits="userSpaceOnUse"
        x1={940.353}
        y1={117.652}
        x2={940.353}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_592_)" }}
        d="M936.29 107.32h8.14v9.56h-8.14z"
      />
      <linearGradient
        id="SVGID_593_"
        gradientUnits="userSpaceOnUse"
        x1={947.335}
        y1={117.652}
        x2={947.335}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_593_)" }}
        d="M941.22 103.65h12.24v13.22h-12.24z"
      />
      <linearGradient
        id="SVGID_594_"
        gradientUnits="userSpaceOnUse"
        x1={880.438}
        y1={117.651}
        x2={880.438}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_594_)" }}
        d="M880.61 102.35h-.34l.17-5.92z"
      />
      <linearGradient
        id="SVGID_595_"
        gradientUnits="userSpaceOnUse"
        x1={880.919}
        y1={117.651}
        x2={880.919}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_595_)" }}
        d="M881.01 102.35h-.18l.09-3.15z"
      />
      <linearGradient
        id="SVGID_596_"
        gradientUnits="userSpaceOnUse"
        x1={869.635}
        y1={117.651}
        x2={869.635}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_596_)" }}
        d="M869.8 107.22h-.33l.17-5.92z"
      />
      <linearGradient
        id="SVGID_597_"
        gradientUnits="userSpaceOnUse"
        x1={871.828}
        y1={117.651}
        x2={871.828}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_597_)" }}
        d="M871.92 107.22h-.18l.09-3.16z"
      />
      <linearGradient
        id="SVGID_598_"
        gradientUnits="userSpaceOnUse"
        x1={850.321}
        y1={117.652}
        x2={850.321}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_598_)" }}
        d="M850.41 99.55h-.18l.09-3.16z"
      />
      <linearGradient
        id="SVGID_599_"
        gradientUnits="userSpaceOnUse"
        x1={863.525}
        y1={117.652}
        x2={863.525}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_599_)" }}
        d="M858.24 97.9h10.57v18.41h-10.57z"
      />
      <linearGradient
        id="SVGID_600_"
        gradientUnits="userSpaceOnUse"
        x1={881.897}
        y1={117.652}
        x2={881.897}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_600_)" }}
        d="M874.68 106.16h14.43v10.72h-14.43z"
      />
      <linearGradient
        id="SVGID_601_"
        gradientUnits="userSpaceOnUse"
        x1={889.065}
        y1={117.652}
        x2={889.065}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_601_)" }}
        d="M886.64 108.16h4.86v8.71h-4.86z"
      />
      <linearGradient
        id="SVGID_602_"
        gradientUnits="userSpaceOnUse"
        x1={873.465}
        y1={117.652}
        x2={873.465}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_602_)" }}
        d="M872.25 109.73h2.43v7.14h-2.43z"
      />
      <linearGradient
        id="SVGID_603_"
        gradientUnits="userSpaceOnUse"
        x1={867.08}
        y1={117.652}
        x2={867.08}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_603_)" }}
        d="M861.91 106.99h10.34v9.88h-10.34z"
      />
      <linearGradient
        id="SVGID_604_"
        gradientUnits="userSpaceOnUse"
        x1={883.84}
        y1={117.652}
        x2={883.84}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_604_)" }}
        d="M878.61 103.65h10.45v2.51h-10.45z"
      />
      <linearGradient
        id="SVGID_605_"
        gradientUnits="userSpaceOnUse"
        x1={847.272}
        y1={117.651}
        x2={847.272}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_605_)" }}
        d="M843.43 99.55h7.69v4.1h-7.69z"
      />
      <linearGradient
        id="SVGID_606_"
        gradientUnits="userSpaceOnUse"
        x1={882.781}
        y1={117.652}
        x2={882.781}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_606_)" }}
        d="M878.61 102.31h8.33v1.38h-8.33z"
      />
      <linearGradient
        id="SVGID_607_"
        gradientUnits="userSpaceOnUse"
        x1={851.734}
        y1={117.651}
        x2={851.734}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_607_)" }}
        d="M846.23 110.17h11v1.38h-11z"
      />
      <linearGradient
        id="SVGID_608_"
        gradientUnits="userSpaceOnUse"
        x1={857.235}
        y1={117.652}
        x2={857.235}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_608_)" }}
        d="M851.12 111.51h12.24v5.36h-12.24z"
      />
      <linearGradient
        id="SVGID_609_"
        gradientUnits="userSpaceOnUse"
        x1={840.712}
        y1={117.652}
        x2={840.712}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_609_)" }}
        d="M836.64 107.32h8.14v9.56h-8.14z"
      />
      <linearGradient
        id="SVGID_610_"
        gradientUnits="userSpaceOnUse"
        x1={847.694}
        y1={117.652}
        x2={847.694}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_610_)" }}
        d="M841.58 103.65h12.24v13.22h-12.24z"
      />
      <linearGradient
        id="SVGID_611_"
        gradientUnits="userSpaceOnUse"
        x1={842.675}
        y1={117.651}
        x2={842.675}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_611_)" }}
        d="M842.84 102.35h-.33l.17-5.92z"
      />
      <linearGradient
        id="SVGID_612_"
        gradientUnits="userSpaceOnUse"
        x1={843.156}
        y1={117.651}
        x2={843.156}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_612_)" }}
        d="M843.25 102.35h-.18l.09-3.15z"
      />
      <linearGradient
        id="SVGID_613_"
        gradientUnits="userSpaceOnUse"
        x1={831.872}
        y1={117.651}
        x2={831.872}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_613_)" }}
        d="M832.04 107.22h-.33l.16-5.92z"
      />
      <linearGradient
        id="SVGID_614_"
        gradientUnits="userSpaceOnUse"
        x1={834.065}
        y1={117.651}
        x2={834.065}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_614_)" }}
        d="M834.15 107.22h-.17l.08-3.16z"
      />
      <linearGradient
        id="SVGID_615_"
        gradientUnits="userSpaceOnUse"
        x1={812.558}
        y1={117.652}
        x2={812.558}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_615_)" }}
        d="M812.65 99.55h-.18l.09-3.16z"
      />
      <linearGradient
        id="SVGID_616_"
        gradientUnits="userSpaceOnUse"
        x1={825.762}
        y1={117.652}
        x2={825.762}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_616_)" }}
        d="M820.48 97.9h10.57v18.41h-10.57z"
      />
      <linearGradient
        id="SVGID_617_"
        gradientUnits="userSpaceOnUse"
        x1={844.134}
        y1={117.652}
        x2={844.134}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_617_)" }}
        d="M836.92 106.16h14.43v10.72h-14.43z"
      />
      <linearGradient
        id="SVGID_618_"
        gradientUnits="userSpaceOnUse"
        x1={851.302}
        y1={117.652}
        x2={851.302}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_618_)" }}
        d="M848.87 108.16h4.86v8.71h-4.86z"
      />
      <linearGradient
        id="SVGID_619_"
        gradientUnits="userSpaceOnUse"
        x1={835.702}
        y1={117.652}
        x2={835.702}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_619_)" }}
        d="M834.49 109.73h2.43v7.14h-2.43z"
      />
      <linearGradient
        id="SVGID_620_"
        gradientUnits="userSpaceOnUse"
        x1={829.317}
        y1={117.652}
        x2={829.317}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_620_)" }}
        d="M824.15 106.99h10.34v9.88h-10.34z"
      />
      <linearGradient
        id="SVGID_621_"
        gradientUnits="userSpaceOnUse"
        x1={846.077}
        y1={117.652}
        x2={846.077}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_621_)" }}
        d="M840.85 103.65h10.45v2.51h-10.45z"
      />
      <linearGradient
        id="SVGID_622_"
        gradientUnits="userSpaceOnUse"
        x1={809.509}
        y1={117.651}
        x2={809.509}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_622_)" }}
        d="M805.67 99.55h7.69v4.1h-7.69z"
      />
      <linearGradient
        id="SVGID_623_"
        gradientUnits="userSpaceOnUse"
        x1={845.018}
        y1={117.652}
        x2={845.018}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_623_)" }}
        d="M840.85 102.31h8.33v1.38h-8.33z"
      />
      <linearGradient
        id="SVGID_624_"
        gradientUnits="userSpaceOnUse"
        x1={813.971}
        y1={117.651}
        x2={813.971}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_624_)" }}
        d="M808.47 110.17h11v1.38h-11z"
      />
      <linearGradient
        id="SVGID_625_"
        gradientUnits="userSpaceOnUse"
        x1={819.472}
        y1={117.652}
        x2={819.472}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_625_)" }}
        d="M813.35 111.51h12.24v5.36h-12.24z"
      />
      <linearGradient
        id="SVGID_626_"
        gradientUnits="userSpaceOnUse"
        x1={802.949}
        y1={117.652}
        x2={802.949}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_626_)" }}
        d="M798.88 107.32h8.14v9.56h-8.14z"
      />
      <linearGradient
        id="SVGID_627_"
        gradientUnits="userSpaceOnUse"
        x1={809.931}
        y1={117.652}
        x2={809.931}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_627_)" }}
        d="M803.81 103.65h12.24v13.22h-12.24z"
      />
      <linearGradient
        id="SVGID_628_"
        gradientUnits="userSpaceOnUse"
        x1={933.448}
        y1={117.652}
        x2={933.448}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_628_)" }}
        d="M933.61 92.27h-.33l.17-10.03z"
      />
      <linearGradient
        id="SVGID_629_"
        gradientUnits="userSpaceOnUse"
        x1={933.929}
        y1={117.652}
        x2={933.929}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_629_)" }}
        d="M934.02 92.27h-.18l.09-5.35z"
      />
      <linearGradient
        id="SVGID_630_"
        gradientUnits="userSpaceOnUse"
        x1={922.644}
        y1={117.651}
        x2={922.644}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_630_)" }}
        d="M922.81 100.51h-.33l.16-10.03z"
      />
      <linearGradient
        id="SVGID_631_"
        gradientUnits="userSpaceOnUse"
        x1={924.837}
        y1={117.651}
        x2={924.837}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_631_)" }}
        d="M924.93 100.51h-.18l.09-5.35z"
      />
      <linearGradient
        id="SVGID_632_"
        gradientUnits="userSpaceOnUse"
        x1={903.33}
        y1={117.652}
        x2={903.33}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_632_)" }}
        d="M903.42 87.52h-.18l.09-5.35z"
      />
      <linearGradient
        id="SVGID_633_"
        gradientUnits="userSpaceOnUse"
        x1={916.534}
        y1={117.652}
        x2={916.534}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_633_)" }}
        d="M911.25 84.74h10.57v31.18h-10.57z"
      />
      <linearGradient
        id="SVGID_634_"
        gradientUnits="userSpaceOnUse"
        x1={934.906}
        y1={117.652}
        x2={934.906}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_634_)" }}
        d="M927.69 98.72h14.43v18.15h-14.43z"
      />
      <linearGradient
        id="SVGID_635_"
        gradientUnits="userSpaceOnUse"
        x1={942.074}
        y1={117.652}
        x2={942.074}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_635_)" }}
        d="M939.64 102.11h4.86v14.76h-4.86z"
      />
      <linearGradient
        id="SVGID_636_"
        gradientUnits="userSpaceOnUse"
        x1={926.474}
        y1={117.652}
        x2={926.474}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_636_)" }}
        d="M925.26 104.77h2.43v12.1h-2.43z"
      />
      <linearGradient
        id="SVGID_637_"
        gradientUnits="userSpaceOnUse"
        x1={920.09}
        y1={117.652}
        x2={920.09}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_637_)" }}
        d="M914.92 100.13h10.34v16.75h-10.34z"
      />
      <linearGradient
        id="SVGID_638_"
        gradientUnits="userSpaceOnUse"
        x1={936.849}
        y1={117.652}
        x2={936.849}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_638_)" }}
        d="M931.62 94.47h10.45v4.25h-10.45z"
      />
      <linearGradient
        id="SVGID_639_"
        gradientUnits="userSpaceOnUse"
        x1={900.282}
        y1={117.652}
        x2={900.282}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_639_)" }}
        d="M896.44 87.52h7.69v6.95h-7.69z"
      />
      <linearGradient
        id="SVGID_640_"
        gradientUnits="userSpaceOnUse"
        x1={935.79}
        y1={117.652}
        x2={935.79}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_640_)" }}
        d="M931.62 92.2h8.33v2.34h-8.33z"
      />
      <linearGradient
        id="SVGID_641_"
        gradientUnits="userSpaceOnUse"
        x1={904.743}
        y1={117.652}
        x2={904.743}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_641_)" }}
        d="M899.24 105.51h11v2.34h-11z"
      />
      <linearGradient
        id="SVGID_642_"
        gradientUnits="userSpaceOnUse"
        x1={910.244}
        y1={117.652}
        x2={910.244}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_642_)" }}
        d="M904.13 107.79h12.24v9.08h-12.24z"
      />
      <linearGradient
        id="SVGID_643_"
        gradientUnits="userSpaceOnUse"
        x1={893.722}
        y1={117.652}
        x2={893.722}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_643_)" }}
        d="M889.65 100.68h8.14v16.19h-8.14z"
      />
      <linearGradient
        id="SVGID_644_"
        gradientUnits="userSpaceOnUse"
        x1={900.703}
        y1={117.652}
        x2={900.703}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_644_)" }}
        d="M894.58 94.47h12.24v22.4h-12.24z"
      />
      <linearGradient
        id="SVGID_645_"
        gradientUnits="userSpaceOnUse"
        x1={810.236}
        y1={117.652}
        x2={810.236}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_645_)" }}
        d="M810.4 92.27h-.33l.17-10.03z"
      />
      <linearGradient
        id="SVGID_646_"
        gradientUnits="userSpaceOnUse"
        x1={810.717}
        y1={117.652}
        x2={810.717}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_646_)" }}
        d="M810.81 92.27h-.18l.09-5.35z"
      />
      <linearGradient
        id="SVGID_647_"
        gradientUnits="userSpaceOnUse"
        x1={799.432}
        y1={117.651}
        x2={799.432}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_647_)" }}
        d="M799.6 100.51h-.33l.16-10.03z"
      />
      <linearGradient
        id="SVGID_648_"
        gradientUnits="userSpaceOnUse"
        x1={801.625}
        y1={117.651}
        x2={801.625}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_648_)" }}
        d="M801.71 100.51h-.17l.09-5.35z"
      />
      <linearGradient
        id="SVGID_649_"
        gradientUnits="userSpaceOnUse"
        x1={793.322}
        y1={117.652}
        x2={793.322}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_649_)" }}
        d="M788.04 84.74h10.57v31.18h-10.57z"
      />
      <linearGradient
        id="SVGID_650_"
        gradientUnits="userSpaceOnUse"
        x1={811.694}
        y1={117.652}
        x2={811.694}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_650_)" }}
        d="M804.48 98.72h14.43v18.15h-14.43z"
      />
      <linearGradient
        id="SVGID_651_"
        gradientUnits="userSpaceOnUse"
        x1={818.862}
        y1={117.652}
        x2={818.862}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_651_)" }}
        d="M816.43 102.11h4.86v14.76h-4.86z"
      />
      <linearGradient
        id="SVGID_652_"
        gradientUnits="userSpaceOnUse"
        x1={803.262}
        y1={117.652}
        x2={803.262}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_652_)" }}
        d="M802.05 104.77h2.43v12.1h-2.43z"
      />
      <linearGradient
        id="SVGID_653_"
        gradientUnits="userSpaceOnUse"
        x1={796.878}
        y1={117.652}
        x2={796.878}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_653_)" }}
        d="M791.71 100.13h10.34v16.75h-10.34z"
      />
      <linearGradient
        id="SVGID_654_"
        gradientUnits="userSpaceOnUse"
        x1={813.637}
        y1={117.652}
        x2={813.637}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_654_)" }}
        d="M808.41 94.47h10.45v4.25h-10.45z"
      />
      <linearGradient
        id="SVGID_655_"
        gradientUnits="userSpaceOnUse"
        x1={812.579}
        y1={117.652}
        x2={812.579}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_655_)" }}
        d="M808.41 92.2h8.33v2.34h-8.33z"
      />
      <linearGradient
        id="SVGID_656_"
        gradientUnits="userSpaceOnUse"
        x1={777.448}
        y1={117.652}
        x2={777.448}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_656_)" }}
        d="M771.95 101.66h11v6.19h-11z"
      />
      <linearGradient
        id="SVGID_657_"
        gradientUnits="userSpaceOnUse"
        x1={782.408}
        y1={117.652}
        x2={782.408}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_657_)" }}
        d="M771.66 107.79h21.49v9.08h-21.49z"
      />
      <linearGradient
        id="SVGID_658_"
        gradientUnits="userSpaceOnUse"
        x1={255.58}
        y1={117.651}
        x2={255.58}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_658_)" }}
        d="M255.41 102.35h.34l-.17-5.92z"
      />
      <linearGradient
        id="SVGID_659_"
        gradientUnits="userSpaceOnUse"
        x1={255.099}
        y1={117.651}
        x2={255.099}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_659_)" }}
        d="M255.01 102.35h.18l-.09-3.15z"
      />
      <linearGradient
        id="SVGID_660_"
        gradientUnits="userSpaceOnUse"
        x1={266.384}
        y1={117.651}
        x2={266.384}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_660_)" }}
        d="M266.22 107.22h.33l-.17-5.92z"
      />
      <linearGradient
        id="SVGID_661_"
        gradientUnits="userSpaceOnUse"
        x1={264.19}
        y1={117.651}
        x2={264.19}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_661_)" }}
        d="M264.1 107.22h.18l-.09-3.16z"
      />
      <linearGradient
        id="SVGID_662_"
        gradientUnits="userSpaceOnUse"
        x1={285.698}
        y1={117.652}
        x2={285.698}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_662_)" }}
        d="M285.61 99.55h.18l-.09-3.16z"
      />
      <linearGradient
        id="SVGID_663_"
        gradientUnits="userSpaceOnUse"
        x1={272.493}
        y1={117.652}
        x2={272.493}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_663_)" }}
        d="M267.21 97.9h10.57v18.41h-10.57z"
      />
      <linearGradient
        id="SVGID_664_"
        gradientUnits="userSpaceOnUse"
        x1={254.122}
        y1={117.652}
        x2={254.122}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_664_)" }}
        d="M246.9 106.16h14.43v10.72H246.9z"
      />
      <linearGradient
        id="SVGID_665_"
        gradientUnits="userSpaceOnUse"
        x1={246.953}
        y1={117.652}
        x2={246.953}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_665_)" }}
        d="M244.52 108.16h4.86v8.71h-4.86z"
      />
      <linearGradient
        id="SVGID_666_"
        gradientUnits="userSpaceOnUse"
        x1={262.554}
        y1={117.652}
        x2={262.554}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_666_)" }}
        d="M261.34 109.73h2.43v7.14h-2.43z"
      />
      <linearGradient
        id="SVGID_667_"
        gradientUnits="userSpaceOnUse"
        x1={268.938}
        y1={117.652}
        x2={268.938}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_667_)" }}
        d="M263.77 106.99h10.34v9.88h-10.34z"
      />
      <linearGradient
        id="SVGID_668_"
        gradientUnits="userSpaceOnUse"
        x1={252.179}
        y1={117.652}
        x2={252.179}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_668_)" }}
        d="M246.95 103.65h10.45v2.51h-10.45z"
      />
      <linearGradient
        id="SVGID_669_"
        gradientUnits="userSpaceOnUse"
        x1={288.746}
        y1={117.651}
        x2={288.746}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_669_)" }}
        d="M284.9 99.55h7.69v4.1h-7.69z"
      />
      <linearGradient
        id="SVGID_670_"
        gradientUnits="userSpaceOnUse"
        x1={253.237}
        y1={117.652}
        x2={253.237}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_670_)" }}
        d="M249.07 102.31h8.33v1.38h-8.33z"
      />
      <linearGradient
        id="SVGID_671_"
        gradientUnits="userSpaceOnUse"
        x1={284.285}
        y1={117.651}
        x2={284.285}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_671_)" }}
        d="M278.78 110.17h11v1.38h-11z"
      />
      <linearGradient
        id="SVGID_672_"
        gradientUnits="userSpaceOnUse"
        x1={278.784}
        y1={117.652}
        x2={278.784}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_672_)" }}
        d="M272.67 111.51h12.24v5.36h-12.24z"
      />
      <linearGradient
        id="SVGID_673_"
        gradientUnits="userSpaceOnUse"
        x1={295.306}
        y1={117.652}
        x2={295.306}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_673_)" }}
        d="M291.24 107.32h8.14v9.56h-8.14z"
      />
      <linearGradient
        id="SVGID_674_"
        gradientUnits="userSpaceOnUse"
        x1={288.325}
        y1={117.652}
        x2={288.325}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_674_)" }}
        d="M282.21 103.65h12.24v13.22h-12.24z"
      />
      <linearGradient
        id="SVGID_675_"
        gradientUnits="userSpaceOnUse"
        x1={355.221}
        y1={117.651}
        x2={355.221}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_675_)" }}
        d="M355.05 102.35h.34l-.17-5.92z"
      />
      <linearGradient
        id="SVGID_676_"
        gradientUnits="userSpaceOnUse"
        x1={354.74}
        y1={117.651}
        x2={354.74}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_676_)" }}
        d="M354.65 102.35h.18l-.09-3.15z"
      />
      <linearGradient
        id="SVGID_677_"
        gradientUnits="userSpaceOnUse"
        x1={366.024}
        y1={117.651}
        x2={366.024}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_677_)" }}
        d="M365.86 107.22h.33l-.17-5.92z"
      />
      <linearGradient
        id="SVGID_678_"
        gradientUnits="userSpaceOnUse"
        x1={363.831}
        y1={117.651}
        x2={363.831}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_678_)" }}
        d="M363.74 107.22h.18l-.09-3.16z"
      />
      <linearGradient
        id="SVGID_679_"
        gradientUnits="userSpaceOnUse"
        x1={385.339}
        y1={117.652}
        x2={385.339}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_679_)" }}
        d="M385.25 99.55h.18l-.09-3.16z"
      />
      <linearGradient
        id="SVGID_680_"
        gradientUnits="userSpaceOnUse"
        x1={372.134}
        y1={117.652}
        x2={372.134}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_680_)" }}
        d="M366.85 97.9h10.57v18.41h-10.57z"
      />
      <linearGradient
        id="SVGID_681_"
        gradientUnits="userSpaceOnUse"
        x1={353.762}
        y1={117.652}
        x2={353.762}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_681_)" }}
        d="M346.55 106.16h14.43v10.72h-14.43z"
      />
      <linearGradient
        id="SVGID_682_"
        gradientUnits="userSpaceOnUse"
        x1={346.594}
        y1={117.652}
        x2={346.594}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_682_)" }}
        d="M344.16 108.16h4.86v8.71h-4.86z"
      />
      <linearGradient
        id="SVGID_683_"
        gradientUnits="userSpaceOnUse"
        x1={362.194}
        y1={117.652}
        x2={362.194}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_683_)" }}
        d="M360.98 109.73h2.43v7.14h-2.43z"
      />
      <linearGradient
        id="SVGID_684_"
        gradientUnits="userSpaceOnUse"
        x1={368.579}
        y1={117.652}
        x2={368.579}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_684_)" }}
        d="M363.41 106.99h10.34v9.88h-10.34z"
      />
      <linearGradient
        id="SVGID_685_"
        gradientUnits="userSpaceOnUse"
        x1={351.819}
        y1={117.652}
        x2={351.819}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_685_)" }}
        d="M346.59 103.65h10.45v2.51h-10.45z"
      />
      <linearGradient
        id="SVGID_686_"
        gradientUnits="userSpaceOnUse"
        x1={388.387}
        y1={117.651}
        x2={388.387}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_686_)" }}
        d="M384.54 99.55h7.69v4.1h-7.69z"
      />
      <linearGradient
        id="SVGID_687_"
        gradientUnits="userSpaceOnUse"
        x1={352.878}
        y1={117.652}
        x2={352.878}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_687_)" }}
        d="M348.71 102.31h8.33v1.38h-8.33z"
      />
      <linearGradient
        id="SVGID_688_"
        gradientUnits="userSpaceOnUse"
        x1={383.925}
        y1={117.651}
        x2={383.925}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_688_)" }}
        d="M378.42 110.17h11v1.38h-11z"
      />
      <linearGradient
        id="SVGID_689_"
        gradientUnits="userSpaceOnUse"
        x1={378.424}
        y1={117.652}
        x2={378.424}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_689_)" }}
        d="M372.31 111.51h12.24v5.36h-12.24z"
      />
      <linearGradient
        id="SVGID_690_"
        gradientUnits="userSpaceOnUse"
        x1={394.947}
        y1={117.652}
        x2={394.947}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_690_)" }}
        d="M390.88 107.32h8.14v9.56h-8.14z"
      />
      <linearGradient
        id="SVGID_691_"
        gradientUnits="userSpaceOnUse"
        x1={387.965}
        y1={117.652}
        x2={387.965}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_691_)" }}
        d="M381.85 103.65h12.24v13.22h-12.24z"
      />
      <linearGradient
        id="SVGID_692_"
        gradientUnits="userSpaceOnUse"
        x1={392.984}
        y1={117.651}
        x2={392.984}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_692_)" }}
        d="M392.82 102.35h.33l-.17-5.92z"
      />
      <linearGradient
        id="SVGID_693_"
        gradientUnits="userSpaceOnUse"
        x1={392.503}
        y1={117.651}
        x2={392.503}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_693_)" }}
        d="M392.41 102.35h.18l-.09-3.15z"
      />
      <linearGradient
        id="SVGID_694_"
        gradientUnits="userSpaceOnUse"
        x1={403.787}
        y1={117.651}
        x2={403.787}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_694_)" }}
        d="M403.62 107.22h.33l-.16-5.92z"
      />
      <linearGradient
        id="SVGID_695_"
        gradientUnits="userSpaceOnUse"
        x1={401.594}
        y1={117.651}
        x2={401.594}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_695_)" }}
        d="M401.51 107.22h.17l-.09-3.16z"
      />
      <linearGradient
        id="SVGID_696_"
        gradientUnits="userSpaceOnUse"
        x1={423.102}
        y1={117.652}
        x2={423.102}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_696_)" }}
        d="M423.01 99.55h.18l-.09-3.16z"
      />
      <linearGradient
        id="SVGID_697_"
        gradientUnits="userSpaceOnUse"
        x1={409.897}
        y1={117.652}
        x2={409.897}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_697_)" }}
        d="M404.61 97.9h10.57v18.41h-10.57z"
      />
      <linearGradient
        id="SVGID_698_"
        gradientUnits="userSpaceOnUse"
        x1={391.526}
        y1={117.652}
        x2={391.526}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_698_)" }}
        d="M384.31 106.16h14.43v10.72h-14.43z"
      />
      <linearGradient
        id="SVGID_699_"
        gradientUnits="userSpaceOnUse"
        x1={384.357}
        y1={117.652}
        x2={384.357}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_699_)" }}
        d="M381.93 108.16h4.86v8.71h-4.86z"
      />
      <linearGradient
        id="SVGID_700_"
        gradientUnits="userSpaceOnUse"
        x1={399.957}
        y1={117.652}
        x2={399.957}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_700_)" }}
        d="M398.74 109.73h2.43v7.14h-2.43z"
      />
      <linearGradient
        id="SVGID_701_"
        gradientUnits="userSpaceOnUse"
        x1={406.342}
        y1={117.652}
        x2={406.342}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_701_)" }}
        d="M401.17 106.99h10.34v9.88h-10.34z"
      />
      <linearGradient
        id="SVGID_702_"
        gradientUnits="userSpaceOnUse"
        x1={389.582}
        y1={117.652}
        x2={389.582}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_702_)" }}
        d="M384.36 103.65h10.45v2.51h-10.45z"
      />
      <linearGradient
        id="SVGID_703_"
        gradientUnits="userSpaceOnUse"
        x1={426.15}
        y1={117.651}
        x2={426.15}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_703_)" }}
        d="M422.31 99.55H430v4.1h-7.69z"
      />
      <linearGradient
        id="SVGID_704_"
        gradientUnits="userSpaceOnUse"
        x1={390.641}
        y1={117.652}
        x2={390.641}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_704_)" }}
        d="M386.47 102.31h8.33v1.38h-8.33z"
      />
      <linearGradient
        id="SVGID_705_"
        gradientUnits="userSpaceOnUse"
        x1={421.688}
        y1={117.651}
        x2={421.688}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_705_)" }}
        d="M416.19 110.17h11v1.38h-11z"
      />
      <linearGradient
        id="SVGID_706_"
        gradientUnits="userSpaceOnUse"
        x1={416.188}
        y1={117.652}
        x2={416.188}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_706_)" }}
        d="M410.07 111.51h12.24v5.36h-12.24z"
      />
      <linearGradient
        id="SVGID_707_"
        gradientUnits="userSpaceOnUse"
        x1={432.71}
        y1={117.652}
        x2={432.71}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_707_)" }}
        d="M428.64 107.32h8.14v9.56h-8.14z"
      />
      <linearGradient
        id="SVGID_708_"
        gradientUnits="userSpaceOnUse"
        x1={425.728}
        y1={117.652}
        x2={425.728}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_708_)" }}
        d="M419.61 103.65h12.24v13.22h-12.24z"
      />
      <linearGradient
        id="SVGID_709_"
        gradientUnits="userSpaceOnUse"
        x1={302.211}
        y1={117.652}
        x2={302.211}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_709_)" }}
        d="M302.04 92.27h.34l-.17-10.03z"
      />
      <linearGradient
        id="SVGID_710_"
        gradientUnits="userSpaceOnUse"
        x1={301.73}
        y1={117.652}
        x2={301.73}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_710_)" }}
        d="M301.64 92.27h.18l-.09-5.35z"
      />
      <linearGradient
        id="SVGID_711_"
        gradientUnits="userSpaceOnUse"
        x1={313.015}
        y1={117.651}
        x2={313.015}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_711_)" }}
        d="M312.85 100.51h.33l-.17-10.03z"
      />
      <linearGradient
        id="SVGID_712_"
        gradientUnits="userSpaceOnUse"
        x1={310.822}
        y1={117.651}
        x2={310.822}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_712_)" }}
        d="M310.73 100.51h.18l-.09-5.35z"
      />
      <linearGradient
        id="SVGID_713_"
        gradientUnits="userSpaceOnUse"
        x1={332.329}
        y1={117.652}
        x2={332.329}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_713_)" }}
        d="M332.24 87.52h.18l-.09-5.35z"
      />
      <linearGradient
        id="SVGID_714_"
        gradientUnits="userSpaceOnUse"
        x1={319.125}
        y1={117.652}
        x2={319.125}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_714_)" }}
        d="M313.84 84.74h10.57v31.18h-10.57z"
      />
      <linearGradient
        id="SVGID_715_"
        gradientUnits="userSpaceOnUse"
        x1={300.753}
        y1={117.652}
        x2={300.753}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_715_)" }}
        d="M293.54 98.72h14.43v18.15h-14.43z"
      />
      <linearGradient
        id="SVGID_716_"
        gradientUnits="userSpaceOnUse"
        x1={293.585}
        y1={117.652}
        x2={293.585}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_716_)" }}
        d="M291.15 102.11h4.86v14.76h-4.86z"
      />
      <linearGradient
        id="SVGID_717_"
        gradientUnits="userSpaceOnUse"
        x1={309.185}
        y1={117.652}
        x2={309.185}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_717_)" }}
        d="M307.97 104.77h2.43v12.1h-2.43z"
      />
      <linearGradient
        id="SVGID_718_"
        gradientUnits="userSpaceOnUse"
        x1={315.57}
        y1={117.652}
        x2={315.57}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_718_)" }}
        d="M310.4 100.13h10.34v16.75H310.4z"
      />
      <linearGradient
        id="SVGID_719_"
        gradientUnits="userSpaceOnUse"
        x1={298.81}
        y1={117.652}
        x2={298.81}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_719_)" }}
        d="M293.58 94.47h10.45v4.25h-10.45z"
      />
      <linearGradient
        id="SVGID_720_"
        gradientUnits="userSpaceOnUse"
        x1={335.378}
        y1={117.652}
        x2={335.378}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_720_)" }}
        d="M331.53 87.52h7.69v6.95h-7.69z"
      />
      <linearGradient
        id="SVGID_721_"
        gradientUnits="userSpaceOnUse"
        x1={299.869}
        y1={117.652}
        x2={299.869}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_721_)" }}
        d="M295.7 92.2h8.33v2.34h-8.33z"
      />
      <linearGradient
        id="SVGID_722_"
        gradientUnits="userSpaceOnUse"
        x1={330.916}
        y1={117.652}
        x2={330.916}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_722_)" }}
        d="M325.42 105.51h11v2.34h-11z"
      />
      <linearGradient
        id="SVGID_723_"
        gradientUnits="userSpaceOnUse"
        x1={325.415}
        y1={117.652}
        x2={325.415}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_723_)" }}
        d="M319.3 107.79h12.24v9.08H319.3z"
      />
      <linearGradient
        id="SVGID_724_"
        gradientUnits="userSpaceOnUse"
        x1={341.937}
        y1={117.652}
        x2={341.937}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_724_)" }}
        d="M337.87 100.68h8.14v16.19h-8.14z"
      />
      <linearGradient
        id="SVGID_725_"
        gradientUnits="userSpaceOnUse"
        x1={334.956}
        y1={117.652}
        x2={334.956}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_725_)" }}
        d="M328.84 94.47h12.24v22.4h-12.24z"
      />
      <linearGradient
        id="SVGID_726_"
        gradientUnits="userSpaceOnUse"
        x1={425.423}
        y1={117.652}
        x2={425.423}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_726_)" }}
        d="M425.26 92.27h.33l-.17-10.03z"
      />
      <linearGradient
        id="SVGID_727_"
        gradientUnits="userSpaceOnUse"
        x1={424.942}
        y1={117.652}
        x2={424.942}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_727_)" }}
        d="M424.85 92.27h.18l-.09-5.35z"
      />
      <linearGradient
        id="SVGID_728_"
        gradientUnits="userSpaceOnUse"
        x1={436.227}
        y1={117.651}
        x2={436.227}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_728_)" }}
        d="M436.06 100.51h.33l-.16-10.03z"
      />
      <linearGradient
        id="SVGID_729_"
        gradientUnits="userSpaceOnUse"
        x1={434.034}
        y1={117.651}
        x2={434.034}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_729_)" }}
        d="M433.94 100.51h.18l-.09-5.35z"
      />
      <linearGradient
        id="SVGID_730_"
        gradientUnits="userSpaceOnUse"
        x1={442.337}
        y1={117.652}
        x2={442.337}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_730_)" }}
        d="M437.05 84.74h10.57v31.18h-10.57z"
      />
      <linearGradient
        id="SVGID_731_"
        gradientUnits="userSpaceOnUse"
        x1={423.965}
        y1={117.652}
        x2={423.965}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_731_)" }}
        d="M416.75 98.72h14.43v18.15h-14.43z"
      />
      <linearGradient
        id="SVGID_732_"
        gradientUnits="userSpaceOnUse"
        x1={416.796}
        y1={117.652}
        x2={416.796}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_732_)" }}
        d="M414.37 102.11h4.86v14.76h-4.86z"
      />
      <linearGradient
        id="SVGID_733_"
        gradientUnits="userSpaceOnUse"
        x1={432.397}
        y1={117.652}
        x2={432.397}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_733_)" }}
        d="M431.18 104.77h2.43v12.1h-2.43z"
      />
      <linearGradient
        id="SVGID_734_"
        gradientUnits="userSpaceOnUse"
        x1={438.781}
        y1={117.652}
        x2={438.781}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_734_)" }}
        d="M433.61 100.13h10.34v16.75h-10.34z"
      />
      <linearGradient
        id="SVGID_735_"
        gradientUnits="userSpaceOnUse"
        x1={422.022}
        y1={117.652}
        x2={422.022}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_735_)" }}
        d="M416.8 94.47h10.45v4.25H416.8z"
      />
      <linearGradient
        id="SVGID_736_"
        gradientUnits="userSpaceOnUse"
        x1={423.08}
        y1={117.652}
        x2={423.08}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_736_)" }}
        d="M418.91 92.2h8.33v2.34h-8.33z"
      />
      <linearGradient
        id="SVGID_737_"
        gradientUnits="userSpaceOnUse"
        x1={458.211}
        y1={117.652}
        x2={458.211}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_737_)" }}
        d="M452.71 101.66h11v6.19h-11z"
      />
      <linearGradient
        id="SVGID_738_"
        gradientUnits="userSpaceOnUse"
        x1={453.252}
        y1={117.652}
        x2={453.252}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_738_)" }}
        d="M442.51 107.79H464v9.08h-21.49z"
      />
      <linearGradient
        id="SVGID_739_"
        gradientUnits="userSpaceOnUse"
        x1={255.58}
        y1={117.651}
        x2={255.58}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_739_)" }}
        d="M255.41 102.35h.34l-.17-5.92z"
      />
      <linearGradient
        id="SVGID_740_"
        gradientUnits="userSpaceOnUse"
        x1={255.099}
        y1={117.651}
        x2={255.099}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_740_)" }}
        d="M255.01 102.35h.18l-.09-3.15z"
      />
      <linearGradient
        id="SVGID_741_"
        gradientUnits="userSpaceOnUse"
        x1={266.384}
        y1={117.651}
        x2={266.384}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_741_)" }}
        d="M266.22 107.22h.33l-.17-5.92z"
      />
      <linearGradient
        id="SVGID_742_"
        gradientUnits="userSpaceOnUse"
        x1={264.19}
        y1={117.651}
        x2={264.19}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_742_)" }}
        d="M264.1 107.22h.18l-.09-3.16z"
      />
      <linearGradient
        id="SVGID_743_"
        gradientUnits="userSpaceOnUse"
        x1={285.698}
        y1={117.652}
        x2={285.698}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_743_)" }}
        d="M285.61 99.55h.18l-.09-3.16z"
      />
      <linearGradient
        id="SVGID_744_"
        gradientUnits="userSpaceOnUse"
        x1={272.493}
        y1={117.652}
        x2={272.493}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_744_)" }}
        d="M267.21 97.9h10.57v18.41h-10.57z"
      />
      <linearGradient
        id="SVGID_745_"
        gradientUnits="userSpaceOnUse"
        x1={254.122}
        y1={117.652}
        x2={254.122}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_745_)" }}
        d="M246.9 106.16h14.43v10.72H246.9z"
      />
      <linearGradient
        id="SVGID_746_"
        gradientUnits="userSpaceOnUse"
        x1={246.953}
        y1={117.652}
        x2={246.953}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_746_)" }}
        d="M244.52 108.16h4.86v8.71h-4.86z"
      />
      <linearGradient
        id="SVGID_747_"
        gradientUnits="userSpaceOnUse"
        x1={262.554}
        y1={117.652}
        x2={262.554}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_747_)" }}
        d="M261.34 109.73h2.43v7.14h-2.43z"
      />
      <linearGradient
        id="SVGID_748_"
        gradientUnits="userSpaceOnUse"
        x1={268.938}
        y1={117.652}
        x2={268.938}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_748_)" }}
        d="M263.77 106.99h10.34v9.88h-10.34z"
      />
      <linearGradient
        id="SVGID_749_"
        gradientUnits="userSpaceOnUse"
        x1={252.179}
        y1={117.652}
        x2={252.179}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_749_)" }}
        d="M246.95 103.65h10.45v2.51h-10.45z"
      />
      <linearGradient
        id="SVGID_750_"
        gradientUnits="userSpaceOnUse"
        x1={288.746}
        y1={117.651}
        x2={288.746}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_750_)" }}
        d="M284.9 99.55h7.69v4.1h-7.69z"
      />
      <linearGradient
        id="SVGID_751_"
        gradientUnits="userSpaceOnUse"
        x1={253.237}
        y1={117.652}
        x2={253.237}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_751_)" }}
        d="M249.07 102.31h8.33v1.38h-8.33z"
      />
      <linearGradient
        id="SVGID_752_"
        gradientUnits="userSpaceOnUse"
        x1={284.285}
        y1={117.651}
        x2={284.285}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_752_)" }}
        d="M278.78 110.17h11v1.38h-11z"
      />
      <linearGradient
        id="SVGID_753_"
        gradientUnits="userSpaceOnUse"
        x1={278.784}
        y1={117.652}
        x2={278.784}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_753_)" }}
        d="M272.67 111.51h12.24v5.36h-12.24z"
      />
      <linearGradient
        id="SVGID_754_"
        gradientUnits="userSpaceOnUse"
        x1={295.306}
        y1={117.652}
        x2={295.306}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_754_)" }}
        d="M291.24 107.32h8.14v9.56h-8.14z"
      />
      <linearGradient
        id="SVGID_755_"
        gradientUnits="userSpaceOnUse"
        x1={288.325}
        y1={117.652}
        x2={288.325}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_755_)" }}
        d="M282.21 103.65h12.24v13.22h-12.24z"
      />
      <linearGradient
        id="SVGID_756_"
        gradientUnits="userSpaceOnUse"
        x1={355.221}
        y1={117.651}
        x2={355.221}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_756_)" }}
        d="M355.05 102.35h.34l-.17-5.92z"
      />
      <linearGradient
        id="SVGID_757_"
        gradientUnits="userSpaceOnUse"
        x1={354.74}
        y1={117.651}
        x2={354.74}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_757_)" }}
        d="M354.65 102.35h.18l-.09-3.15z"
      />
      <linearGradient
        id="SVGID_758_"
        gradientUnits="userSpaceOnUse"
        x1={366.024}
        y1={117.651}
        x2={366.024}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_758_)" }}
        d="M365.86 107.22h.33l-.17-5.92z"
      />
      <linearGradient
        id="SVGID_759_"
        gradientUnits="userSpaceOnUse"
        x1={363.831}
        y1={117.651}
        x2={363.831}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_759_)" }}
        d="M363.74 107.22h.18l-.09-3.16z"
      />
      <linearGradient
        id="SVGID_760_"
        gradientUnits="userSpaceOnUse"
        x1={385.339}
        y1={117.652}
        x2={385.339}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_760_)" }}
        d="M385.25 99.55h.18l-.09-3.16z"
      />
      <linearGradient
        id="SVGID_761_"
        gradientUnits="userSpaceOnUse"
        x1={372.134}
        y1={117.652}
        x2={372.134}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_761_)" }}
        d="M366.85 97.9h10.57v18.41h-10.57z"
      />
      <linearGradient
        id="SVGID_762_"
        gradientUnits="userSpaceOnUse"
        x1={353.762}
        y1={117.652}
        x2={353.762}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_762_)" }}
        d="M346.55 106.16h14.43v10.72h-14.43z"
      />
      <linearGradient
        id="SVGID_763_"
        gradientUnits="userSpaceOnUse"
        x1={346.594}
        y1={117.652}
        x2={346.594}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_763_)" }}
        d="M344.16 108.16h4.86v8.71h-4.86z"
      />
      <linearGradient
        id="SVGID_764_"
        gradientUnits="userSpaceOnUse"
        x1={362.194}
        y1={117.652}
        x2={362.194}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_764_)" }}
        d="M360.98 109.73h2.43v7.14h-2.43z"
      />
      <linearGradient
        id="SVGID_765_"
        gradientUnits="userSpaceOnUse"
        x1={368.579}
        y1={117.652}
        x2={368.579}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_765_)" }}
        d="M363.41 106.99h10.34v9.88h-10.34z"
      />
      <linearGradient
        id="SVGID_766_"
        gradientUnits="userSpaceOnUse"
        x1={351.819}
        y1={117.652}
        x2={351.819}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_766_)" }}
        d="M346.59 103.65h10.45v2.51h-10.45z"
      />
      <linearGradient
        id="SVGID_767_"
        gradientUnits="userSpaceOnUse"
        x1={388.387}
        y1={117.651}
        x2={388.387}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_767_)" }}
        d="M384.54 99.55h7.69v4.1h-7.69z"
      />
      <linearGradient
        id="SVGID_768_"
        gradientUnits="userSpaceOnUse"
        x1={352.878}
        y1={117.652}
        x2={352.878}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_768_)" }}
        d="M348.71 102.31h8.33v1.38h-8.33z"
      />
      <linearGradient
        id="SVGID_769_"
        gradientUnits="userSpaceOnUse"
        x1={383.925}
        y1={117.651}
        x2={383.925}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_769_)" }}
        d="M378.42 110.17h11v1.38h-11z"
      />
      <linearGradient
        id="SVGID_770_"
        gradientUnits="userSpaceOnUse"
        x1={378.424}
        y1={117.652}
        x2={378.424}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_770_)" }}
        d="M372.31 111.51h12.24v5.36h-12.24z"
      />
      <linearGradient
        id="SVGID_771_"
        gradientUnits="userSpaceOnUse"
        x1={394.947}
        y1={117.652}
        x2={394.947}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_771_)" }}
        d="M390.88 107.32h8.14v9.56h-8.14z"
      />
      <linearGradient
        id="SVGID_772_"
        gradientUnits="userSpaceOnUse"
        x1={387.965}
        y1={117.652}
        x2={387.965}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_772_)" }}
        d="M381.85 103.65h12.24v13.22h-12.24z"
      />
      <linearGradient
        id="SVGID_773_"
        gradientUnits="userSpaceOnUse"
        x1={392.984}
        y1={117.651}
        x2={392.984}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_773_)" }}
        d="M392.82 102.35h.33l-.17-5.92z"
      />
      <linearGradient
        id="SVGID_774_"
        gradientUnits="userSpaceOnUse"
        x1={392.503}
        y1={117.651}
        x2={392.503}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_774_)" }}
        d="M392.41 102.35h.18l-.09-3.15z"
      />
      <linearGradient
        id="SVGID_775_"
        gradientUnits="userSpaceOnUse"
        x1={403.787}
        y1={117.651}
        x2={403.787}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_775_)" }}
        d="M403.62 107.22h.33l-.16-5.92z"
      />
      <linearGradient
        id="SVGID_776_"
        gradientUnits="userSpaceOnUse"
        x1={401.594}
        y1={117.651}
        x2={401.594}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_776_)" }}
        d="M401.51 107.22h.17l-.09-3.16z"
      />
      <linearGradient
        id="SVGID_777_"
        gradientUnits="userSpaceOnUse"
        x1={423.102}
        y1={117.652}
        x2={423.102}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_777_)" }}
        d="M423.01 99.55h.18l-.09-3.16z"
      />
      <linearGradient
        id="SVGID_778_"
        gradientUnits="userSpaceOnUse"
        x1={409.897}
        y1={117.652}
        x2={409.897}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_778_)" }}
        d="M404.61 97.9h10.57v18.41h-10.57z"
      />
      <linearGradient
        id="SVGID_779_"
        gradientUnits="userSpaceOnUse"
        x1={391.526}
        y1={117.652}
        x2={391.526}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_779_)" }}
        d="M384.31 106.16h14.43v10.72h-14.43z"
      />
      <linearGradient
        id="SVGID_780_"
        gradientUnits="userSpaceOnUse"
        x1={384.357}
        y1={117.652}
        x2={384.357}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_780_)" }}
        d="M381.93 108.16h4.86v8.71h-4.86z"
      />
      <linearGradient
        id="SVGID_781_"
        gradientUnits="userSpaceOnUse"
        x1={399.957}
        y1={117.652}
        x2={399.957}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_781_)" }}
        d="M398.74 109.73h2.43v7.14h-2.43z"
      />
      <linearGradient
        id="SVGID_782_"
        gradientUnits="userSpaceOnUse"
        x1={406.342}
        y1={117.652}
        x2={406.342}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_782_)" }}
        d="M401.17 106.99h10.34v9.88h-10.34z"
      />
      <linearGradient
        id="SVGID_783_"
        gradientUnits="userSpaceOnUse"
        x1={389.582}
        y1={117.652}
        x2={389.582}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_783_)" }}
        d="M384.36 103.65h10.45v2.51h-10.45z"
      />
      <linearGradient
        id="SVGID_784_"
        gradientUnits="userSpaceOnUse"
        x1={426.15}
        y1={117.651}
        x2={426.15}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_784_)" }}
        d="M422.31 99.55H430v4.1h-7.69z"
      />
      <linearGradient
        id="SVGID_785_"
        gradientUnits="userSpaceOnUse"
        x1={390.641}
        y1={117.652}
        x2={390.641}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_785_)" }}
        d="M386.47 102.31h8.33v1.38h-8.33z"
      />
      <linearGradient
        id="SVGID_786_"
        gradientUnits="userSpaceOnUse"
        x1={421.688}
        y1={117.651}
        x2={421.688}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_786_)" }}
        d="M416.19 110.17h11v1.38h-11z"
      />
      <linearGradient
        id="SVGID_787_"
        gradientUnits="userSpaceOnUse"
        x1={416.188}
        y1={117.652}
        x2={416.188}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_787_)" }}
        d="M410.07 111.51h12.24v5.36h-12.24z"
      />
      <linearGradient
        id="SVGID_788_"
        gradientUnits="userSpaceOnUse"
        x1={432.71}
        y1={117.652}
        x2={432.71}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_788_)" }}
        d="M428.64 107.32h8.14v9.56h-8.14z"
      />
      <linearGradient
        id="SVGID_789_"
        gradientUnits="userSpaceOnUse"
        x1={425.728}
        y1={117.652}
        x2={425.728}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_789_)" }}
        d="M419.61 103.65h12.24v13.22h-12.24z"
      />
      <linearGradient
        id="SVGID_790_"
        gradientUnits="userSpaceOnUse"
        x1={302.211}
        y1={117.652}
        x2={302.211}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_790_)" }}
        d="M302.04 92.27h.34l-.17-10.03z"
      />
      <linearGradient
        id="SVGID_791_"
        gradientUnits="userSpaceOnUse"
        x1={301.73}
        y1={117.652}
        x2={301.73}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_791_)" }}
        d="M301.64 92.27h.18l-.09-5.35z"
      />
      <linearGradient
        id="SVGID_792_"
        gradientUnits="userSpaceOnUse"
        x1={313.015}
        y1={117.651}
        x2={313.015}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_792_)" }}
        d="M312.85 100.51h.33l-.17-10.03z"
      />
      <linearGradient
        id="SVGID_793_"
        gradientUnits="userSpaceOnUse"
        x1={310.822}
        y1={117.651}
        x2={310.822}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_793_)" }}
        d="M310.73 100.51h.18l-.09-5.35z"
      />
      <linearGradient
        id="SVGID_794_"
        gradientUnits="userSpaceOnUse"
        x1={332.329}
        y1={117.652}
        x2={332.329}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_794_)" }}
        d="M332.24 87.52h.18l-.09-5.35z"
      />
      <linearGradient
        id="SVGID_795_"
        gradientUnits="userSpaceOnUse"
        x1={319.125}
        y1={117.652}
        x2={319.125}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_795_)" }}
        d="M313.84 84.74h10.57v31.18h-10.57z"
      />
      <linearGradient
        id="SVGID_796_"
        gradientUnits="userSpaceOnUse"
        x1={300.753}
        y1={117.652}
        x2={300.753}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_796_)" }}
        d="M293.54 98.72h14.43v18.15h-14.43z"
      />
      <linearGradient
        id="SVGID_797_"
        gradientUnits="userSpaceOnUse"
        x1={293.585}
        y1={117.652}
        x2={293.585}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_797_)" }}
        d="M291.15 102.11h4.86v14.76h-4.86z"
      />
      <linearGradient
        id="SVGID_798_"
        gradientUnits="userSpaceOnUse"
        x1={309.185}
        y1={117.652}
        x2={309.185}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_798_)" }}
        d="M307.97 104.77h2.43v12.1h-2.43z"
      />
      <linearGradient
        id="SVGID_799_"
        gradientUnits="userSpaceOnUse"
        x1={315.57}
        y1={117.652}
        x2={315.57}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_799_)" }}
        d="M310.4 100.13h10.34v16.75H310.4z"
      />
      <linearGradient
        id="SVGID_800_"
        gradientUnits="userSpaceOnUse"
        x1={298.81}
        y1={117.652}
        x2={298.81}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_800_)" }}
        d="M293.58 94.47h10.45v4.25h-10.45z"
      />
      <linearGradient
        id="SVGID_801_"
        gradientUnits="userSpaceOnUse"
        x1={335.378}
        y1={117.652}
        x2={335.378}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_801_)" }}
        d="M331.53 87.52h7.69v6.95h-7.69z"
      />
      <linearGradient
        id="SVGID_802_"
        gradientUnits="userSpaceOnUse"
        x1={299.869}
        y1={117.652}
        x2={299.869}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_802_)" }}
        d="M295.7 92.2h8.33v2.34h-8.33z"
      />
      <linearGradient
        id="SVGID_803_"
        gradientUnits="userSpaceOnUse"
        x1={330.916}
        y1={117.652}
        x2={330.916}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_803_)" }}
        d="M325.42 105.51h11v2.34h-11z"
      />
      <linearGradient
        id="SVGID_804_"
        gradientUnits="userSpaceOnUse"
        x1={325.415}
        y1={117.652}
        x2={325.415}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_804_)" }}
        d="M319.3 107.79h12.24v9.08H319.3z"
      />
      <linearGradient
        id="SVGID_805_"
        gradientUnits="userSpaceOnUse"
        x1={341.937}
        y1={117.652}
        x2={341.937}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_805_)" }}
        d="M337.87 100.68h8.14v16.19h-8.14z"
      />
      <linearGradient
        id="SVGID_806_"
        gradientUnits="userSpaceOnUse"
        x1={334.956}
        y1={117.652}
        x2={334.956}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_806_)" }}
        d="M328.84 94.47h12.24v22.4h-12.24z"
      />
      <linearGradient
        id="SVGID_807_"
        gradientUnits="userSpaceOnUse"
        x1={425.423}
        y1={117.652}
        x2={425.423}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_807_)" }}
        d="M425.26 92.27h.33l-.17-10.03z"
      />
      <linearGradient
        id="SVGID_808_"
        gradientUnits="userSpaceOnUse"
        x1={424.942}
        y1={117.652}
        x2={424.942}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_808_)" }}
        d="M424.85 92.27h.18l-.09-5.35z"
      />
      <linearGradient
        id="SVGID_809_"
        gradientUnits="userSpaceOnUse"
        x1={436.227}
        y1={117.651}
        x2={436.227}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_809_)" }}
        d="M436.06 100.51h.33l-.16-10.03z"
      />
      <linearGradient
        id="SVGID_810_"
        gradientUnits="userSpaceOnUse"
        x1={434.034}
        y1={117.651}
        x2={434.034}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_810_)" }}
        d="M433.94 100.51h.18l-.09-5.35z"
      />
      <linearGradient
        id="SVGID_811_"
        gradientUnits="userSpaceOnUse"
        x1={442.337}
        y1={117.652}
        x2={442.337}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_811_)" }}
        d="M437.05 84.74h10.57v31.18h-10.57z"
      />
      <linearGradient
        id="SVGID_812_"
        gradientUnits="userSpaceOnUse"
        x1={423.965}
        y1={117.652}
        x2={423.965}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_812_)" }}
        d="M416.75 98.72h14.43v18.15h-14.43z"
      />
      <linearGradient
        id="SVGID_813_"
        gradientUnits="userSpaceOnUse"
        x1={416.796}
        y1={117.652}
        x2={416.796}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_813_)" }}
        d="M414.37 102.11h4.86v14.76h-4.86z"
      />
      <linearGradient
        id="SVGID_814_"
        gradientUnits="userSpaceOnUse"
        x1={432.397}
        y1={117.652}
        x2={432.397}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_814_)" }}
        d="M431.18 104.77h2.43v12.1h-2.43z"
      />
      <linearGradient
        id="SVGID_815_"
        gradientUnits="userSpaceOnUse"
        x1={438.781}
        y1={117.652}
        x2={438.781}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_815_)" }}
        d="M433.61 100.13h10.34v16.75h-10.34z"
      />
      <linearGradient
        id="SVGID_816_"
        gradientUnits="userSpaceOnUse"
        x1={422.022}
        y1={117.652}
        x2={422.022}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_816_)" }}
        d="M416.8 94.47h10.45v4.25H416.8z"
      />
      <linearGradient
        id="SVGID_817_"
        gradientUnits="userSpaceOnUse"
        x1={423.08}
        y1={117.652}
        x2={423.08}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_817_)" }}
        d="M418.91 92.2h8.33v2.34h-8.33z"
      />
      <linearGradient
        id="SVGID_818_"
        gradientUnits="userSpaceOnUse"
        x1={458.211}
        y1={117.652}
        x2={458.211}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_818_)" }}
        d="M452.71 101.66h11v6.19h-11z"
      />
      <linearGradient
        id="SVGID_819_"
        gradientUnits="userSpaceOnUse"
        x1={453.252}
        y1={117.652}
        x2={453.252}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_819_)" }}
        d="M442.51 107.79H464v9.08h-21.49z"
      />
      <linearGradient
        id="SVGID_820_"
        gradientUnits="userSpaceOnUse"
        x1={255.58}
        y1={117.651}
        x2={255.58}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_820_)" }}
        d="M255.41 102.35h.34l-.17-5.92z"
      />
      <linearGradient
        id="SVGID_821_"
        gradientUnits="userSpaceOnUse"
        x1={255.099}
        y1={117.651}
        x2={255.099}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_821_)" }}
        d="M255.01 102.35h.18l-.09-3.15z"
      />
      <linearGradient
        id="SVGID_822_"
        gradientUnits="userSpaceOnUse"
        x1={266.384}
        y1={117.651}
        x2={266.384}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_822_)" }}
        d="M266.22 107.22h.33l-.17-5.92z"
      />
      <linearGradient
        id="SVGID_823_"
        gradientUnits="userSpaceOnUse"
        x1={264.19}
        y1={117.651}
        x2={264.19}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_823_)" }}
        d="M264.1 107.22h.18l-.09-3.16z"
      />
      <linearGradient
        id="SVGID_824_"
        gradientUnits="userSpaceOnUse"
        x1={285.698}
        y1={117.652}
        x2={285.698}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_824_)" }}
        d="M285.61 99.55h.18l-.09-3.16z"
      />
      <linearGradient
        id="SVGID_825_"
        gradientUnits="userSpaceOnUse"
        x1={272.493}
        y1={117.652}
        x2={272.493}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_825_)" }}
        d="M267.21 97.9h10.57v18.41h-10.57z"
      />
      <linearGradient
        id="SVGID_826_"
        gradientUnits="userSpaceOnUse"
        x1={254.122}
        y1={117.652}
        x2={254.122}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_826_)" }}
        d="M246.9 106.16h14.43v10.72H246.9z"
      />
      <linearGradient
        id="SVGID_827_"
        gradientUnits="userSpaceOnUse"
        x1={246.953}
        y1={117.652}
        x2={246.953}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_827_)" }}
        d="M244.52 108.16h4.86v8.71h-4.86z"
      />
      <linearGradient
        id="SVGID_828_"
        gradientUnits="userSpaceOnUse"
        x1={262.554}
        y1={117.652}
        x2={262.554}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_828_)" }}
        d="M261.34 109.73h2.43v7.14h-2.43z"
      />
      <linearGradient
        id="SVGID_829_"
        gradientUnits="userSpaceOnUse"
        x1={268.938}
        y1={117.652}
        x2={268.938}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_829_)" }}
        d="M263.77 106.99h10.34v9.88h-10.34z"
      />
      <linearGradient
        id="SVGID_830_"
        gradientUnits="userSpaceOnUse"
        x1={252.179}
        y1={117.652}
        x2={252.179}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_830_)" }}
        d="M246.95 103.65h10.45v2.51h-10.45z"
      />
      <linearGradient
        id="SVGID_831_"
        gradientUnits="userSpaceOnUse"
        x1={288.746}
        y1={117.651}
        x2={288.746}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_831_)" }}
        d="M284.9 99.55h7.69v4.1h-7.69z"
      />
      <linearGradient
        id="SVGID_832_"
        gradientUnits="userSpaceOnUse"
        x1={253.237}
        y1={117.652}
        x2={253.237}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_832_)" }}
        d="M249.07 102.31h8.33v1.38h-8.33z"
      />
      <linearGradient
        id="SVGID_833_"
        gradientUnits="userSpaceOnUse"
        x1={284.285}
        y1={117.651}
        x2={284.285}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_833_)" }}
        d="M278.78 110.17h11v1.38h-11z"
      />
      <linearGradient
        id="SVGID_834_"
        gradientUnits="userSpaceOnUse"
        x1={278.784}
        y1={117.652}
        x2={278.784}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_834_)" }}
        d="M272.67 111.51h12.24v5.36h-12.24z"
      />
      <linearGradient
        id="SVGID_835_"
        gradientUnits="userSpaceOnUse"
        x1={295.306}
        y1={117.652}
        x2={295.306}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_835_)" }}
        d="M291.24 107.32h8.14v9.56h-8.14z"
      />
      <linearGradient
        id="SVGID_836_"
        gradientUnits="userSpaceOnUse"
        x1={288.325}
        y1={117.652}
        x2={288.325}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_836_)" }}
        d="M282.21 103.65h12.24v13.22h-12.24z"
      />
      <linearGradient
        id="SVGID_837_"
        gradientUnits="userSpaceOnUse"
        x1={355.221}
        y1={117.651}
        x2={355.221}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_837_)" }}
        d="M355.05 102.35h.34l-.17-5.92z"
      />
      <linearGradient
        id="SVGID_838_"
        gradientUnits="userSpaceOnUse"
        x1={354.74}
        y1={117.651}
        x2={354.74}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_838_)" }}
        d="M354.65 102.35h.18l-.09-3.15z"
      />
      <linearGradient
        id="SVGID_839_"
        gradientUnits="userSpaceOnUse"
        x1={366.024}
        y1={117.651}
        x2={366.024}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_839_)" }}
        d="M365.86 107.22h.33l-.17-5.92z"
      />
      <linearGradient
        id="SVGID_840_"
        gradientUnits="userSpaceOnUse"
        x1={363.831}
        y1={117.651}
        x2={363.831}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_840_)" }}
        d="M363.74 107.22h.18l-.09-3.16z"
      />
      <linearGradient
        id="SVGID_841_"
        gradientUnits="userSpaceOnUse"
        x1={385.339}
        y1={117.652}
        x2={385.339}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_841_)" }}
        d="M385.25 99.55h.18l-.09-3.16z"
      />
      <linearGradient
        id="SVGID_842_"
        gradientUnits="userSpaceOnUse"
        x1={372.134}
        y1={117.652}
        x2={372.134}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_842_)" }}
        d="M366.85 97.9h10.57v18.41h-10.57z"
      />
      <linearGradient
        id="SVGID_843_"
        gradientUnits="userSpaceOnUse"
        x1={353.762}
        y1={117.652}
        x2={353.762}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_843_)" }}
        d="M346.55 106.16h14.43v10.72h-14.43z"
      />
      <linearGradient
        id="SVGID_844_"
        gradientUnits="userSpaceOnUse"
        x1={346.594}
        y1={117.652}
        x2={346.594}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_844_)" }}
        d="M344.16 108.16h4.86v8.71h-4.86z"
      />
      <linearGradient
        id="SVGID_845_"
        gradientUnits="userSpaceOnUse"
        x1={362.194}
        y1={117.652}
        x2={362.194}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_845_)" }}
        d="M360.98 109.73h2.43v7.14h-2.43z"
      />
      <linearGradient
        id="SVGID_846_"
        gradientUnits="userSpaceOnUse"
        x1={368.579}
        y1={117.652}
        x2={368.579}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_846_)" }}
        d="M363.41 106.99h10.34v9.88h-10.34z"
      />
      <linearGradient
        id="SVGID_847_"
        gradientUnits="userSpaceOnUse"
        x1={351.819}
        y1={117.652}
        x2={351.819}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_847_)" }}
        d="M346.59 103.65h10.45v2.51h-10.45z"
      />
      <linearGradient
        id="SVGID_848_"
        gradientUnits="userSpaceOnUse"
        x1={388.387}
        y1={117.651}
        x2={388.387}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_848_)" }}
        d="M384.54 99.55h7.69v4.1h-7.69z"
      />
      <linearGradient
        id="SVGID_849_"
        gradientUnits="userSpaceOnUse"
        x1={352.878}
        y1={117.652}
        x2={352.878}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_849_)" }}
        d="M348.71 102.31h8.33v1.38h-8.33z"
      />
      <linearGradient
        id="SVGID_850_"
        gradientUnits="userSpaceOnUse"
        x1={383.925}
        y1={117.651}
        x2={383.925}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_850_)" }}
        d="M378.42 110.17h11v1.38h-11z"
      />
      <linearGradient
        id="SVGID_851_"
        gradientUnits="userSpaceOnUse"
        x1={378.424}
        y1={117.652}
        x2={378.424}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_851_)" }}
        d="M372.31 111.51h12.24v5.36h-12.24z"
      />
      <linearGradient
        id="SVGID_852_"
        gradientUnits="userSpaceOnUse"
        x1={394.947}
        y1={117.652}
        x2={394.947}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_852_)" }}
        d="M390.88 107.32h8.14v9.56h-8.14z"
      />
      <linearGradient
        id="SVGID_853_"
        gradientUnits="userSpaceOnUse"
        x1={387.965}
        y1={117.652}
        x2={387.965}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_853_)" }}
        d="M381.85 103.65h12.24v13.22h-12.24z"
      />
      <linearGradient
        id="SVGID_854_"
        gradientUnits="userSpaceOnUse"
        x1={392.984}
        y1={117.651}
        x2={392.984}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_854_)" }}
        d="M392.82 102.35h.33l-.17-5.92z"
      />
      <linearGradient
        id="SVGID_855_"
        gradientUnits="userSpaceOnUse"
        x1={392.503}
        y1={117.651}
        x2={392.503}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_855_)" }}
        d="M392.41 102.35h.18l-.09-3.15z"
      />
      <linearGradient
        id="SVGID_856_"
        gradientUnits="userSpaceOnUse"
        x1={403.787}
        y1={117.651}
        x2={403.787}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_856_)" }}
        d="M403.62 107.22h.33l-.16-5.92z"
      />
      <linearGradient
        id="SVGID_857_"
        gradientUnits="userSpaceOnUse"
        x1={401.594}
        y1={117.651}
        x2={401.594}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_857_)" }}
        d="M401.51 107.22h.17l-.09-3.16z"
      />
      <linearGradient
        id="SVGID_858_"
        gradientUnits="userSpaceOnUse"
        x1={423.102}
        y1={117.652}
        x2={423.102}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_858_)" }}
        d="M423.01 99.55h.18l-.09-3.16z"
      />
      <linearGradient
        id="SVGID_859_"
        gradientUnits="userSpaceOnUse"
        x1={409.897}
        y1={117.652}
        x2={409.897}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_859_)" }}
        d="M404.61 97.9h10.57v18.41h-10.57z"
      />
      <linearGradient
        id="SVGID_860_"
        gradientUnits="userSpaceOnUse"
        x1={391.526}
        y1={117.652}
        x2={391.526}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_860_)" }}
        d="M384.31 106.16h14.43v10.72h-14.43z"
      />
      <linearGradient
        id="SVGID_861_"
        gradientUnits="userSpaceOnUse"
        x1={384.357}
        y1={117.652}
        x2={384.357}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_861_)" }}
        d="M381.93 108.16h4.86v8.71h-4.86z"
      />
      <linearGradient
        id="SVGID_862_"
        gradientUnits="userSpaceOnUse"
        x1={399.957}
        y1={117.652}
        x2={399.957}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_862_)" }}
        d="M398.74 109.73h2.43v7.14h-2.43z"
      />
      <linearGradient
        id="SVGID_863_"
        gradientUnits="userSpaceOnUse"
        x1={406.342}
        y1={117.652}
        x2={406.342}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_863_)" }}
        d="M401.17 106.99h10.34v9.88h-10.34z"
      />
      <linearGradient
        id="SVGID_864_"
        gradientUnits="userSpaceOnUse"
        x1={389.582}
        y1={117.652}
        x2={389.582}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_864_)" }}
        d="M384.36 103.65h10.45v2.51h-10.45z"
      />
      <linearGradient
        id="SVGID_865_"
        gradientUnits="userSpaceOnUse"
        x1={426.15}
        y1={117.651}
        x2={426.15}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_865_)" }}
        d="M422.31 99.55H430v4.1h-7.69z"
      />
      <linearGradient
        id="SVGID_866_"
        gradientUnits="userSpaceOnUse"
        x1={390.641}
        y1={117.652}
        x2={390.641}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_866_)" }}
        d="M386.47 102.31h8.33v1.38h-8.33z"
      />
      <linearGradient
        id="SVGID_867_"
        gradientUnits="userSpaceOnUse"
        x1={421.688}
        y1={117.651}
        x2={421.688}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_867_)" }}
        d="M416.19 110.17h11v1.38h-11z"
      />
      <linearGradient
        id="SVGID_868_"
        gradientUnits="userSpaceOnUse"
        x1={416.188}
        y1={117.652}
        x2={416.188}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_868_)" }}
        d="M410.07 111.51h12.24v5.36h-12.24z"
      />
      <linearGradient
        id="SVGID_869_"
        gradientUnits="userSpaceOnUse"
        x1={432.71}
        y1={117.652}
        x2={432.71}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_869_)" }}
        d="M428.64 107.32h8.14v9.56h-8.14z"
      />
      <linearGradient
        id="SVGID_870_"
        gradientUnits="userSpaceOnUse"
        x1={425.728}
        y1={117.652}
        x2={425.728}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_870_)" }}
        d="M419.61 103.65h12.24v13.22h-12.24z"
      />
      <linearGradient
        id="SVGID_871_"
        gradientUnits="userSpaceOnUse"
        x1={302.211}
        y1={117.652}
        x2={302.211}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_871_)" }}
        d="M302.04 92.27h.34l-.17-10.03z"
      />
      <linearGradient
        id="SVGID_872_"
        gradientUnits="userSpaceOnUse"
        x1={301.73}
        y1={117.652}
        x2={301.73}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_872_)" }}
        d="M301.64 92.27h.18l-.09-5.35z"
      />
      <linearGradient
        id="SVGID_873_"
        gradientUnits="userSpaceOnUse"
        x1={313.015}
        y1={117.651}
        x2={313.015}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_873_)" }}
        d="M312.85 100.51h.33l-.17-10.03z"
      />
      <linearGradient
        id="SVGID_874_"
        gradientUnits="userSpaceOnUse"
        x1={310.822}
        y1={117.651}
        x2={310.822}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_874_)" }}
        d="M310.73 100.51h.18l-.09-5.35z"
      />
      <linearGradient
        id="SVGID_875_"
        gradientUnits="userSpaceOnUse"
        x1={332.329}
        y1={117.652}
        x2={332.329}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_875_)" }}
        d="M332.24 87.52h.18l-.09-5.35z"
      />
      <linearGradient
        id="SVGID_876_"
        gradientUnits="userSpaceOnUse"
        x1={319.125}
        y1={117.652}
        x2={319.125}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_876_)" }}
        d="M313.84 84.74h10.57v31.18h-10.57z"
      />
      <linearGradient
        id="SVGID_877_"
        gradientUnits="userSpaceOnUse"
        x1={300.753}
        y1={117.652}
        x2={300.753}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_877_)" }}
        d="M293.54 98.72h14.43v18.15h-14.43z"
      />
      <linearGradient
        id="SVGID_878_"
        gradientUnits="userSpaceOnUse"
        x1={293.585}
        y1={117.652}
        x2={293.585}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_878_)" }}
        d="M291.15 102.11h4.86v14.76h-4.86z"
      />
      <linearGradient
        id="SVGID_879_"
        gradientUnits="userSpaceOnUse"
        x1={309.185}
        y1={117.652}
        x2={309.185}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_879_)" }}
        d="M307.97 104.77h2.43v12.1h-2.43z"
      />
      <linearGradient
        id="SVGID_880_"
        gradientUnits="userSpaceOnUse"
        x1={315.57}
        y1={117.652}
        x2={315.57}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_880_)" }}
        d="M310.4 100.13h10.34v16.75H310.4z"
      />
      <linearGradient
        id="SVGID_881_"
        gradientUnits="userSpaceOnUse"
        x1={298.81}
        y1={117.652}
        x2={298.81}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_881_)" }}
        d="M293.58 94.47h10.45v4.25h-10.45z"
      />
      <linearGradient
        id="SVGID_882_"
        gradientUnits="userSpaceOnUse"
        x1={335.378}
        y1={117.652}
        x2={335.378}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_882_)" }}
        d="M331.53 87.52h7.69v6.95h-7.69z"
      />
      <linearGradient
        id="SVGID_883_"
        gradientUnits="userSpaceOnUse"
        x1={299.869}
        y1={117.652}
        x2={299.869}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_883_)" }}
        d="M295.7 92.2h8.33v2.34h-8.33z"
      />
      <linearGradient
        id="SVGID_884_"
        gradientUnits="userSpaceOnUse"
        x1={330.916}
        y1={117.652}
        x2={330.916}
        y2={74.383}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_884_)" }}
        d="M325.42 105.51h11v2.34h-11z"
      />
      <linearGradient
        id="SVGID_885_"
        gradientUnits="userSpaceOnUse"
        x1={325.415}
        y1={117.652}
        x2={325.415}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_885_)" }}
        d="M319.3 107.79h12.24v9.08H319.3z"
      />
      <linearGradient
        id="SVGID_886_"
        gradientUnits="userSpaceOnUse"
        x1={341.937}
        y1={117.652}
        x2={341.937}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_886_)" }}
        d="M337.87 100.68h8.14v16.19h-8.14z"
      />
      <linearGradient
        id="SVGID_887_"
        gradientUnits="userSpaceOnUse"
        x1={334.956}
        y1={117.652}
        x2={334.956}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_887_)" }}
        d="M328.84 94.47h12.24v22.4h-12.24z"
      />
      <linearGradient
        id="SVGID_888_"
        gradientUnits="userSpaceOnUse"
        x1={425.423}
        y1={117.652}
        x2={425.423}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_888_)" }}
        d="M425.26 92.27h.33l-.17-10.03z"
      />
      <linearGradient
        id="SVGID_889_"
        gradientUnits="userSpaceOnUse"
        x1={424.942}
        y1={117.652}
        x2={424.942}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_889_)" }}
        d="M424.85 92.27h.18l-.09-5.35z"
      />
      <linearGradient
        id="SVGID_890_"
        gradientUnits="userSpaceOnUse"
        x1={436.227}
        y1={117.651}
        x2={436.227}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_890_)" }}
        d="M436.06 100.51h.33l-.16-10.03z"
      />
      <linearGradient
        id="SVGID_891_"
        gradientUnits="userSpaceOnUse"
        x1={434.034}
        y1={117.651}
        x2={434.034}
        y2={74.385}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_891_)" }}
        d="M433.94 100.51h.18l-.09-5.35z"
      />
      <linearGradient
        id="SVGID_892_"
        gradientUnits="userSpaceOnUse"
        x1={442.337}
        y1={117.652}
        x2={442.337}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_892_)" }}
        d="M437.05 84.74h10.57v31.18h-10.57z"
      />
      <linearGradient
        id="SVGID_893_"
        gradientUnits="userSpaceOnUse"
        x1={423.965}
        y1={117.652}
        x2={423.965}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_893_)" }}
        d="M416.75 98.72h14.43v18.15h-14.43z"
      />
      <linearGradient
        id="SVGID_894_"
        gradientUnits="userSpaceOnUse"
        x1={416.796}
        y1={117.652}
        x2={416.796}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_894_)" }}
        d="M414.37 102.11h4.86v14.76h-4.86z"
      />
      <linearGradient
        id="SVGID_895_"
        gradientUnits="userSpaceOnUse"
        x1={432.397}
        y1={117.652}
        x2={432.397}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_895_)" }}
        d="M431.18 104.77h2.43v12.1h-2.43z"
      />
      <linearGradient
        id="SVGID_896_"
        gradientUnits="userSpaceOnUse"
        x1={438.781}
        y1={117.652}
        x2={438.781}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_896_)" }}
        d="M433.61 100.13h10.34v16.75h-10.34z"
      />
      <linearGradient
        id="SVGID_897_"
        gradientUnits="userSpaceOnUse"
        x1={422.022}
        y1={117.652}
        x2={422.022}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_897_)" }}
        d="M416.8 94.47h10.45v4.25H416.8z"
      />
      <linearGradient
        id="SVGID_898_"
        gradientUnits="userSpaceOnUse"
        x1={423.08}
        y1={117.652}
        x2={423.08}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_898_)" }}
        d="M418.91 92.2h8.33v2.34h-8.33z"
      />
      <linearGradient
        id="SVGID_899_"
        gradientUnits="userSpaceOnUse"
        x1={458.211}
        y1={117.652}
        x2={458.211}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_899_)" }}
        d="M452.71 101.66h11v6.19h-11z"
      />
      <linearGradient
        id="SVGID_900_"
        gradientUnits="userSpaceOnUse"
        x1={453.252}
        y1={117.652}
        x2={453.252}
        y2={74.384}
      >
        <stop offset={0} style={{ stopColor: "#00adaa" }} />
        <stop offset={0.059} style={{ stopColor: "#03999e" }} />
        <stop offset={0.179} style={{ stopColor: "#077689" }} />
        <stop offset={0.306} style={{ stopColor: "#0b5a77" }} />
        <stop offset={0.442} style={{ stopColor: "#0e446a" }} />
        <stop offset={0.591} style={{ stopColor: "#103460" }} />
        <stop offset={0.76} style={{ stopColor: "#122b5b" }} />
        <stop offset={1} style={{ stopColor: "#122859" }} />
      </linearGradient>
      <path
        style={{ fill: "url(#SVGID_900_)" }}
        d="M442.51 107.79H464v9.08h-21.49z"
      />
    </g>
  </svg>
)
export default SvgComponent
