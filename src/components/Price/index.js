import React from 'react';
import { connect } from "react-redux";
import numeral from 'numeral';

function Price( { user, item, fixed } ) {
  const value = item.status === 'onauction' ? (fixed || item.bid || item.start || 0) : (fixed || item.price || item.bid || item.start || 0);
  const price = item.currency === 'mxn' ? value : (value * user.eth * user.dollar);
  return <>${numeral(price).format('##,###.00')}</>
}

export default connect(({ user }) => ({ user }))( Price );