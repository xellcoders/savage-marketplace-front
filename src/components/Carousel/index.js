import React from 'react';
import { Link } from "react-router-dom";
import styles from './Carousel.module.sass'
import { history } from '../../index'

export default function({title, link, items, path="/", showNumbers=false}){
  return (
    <div className={styles.wrapper}>
      <div className={styles.backward}><img src="/images/icons/arc_backward.svg" alt="Atras" /></div>
      <div className={styles.content}>
        <div className={styles.title}>
          <span>{title}</span>
          <Link to={link}>Ver todo <small>&rsaquo;</small></Link>
        </div>
        <div className={styles.gallery}>
          {items.map( (item, index) => (
            <div
              key={item.id}
              className={styles.card}
              role="button"
              tabIndex={0}
              onKeyDown={()=>{}}
              onClick={() => history.push( `${path}${item.id}` )}
            >
              <figure className={styles.image}>
                <img src={item.image} alt={item.name} />
              </figure>
              {showNumbers && <div className={styles.number}>{index+1}</div>}
              <div>
                {item.name}
                <svg width="7" height="10" viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path fillRule="evenodd" clipRule="evenodd" d="M4.54667 4.79062L0.950279 1.19423L1.79881 0.345703L6.24372 4.79062L1.79506 9.23928L0.946533 8.39075L4.54667 4.79062Z" fill="#01B4C3" />
                  <path fillRule="evenodd" clipRule="evenodd" d="M4.54667 4.79062L0.950279 1.19423L1.79881 0.345703L6.24372 4.79062L1.79506 9.23928L0.946533 8.39075L4.54667 4.79062Z" fill="#01B4C3" />
                </svg>
              </div>
            </div>
          ) )}
        </div>
      </div>
      <div className={styles.forward}><img src="/images/icons/arc_forward.svg" alt="Atras" /></div>
    </div>
  )
}