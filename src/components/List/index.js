import React from 'react';
import moment from "moment";
import { getItemImage } from "util/files";
import styles from './List.module.sass'
import { history } from "../../index";
import Price from "../Price";

export default function({prefixDate, suffixTitle, items=[]}) {
  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
        <span>{items.length} NFTs </span>{suffixTitle}
      </div>
      {items.map( item => (
        <div
          className={styles.item}
          role="button"
          tabIndex={0}
          onKeyDown={()=>{}}
          /* eslint-disable-next-line no-underscore-dangle */
          onClick={() => history.push( `/nft/${item._id}` )}
        >
          <div className={styles.icon}>
            <figure>
              {item.nft.image && <img src={getItemImage(item.nft)} alt={item.nft.name} />}
            </figure>
          </div>
          <div className={styles.title}>
            {item.nft.name}
            <div className={styles.subtitle}>
              {prefixDate} <span className={styles.date}>{moment(item.created_at).format('DD/MM/YYYY')}</span>
            </div>
          </div>
          <div className={styles.price}>
            <Price item={item} />
          </div>
        </div>
      ) )}

    </div>
  )
}