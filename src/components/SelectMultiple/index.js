import React from "react";
import Select from "react-select";
import cn from "classnames";
import styles from "./SelectMultiple.module.sass";

const SelectMultiple = ({
  options,
  value,
  onChange,
  placeholder,
  stylesError
}) => {
  const customStyles = {
    dropdownIndicator: () => ({
      color: "#00B7C4",
      marginRight: "8px"
    }),
    placeholder: base => ({
      ...base,
      fontSize: "1em",
      color: "#fff",
      fontWeight: 400
    })
  };

  return (
    <Select
      theme={theme => ({
        ...theme,
        borderRadius: 0,
        colors: {
          ...theme.colors,
          primary25: "#3AB7C4",
          neutral5: "#3AB7C4",
          primary: "#3AB7C4",
          neutral20: "#28293D",
          neutral30: "#3AB7C4",
          neutral40: "#3AB7C4",
          neutral50: "white",
          neutral60: "#3AB7C4",
          neutral80: "#3AB7C4"
        }
      })}
      value={value}
      options={Array.isArray(options) ? options : []}
      className={cn(styles.select, stylesError)}
      classNamePrefix="custom"
      onChange={onChange}
      getOptionLabel={option => option.name}
      getOptionValue={option => option.id}
      styles={customStyles}
      placeholder={placeholder}
    />
  );
};

export default SelectMultiple;
