import React, { createContext, useContext, useState } from 'react'
import { useWeb3React } from "@web3-react/core";

export const walletContext = createContext({});

export const useWalletProvider = () => useContext( walletContext );

function WalletProvider( { children } ) {
  const [error, setError] = useState();
  const { connector, isActivating, isActive, account, chainId, provider } = useWeb3React();

  const connect = () => {
    setError( null );
    connector.activate().catch( setError );
  }

  return (
    <walletContext.Provider value={{
      connecting: isActivating,
      connected: isActive,
      network: chainId,
      account,
      provider,
      error,
      connect
    }}
    >
      {children}
    </walletContext.Provider>
  )
}

export default WalletProvider;