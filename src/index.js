import React from "react";
import ReactDOM from "react-dom";
import "antd/lib/style/index.less"; // antd core styles
import "antd/lib/style/components.less"; // default theme antd components
import "./styles/app.sass";
import { createHashHistory } from "history";
import { createStore, applyMiddleware, compose } from "redux";
import { Provider } from "react-redux";
import * as ethers from "ethers";
import { Web3ReactProvider } from '@web3-react/core'
import { hooks as metaMaskHooks, metaMask } from 'connectors/metamask';
import createSagaMiddleware from "redux-saga";
import { routerMiddleware } from "connected-react-router";
import WalletProvider from "providers/wallet";
import { GoogleOAuthProvider } from '@react-oauth/google';
import reducers from "./redux/reducers";
import sagas from "./redux/sagas";
import Localization from "./localization";
import Router from "./router";
import * as serviceWorker from "./serviceWorker";

// middlewared
const history = createHashHistory();
const sagaMiddleware = createSagaMiddleware();
const routeMiddleware = routerMiddleware(history);
const middlewares = [sagaMiddleware, routeMiddleware];
// if (process.env.NODE_ENV === 'development') {
//   middlewares.push(logger)
// }
const store = createStore(
  reducers(history),
  compose(applyMiddleware(...middlewares))
);
sagaMiddleware.run(sagas);

const connectors = [[metaMask, metaMaskHooks]];
const getLibrary = provider => new ethers.providers.Web3Provider(provider);

ReactDOM.render(
  <Provider store={store}>
    <GoogleOAuthProvider clientId="460585283800-qpfpejgpik4eop5dfsdgc0g746fjr38t.apps.googleusercontent.com">
      <Web3ReactProvider connectors={connectors} getLibrary={getLibrary}>
        <WalletProvider connectors={connectors} getLibrary={getLibrary}>
          <Localization>
            <Router history={history} />
          </Localization>
        </WalletProvider>
      </Web3ReactProvider>
    </GoogleOAuthProvider>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
export { store, history };
