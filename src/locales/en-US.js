import localeAntd from "antd/es/locale/en_US";

const messages = {
  "topBar.title": "Title"
};

export default {
  locale: "en-US",
  localeAntd,
  messages
};
