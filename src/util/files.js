export const getBase64 = (img, callback) => {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(img);
};

export const getItemImage = item => {
  switch (item?.image) {
    case "uri:audio:default":
      return "/images/content/sound.png";
    case "uri:video:default":
      return "/images/content/video.png";
    case "uri:model:default":
      return "/images/content/3d-model.png";
    default:
  }

  if (item?.image?.startsWith("uri:image:")) {
    const base = item?.image.replace("uri:image:", "");
    // eslint-disable-next-line no-underscore-dangle
    return `${base + item._id}/resource`;
  }
  return item?.image || '/images/content/image.png';
};
