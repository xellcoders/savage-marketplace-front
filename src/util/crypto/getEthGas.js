import Units from "ethereumjs-units";

const getEthGas = async () => {
  const url =
    "https://data-api.defipulse.com/api/v1/egs/api/ethgasAPI.json?api-key=98d9f52166be2b0ec537e1703e8bc6e0a2b923e0ea75d366393950b8d290";
  const obj = await (await fetch(url)).json();
  const gwei = (obj.fastest / 10).toString();

  return Units.convert(gwei, "gwei", "wei");
};

export default getEthGas;
