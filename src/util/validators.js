import numeral from "numeral";

export default {
  minimum: (min, suffix = '') => ({
    validator: async (_, value) => {
      console.log(value, min, value < min);
      // eslint-disable-next-line no-restricted-globals
      if (isNaN(value)) throw new Error('Debe ser un número')
      if (value < min) throw new Error(`Debe ser un número mayor a ${numeral(min).format('##,###.00')} ${suffix}`)
      return value
    }
  }),
  number: {
    validator: async (_, value) => {
      // eslint-disable-next-line no-restricted-globals
      if (isNaN(value)) throw new Error('Debe ser un número')
      if (value <= 0) throw new Error('Debe ser un número mayor a 0')
      return value
    }
  },
  password: ({ getFieldValue }) => ({
    validator(_, value) {
      if (!value || getFieldValue('password') === value) {
        return Promise.resolve();
      }
      return Promise.reject(new Error('Las dos contraseñas deben ser iguales'));
    },
  })
}