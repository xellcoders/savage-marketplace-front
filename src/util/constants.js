export const TokenType = {
  normal: "Normal",
  rare: "Raro",
  very: "Muy raro",
  ultra: "Ultra raro",
  common: "Común",
  "very-common": "Poco común",
  epic: "Épico",
  legendary: "Legendario",
  mythical: "Mítico"
}

export const ItemModes = {
  onsale: 'Venta',
  onauction: 'Subasta',
  ongame: 'Juega'
};